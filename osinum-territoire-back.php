<?php

/*
 * Plugin Name: Osinum Territoire
 * Description: Logique back-end du site Osinum Territoire.
 * Author: BsaWeb
 * Author URI: https://bsa-web.fr
 * Version: 1.0.0
 * Text Domain: ositer
 * Domain Path: /languages
*/

use ComposePress\Dice\Dice;

/**
 * Singleton instance function. We will not use a global at all as that defeats the purpose of a singleton and is a bad design overall
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @return OsinumTerritoire\Plugin
 */
function _ositer() {
	return ositer_container()->create( '\OsinumTerritoire\Plugin' );
}

/**
* This container singleton enables you to setup unit testing by passing an environment file to map classes in Dice
 *
 * @param string $env
 *
* @return \ComposePress\Dice\Dice
 */
function ositer_container( $env = 'prod' ) {
	static $container;
	if ( empty( $container ) ) {
		include_once __DIR__ . '/vendor/autoload.php';

		$container = new Dice();

		include __DIR__ . "/config_{$env}.php";
	}

	return $container;
}

/**
 * Init function shortcut
 */
function ositer_init() {
	_ositer()->init();
}

/**
 * Activate function shortcut
 */
function ositer_activate( $network_wide ) {
	register_uninstall_hook( __FILE__, 'ositer_uninstall' );
	_ositer()->init();
	_ositer()->activate( $network_wide );
}

/**
 * Deactivate function shortcut
 */
function ositer_deactivate( $network_wide ) {
	_ositer()->deactivate( $network_wide );
}

/**
* Uninstall function shortcut
*/
function ositer_uninstall() {
	_ositer()->uninstall();
}

/**
 * Error for older php
 */
function ositer_php_upgrade_notice() {
	$info = get_plugin_data( __FILE__ );
	_e(
		sprintf(
			'
	<div class="error notice">
		<p>Opps! %s requiert PHP 8.0.0 au minimum. Votre version actuelle est %s. Merci de mettre à jour votre version de PHP.</p>
	</div>', $info['Name'], PHP_VERSION
		)
	);
}

/**
 * Error if vendors autoload is missing
 */
function ositer_php_vendor_missing() {
	$info = get_plugin_data( __FILE__ );
	_e(
		sprintf(
			'
	<div class="error notice">
		<p>Opps! %s is corrupted it seems, please re-install the plugin.</p>
	</div>', $info['Name']
		)
	);
}

/*
 * We want to use a fairly modern php version, feel free to increase the minimum requirement
 */
if ( version_compare( PHP_VERSION, '8.0.0' ) < 0 ) {
	add_action( 'admin_notices', 'ositer_php_upgrade_notice' );
} else {
	if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
		// Composer autoloader.
		include_once __DIR__ . '/vendor/autoload.php';

		// External libraries not handled by Composer.
		require_once plugin_dir_path( __FILE__ ) . '/libraries/action-scheduler/action-scheduler.php';

		add_action( 'plugins_loaded', 'ositer_init', 11 );

		register_activation_hook( __FILE__, 'ositer_activate' );
		register_deactivation_hook( __FILE__, 'ositer_deactivate' );
	} else {
		add_action( 'admin_notices', 'ositer_php_vendor_missing' );
	}
}

/**
 * Return main OsinumTerritoire functions utility class, dispatching/proxying functions to correct classes.
 *
 * @return \OsinumTerritoire\Functions
 */
function ositer() {
	return _ositer()->functions;
}

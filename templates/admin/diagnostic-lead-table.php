<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $values */

?>

<ul>
	<?php foreach ( $values as $label => $value ) {
		printf(
			'<li><strong>%1$s</strong> : %2$s</li>',
			Helpers::readify( $label ),
			Helpers::readify( $value, $label ),
		);
	} ?>
</ul>

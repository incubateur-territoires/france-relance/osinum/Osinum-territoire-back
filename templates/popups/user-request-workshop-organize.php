<?php

defined( 'ABSPATH' ) || die();

?>

<h2 class="modal-title"><?php _e( 'Organiser un atelier', 'ositer' ); ?></h2>

<p><?php _e( 'Les ateliers n\'ont pas encore démarré. Vous êtes intéressé(e) ? Laissez-nous votre mail pour vous tenir informé(e).', 'ositer' ); ?></p>

<form action="#" class="user-request-container mt150">
	<input type="hidden" class="type-field" name="type" value="workshop-organize" />

	<div class="flex-row flex-valign-bottom flex-row-mobile">
		<div class="input-container flex">
			<div class="input">
				<label class="tiny-label" for="email-workshop-organize"><span><?php _e( 'Votre adresse mail', 'ositer' ); ?></span></label>
				<input type="email" class="email-field" id="email-workshop-organize" name="email" required placeholder="<?php esc_attr_e( 'Adresse mail', 'ositer' ); ?>" />
			</div>
		</div>

		<div class="button-container">
			<button class="fr" type="submit">
				<?php _e( 'Envoyer', 'ositer' ); ?>
				<?php ositer()->icon( 'email' ); ?>
			</button>
		</div>
	</div>
</form>

<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/**
 * Render a <th> header cell.
 *
 * @param object $criteria
 * @return void
 */
function render_header_cell( $criteria ) {
	$is_term_criteria = is_numeric( $criteria->id );

	// If we're rendering the PDF, use the ACF header image to display the vertical text.
	if ( _ositer()->get_urls()->is_rendering_pdf() ) {
		if ( ! $is_term_criteria ) {
			$header_url = _ositer()->get_plugin_url() . "/dist/images/pdf-outils-entete-{$criteria->id}.png";
		} else {
			$header_url = get_field( 'resultats_tableau_entete', "term_{$criteria->id}" );
		}	
		
		var_dump($header_url);

		if ( ! empty( $header_url ) ) {
			printf( '<th><img src="%1$s" alt="%2$s"></th>', esc_url( $header_url ), esc_attr( $criteria->title ) );
		} else {
			printf( '<th>%1$s</th>', $criteria->title );
		}
	} else {
	
		printf(
			'<th class="%2$s"><span>%1$s</span></th>',
			$criteria->title,
			$is_term_criteria ? 'type-term' : 'type-usage'
		);
	}
}

/**
 * Render a <td> body cell.
 *
 * @param object $tool
 * @param object $criteria
 * @return void
 */
function render_body_cell( $tool, $criteria ) {
	$is_term_criteria = is_numeric( $criteria->id );
	$main_class       = sprintf( '%1$s', $is_term_criteria ? 'type-term ' : 'type-usage ' );
	$cell_data        = '';
	$cell_class       = $main_class;

	// Satisfaction and Frequency static criterias.
	if ( ! $is_term_criteria ) {
		$value = $tool->configuration->usages->{$criteria->id};
		$data  = Helpers::get_criteria_data( sprintf( '%1$s:%2$s', $criteria->id, $value ) );

		if ( $data ) {
			$cell_class .= sprintf( '%1$s ', str_replace( ':', '__', $data->slug ) );
			$cell_data   = sprintf(
				'%1$s <span>%2$s</span>',
				ositer()->icon( $data->icon, false ),
				$data->label
			);
		}
	} else {
		$has_criteria = in_array( $criteria->id, $tool->criterias );

		if ( $has_criteria ) {
			$cell_class .= 'yes ';
			$cell_data   = sprintf( '<span>%1$s</span>', __( 'Validé', 'ositer' ) );
		} else {
			$cell_class .= 'no ';
			$cell_data   = sprintf( '<span>%1$s</span>', __( 'Invalidé', 'ositer' ) );
		}
	}

	printf( '<td class="%3$s"><div class="cell %2$s">%1$s</div></td>', $cell_data, trim( $cell_class ), $main_class );
}

?>

<style>body { --criterias-count: <?php echo count( $criterias ); ?>; }</style>

<div class="table-container">
	<table id="outils-tableau">
		<thead>
			<tr class="headers-legend">
				<th class="tool-name"><?php _e( 'Outils utilisés :', 'ositer' ); ?></th>
				<th colspan="2"><?php _e( 'Critères d\'utilisation :', 'ositer' ); ?></th>
				<th colspan="<?php echo count( $criterias ) - 2; ?>"><?php _e( 'Critères d\'outils :', 'ositer' ); ?></th>
			</tr>
			<tr class="headers">
				<th class="tool-name">&nbsp;</th>
				<?php array_map( 'render_header_cell', $criterias ); ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $selected_tools as $t => $tool ) {
				$title = $tool->title;

				if ( _ositer()->get_urls()->is_rendering_pdf() ) {
					$title = mb_strimwidth( $title, 0, 20, '...' );
				}
				?>
			<tr>
				<td class="tool-name">
					<?php printf(
						'%2$s<span class="text">%1$s</span>',
						$title . ( $tool->type === 'custom' ? '*' : '' ),
						sprintf( '<span class="index">%1$d</span>', $t + 1 )
					); ?>
				</td>
				<?php array_map( function ( $criteria ) use ( $tool ) {
					return render_body_cell( $tool, $criteria );
				}, $criterias ); ?>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<footer class="table-footer mt150">
	<?php if ( ! empty( array_filter( $selected_tools, fn ( $tool ) => $tool->type === 'custom' ) ) ) { ?>
		<p class="legend fl"><?php _e( '* Outils non-répertoriés par OSINUM, leurs informations dépendent de celles que vous avez renseignées.', 'ositer' ); ?></p>
	<?php } ?>

	<button class="button fr" data-print="outils-tableau">
		<?php ositer()->icon( 'download' ); ?>
		<?php _e( 'Télécharger le tableau', 'ositer' ); ?>
	</button>
</footer>

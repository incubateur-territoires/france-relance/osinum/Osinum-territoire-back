<?php

defined('ABSPATH') || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

?>

<p class="mt200 mb175"><?php printf(__('Cliquez ci-dessous sur le réseau via lequel vous souhaitez partager votre diagnostic <em>%1$s</em>&nbsp;:', 'ositer'), $diagnostic->get_title()); ?></p>

<ul class="socials">
	<li>
		<a class="button" href="<?php echo esc_attr( sprintf( 'https://www.linkedin.com/shareArticle?url=%1$s&title=%2$s', $diagnostic->get_private_permalink( 'resultats' ), $diagnostic->get_title() ) ); ?>" rel="noopener nofollow" target="_blank">
			<?php _e( 'Sur LinkedIn', 'ositer' ); ?>
			<?php ositer()->icon( 'linkedin' ); ?>
		</a>
	</li>
	<li>
		<a class="button" href="<?php echo esc_attr( sprintf( 'https://www.facebook.com/sharer/sharer.php?u=%1$s', $diagnostic->get_private_permalink( 'resultats' ), $diagnostic->get_title() ) ); ?>" rel="noopener nofollow" target="_blank">
			<?php _e( 'Sur Facebook', 'ositer' ); ?>
			<?php ositer()->icon( 'facebook' ); ?>
		</a>
	</li>
	<li>
		<a class="button" href="<?php echo esc_attr( sprintf( 'https://twitter.com/share?url=%1$s&amp;text=%2$s', $diagnostic->get_private_permalink( 'resultats' ), $diagnostic->get_title() ) ); ?>" rel="noopener nofollow" target="_blank">
			<?php _e( 'Sur Twitter', 'ositer' ); ?>
			<?php ositer()->icon( 'twitter' ); ?>
		</a>
	</li>
	<li>
		<a class="button" href="<?php echo esc_attr( sprintf( 'mailto:?subject=Cet diagnostic OSINUM Territoires pourrait vous intéresser&body=%2$s - %1$s', $diagnostic->get_private_permalink( 'resultats' ), $diagnostic->get_title() ) ); ?>" rel="noopener nofollow" target="_blank">
			<?php _e( 'Par mail', 'ositer' ); ?>
			<?php ositer()->icon( 'email' ); ?>
		</a>
	</li>
</ul>
<?php

defined( 'ABSPATH' ) || die();

?>

<h2 class="modal-title"><?php _e( 'Contacter Benoit de OSINUM', 'ositer' ); ?></h2>

<p><?php _e( 'Laissez-nous votre mail afin que Benoit vous contacte.', 'ositer' ); ?></p>

<form action="#" class="user-request-container mt150">
	<input type="hidden" class="type-field" name="type" value="osinum-contact" />

	<div class="flex-row flex-valign-bottom flex-row-mobile">
		<div class="input-container flex">
			<div class="input">
				<label for="email-osinum-contact"><span><?php _e( 'Votre adresse mail', 'ositer' ); ?></span></label>
				<input type="email" class="email-field" id="email-osinum-contact" name="email" required placeholder="<?php esc_attr_e( 'Adresse mail', 'ositer' ); ?>" />
			</div>
		</div>

		<div class="button-container">
			<button class="fr" type="submit">
				<?php _e( 'Envoyer', 'ositer' ); ?>
				<?php ositer()->icon( 'email' ); ?>
			</button>
		</div>
	</div>
</form>

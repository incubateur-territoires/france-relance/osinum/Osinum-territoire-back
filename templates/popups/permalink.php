<?php

defined( 'ABSPATH' ) || die();

?>

<h3><?php _e( 'Qu\'est-ce qu\'un permalien ?', 'ositer' ); ?></h3>
<p><?php _e( 'C’est un lien, qu’il vous suffira de copier et coller dans votre barre url de navigateur, ou bien dans le champ ci-dessus, pour accéder à votre diagnostic. Ainsi, vous pouvez le partager à d’autres facilement, et il n’est pas nécessaire de créer de compte ou chercher un mot de passe.', 'ositer' ); ?></p>

<div class="info-icon"><?php ositer()->icon( 'info-plein' ); ?></div>
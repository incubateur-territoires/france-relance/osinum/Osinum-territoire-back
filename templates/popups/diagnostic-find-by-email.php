<?php

defined( 'ABSPATH' ) || die();

?>

<h2 class="modal-title"><?php _e( 'Diagnostic perdu ?', 'ositer' ); ?></h2>

<p><?php _e( 'Retrouvez votre diagnostic en renseignant l\'adresse renseignée à l\'inscription, nous vous renverrons le lien.', 'ositer' ); ?></p>

<form action="#" id="diagnostic-find-by-email-container">
	<h3><?php _e( 'Renseignez l\'adresse mail liée au diagnostic', 'ositer' ); ?></h3>

	<div class="input">
		<label for="author-email"><span><?php _e( 'Votre adresse mail', 'ositer' ); ?></span></label>
		<input type="email" id="author-email" name="author-email" required placeholder="<?php esc_attr_e( 'Adresse mail', 'ositer' ); ?>" />
	</div>

	<footer class="mt250 clearfix">
		<button class="fr" type="submit">
			<?php _e( 'Renvoyer', 'ositer' ); ?>
			<?php ositer()->icon( 'fleche-droite' ); ?>
		</button>
	</footer>
</form>

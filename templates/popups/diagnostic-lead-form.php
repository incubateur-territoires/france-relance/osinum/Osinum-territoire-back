<?php

defined( 'ABSPATH' ) || die();

?>

<h2 class="modal-title"><?php _e( 'Diagnostic terminé ?', 'ositer' ); ?></h2>

<form action="#" id="diagnostic-lead-form-container">
	<div class="grid style-2-1">
		<div>
			<h4 class="mb150"><?php _e( 'Vos informations', 'ositer' ); ?></h4>
			<div class="input">
				<label for="infos-region"><span><?php _e( 'Votre région', 'ositer' ); ?></span></label>
				<select class="fake-select" name="infos-region" id="infos-region">
					<option value="" disabled hidden <?php selected( '', $lead_form_values['infos-region'] ?? '' ); ?>><?php _e( 'Votre région', 'ositer' ); ?></option>
					<option value="auvergne-rhone-alpes" <?php selected( 'auvergne-rhone-alpes', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Auvergne-Rhône-Alpes', 'ositer' ); ?>
					</option>
					<option value="bourgogne-franche-comte" <?php selected( 'bourgogne-franche-comte', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Bourgogne-Franche-Comté', 'ositer' ); ?>
					</option>
					<option value="bretagne" <?php selected( 'bretagne', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Bretagne', 'ositer' ); ?>
					</option>
					<option value="centre-val-loire" <?php selected( 'centre-val-loire', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Centre-Val de Loire', 'ositer' ); ?>
					</option>
					<option value="corse" <?php selected( 'corse', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Corse', 'ositer' ); ?>
					</option>
					<option value="grand-est" <?php selected( 'grand-est', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Grand Est', 'ositer' ); ?>
					</option>
					<option value="guadeloupe" <?php selected( 'guadeloupe', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Guadeloupe', 'ositer' ); ?>
					</option>
					<option value="guyane" <?php selected( 'guyane', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Guyane', 'ositer' ); ?>
					</option>
					<option value="hauts-france" <?php selected( 'hauts-france', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Hauts-de-France', 'ositer' ); ?>
					</option>
					<option value="ile-france" <?php selected( 'ile-france', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Ile-de-France', 'ositer' ); ?>
					</option>
					<option value="reunion" <?php selected( 'reunion', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'La Réunion', 'ositer' ); ?>
					</option>
					<option value="martinique" <?php selected( 'martinique', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Martinique', 'ositer' ); ?>
					</option>
					<option value="mayotte" <?php selected( 'mayotte', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Mayotte', 'ositer' ); ?>
					</option>
					<option value="normandie" <?php selected( 'normandie', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Normandie', 'ositer' ); ?>
					</option>
					<option value="nouvelle-aquitaine" <?php selected( 'nouvelle-aquitaine', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Nouvelle-Aquitaine', 'ositer' ); ?>
					</option>
					<option value="occitanie" <?php selected( 'occitanie', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Occitanie', 'ositer' ); ?>
					</option>
					<option value="pays-loire" <?php selected( 'pays-loire', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Pays de la Loire', 'ositer' ); ?>
					</option>
					<option value="paca" <?php selected( 'paca', $lead_form_values['infos-region'] ?? '' ); ?>>
						<?php _e( 'Provence-Alpes-Côte d’Azur', 'ositer' ); ?>
					</option>
				</select>
			</div>
			<div class="input">
				<label for="infos-type-collectivite"><span><?php _e( 'Votre type de collectivité', 'ositer' ); ?></span></label>
				<select class="fake-select" name="infos-type-collectivite" id="infos-type-collectivite">
					<option value="" disabled hidden <?php selected( '', $lead_form_values['infos-type-collectivite'] ?? '' ); ?>><?php _e( 'Votre type de collectivité', 'ositer' ); ?></option>
					<option value="commune" <?php selected( 'commune', $lead_form_values['infos-type-collectivite'] ?? '' ); ?>>
						<?php _e( 'Commune', 'ositer' ); ?>
					</option>
					<option value="commu-communes" <?php selected( 'commu-communes', $lead_form_values['infos-type-collectivite'] ?? '' ); ?>>
						<?php _e( 'Communauté de communes', 'ositer' ); ?>
					</option>
					<option value="commu-agglo" <?php selected( 'commu-agglo', $lead_form_values['infos-type-collectivite'] ?? '' ); ?>>
						<?php _e( 'Communauté d\'agglomération', 'ositer' ); ?>
					</option>
					<option value="departement" <?php selected( 'departement', $lead_form_values['infos-type-collectivite'] ?? '' ); ?>>
						<?php _e( 'Département', 'ositer' ); ?>
					</option>
				</select>
			</div>
			<div class="input">
				<label for="infos-taille-collectivite"><span><?php _e( 'Votre taille de collectivité', 'ositer' ); ?></span></label>
				<select class="fake-select" name="infos-taille-collectivite" id="infos-taille-collectivite">
					<option value="" disabled hidden <?php selected( '', $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>><?php _e( 'Votre taille de collectivité', 'ositer' ); ?></option>
					<option value="-500" <?php selected( "-500", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '- de 500', 'ositer' ); ?>
					</option>
					<option value="500-999" <?php selected( "500-999", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '500 à 999', 'ositer' ); ?>
					</option>
					<option value="1000-4999" <?php selected( "1000-4999", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '1 000 à 4 999', 'ositer' ); ?>
					</option>
					<option value="5000-9999" <?php selected( "5000-9999", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '5 000 à 9 999', 'ositer' ); ?>
					</option>
					<option value="10000-19999" <?php selected( "10000-19999", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '10 000 à 19 999', 'ositer' ); ?>
					</option>
					<option value="20000-49999" <?php selected( "20000-49999", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '20 000 à 49 999', 'ositer' ); ?>
					</option>
					<option value="50000+" <?php selected( "50000+", $lead_form_values['infos-taille-collectivite'] ?? '' ); ?>>
						<?php _e( '50 000 et +', 'ositer' ); ?>
					</option>
				</select>
			</div>
			<div class="input">
				<label for="infos-role"><span><?php _e( 'Votre rôle', 'ositer' ); ?></span></label>
				<select class="fake-select" name="infos-role" id="infos-role">
					<option value="" disabled hidden <?php selected( '', $lead_form_values['infos-role'] ?? '' ); ?>><?php _e( 'Votre rôle', 'ositer' ); ?></option>
					<option value="agent" <?php selected( 'agent', $lead_form_values['infos-role'] ?? '' ); ?>>
						<?php _e( 'Agent', 'ositer' ); ?>
					</option>
					<option value="agent-num" <?php selected( 'agent-num', $lead_form_values['infos-role'] ?? '' ); ?>>
						<?php _e( 'Agent en charge du numérique', 'ositer' ); ?>
					</option>
					<option value="elu" <?php selected( 'elu', $lead_form_values['infos-role'] ?? '' ); ?>>
						<?php _e( 'Élu-e', 'ositer' ); ?>
					</option>
					<option value="elu-num" <?php selected( 'elu-num', $lead_form_values['infos-role'] ?? '' ); ?>>
						<?php _e( 'Élu-e au numérique', 'ositer' ); ?>
					</option>
				</select>
			</div>
		</div>

		<div>
			<h4 class="mb150"><?php _e( 'Et pour la suite', 'ositer' ); ?></h4>
			<div class="input">
				<label for="suite-contact" class="checkbox">
					<span class="fake-toggle positioned-before">
						<input type="checkbox" name="suite-contact" id="suite-contact" value="1" <?php checked( 1, $lead_form_values['suite-contact'] ?? '' ); ?> />
						<span><?php _e( 'Je souhaite être contacté par OSINUM, pour poursuivre avec un entretien téléphonique', 'ositer' ); ?></span>
					</span>
				</label>
			</div>
			<div class="input">
				<label for="suite-consulter-resultats-comparaison" class="checkbox">
					<span class="fake-toggle positioned-before">
						<input type="checkbox" name="suite-consulter-resultats-comparaison" id="suite-consulter-resultats-comparaison" value="1" <?php checked( 1, $lead_form_values['suite-consulter-resultats-comparaison'] ?? '' ); ?> />
						<span><?php _e( 'J’autorise d’autres structures à consulter mes résultats (pour comparaison)', 'ositer' ); ?></span>
					</span>
				</label>
			</div>
			<div class="input">
				<label for="suite-disposer-donnees" class="checkbox">
					<span class="fake-toggle positioned-before">
						<input type="checkbox" name="suite-disposer-donnees" id="suite-disposer-donnees" value="1" <?php checked( 1, $lead_form_values['suite-disposer-donnees'] ?? '' ); ?> />
						<span><?php _e( 'J’autorise OSINUM à disposer des données', 'ositer' ); ?></span>
					</span>
				</label>
			</div>
		</div>
	</div>

	<footer class="mt250 clearfix">
		<button class="fr" type="submit">
			<?php _e( 'Envoyer', 'ositer' ); ?>
			<?php ositer()->icon( 'fleche-droite' ); ?>
		</button>
	</footer>
</form>

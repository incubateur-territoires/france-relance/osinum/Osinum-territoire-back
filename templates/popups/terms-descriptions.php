<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Abstracts\Term $terms */

?>

<?php foreach ( $terms as $term ) {
	printf(
		'<div id="term-content-%1$d" class="term-content"><h2 class="modal-title">%2$s</h2><div class="description">%3$s</div></div>',
		$term->id,
		$term->title,
		$term->description
	);
}

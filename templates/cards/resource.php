<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Resource $resource */
$resource = isset( $args ) ? $args['resource'] : $resource;
?>

<article class="post-card resource">
	<a href="<?php echo $resource->get_permalink(); ?>">
		<h3><?php echo $resource->get_name(); ?></h3>
		<p class="excerpt"><?php echo $resource->get_excerpt(); ?></p>
		<ol class="practices-list">
			<?php foreach ( $resource->get_practices() as $practice ) {
				printf( '<li class="practice">%2$s</li>', $practice->get_permalink(), $practice->get_name() );
			} ?>
			<?php foreach ( $resource->get_topics() as $topic ) {
				printf( '<li class="topic">%2$s</li>', $topic->get_permalink(), $topic->get_name() );
			} ?>
			<?php foreach ( $resource->get_difficulty_groups() as $difficulty_group ) {
				printf( '<li class="difficulty-group">%2$s</li>', $difficulty_group->get_permalink(), $difficulty_group->get_name() );
			} ?>
		</ol>
		<time><?php echo $resource->get_creation_date( 'j D Y' ); ?></time>
	</a>
</article>

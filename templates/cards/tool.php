<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Tool $tool */
$tool      = isset( $args ) ? $args['tool'] : $tool;
$practices = array_map( fn( $practice_id ) => ositer()->get_practice( $practice_id ), $tool->get_practices( true ) );
?>

<article class="post-card tool">
	<a href="<?php echo $tool->get_permalink(); ?>">
		<h3><?php echo $tool->get_name(); ?></h3>
		<p class="excerpt"><?php echo $tool->get_excerpt(); ?></p>
		<div class="description"><?php echo wpautop( $tool->get_description() ); ?></div>
		<ol class="practices-list">
			<?php foreach ( $practices as $practice ) {
				printf( '<li>%2$s</li>', $practice->get_permalink(), $practice->get_name() );
			} ?>
		</ol>
	</a>
</article>

<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Practice $practice */
$practice = isset( $args ) ? $args['practice'] : $practice;

?>

<article class="post-card practice">
	<a href="<?php echo $practice->get_permalink(); ?>">
		<?php ositer()->icon( 'ecran' ); ?>
		<h3><?php echo $practice->get_name(); ?></h3>
		<p class="label"><?php _e( 'pratique', 'ositer' ); ?></p>
	</a>
</article>

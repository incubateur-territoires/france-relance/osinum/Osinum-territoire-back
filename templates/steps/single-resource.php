<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package diagnostick-back
 */
$resource = new \OsinumTerritoire\Models\Resource( get_the_ID() );
?>
<section class="widget-panel home">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->

	</article><!-- #post-<?php the_ID(); ?> -->
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resource-sidebar' ); ?>
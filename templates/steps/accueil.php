<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var boolean $is_creation */
/** @var boolean $is_edition */

?>

<section class="widget-panel home">
	<?php if ( $is_edition ) { ?>
		<h1 class="color-vert-1-dark"><?php _e( 'Mon diagnostic numérique - OSINUM Territoires', 'ositer' ); ?></h1>

		<div class="block bg-blanc">
			<h3 class="m0 color-vert-1-dark"><?php printf( '%1$s - %2$s', $diagnostic->get_title(), $diagnostic->get_creation_date( 'd F Y' ) ); ?></h3>
			<p class="m0 mt50">
				<?php printf( __( 'Enregistré à l\'adresse %1$s', 'ositer' ), esc_html( $diagnostic->get_email() ) ); ?><br />
				<?php printf( __( 'Dernière modification le %1$s', 'ositer' ), $diagnostic->get_last_update( 'd F Y' ) ); ?>
			</p>
		</div>

		<h2 class="mt125 color-vert-1-dark"><?php _e( '4 exercices', 'ositer' ); ?></h2>
	<?php } else { ?>
		<h1 class="color-vert-1-dark"><?php _e( '4 exercices pour faire votre diagnostic numérique en ligne.', 'ositer' ); ?></h1>
	<?php } ?>

	<div class="grid per-2">
		<div class="block card card-big slug-situations bg-vert-1 bg-vert-1-hover clickable-card" <?php if ( $is_creation ) { ?> data-popup="diagnostic-create:situations" id="new" <?php } ?>>
			<div class="card-label top-right"><p><?php _e( '6 minutes', 'ositer' ); ?></p></div>
			<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
			<h3><?php _e( 'Situations', 'ositer' ); ?></h3>
			<p><?php _e( 'Quelles sont vos pratiques numériques actuelles&nbsp;?', 'ositer' ); ?></p>

			<?php if ( $is_creation ) { ?>
			<button data-popup="diagnostic-create:situations" id="new">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</button>
			<?php } else { ?>
			<a class="button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'situations' ) ); ?>">
				<?php if ( ! $diagnostic->has_done_step( 'situations' ) ) {
					_e( 'Faire l\'exercice', 'ositer' );
				} else {
					_e( 'Refaire l\'exercice', 'ositer' );
				} ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</a>
			<?php } ?>
		</div>
		<div class="block card card-big slug-outils bg-vert-1 bg-vert-1-hover clickable-card" <?php if ( $is_creation ) { ?> data-popup="diagnostic-create:outils" <?php } ?>>
			<div class="card-label top-right"><p><?php _e( '10 minutes', 'ositer' ); ?></p></div>
			<div class="icon"><?php ositer()->icon( 'ecran' ); ?></div>
			<h3><?php _e( 'Outils', 'ositer' ); ?></h3>
			<p><?php _e( 'Quels sont vos outils, logiciels, services numériques actuels&nbsp;?', 'ositer' ); ?></p>

			<?php if ( $is_creation ) { ?>
			<button data-popup="diagnostic-create:outils">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</button>
			<?php } else { ?>
			<a class="button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'outils' ) ); ?>">
				<?php if ( ! $diagnostic->has_done_step( 'outils' ) ) {
					_e( 'Faire l\'exercice', 'ositer' );
				} else {
					_e( 'Refaire l\'exercice', 'ositer' );
				} ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</a>
			<?php } ?>
		</div>
		<div class="block card card-big slug-criteres bg-vert-1 bg-vert-1-hover clickable-card" <?php if ( $is_creation ) { ?> data-popup="diagnostic-create:criteres" <?php } ?>>
			<div class="card-label top-right"><p><?php _e( '5 minutes', 'ositer' ); ?></p></div>
			<div class="icon"><?php ositer()->icon( 'etoile' ); ?></div>
			<h3><?php _e( 'Critères', 'ositer' ); ?></h3>
			<p><?php _e( 'Comment choisissez-vous vos outils ? Qu\'est-ce qui importe&nbsp;?', 'ositer' ); ?></p>

			<?php if ( $is_creation ) { ?>
			<button data-popup="diagnostic-create:criteres">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</button>
			<?php } else { ?>
			<a class="button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'criteres' ) ); ?>">
				<?php if ( ! $diagnostic->has_done_step( 'criteres' ) ) {
					_e( 'Faire l\'exercice', 'ositer' );
				} else {
					_e( 'Refaire l\'exercice', 'ositer' );
				} ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</a>
			<?php } ?>
		</div>
		<div class="block card card-big slug-difficultes bg-vert-1 bg-vert-1-hover clickable-card" <?php if ( $is_creation ) { ?> data-popup="diagnostic-create:difficultes" <?php } ?>>
			<div class="card-label top-right"><p><?php _e( '5 minutes', 'ositer' ); ?></p></div>
			<div class="icon"><?php ositer()->icon( 'chantier' ); ?></div>
			<h3><?php _e( 'Difficultés', 'ositer' ); ?></h3>
			<p><?php _e( 'Qu\'est-ce qui est difficile dans la mise en place d\'outils numériques, logiciels, applications&nbsp;?', 'ositer' ); ?></p>

			<?php if ( $is_creation ) { ?>
			<button data-popup="diagnostic-create:difficultes">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</button>
			<?php } else { ?>
			<a class="button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'difficultes' ) ); ?>">
				<?php if ( ! $diagnostic->has_done_step( 'difficultes' ) ) {
					_e( 'Faire l\'exercice', 'ositer' );
				} else {
					_e( 'Refaire l\'exercice', 'ositer' );
				} ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</a>
			<?php } ?>
		</div>
	</div>

	<?php if ( $is_edition ) { ?>
		<div class="block bg-vert-1 bg-vert-1-hover clickable-card mt150 results">
			<div class="flex-row flex-valign-center">
				<h3 class="m0"><?php _e( 'Voir les résultats', 'ositer' ); ?></h3>
				<a class="button mla" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'resultats' ) ); ?>">
					<?php _e( 'Consulter les résultats', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>

		<h2 class="mt350"><?php _e( 'Commencer un nouveau diagnostic', 'ositer' ); ?></h2>
		<div class="block bg-blanc">
			<div class="flex-row flex-valign-center">
				<p class="m0"><?php _e( 'Commencez un diagnostic avec nouveau permalien.', 'ositer' ); ?></p>
				<a class="button mla" href="<?php echo _ositer()->get_urls()->get_url( 'diagnostic-create' ); ?>?open=new">
					<?php _e( 'Commencer', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
	<?php } ?>

	<?php if ( $is_creation ) { ?>
		<div class="block bg-blanc mt150">
			<h3 class="m0 mb25 color-vert-1-dark"><?php _e( 'Premier diagnostic ?', 'ositer' ); ?></h3>
			<p class="m0"><?php _e( 'Commencez le diagnostic par un de ces 4 exercices proposés ci-dessus.', 'ositer' ); ?></p>
		</div>

		<div class="grid style-2-1 tablet-style-1-1 mt150">
			<div>
				<div class="block bg-blanc">
					<h3 class="mt0 mb25 color-vert-1-dark"><?php _e( 'Retrouver un diagnostic déjà commencé ou réalisé', 'ositer' ); ?></h3>
					<p><?php _e( 'Entrez le permalien que nous vous avons envoyé par mail dans la barre URL de votre navigateur ou bien dans le champ suivant :', 'ositer' ); ?></p>
					<?php include _ositer()->get_frontend()->template->get_template_file( 'steps/partials/accueil-permalink-form' ); ?>
				</div>
			</div>

			<div class="block bg-blanc">
				<h3 class="mt0 mb25 color-vert-1-dark"><?php _e( 'Où trouver mon permalien ?', 'ositer' ); ?></h3>
				<p><?php _e( 'Retrouvez le lien de votre ancien diagnostic parmis vos mails, à l’adresse que vous avez renseigné au moment de sa création.', 'ositer' ); ?></p>
				<h3 class="mb25 mt100 color-vert-1-dark"><?php _e( 'Je ne retrouve pas mon permalien', 'ositer' ); ?></h3>
				<p><?php _e( 'Envoyez-nous votre adresse mail et nous vous renvoyons le lien de votre diagnostic à votre adresse mail.', 'ositer' ); ?></p>
				<button class="mt150 db" data-popup="diagnostic-find-by-email">
					<?php _e( 'Retrouver mon diagnostic', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</button>
			</div>
		</div>
	<?php } ?>

	<h2 class="mt350 color-vert-1-dark"><?php _e( 'Faire le diagnostic autrement', 'ositer' ); ?></h2>

	<div class="grid per-3">
		<div class="block card bg-vert-1">
			<div class="card-label"><p><?php _e( 'À venir', 'ositer' ); ?></p></div>
			<h3 class="mt250 mb50"><?php _e( 'Participer à un atelier', 'ositer' ); ?></h3>
			<p class="m0"><?php _e( 'Vous désirez faire votre diagnostic accompagné ? Renseignez-vous sur le calendrier des ateliers dans les Ressources.', 'ositer' ); ?></p>
			<button data-popup="user-request-workshop-attend"><?php _e( 'En savoir plus…', 'ositer' ); ?></button>
		</div>
		<div class="block card bg-vert-1">
			<div class="card-label"><p><?php _e( 'À venir', 'ositer' ); ?></p></div>
			<h3 class="mt250 mb50"><?php _e( 'Organiser son atelier en équipe', 'ositer' ); ?></h3>
			<p class="m0"><?php _e( 'Vous désirez faire votre diagnostic en équipe, ou hors de l\'ordinateur ? Retrouvez tous les supports à imprimer dans les Ressources.', 'ositer' ); ?></p>
			<button data-popup="user-request-workshop-organize"><?php _e( 'En savoir plus…', 'ositer' ); ?></button>
		</div>
		<div class="block card bg-vert-1">
			<div class="card-label"><p><?php _e( 'À venir', 'ositer' ); ?></p></div>
			<h3 class="mt250 mb50"><?php _e( 'S\'entretenir avec Benoit de OSINUM', 'ositer' ); ?></h3>
			<p class="m0"><?php _e( 'Contacter Benoit de OSINUM pour programmer un temps d\'échange et réaliser un diagnostic plus approfondi.', 'ositer' ); ?></p>
			<button data-popup="user-request-osinum-contact"><?php _e( 'En savoir plus…', 'ositer' ); ?></button>
		</div>
	</div>
</section>

<?php OsinumTerritoire\Helpers::output_popup( 'diagnostic-create' ); ?>
<?php OsinumTerritoire\Helpers::output_popup( 'diagnostic-find-by-email' ); ?>
<?php OsinumTerritoire\Helpers::output_popup( 'user-request-workshop-attend' ); ?>
<?php OsinumTerritoire\Helpers::output_popup( 'user-request-workshop-organize' ); ?>
<?php OsinumTerritoire\Helpers::output_popup( 'user-request-osinum-contact' ); ?>

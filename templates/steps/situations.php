<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
?>

<section class="intro-panel">
	<div class="content">
		<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
		<h1><?php _e( 'Situations', 'ositer' ); ?></h1>
		<p><?php _e( 'Quelles sont les situations pratiques actuelles de votre structure&nbsp;?', 'ositer' ); ?></p>
		<button class="access-widget">
			<?php echo $diagnostic->has_done_step( 'situations' ) ? __( 'Reprendre', 'ositer' ) : __( 'C\'est parti', 'ositer' ); ?>
			<?php ositer()->icon( 'fleche-droite' ); ?>
		</button>
	</div>
</section>

<section class="widget-panel">
	<div class="content-container">
		<header class="step-title">
			<ul class="breadcrumbs">
				<li><a href="<?php echo $diagnostic->get_private_permalink(); ?>"><?php _e( 'Diagnostic', 'ositer' ); ?></a></li>
				<li class="active"><?php _e( 'Vos situations numériques', 'ositer' ); ?></li>
			</ul>
			<h1 class="sr-only"><?php _e( 'Vos situations numériques', 'ositer' ); ?></h1>
			<p><?php _e( 'Sélectionnez les “Cartes Situations” que vous vivez dans votre structure. Pour afficher les cartes par thématiques, triez-les avec les filtres à droite.', 'ositer' ); ?></p>
		</header>
		<div id="situations-selector-container"></div>
	</div>
</section>

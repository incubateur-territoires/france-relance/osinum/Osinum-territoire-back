<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $practices */
/** @var array $tools */
/** @var array $selected_tools */
/** @var array $criterias */

/**
 * Note : si le tableau de critères utilise des critères en -1/0/+1, réhabiliter cette logique
 * qui a été supprimée par le commit b8ab3e4 du 4 août 2022 à 10:34 (maintenant les critères existants sont +1 uniquement).
 */

?>

<?php if ( ! $diagnostic->has_done_step( 'outils' ) ) { ?>
	<section class="widget-panel resultats">
		<div class="content-container">
			<h1>
				<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
				<?php _e( 'Vos outils <span class="fwn">Résultats</span>', 'ositer' ); ?>
			</h1>
			<div class="missing-data">
				<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>
				<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'outils' ) ); ?>">
					<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				</a>
			</div>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats outils">
	<div class="content-container">
		<h1>
			<div class="icon"><?php ositer()->icon( 'ecran' ); ?></div>
			<?php _e( 'Vos outils <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>
		<h3><?php _e( 'État des lieux', 'ositer' ); ?></h3>
		<div class="block bg-blanc mb400">
			<p class="m0"><?php _e( 'Vous utilisez les outils (logiciels, applications, services) et pratiques numériques suivants&nbsp;:', 'ositer' ); ?></p>
			<nav>
				<p class="mt200 mb50"><?php _e( 'Afficher par :', 'ositer' ); ?></p>
				<ul class="tabs">
					<li class="active" data-tab="practices"><?php _e( 'Pratiques', 'ositer' ); ?></li>
					<li data-tab="tools" class="blue"><?php _e( 'Outils', 'ositer' ); ?></li>
				</ul>
			</nav>
			<div class="tab-content active mt200" data-tab="practices">
				<div class="dual-table for-practices">
					<ol>
						<li class="header-row">
							<div class="column-1"><span><?php _e( 'Pratiques de la structure', 'ositer' ); ?></span></div>
							<div class="column-2"><span><?php _e( 'Outils recensés par pratique', 'ositer' ); ?></span></div>
						</li>
						<?php foreach ( $practices as $p => $practice ) {
							$progress_bar_width = count( $practice->selected_tools ) / count( $selected_tools ) * 100;
							?>
							<li class="item-row">
								<div class="column-1">
									<h5><?php echo $practice->title; ?></h5>
								</div>
								<div class="column-2">
									<h6><?php _e( 'Nombre d\'outils :', 'ositer' ); ?></h6>
									<div class="pb-and-button">
										<div class="progress-bar-container">
											<div class="progress-bar" style="width: <?php echo (int) $progress_bar_width; ?>%"></div>
											<span class="count"><?php printf( '%1$d/%2$d', count( $practice->selected_tools ), count( $selected_tools ) ); ?></span>
										</div>
										<button class="theme-bleu-1-outline theme-bleu-1-dark-hover" data-reveal="<?php printf( '.details-list-practice-%1$d', $p ); ?>"><?php ositer()->icon( 'caret-bas' ); ?></button>
									</div>
									<ol class="details-list <?php printf( 'details-list-practice-%1$d', $p ); ?>">
										<?php foreach ( $practice->selected_tools as $tool ) { ?>
											<li><?php ositer()->icon( 'fleche-droite' ); ?> <?php echo $tool->title; ?></li>
										<?php } ?>
									</ol>
								</div>
							</li>
						<?php } ?>
					</ol>
				</div>
			</div>
			<div class="tab-content mt200" data-tab="tools">
				<div class="dual-table for-tools">
					<ol>
						<li class="header-row">
							<div class="column-1"><span><?php _e( 'Outils de la structure', 'ositer' ); ?></span></div>
							<div class="column-2"><span><?php _e( 'Pratiques recensées par outil', 'ositer' ); ?></span></div>
						</li>
						<?php foreach ( $selected_tools as $t => $selected_tool ) {
							$progress_bar_width = count( $selected_tool->configuration->practices ) / count( $practices ) * 100;
							?>
							<li class="item-row">
								<div class="column-1">
									<h5><?php echo $selected_tool->title; ?></h5>
								</div>
								<div class="column-2">
									<h6><?php _e( 'Nombre de pratiques :', 'ositer' ); ?></h6>
									<div class="pb-and-button">
										<div class="progress-bar-container">
											<div class="progress-bar" style="width: <?php echo (int) $progress_bar_width; ?>%"></div>
											<span class="count"><?php printf( '%1$d/%2$d', count( $selected_tool->configuration->practices ), count( $practices ) ); ?></span>
										</div>
										<button class="theme-vert-1-outline theme-vert-1-dark-hover" data-reveal="<?php printf( '.details-list-tool-%1$d', $t ); ?>"><?php ositer()->icon( 'caret-bas' ); ?></button>
									</div>
									<ol class="details-list <?php printf( 'details-list-tool-%1$d', $t ); ?>">
										<?php foreach ( $selected_tool->configuration->practices as $practice_id ) {
											// Find practice by ID in the $practices array.
											$practice = Helpers::array_find( fn( $practice ) => $practice->id === $practice_id, $practices );
											if ( $practice ) { ?>
											<li><?php ositer()->icon( 'fleche-droite' ); ?> <?php echo $practice->title; ?></li>
											<?php } ?>
										<?php } ?>
									</ol>
								</div>
							</li>
						<?php } ?>
					</ol>
				</div>
			</div>
			<div class="clearfix mt250">
				<a class="button fr theme-vert-1-outline" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'outils' ) ); ?>">
					<?php _e( 'Retourner à l\'exercice', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
		<div class="block bg-blanc mb400">
			<p class="m0"><?php _e( 'Choisissez le critère que vous souhaitez afficher dans la colonne, en cliquant sur la colonne, ou visualisez le tableau complet.', 'ositer' ); ?></p>
			<div id="resultats-outils-comparaison-container" class="mt200"></div>
			<div class="clearfix buttons mt100 mb150">
				<a class="button" href="<?php echo $diagnostic->get_private_permalink( 'imprimer' ); ?>" target="_blank">
					<?php ositer()->icon( 'download' ); ?>
					<?php _e( 'Télécharger le tableau', 'ositer' ); ?>
				</a>
				<button class="button" data-popup="resultats-outil-tableau">
					<?php ositer()->icon( 'oeil' ); ?>
					<?php _e( 'Voir le tableau complet', 'ositer' ); ?>
				</button>
			</div>
			<hr />
			<div class="clearfix mt150">
				<a class="button fr theme-vert-1-outline" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'outils' ) ); ?>">
					<?php _e( 'Retourner à l\'exercice', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
		<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-footer' ); ?>
	</div>
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-sidebar' ); ?>

<?php OsinumTerritoire\Helpers::output_popup( 'resultats-outil-tableau', null, [ 'criterias' => $criterias, 'selected_tools' => $selected_tools ] ); ?>

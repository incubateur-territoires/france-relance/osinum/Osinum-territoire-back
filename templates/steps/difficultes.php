<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

?>

<section class="intro-panel">
	<div class="content">
		<div class="icon"><?php ositer()->icon( 'chantier' ); ?></div>
		<h1><?php _e( 'Difficultés', 'ositer' ); ?></h1>
		<p><?php _e( 'Qu\'est-ce qui est difficile dans la mise en place d\'outils numériques, logiciels, applications&nbsp;?', 'ositer' ); ?></p>
		<button class="access-widget">
			<?php echo $diagnostic->has_done_step( 'difficultes' ) ? __( 'Reprendre', 'ositer' ) : __( 'C\'est parti', 'ositer' ); ?>
			<?php ositer()->icon( 'fleche-droite' ); ?>
		</button>
	</div>
</section>

<section class="widget-panel">
	<div class="content-container">
		<header class="step-title">
			<ul class="breadcrumbs">
				<li><a href="<?php echo $diagnostic->get_private_permalink(); ?>"><?php _e( 'Diagnostic', 'ositer' ); ?></a></li>
				<li class="active"><?php _e( 'Vos difficultés au changement', 'ositer' ); ?></li>
			</ul>
			<h1 class="sr-only"><?php _e( 'Vos difficultés au changement', 'ositer' ); ?></h1>
			<p><?php _e( 'Sélectionnez vos "Cartes Difficultés" à la mise en place d\'outils numériques, logiciels, applications. Pour afficher les cartes par thématique, triez-les avec les filtres à droite.', 'ositer' ); ?></p>
		</header>
		<div id="difficultes-selector-container"></div>
	</div>
</section>

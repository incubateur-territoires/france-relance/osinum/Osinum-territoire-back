<?php

defined( 'ABSPATH' ) || die();

?>
<form action="#" id="diagnostic-permalink" class="mt150">
	<div class="flex-row flex-valign-bottom">
		<div class="input flex">
			<div class="input-container">
				<label class="tiny-label" for="permalink"><span><?php _e( 'Votre permalien reçu par mail', 'ositer' ); ?></span></label>
				<input type="url" id="permalink" name="permalink" required placeholder="<?php esc_attr_e( 'Copier-coller votre permalien ici', 'ositer' ); ?>" />
			</div>
		</div>

		<div class="button-container">
			<button type="submit">
				<?php _e( 'Aller au diagnostic', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-droite' ); ?>
			</button>
		</div>
	</div>
</form>

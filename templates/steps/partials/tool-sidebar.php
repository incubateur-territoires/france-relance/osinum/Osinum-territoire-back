<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $tool */
/** @var string $current_step */

?>
<aside class="results-nav print-hidden">
	<ul>
		<li class="mini-title">
			<?php _e( 'Que faire de cette fiche ?', 'ositer' ); ?>
		</li>
		<li>
			<a class="button theme-bleu-1-outline" data-save="post-<?php esc_attr_e( $tool->get_id() ); ?>">
				<?php _e( 'Télécharger', 'ositer' ); ?>
				<?php ositer()->icon( 'download' ); ?>
			</a>
		</li>
		<li>
			<a class="button theme-bleu-1-outline" href="<?php echo $tool->get_permalink(); ?>" data-copy-clipboard="<?php echo $tool->get_permalink(); ?>">
				<?php _e( 'Copier le lien', 'ositer' ); ?>
				<?php ositer()->icon( 'lien' ); ?>
			</a>
		</li>
		<li>
			<button class="button theme-bleu-1-outline" data-popup="share-single-tool">
				<?php _e( 'Partager', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-haut' ); ?>
			</a>
		</li>
	</ul>
</aside>

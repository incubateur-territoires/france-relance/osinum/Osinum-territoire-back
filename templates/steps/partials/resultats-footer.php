<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var string $current_step */

$grid = 3;

if ( $current_step === 'accueil' ) {
	$grid = 4;
}

?>
<footer class="print-hidden resultats-footer">	
	<?php if ( $current_step === 'accueil' ) { ?>
		<h2><?php _e( 'Visualisez vos résultats par exercice', 'ositer' ); ?></h2>
	<?php } else { ?>
		<h2><?php _e( 'Résultats des autres exercices', 'ositer' ); ?></h2>
	<?php } ?>

	<div class="grid per-<?php echo $grid; ?>">
		<?php foreach ( _ositer()->get_data()->get_results_steps() as $step ) {
			if ( $step->slug === $current_step ) {
				continue;
			} ?>
			<div class="block card bg-vert-1 bg-vert-1-hover clickable-card step-<?php echo $step->slug; ?>">
				<div class="icon"><?php ositer()->icon( $step->icon ); ?></div>
				<h3><?php echo $step->title; ?></h3>
				<a class="button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'resultats/' . $step->slug ) ); ?>">
					<?php _e( 'Résultats', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
			<?php
		} ?>
	</div>
</footer>

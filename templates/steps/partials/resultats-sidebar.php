<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var string $current_step */

?>
<aside class="results-nav print-hidden">
	<ul>
		<?php if ( $current_step !== 'accueil' ) { ?>
			<li>
				<a class="button" href="<?php echo $diagnostic->get_private_permalink( 'resultats' ); ?>">
					<?php _e( 'Accueil Résultats', 'ositer' ); ?>
				</a>
			</li>
		<?php } ?>
		<li class="mini-title">
			<?php _e( 'Que faire de vos résultats ?', 'ositer' ); ?>
		</li>
		<li>
			<a class="button theme-vert-1-outline" target="_blank" href="<?php echo $diagnostic->get_private_permalink( 'imprimer' ); ?>">
				<?php _e( 'Télécharger', 'ositer' ); ?>
				<?php ositer()->icon( 'download' ); ?>
			</a>
		</li>
		<li>
			<a class="button theme-vert-1-outline" href="<?php echo $diagnostic->get_private_permalink(); ?>" data-copy-clipboard="<?php echo $diagnostic->get_private_permalink(); ?>">
				<?php _e( 'Copier le lien', 'ositer' ); ?>
				<?php ositer()->icon( 'lien' ); ?>
			</a>
		</li>
		<li>
			<button class="button theme-vert-1-outline" data-popup="share">
				<?php _e( 'Partager', 'ositer' ); ?>
				<?php ositer()->icon( 'fleche-haut' ); ?>
			</a>
		</li>
	</ul>
</aside>

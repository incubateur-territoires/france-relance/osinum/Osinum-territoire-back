<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

?>

<section class="intro-panel">
	<div class="content">
		<div class="icon"><?php ositer()->icon( 'etoile' ); ?></div>
		<h1><?php _e( 'Critères', 'ositer' ); ?></h1>
		<p><?php _e( 'Comment choisissez-vous vos outils ? Qu\'est-ce qui importe&nbsp;?', 'ositer' ); ?></p>
		<button class="access-widget">
			<?php echo $diagnostic->has_done_step( 'criteres' ) ? __( 'Reprendre', 'ositer' ) : __( 'C\'est parti', 'ositer' ); ?>
			<?php ositer()->icon( 'fleche-droite' ); ?>
		</button>
	</div>
</section>

<section class="widget-panel">
	<div class="content-container">
		<header class="step-title">
			<ul class="breadcrumbs">
				<li><a href="<?php echo $diagnostic->get_private_permalink(); ?>"><?php _e( 'Diagnostic', 'ositer' ); ?></a></li>
				<li class="active"><?php _e( 'Vos critères de choix d\'outils', 'ositer' ); ?></li>
			</ul>
			<h1 class="sr-only"><?php _e( 'Vos critères de choix d\'outils', 'ositer' ); ?></h1>
			<p><?php _e( 'Évaluez les critères qui sont importants pour vous, lorsque vous devez choisir et utiliser un outil numérique.', 'ositer' ); ?></p>
		</header>
		<div id="criteres-selector-container"></div>
	</div>
</section>

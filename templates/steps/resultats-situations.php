<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $situations */
/** @var array $topics */
/** @var array $selected_situations_ids */
/** @var array $suggested_practices */
/** @var array $suggested_tools */
/** @var array $suggested_resources */
/** @var boolean $print */

?>

<?php if ( ! $diagnostic->has_done_step( 'situations' ) ) { ?>
	<section class="widget-panel resultats situations">
		<div class="content-container">
			<h1>
				<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
				<?php _e( 'Vos situations <span class="fwn">Résultats</span>', 'ositer' ); ?>
			</h1>
			<div class="missing-data">
				<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>
				<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'situations' ) ); ?>">
					<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				</a>
			</div>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats situations">
	<div class="content-container">
		<h1>
			<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
			<?php _e( 'Vos situations <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>
		<h2><?php _e( 'État des lieux', 'ositer' ); ?></h2>
		<div class="block bg-blanc mb400">
			<h4 class="m0 mb50"><?php _e( 'Tendances de vos situations numériques', 'ositer' ); ?></h4>
			<p><?php _e( 'Les situations numériques que vous avez sélectionnées semblent concernées par les thématiques suivantes&nbsp;:', 'ositer' ); ?></p>
			<div class="comparison-table topics-situations-table">
				<ol>
					<li class="mini-title">
						<div class="category-header topic"><span><?php _e( 'Thématiques', 'ositer' ); ?></span></div>
						<div class="entities-header situations"><span><?php _e( 'Situations vécues', 'ositer' ); ?></span></div>
					</li>
					<?php foreach ( $topics as $topic ) {
						$situations_in_topic = array_filter( $situations, function ( $situation ) use ( $topic ) {
							return in_array( $topic->id, $situation->topics, true );
						} );
						$situations_ids_in_topic          = array_map( fn ( $situation ) => $situation->id, $situations_in_topic );
						$selected_situations_ids_in_topic = array_intersect( $selected_situations_ids, $situations_ids_in_topic );
						$progress_bar_width				  = 0;
						if ( count( $selected_situations_ids_in_topic ) > 0 && count( $situations_ids_in_topic ) > 0 ) {
							$progress_bar_width           = count( $selected_situations_ids_in_topic ) / count( $situations_ids_in_topic ) * 100;
						}
						$tooltip_content                  = sprintf( '%1$d/%2$d carte(s) Situations sélectionnée(s) concernent la thématique %3$s', count( $selected_situations_ids_in_topic ), count( $situations_ids_in_topic ), $topic->title );
						?>
						<li class="item">
							<div class="category">
								<div class="icon-<?php echo $topic->color; ?>-dark"><?php ositer()->icon( $topic->icon ); ?></div>
								<span class="color-<?php echo $topic->color; ?>-dark"><?php echo $topic->title; ?></span>
								<button class="icon-only" data-popup="terms-descriptions:<?php echo $topic->id; ?>"><?php ositer()->icon( 'info' ); ?></button>
							</div>
							<div class="entities">
								<div class="progress-bar-container" data-microtip-size="large" role="tooltip" aria-label="<?php echo ! empty( $tooltip_content ) ? esc_attr( $tooltip_content ) : 'Aucune'; ?>" data-microtip-position="top">
									<div class="progress-bar bg-<?php echo $topic->color; ?>" style="width:<?php echo (int) $progress_bar_width; ?>%;"></div>
									<span class="count color-<?php echo $topic->color; ?>-dark"><?php printf( '%1$d/%2$d', count( $selected_situations_ids_in_topic ), count( $situations_ids_in_topic ) ); ?></span>
								</div>
							</div>
						</li>
					<?php } ?>
				</ol>
			</div>
			<div class="clearfix mt150">
				<a class="button fr theme-vert-1-outline" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'situations' ) ); ?>">
					<?php _e( 'Retourner à l\'exercice', 'ositer' ); ?>
				</a>
			</div>
		</div>
		<div class="mb400">
			<h2 class="m0"><?php _e( 'Pratiques suggérées', 'ositer' ); ?> <span class="count">(<?php echo count( $suggested_practices ); ?>)</span></h2>
			<p><?php _e( 'Votre sélection de Situations nous permet de vous suggérer d\'outiller les pratiques suivantes&nbsp;:', 'ositer' ); ?></p>
			<div class="swiper-element">
				<div class="swiper" data-slides="3.1" data-slides-mobile="1.4">
					<div class="swiper-wrapper">
						<?php foreach ( $suggested_practices as $practice ) {
							_ositer()->get_frontend()->template->render_card( $practice, 'practice', 'swiper-slide' );
						} ?>
					</div>
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
		</div>
		<div class="mb400">
			<h2 class="m0"><?php _e( 'Outils suggérés', 'ositer' ); ?> <span class="count">(<?php echo count( $suggested_tools ); ?>)</h2>
			<p><?php _e( 'Votre sélection de Situations nous permet de vous suggérer de vous intéresser à des outils numériques comme&nbsp;:', 'ositer' ); ?></p>
			<div class="swiper-element">
				<div class="swiper" data-slides="2.6" data-slides-mobile="1.2" data-space="24">
					<div class="swiper-wrapper">
						<?php foreach ( $suggested_tools as $tool ) {
							_ositer()->get_frontend()->template->render_card( $tool, 'tool', 'swiper-slide' );
						} ?>
					</div>
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
			<div class="clearfix mt100">
				<a class="button fr" href="<?php echo _ositer()->get_urls()->get_url( 'tools' ) ?>">
					<?php _e( 'Découvrir le catalogue d\'outils', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
		<div class="mb400">
			<h2 class="m0"><?php _e( 'Ressources suggérées', 'ositer' ); ?> <span class="count">(<?php echo count( $suggested_resources ); ?>)</h2>
			<p><?php _e( 'Votre sélection de Situations nous permet de vous suggérer de vous interesser à des ressources comme&nbsp;:', 'ositer' ); ?></p>
			<div class="swiper-element">
				<div class="swiper" data-slides="2.6" data-slides-mobile="1.2" data-space="24">
					<div class="swiper-wrapper">
					<?php foreach ( $suggested_resources as $resource ) {
						_ositer()->get_frontend()->template->render_card( $resource, 'resource', 'swiper-slide post-card__cover' );
					} ?>
					</div>
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
			<div class="clearfix mt100">
				<a class="button fr" href="<?php echo _ositer()->get_urls()->get_url( 'resources' ) ?>">
					<?php _e( 'Découvrir la ressourcerie', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
		<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-footer' ); ?>
	</div>
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-sidebar' ); ?>

<?php OsinumTerritoire\Helpers::output_popup( 'terms-descriptions', null, [ 'terms' => $topics, ] ); ?>

<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $criterias */
/** @var array $tools */

?>

<?php if ( ! $diagnostic->has_done_step( 'criteres' ) ) { ?>
	<section class="widget-panel resultats">
		<div class="content-container">
			<h1>
				<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
				<?php _e( 'Vos critères <span class="fwn">Résultats</span>', 'ositer' ); ?>
			</h1>
			<div class="missing-data">
				<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>
				<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'criteres' ) ); ?>">
					<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				</a>
			</div>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats">
	<div class="content-container">
		<h1>
			<div class="icon"><?php ositer()->icon( 'etoile' ); ?></div>
			<?php _e( 'Vos critères <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>
		<h3><?php _e( 'État des lieux', 'ositer' ); ?></h3>
		<div class="block bg-blanc mb400">
			<h4 class="m0 mb50"><?php _e( 'Évaluation de vos critères de choix d\'outils', 'ositer' ); ?></h4>
			<p><?php _e( 'Vous avez évalué l\'importance des critères suivants, pour choisir et utiliser des outils numériques, entre 1 et 5 :', 'ositer' ); ?></p>
			<div class="comparison-table with-3-columns criterias-tools-table">
				<ol>
					<li class="mini-title">
						<div class="category-header criteria"><span><?php _e( 'Critères', 'ositer' ); ?></span></div>
						<div class="evaluation-header importance"><span><?php _e( 'Importance du critère', 'ositer' ); ?></span></div>
						<div class="entities-header tools"><span><?php _e( 'Outils (recensés) respectant le critère', 'ositer' ); ?></span></div>
					</li>
					<?php foreach ( $criterias as $c => $criteria ) {
						$progress_bar_width = count( $tools ) > 0 ? count( $criteria->tools ) * 100 / count( $tools ) : 0;
						?>
						<li class="item<?php echo is_null( $criteria->rating ) ? ' non-evaluated' : ' evaluated'; ?>">
							<div class="category criteria">
								<span><?php echo $criteria->title; ?></span>
								<button class="icon-only" data-popup="terms-descriptions:<?php echo $criteria->id; ?>"><?php ositer()->icon( 'info' ); ?></button>
							</div>
							<div class="evaluation">
								<?php if ( ! is_null( $criteria->rating ) ) {
									echo '<ul class="rating-stars">';
									for ( $i = 0; $i < 4; $i++ ) {
										if ( $i < $criteria->rating ) {
											printf( '<li class="star star-1">%1$s</li>', ositer()->icon( 'etoile-pleine', false ) );
										} else {
											printf( '<li class="star star-0">%1$s</li>', ositer()->icon( 'etoile-pleine', false ) );
										}
									}
									echo '</ul>';
								} else {
									printf( '<p class="no-value">%1$s</p>', __( 'Non-évalué', 'ositer' ) );
								} ?>
							</div>
							<div class="entities tools">
								<?php if ( ! $diagnostic->has_done_step( 'outils' ) ) { ?>
									<?php if ( $c === 0 ) { ?>
									<p class="no-value">
										<?php printf(
											__( 'Pour obtenir des résultats, <a href="%1$s">réalisez l\'exercice</a>.', 'ositer' ),
											esc_url( $diagnostic->get_private_permalink( 'outils' ) )
										); ?>
									</p>
									<?php } ?>
								<?php } else { ?>
								<div class="progress-bar-container" data-microtip-size="medium" role="tooltip" aria-label="<?php echo ! empty( $criteria->tools ) ? esc_attr( implode( "\n", wp_list_pluck( $criteria->tools, 'title' ) ) ) : 'Aucun'; ?>" data-microtip-position="top">
									<div class="progress-bar bg-vert-1" style="width:<?php echo (int) $progress_bar_width; ?>%;"></div>
									<span class="count color-vert-1-dark"><?php printf( '%1$d/%2$d', count( $criteria->tools ), count( $tools ) ); ?></span>
								</div>
								<?php } ?>
							</div>
						</li>
					<?php } ?>
				</ol>
			</div>
			<div class="clearfix mt150">
				<a class="button fr theme-vert-1-outline" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'criteres' ) ); ?>">
					<?php _e( 'Retourner à l\'exercice', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
		<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-footer' ); ?>
	</div>
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-sidebar' ); ?>

<?php OsinumTerritoire\Helpers::output_popup( 'terms-descriptions', null, [ 'terms' => $criterias, ] ); ?>

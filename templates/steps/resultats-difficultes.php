<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $difficulties */
/** @var array $groups */
/** @var array $selected_difficulties_ids */

?>

<?php if ( ! $diagnostic->has_done_step( 'difficultes' ) ) { ?>
	<section class="widget-panel resultats">
		<div class="content-container">
			<h1>
				<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
				<?php _e( 'Vos difficultés <span class="fwn">Résultats</span>', 'ositer' ); ?>
			</h1>
			<div class="missing-data">
				<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>
				<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'difficultes' ) ); ?>">
					<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
				</a>
			</div>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats">
	<div class="content-container">
		<h1>
			<div class="icon"><?php ositer()->icon( 'chantier' ); ?></div>
			<?php _e( 'Vos difficultés <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>
		<h3><?php _e( 'État des lieux', 'ositer' ); ?></h3>
		<div class="block bg-blanc mb400">
			<h4 class="m0 mb50"><?php _e( 'Tendances de vos difficultés', 'ositer' ); ?></h4>
			<p><?php _e( 'Les difficultés que vous avez sélectionnées, pour lesquelles vous avez indiqué être concerné, semblent concernées par les thématiques suivantes :', 'ositer' ); ?></p>
			<div class="comparison-table groups-difficulties-table">
				<ol>
					<li class="mini-title">
						<div class="category-header group"><span><?php _e( 'Thématiques', 'ositer' ); ?></span></div>
						<div class="entities-header difficulties"><span><?php _e( 'Difficultés sélectionnées', 'ositer' ); ?></span></div>
					</li>
					<?php foreach ( $groups as $group ) {
						$difficulties_in_group = array_filter( $difficulties, function ( $difficulty ) use ( $group ) {
							return in_array( $group->id, $difficulty->groups, true );
						} );
						$difficulties_ids_in_group          = array_map( fn ( $difficulty ) => $difficulty->id, $difficulties_in_group );
						$selected_difficulties_ids_in_group = array_intersect( $selected_difficulties_ids, $difficulties_ids_in_group );
						$progress_bar_width				  	= 0;
						if ( count( $selected_difficulties_ids_in_group ) > 0 && count( $difficulties_ids_in_group ) > 0 ) {
							$progress_bar_width           	= count( $selected_difficulties_ids_in_group ) / count( $difficulties_ids_in_group ) * 100;
						}
						$tooltip_content                    = sprintf( '%1$d/%2$d carte(s) Difficultés sélectionnée(s) concernent la thématique %3$s', count( $selected_difficulties_ids_in_group ), count( $difficulties_ids_in_group ), $group->title );
						?>
						<li class="item">
							<div class="category">
								<div class="icon-<?php echo $group->color; ?>-dark"><?php ositer()->icon( $group->icon ); ?></div>
								<span class="color-<?php echo $group->color; ?>-dark"><?php echo $group->title; ?></span>
								<button class="icon-only" data-popup="terms-descriptions:<?php echo $group->id; ?>"><?php ositer()->icon( 'info' ); ?></button>
							</div>
							<div class="entities">
								<div class="progress-bar-container" data-microtip-size="large" role="tooltip" aria-label="<?php echo ! empty( $tooltip_content ) ? esc_attr( $tooltip_content ) : 'Aucune'; ?>" data-microtip-position="top">
									<div class="progress-bar bg-<?php echo $group->color; ?>-dark" style="width:<?php echo (int) $progress_bar_width; ?>%;"></div>
									<span class="count color-<?php echo $group->color; ?>"><?php printf( '%1$d/%2$d', count( $selected_difficulties_ids_in_group ), count( $difficulties_ids_in_group ) ); ?></span>
								</div>
							</div>
						</li>
					<?php } ?>
				</ol>
			</div>
			<div class="clearfix mt150">
				<a class="button fr theme-vert-1-outline" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'difficultes' ) ); ?>">
					<?php _e( 'Retourner à l\'exercice', 'ositer' ); ?>
				</a>
			</div>
		</div>
		<div class="mb400">
			<h3 class="m0"><?php _e( 'Ressources suggérées', 'ositer' ); ?> <span class="count">(<?php echo count( $suggested_resources ); ?>)</h3>
			<p><?php _e( 'Votre sélection de Difficultés nous permet de vous suggérer de vous interesser à des ressources comme&nbsp;:', 'ositer' ); ?></p>
			<div class="swiper-element">
				<div class="swiper" data-slides="2.6" data-slides-mobile="1.2" data-space="24">
					<div class="swiper-wrapper">
						<?php foreach ( $suggested_resources as $resource ) {
							_ositer()->get_frontend()->template->render_card( $resource, 'resource', 'swiper-slide post-card__cover' );
						} ?>
					</div>
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
			<div class="clearfix mt100">
				<a class="button fr" href="<?php echo _ositer()->get_urls()->get_url( 'resources' ) ?>">
					<?php _e( 'Découvrir la ressourcerie', 'ositer' ); ?>
					<?php ositer()->icon( 'fleche-droite' ); ?>
				</a>
			</div>
		</div>
		<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-footer' ); ?>
	</div>
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-sidebar' ); ?>

<?php OsinumTerritoire\Helpers::output_popup( 'terms-descriptions', null, [ 'terms' => $groups, ] ); ?>

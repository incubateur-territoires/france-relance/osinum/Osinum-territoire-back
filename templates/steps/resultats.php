<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

?>

<section class="widget-panel resultats">
	<h1><?php _e( 'Résultats <span class="fwn">Diagnostic Osinum Territoire</span>', 'ositer' ); ?></h1>

	<div class="block bg-blanc mb150">
		<h2 class="m0"><?php printf( '%1$s - %2$s', $diagnostic->get_title(), $diagnostic->get_creation_date( 'd F Y' ) ); ?></h2>
		<div class="mt100 flex-row flex-valign-center">
			<p class="m0">
				<?php printf( __( 'Enregistré à l\'adresse %1$s', 'ositer' ), esc_html( $diagnostic->get_email() ) ); ?><br />
				<?php printf( __( 'Dernière modification le %1$s', 'ositer' ), $diagnostic->get_last_update( 'd F Y' ) ); ?>
			</p>

			<button class="mla" data-popup="diagnostic-lead-form"><?php _e( 'Compléter les infos de ma structure', 'ositer' ); ?></button>
		</div>
	</div>

	<div class="block bg-blanc mb300">
		<h2 class="m0"><?php _e( 'À propos des résultats', 'ositer' ); ?></h2>
		<p class="mb0"><?php _e( 'Les résultats sont issus des sélections et renseignements que vous avez donner dans les exercices précédents. Les suggestions sont faites en fonction de ces informations. Vous pouvez revenir sur chaque exercice pour ajuster vos informations.', 'ositer' ); ?></p>
	</div>

	<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-footer' ); ?>
</section>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'steps/partials/resultats-sidebar' ); ?>

<?php OsinumTerritoire\Helpers::output_popup( 'diagnostic-lead-form', null, [ 'lead_form_values' => $lead_form_values ] ); ?>

<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

$description = get_the_archive_description();
?>

<section class="widget-panel home">

    <h1 class="color-bleu-1-dark"><?php echo get_the_archive_title(); ?></h1>
    <?php if ( $description ) : ?>
        <div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
    <?php endif; ?>

    <?php while ( have_posts() ) :
        the_post();
        _ositer()->get_frontend()->template->render_card( get_the_ID(), 'tool' );
    endwhile; ?>

</section>
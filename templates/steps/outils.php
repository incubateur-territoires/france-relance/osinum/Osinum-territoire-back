<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

?>

<section class="intro-panel">
	<div class="content">
		<div class="icon"><?php ositer()->icon( 'ecran' ); ?></div>
		<h1><?php _e( 'Outils', 'ositer' ); ?></h1>
		<p><?php _e( 'Quels sont vos outils, logiciels, services numériques actuels&nbsp;?', 'ositer' ); ?></p>
		<button class="access-widget">
			<?php echo $diagnostic->has_done_step( 'outils' ) ? __( 'Reprendre', 'ositer' ) : __( 'C\'est parti', 'ositer' ); ?>
			<?php ositer()->icon( 'fleche-droite' ); ?>
		</button>
	</div>
</section>

<section class="widget-panel">
	<div class="content-container">
		<header class="step-title">
			<ul class="breadcrumbs">
				<li><a href="<?php echo $diagnostic->get_private_permalink(); ?>"><?php _e( 'Diagnostic', 'ositer' ); ?></a></li>
				<li class="active"><?php _e( 'Vos outils numériques actuels', 'ositer' ); ?></li>
			</ul>
			<h1 class="sr-only"><?php _e( 'Vos outils numériques actuels', 'ositer' ); ?></h1>
			<p class="mobile-hidden"><?php _e( 'Renseigner les outils numériques, logiciels, services numériques que vous utilisez, en choissant parmis les Cartes Pratiques (cliquer sur «&nbsp;Cartes Pratiques&nbsp;») ou en choisissant parmis les Cartes Outils (cliquer sur «&nbsp;Cartes Outils&nbsp;»).', 'ositer' ); ?></p>
			<p class="mobile-only"><?php _e( 'Renseigner les outils numériques, logiciels, services numériques que vous utilisez, en choisissant parmi les Cartes Pratiques.', 'ositer' ); ?></p>
		</header>
		<div id="outils-selector-container"></div>
	</div>
</section>

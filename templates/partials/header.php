<?php

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

defined( 'ABSPATH' ) || die();
$is_results = false;
if ( _ositer()->get_urls()->is_diagnostic_results_page() ) {
	$is_results = true;
}
?>
<nav class="mobile-step-navigation top">
	<a class="button <?php echo ! $is_results ? 'theme-vert-1' : ''; ?>" href="<?php echo esc_url( $diagnostic->get_private_permalink() ); ?>"><?php _e( 'Diagnostic', 'diagnostick-back' ); ?></a>
	<a class="button <?php echo $is_results ? 'theme-vert-1' : ''; ?>" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'resultats' ) ); ?>"><?php _e( 'Résultats', 'diagnostick-back' ); ?></a>
</nav>

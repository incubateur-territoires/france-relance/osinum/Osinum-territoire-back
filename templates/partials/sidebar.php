<?php

use OsinumTerritoire\Config;

defined( 'ABSPATH' ) || die();

$nav_items = apply_filters( 'ositer/sidebar/navigation-items', [] );

?>

<nav id="site-navigation" class="navigation">
	<div class="site-navigation__inner">
		<?php do_action( 'ositer/sidebar/before-navigation' ); ?>

		<?php if ( ! empty( $nav_items ) ) { ?>
		<ul class="menu">
			<?php foreach ( $nav_items as $item ) {
				$classes = array_filter( [
					'menu-item', "slug-{$item->slug}",
					! empty( $item->children ) ? 'has-children' : '',
					(bool) $item->active ? 'active' : '',
				] );

				$children_html = ! empty( $item->children ) ? sprintf( '<ul class="children">%1$s</ul>', implode( '', array_map( function( $child ) {
					return sprintf(
						'<li class="slug-%1$s%2$s%3$s">%4$s</li>',
						$child->slug,
						$child->active ? ' active' : '',
						isset( $child->done ) && $child->done ? ' done' : '',
						sprintf(
							'<a href="%1$s">%2$s</a>',
							$child->url,
							$child->label
						)
					);
				}, $item->children ) ) ) : '';
				
				printf(
					'<li class="%1$s">%2$s%3$s</li>',
					implode( ' ', $classes ),
					sprintf(
						'<a href="%1$s"><span>%2$s</span>%3$s</a>',
						$item->url,
						$item->label,
						isset( $item->icon ) ? ositer()->icon( $item->icon, false ) : '',
					),
					$children_html
				);
			} ?>
		</ul>
		<?php } ?>

		<?php do_action( 'ositer/sidebar/after-navigation' ); ?>
	</div>
</nav>

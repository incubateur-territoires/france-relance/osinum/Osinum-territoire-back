<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */

?>

<div class="permalink-widget">
	<button class="icon-only" data-popup="permalink">
		<?php ositer()->icon( 'info' ); ?>
		<?php ositer()->icon( 'info-plein' ); ?>
	</button>
	<p><?php _e( 'Lien du diagnostic à partager :', 'osinum-territoire' ); ?></p>
	<button class="button" data-copy-clipboard="<?php echo $diagnostic->get_private_permalink(); ?>">
		<?php _e( 'Copier', 'ositer' ); ?> <?php ositer()->icon( 'lien' ); ?>
	</button>
</div>

<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $situations */
/** @var array $topics */
/** @var array $selected_situations_ids */
/** @var array $suggested_practices */
/** @var array $suggested_tools */
/** @var array $suggested_resources */
/** @var boolean $print */
$max_suggestions = 15;
?>

<?php if ( ! $diagnostic->has_done_step( 'situations' ) ) { ?>
	<section class="widget-panel resultats situations">
		<h1>
			<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
			<?php _e( 'Vos situations - <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>

		<div class="missing-data">
			<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>

			<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'situations' ) ); ?>">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
			</a>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats situations">
	<h1>
		<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
		<?php _e( 'Vos situations - <span class="fwn">Résultats</span>', 'ositer' ); ?>
	</h1>

	<h4><?php _e( 'État des lieux', 'ositer' ); ?></h4>

	<div class="box">
		<h3 class="m0 mb50"><?php _e( 'Tendances de vos situations numériques', 'ositer' ); ?></h3>
		<p><?php _e( 'Les situations numériques que vous avez sélectionnées semblent concernées par les thématiques suivantes&nbsp;:', 'ositer' ); ?></p>

		<table class="mt150 row-padding vat">
			<thead>
				<tr>
					<th class="w20"><?php _e( 'Thématiques', 'ositer' ); ?></th>
					<th class="w40"><?php _e( 'Description de la thématique', 'ositer' ); ?></th>
					<th class="w40"><?php _e( 'Situations vécues', 'ositer' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $topics as $topic ) {
					$situations_in_topic = array_filter( $situations, function ( $situation ) use ( $topic ) {
						return in_array( $topic->id, $situation->topics, true );
					} );
					$situations_ids_in_topic          = array_map( fn ( $situation ) => $situation->id, $situations_in_topic );
					$selected_situations_ids_in_topic = array_intersect( $selected_situations_ids, $situations_ids_in_topic );
					$progress_bar_width = count( $situations_ids_in_topic ) > 0 ? count( $selected_situations_ids_in_topic ) * 100 / count( $situations_ids_in_topic ) : 0;
					?>
					<tr>
						<td class="w20">
							<div class="title with-icon">
								<?php ositer()->icon( $topic->icon ); ?>
								<span><?php echo $topic->title; ?></span>
							</div>
						</td>
						<td class="w40">
							<div class="description"><?php echo ! empty( $topic->description ) ? $topic->description : '-'; ?></div>
						</td>
						<td class="w40">
							<div class="progress-bar-container">
								<div class="progress-bar" style="width:<?php echo (int) $progress_bar_width; ?>%;"></div>
								<span class="count"><?php printf( '%1$d/%2$d', count( $selected_situations_ids_in_topic ), count( $situations_ids_in_topic ) ); ?></span>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</section>

<section class="widget-panel resultats situations page-break-before">
	<h1>
		<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
		<?php _e( 'Vos situations - <span class="fwn">Suggestions</span>', 'ositer' ); ?>
	</h1>

	<table class="grid-3 vat">
		<tr>
			<td class="col col1">
				<div class="box">
					<h3 class="m0 mb50"><?php _e( 'Les pratiques suggérées', 'ositer' ); ?></h3>
					<p><?php _e( 'Votre sélection de Situations nous permet de vous suggérer d\'outiller les pratiques suivantes&nbsp;:', 'ositer' ); ?></p>
					<ol class="mt200i">
						<?php foreach ( array_slice( $suggested_practices, 0, $max_suggestions ) as $practice ) {
							printf( '<li>%1$s</li>', $practice->title );
						} ?>
					</ol>
				</div>
			</td>
			<td class="col col2">
				<div class="box">
					<h3 class="m0 mb50"><?php _e( 'Les outils numériques suggérés', 'ositer' ); ?></h3>
					<p><?php _e( 'Votre sélection de Situations nous permet de vous suggérer de vous intéresser à des outils numériques comme&nbsp;:', 'ositer' ); ?></p>
					<ol class="mt200i">
						<?php foreach ( array_slice( $suggested_tools, 0, $max_suggestions ) as $tool ) {
							printf( '<li><a href="%2$s">%1$s</a></li>', $tool->title, $tool->permalink );
						} ?>
					</ol>
				</div>
			</td>
			<td class="col col3">
				<div class="box">
					<h3 class="m0 mb50"><?php _e( 'Ressources suggérées', 'ositer' ); ?></h3>
					<p><?php _e( 'Votre sélection de Situations nous permet de vous suggérer de vous interesser à des ressources comme&nbsp;:', 'ositer' ); ?></p>
					<ol class="mt200i">
						<?php foreach ( array_slice( $suggested_resources, 0, $max_suggestions ) as $resource ) {
							printf( '<li><a href="%2$s">%1$s</a></li>', $resource->title, $resource->permalink );
						} ?>
					</ol>
				</div>
			</td>
		</tr>
	</table>
</section>

<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $practices */
/** @var array $tools */
/** @var array $selected_tools */
/** @var array $criterias */

/**
 * Note : si le tableau de critères utilise des critères en -1/0/+1, réhabiliter cette logique
 * qui a été supprimée par le commit b8ab3e4 du 4 août 2022 à 10:34 (maintenant les critères existants sont +1 uniquement).
 */

?>

<?php if ( ! $diagnostic->has_done_step( 'outils' ) ) { ?>
	<section class="widget-panel resultats">
		<h1>
			<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
			<?php _e( 'Vos outils - <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>

		<div class="missing-data">
			<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>

			<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'outils' ) ); ?>">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
			</a>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats outils">
	<h1>
		<div class="icon"><?php ositer()->icon( 'ecran' ); ?></div>
		<?php _e( 'Vos outils - <span class="fwn">Résultats</span>', 'ositer' ); ?>
	</h1>

	<h4><?php _e( 'État des lieux', 'ositer' ); ?></h4>

	<div class="box">
		<p class="m0"><?php _e( 'Vous utilisez les outils (logiciels, applications, services) et pratiques numériques suivants&nbsp;:', 'ositer' ); ?></p>

		<table class="mt150 row-padding header-border-bottom vat">
			<thead>
				<tr>
					<th class="w25 bb"><?php _e( 'Outils de votre structure', 'ositer' ); ?></th>
					<th class="w75 bb"><?php _e( 'Pratiques recensées par outil', 'ositer' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $selected_tools as $t => $selected_tool ) {
					$progress_bar_width = count( $practices ) > 0 ? count( $selected_tool->configuration->practices ) * 100 / count( $practices ) : 0;
					?>
					<tr>
						<td class="w25">
							<div class="title">
								<span><strong><?php echo $selected_tool->title; ?></strong></span>
							</div>
						</td>
						<td class="w75">
							<table>
								<tr>
									<td class="w35">
										<div class="progress-bar-container">
											<div class="progress-bar" style="width: <?php echo (int) $progress_bar_width; ?>%"></div>
											<span class="count"><?php printf( '%1$d/%2$d', count( $selected_tool->configuration->practices ), count( $practices ) ); ?></span>
										</div>
									</td>
									<td class="w65">
										<ol class="no-counter">
											<?php foreach ( $selected_tool->configuration->practices as $practice_id ) {
												if ( $practice = Helpers::array_find( fn( $practice ) => $practice->id === $practice_id, $practices ) ) { ?>
												<li><?php ositer()->icon( 'fleche-droite' ); ?> <?php echo $practice->title; ?></li>
												<?php } ?>
											<?php } ?>
										</ol>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</section>

<section class="widget-panel resultats outils page-break-before">
	<h4><?php _e( 'Comparaison des outils', 'ositer' ); ?></h4>

	<div class="box">
		<?php include_once _ositer()->get_frontend()->template->get_template_file( 'popups/resultats-outil-tableau' ); ?>
	</div>
</section>

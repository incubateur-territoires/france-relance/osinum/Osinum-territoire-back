<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $criterias */
/** @var array $tools */

?>

<?php if ( ! $diagnostic->has_done_step( 'criteres' ) ) { ?>
	<section class="widget-panel resultats">
		<h1>
			<div class="icon"><?php ositer()->icon( 'support' ); ?></div>
			<?php _e( 'Vos critères - <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>

		<div class="missing-data">
			<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>

			<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'criteres' ) ); ?>">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
			</a>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats">
	<h1>
		<div class="icon"><?php ositer()->icon( 'etoile' ); ?></div>
		<?php _e( 'Vos critères - <span class="fwn">Résultats</span>', 'ositer' ); ?>
	</h1>

	<h4><?php _e( 'État des lieux', 'ositer' ); ?></h4>

	<div class="box">
		<h3 class="m0 mb50"><?php _e( 'Évaluation de vos critères de choix d\'outils', 'ositer' ); ?></h3>
		<p><?php _e( 'Vous avez évalué l\'importance des critères suivants, pour choisir et utiliser des outils numériques, entre 1 et 5 :', 'ositer' ); ?></p>

		<table class="mt150 row-padding vat">
			<thead>
				<tr>
					<th class="w20"><?php _e( 'Critères', 'ositer' ); ?></th>
					<th class="w35"><?php _e( 'Description du critère', 'ositer' ); ?></th>
					<th class="w20"><?php _e( 'Importance du critère', 'ositer' ); ?></th>
					<th class="w25"><?php _e( 'Outils respectant le critère', 'ositer' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $criterias as $c => $criteria ) {
					$progress_bar_width = count( $tools ) > 0 ? count( $criteria->tools ) * 100 / count( $tools ) : 0;
					?>
					<tr>
						<td class="w20">
							<div class="title">
								<span><strong><?php echo $criteria->title; ?></strong></span>
							</div>
						</td>
						<td class="w35">
							<div class="description"><?php echo ! empty( $criteria->description ) ? $criteria->description : '-'; ?></div>
						</td>
						<td class="w20">
							<?php if ( ! is_null( $criteria->rating ) ) {
								echo '<ul class="rating-stars">';

								for ( $i = 0; $i < 4; $i++ ) {
									$full = $i < $criteria->rating;
									$icon = $full ? 'etoile-pleine' : 'etoile2';

									printf(
										'<li class="star star-%2$d">%1$s</li>',
										ositer()->icon( $icon, false ),
										$full ? 'pleine' : 'vide'
									);
								}

								echo '</ul>';
							} else {
								printf( '<p class="no-value">%1$s</p>', __( 'Non-évalué', 'ositer' ) );
							} ?>
						</td>
						<td class="w25">
							<?php if ( ! $diagnostic->has_done_step( 'outils' ) ) { ?>
								<?php if ( $c === 0 ) { ?>
								<p class="no-value">
									<?php printf(
										__( 'Pour obtenir des résultats, <a href="%1$s">réalisez l\'exercice</a>.', 'ositer' ),
										esc_url( $diagnostic->get_private_permalink( 'outils' ) )
									); ?>
								</p>
								<?php } ?>
							<?php } else { ?>
							<div class="progress-bar-container">
								<div class="progress-bar" style="width:<?php echo (int) $progress_bar_width; ?>%;"></div>
								<span class="count"><?php printf( '%1$d/%2$d', count( $criteria->tools ), count( $tools ) ); ?></span>
							</div>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
</section>

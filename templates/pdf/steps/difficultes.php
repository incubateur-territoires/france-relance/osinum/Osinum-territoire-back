<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var array $difficulties */
/** @var array $groups */
/** @var array $selected_difficulties_ids */

?>

<?php if ( ! $diagnostic->has_done_step( 'difficultes' ) ) { ?>
	<section class="widget-panel resultats">
		<h1>
			<div class="icon"><?php ositer()->icon( 'chantier' ); ?></div>
			<?php _e( 'Vos difficultés - <span class="fwn">Résultats</span>', 'ositer' ); ?>
		</h1>

		<div class="missing-data">
			<p><?php _e( 'Pour obtenir des résultats, réalisez l\'exercice.', 'ositer' ); ?></p>

			<a class="mt50 button" href="<?php echo esc_url( $diagnostic->get_private_permalink( 'difficultes' ) ); ?>">
				<?php _e( 'Faire l\'exercice', 'ositer' ); ?>
			</a>
		</div>
	</section>
	<?php
	return;
} ?>

<section class="widget-panel resultats">
	<h1>
		<div class="icon"><?php ositer()->icon( 'chantier' ); ?></div>
		<?php _e( 'Vos difficultés - <span class="fwn">Résultats</span>', 'ositer' ); ?>
	</h1>

	<h4><?php _e( 'État des lieux', 'ositer' ); ?></h4>

	<div class="box">
		<h3 class="m0 mb50"><?php _e( 'Tendances de vos difficultés', 'ositer' ); ?></h3>
		<p><?php _e( 'Les difficultés que vous avez sélectionnées, pour lesquelles vous avez indiqué être concerné, semblent concernées par les thématiques suivantes :', 'ositer' ); ?></p>

		<table class="mt150 row-padding vat">
			<thead>
				<tr>
					<th class="w20"><?php _e( 'Thématiques', 'ositer' ); ?></th>
					<th class="w40"><?php _e( 'Description de la thématique', 'ositer' ); ?></th>
					<th class="w40"><?php _e( 'Difficultés vécues', 'ositer' ); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $groups as $group ) {
					$difficulties_in_group = array_filter( $difficulties, function ( $difficulty ) use ( $group ) {
						return in_array( $group->id, $difficulty->groups, true );
					} );
					$difficulties_ids_in_group          = array_map( fn ( $difficulty ) => $difficulty->id, $difficulties_in_group );
					$selected_difficulties_ids_in_group = array_intersect( $selected_difficulties_ids, $difficulties_ids_in_group );
					$progress_bar_width = count( $difficulties_ids_in_group ) > 0 ? count( $selected_difficulties_ids_in_group ) * 100 / count( $difficulties_ids_in_group ) : 0;
					?>
					<tr>
						<td class="w20">
							<div class="title with-icon">
								<?php ositer()->icon( $group->icon ); ?> <span><?php echo $group->title; ?></span>
							</div>
						</td>
						<td class="w40">
							<div class="description"><?php echo ! empty( $group->description ) ? $group->description : '-'; ?></div>
						</td>
						<td class="w40">
							<div class="progress-bar-container">
								<div class="progress-bar" style="width:<?php echo (int) $progress_bar_width; ?>%;"></div>
								<span class="count"><?php printf( '%1$d/%2$d', count( $selected_difficulties_ids_in_group ), count( $difficulties_ids_in_group ) ); ?></span>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</section>

<section class="widget-panel resultats situations page-break-before">
	<h1>
		<div class="icon"><?php ositer()->icon( 'chantier' ); ?></div>
		<?php _e( 'Vos difficultés - <span class="fwn">Suggestions</span>', 'ositer' ); ?>
	</h1>

	<div class="box">
		<h3 class="m0"><?php _e( 'Les ressources suggérées', 'ositer' ); ?></h3>
		<p><?php _e( 'Votre sélection de Difficultés nous permet de vous suggérer de vous interesser à des ressources comme&nbsp;:', 'ositer' ); ?></p>

		<ol class="mt200i">
			<?php foreach ( $suggested_resources as $resource ) {
				printf( '<li><a href="%2$s">%1$s</a></li>', $resource->title, $resource->permalink );
			} ?>
		</ol>
	</div>
</section>

<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var OsinumTerritoire\Frontend\PdfRenderer $this */

?>

<?php include_once _ositer()->get_frontend()->template->get_template_file( 'pdf/header' ); ?>

<div class="page-break-before">
	<?php $this->output_step( 'situations' ); ?>
</div>

<div class="page-break-before">
	<?php $this->output_step( 'criteres' ); ?>
</div>

<div class="page-break-before">
	<?php $this->output_step( 'outils' ); ?>
</div>

<div class="page-break-before">
	<?php $this->output_step( 'difficultes' ); ?>
</div>

<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var OsinumTerritoire\Frontend\PdfRenderer $this */

?>

<table class="header-section first">
	<tr>
		<td class="left">
			<h1><?php _e( 'Résultats - Diagnostic OSINUM Territoires', 'ositer' ); ?></h1>
		</td>
		<td class="right">
			<img src="<?php echo _ositer()->get_plugin_url() . '/dist/images/logo-black.png'; ?>" class="logo" />
		</td>
	</tr>
</table>

<table class="header-section second">
	<tr>
		<td class="left">
			<div class="box">
				<h3 class="m0"><?php printf( '%1$s - %2$s', $diagnostic->get_title(), $diagnostic->get_creation_date( 'd F Y' ) ); ?></h3>
				<p class="m0 mt50">
					<?php printf( __( 'Enregistré à l\'adresse %1$s', 'ositer' ), esc_html( $diagnostic->get_email() ) ); ?><br />
					<?php printf( __( 'Dernière modification le %1$s', 'ositer' ), $diagnostic->get_last_update( 'd F Y' ) ); ?>
				</p>
			</div>
			<div class="box">
				<h3 class="m0"><?php _e( 'À propos de la synthèse', 'ositer' ); ?></h3>
				<p class="m0 mt50"><?php _e( 'Les résultats sont issus des sélections et renseignements que vous avez donnez dans les exercices précédents. Les suggestions sont faites en fonction de ces informations. Vous pouvez revenir sur chaque exercice pour ajuster vos informations.', 'ositer' ); ?></p>
			</div>
		</td>
		<td class="right">
			<div class="box">
				<figure class="qr-code"><img src="<?php echo $diagnostic->get_qr_code_url(); ?>" /></figure>
				<p class="m0"><?php _e( 'Lien à flasher', 'ositer' ); ?></p>
				<a href="<?php echo $diagnostic->get_private_permalink(); ?>"><?php echo $diagnostic->get_private_permalink(); ?></a>
			</div>
		</td>
	</tr>
</table>

<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
/** @var OsinumTerritoire\Frontend\PdfRenderer $this */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Diagnostic OSINUM Territoires — <?php echo $diagnostic->get_title(); ?></title>
		<?php $this->output_pdf_assets(); ?>
	</head>
	<body class="single-diagnostic diagnostic-print-mode">
		<main id="body">
			<?php include_once _ositer()->get_frontend()->template->get_template_file( 'pdf/content' ); ?>
		</main>
	</body>
</html>

<?php

defined( 'ABSPATH' ) || die();

?>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<!-- END MAIN CONTENT AREA -->
					</table>
					<!-- END CENTERED WHITE CONTAINER -->

					<!-- START FOOTER -->
					<div class="footer">
						<table role="presentation" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="content-block">
									<span class="apple-link"><a href="<?php echo home_url(); ?>">OSINUM Territoires</a></span>
									<br> <a href="https://www.osinumterritoires.fr/contact/">Cliquez ici pour nous contacter</a>.
								</td>
							</tr>
						</table>
					</div>
					<!-- END FOOTER -->

				</div>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</body>

</html>
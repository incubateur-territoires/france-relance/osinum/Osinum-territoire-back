<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Email $this */

?>

<p>Voici un récapitulatif des diagnostics OSINUM Territoires que vous avez créés.</p>

<table class="td" cellspacing="0" cellpadding="4" style="width: 100%; border-color: #EAEAEA;" border="1">
	<thead>
		<tr>
			<th class="td field-cell" scope="col" style="text-align:left;background-color: #EAEAEA; width: 65%;">Titre et permalien</th>
			<th class="td value-cell" scope="col" style="text-align:left;background-color: #EAEAEA; width: 35%;">Date</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $this->get( 'diagnostics' ) as $diagnostic ) {
		/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
		?>
		<tr>
			<td class="td field-cell" style="text-align:left; vertical-align:top; width: 65%;" class="label">
				<a href="<?php echo esc_url( $diagnostic->get_private_permalink() ); ?>">
					<strong><?php echo esc_html( $diagnostic->get_title() ); ?></strong>
				</a><br>
				<a href="<?php echo esc_url( $diagnostic->get_private_permalink() ); ?>">
					<code><?php echo esc_html( $diagnostic->get_private_permalink() ); ?></code>
				</a>
			</td>
			<td class="td value-cell" style="text-align:left; vertical-align:top; width: 35%;" class="value">
				<?php echo $diagnostic->get_creation_date( 'd F Y' ) ?>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

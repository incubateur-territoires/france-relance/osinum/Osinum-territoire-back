<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Email $this */

?>

<p>Une demande de type <em><?php echo $this->get( 'type' ); ?></em> vient d'être déposée sur le site par l'utilisateur <code><?php echo $this->get( 'email' ); ?></code>.</p>

<?php $this->button( sprintf( __( 'Contacter %1$s', 'ositer' ), $this->get( 'email' ) ), sprintf( 'mailto:%1$s', $this->get( 'email' ) ) ); ?>

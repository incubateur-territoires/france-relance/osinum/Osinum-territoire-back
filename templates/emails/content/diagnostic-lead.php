<?php

use OsinumTerritoire\Helpers;

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Email $this */
/** @var OsinumTerritoire\Models\Diagnostic $diagnostic */
$diagnostic = $this->get( 'diagnostic' );
$values     = $diagnostic->get_lead_form_values();
?>

<?php if ( $this->get( 'first_submission' ) ) { ?>
	<p>Le formulaire de fin du <a href="<?php echo $diagnostic->get_private_permalink(); ?>">diagnostic #<?php echo $diagnostic->get_id(); ?></a> vient d'être envoyé.</p>
<?php } else { ?>
	<p>Le formulaire de fin du <a href="<?php echo $diagnostic->get_private_permalink(); ?>">diagnostic #<?php echo $diagnostic->get_id(); ?></a> a été mis à jour.</p>
<?php } ?>

<table class="td" cellspacing="0" cellpadding="4" style="width: 100%; border-color: #EAEAEA;" border="1">
	<thead>
		<tr>
			<th class="td field-cell" scope="col" style="text-align:left;background-color: #EAEAEA; width: 65%;">Champ</th>
			<th class="td value-cell" scope="col" style="text-align:left;background-color: #EAEAEA; width: 35%;">Valeur</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $values as $label => $value ) {
		?>
		<tr>
			<td class="td field-cell" style="text-align:left; vertical-align:top; width: 65%;" class="label">
				<?php echo Helpers::readify( $label ); ?>
			</td>
			<td class="td value-cell" style="text-align:left; vertical-align:top; width: 35%;" class="value">
				<?php echo Helpers::readify( $value, $label ); ?>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<br />

<?php $this->button( __( 'Voir le diagnostic', 'ositer' ), $diagnostic->get_private_permalink() ); ?>

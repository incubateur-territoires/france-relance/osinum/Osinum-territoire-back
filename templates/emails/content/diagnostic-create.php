<?php

defined( 'ABSPATH' ) || die();

/** @var OsinumTerritoire\Email $this */

?>

<p>Vous venez de créer le diagnostic "<?php echo $this->get( 'diagnostic' )->get_title(); ?>" sur le site OSINUM Territoires.</p>

<?php $this->button( 'Accéder au diagnostic', $this->get( 'diagnostic' )->get_private_permalink() ); ?>

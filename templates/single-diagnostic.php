<?php

defined( 'ABSPATH' ) || die();

get_header();
?>

<main id="diagnostic-body">
	<?php do_action( 'ositer/frontend/single-diagnostic/header' ); ?>
	<?php do_action( 'ositer/frontend/single-diagnostic/sidebar' ); ?>
	<?php do_action( 'ositer/frontend/single-diagnostic/body' ); ?>
</main>

<?php

get_footer();

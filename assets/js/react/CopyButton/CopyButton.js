import React, { useState, useEffect } from "react";
import copy from "copy-to-clipboard";

const CopyButton = ({ text, label = "Copier", className = "", style = {} }) => {
  const [indicatorIsDisplayed, displayIndicator] = useState(false);

  const onClick = (e) => {
    copy(text);
    displayIndicator(true);

    setTimeout(() => {
      displayIndicator(false);
    }, 2500);
  };

  return (
    <button onClick={onClick} className={className} style={style}>
      {!indicatorIsDisplayed ? (
        <>
          <span className="label">{label}</span>
          <svg aria-hidden="true" className="icon lien">
            <use xlinkHref="#lien"></use>
          </svg>
        </>
      ) : (
        <>
          <span className="indicator">Copié</span>
          <svg aria-hidden="true" className="icon check">
            <use xlinkHref="#check"></use>
          </svg>
        </>
      )}
    </button>
  );
};

export default CopyButton;

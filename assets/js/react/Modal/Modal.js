import React from "react";
import { default as ReactModal } from "react-modal";

const Modal = ({
  isVisible,
  title = null,
  onClose,
  children,
  id = "modal-id",
  className = "",
}) => {
  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  if (!isVisible) {
    return null;
  }

  return (
    <ReactModal
      isOpen={isVisible}
      contentLabel={title}
      onRequestClose={onClose}
      closeTimeoutMS={500}
      className={`modal ${className}`}
      overlayClassName="overlay"
      id={id}
    >
      <button className="close-popup" onClick={onClose}>
        <svg aria-hidden="true" className="icon fermer">
          <use xlinkHref="#fermer"></use>
        </svg>
      </button>
      {title && <h3 className="modal-title">{title}</h3>}
      {children}
    </ReactModal>
  );
};

export default Modal;

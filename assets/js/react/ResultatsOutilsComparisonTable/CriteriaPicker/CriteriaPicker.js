import React, { useState } from "react";
import Modal from "../../Modal/Modal";

const CriteriaPicker = ({
  index = 0,
  criterias = [],
  currentCriterias = null,
  currentOrder = null,
  mobile = false,
  onSelect = () => {},
  onOrderChange = () => {},
}) => {
  const [popupVisible, setPopupVisible] = useState(false);

  /**
   * When a criteria is selected, close the popup and call the onSelect callback.
   */
  const onItemSelect = (item) => {
    onSelect(index, item);
    setPopupVisible(false);
  };

  /**
   * Helper function to get the selected button name.
   */
  const getButtonText = () => {
    const criteria = currentCriterias[index];

    if (criteria.type === "term") {
      return criterias.find((c) => c.id === criteria.id).title;
    } else if (criteria.type === "usage" && criteria.usage === "satisfaction") {
      return "Satisfaction d'utilisation";
    } else if (criteria.type === "usage" && criteria.usage === "frequency") {
      return "Fréquence d'utilisation";
    }
  };

  const currentOrderSlug = `${currentOrder.column}${currentOrder.order}`;
  const otherIndex = index === 0 ? 1 : 0;
  const activeCriteriasSlug = [
    Object.values(currentCriterias[index]).join("."),
    Object.values(currentCriterias[otherIndex]).join("."),
  ];

  return (
    <>
      <button
        className="dropdown"
        onClick={() => setPopupVisible(!popupVisible)}
      >
        <span>{getButtonText()}</span>
        <svg aria-hidden="true" className="icon caret-bas">
          <use xlinkHref="#caret-bas"></use>
        </svg>
      </button>
      <Modal
        isVisible={popupVisible}
        onClose={() => setPopupVisible(false)}
        id="criterias-selection-popup"
      >
        {!mobile && <h3 className="mb100">Colonne {index + 1}</h3>}
        <p className="mb200">
          Sélectionnez un critère pour classer les outils dans la colonne
          {!mobile ? ` ${index + 1}` : ""}.
        </p>
        <div className="criterias-groups">
          <div className="criterias-group">
            <h6>Critères d'utilisation</h6>
            <ol>
              <li>
                <button
                  onClick={() =>
                    onItemSelect({ type: "usage", usage: "satisfaction" })
                  }
                  disabled={
                    !mobile &&
                    activeCriteriasSlug.includes("usage.satisfaction")
                  }
                >
                  Satisfaction d'utilisation
                </button>
              </li>
              <li>
                <button
                  onClick={() =>
                    onItemSelect({ type: "usage", usage: "frequency" })
                  }
                  disabled={
                    !mobile && activeCriteriasSlug.includes("usage.frequency")
                  }
                >
                  Fréquence d'utilisation
                </button>
              </li>
            </ol>
          </div>
          <div className="criterias-group">
            <h6>Critères d'outils</h6>
            <ol>
              {criterias.map((criteria) => (
                <li key={`criteriaPicker${criteria.id}`}>
                  <button
                    onClick={() =>
                      onItemSelect({ type: "term", id: criteria.id })
                    }
                    disabled={
                      !mobile &&
                      activeCriteriasSlug.includes(`term.${criteria.id}`)
                    }
                  >
                    {criteria.title}
                  </button>
                </li>
              ))}
            </ol>
          </div>
          <div className="criterias-group">
            <span
              className="fake-toggle positioned-before"
              onClick={() => {
                onOrderChange({ column: index, order: "desc" });
                setPopupVisible(false);
              }}
            >
              <input
                type="checkbox"
                checked={!mobile && currentOrderSlug === `${index}desc`}
              />
              <span>
                <small>Trier le tableau selon cette colonne.</small>
              </span>
            </span>
            {/* <h6>Tri</h6>
            <ol>
              <li>
                <button
                  onClick={() => {
                    onOrderChange({ column: index, order: "asc" });
                    setPopupVisible(false);
                  }}
                  disabled={!mobile && currentOrderSlug === `${index}asc`}
                >
                  Trier par ordre croissant
                </button>
              </li>
              <li>
                <button
                  onClick={() => {
                    onOrderChange({ column: index, order: "desc" });
                    setPopupVisible(false);
                  }}
                  disabled={!mobile && currentOrderSlug === `${index}desc`}
                >
                  Trier par ordre décroissant
                </button>
              </li>
            </ol> */}
          </div>
        </div>
      </Modal>
    </>
  );
};

export default CriteriaPicker;

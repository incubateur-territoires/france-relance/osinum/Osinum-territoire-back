import React from "react";
import { getUsageCellData } from "../../../utils/data";

const ToolRow = ({ tool, index, criterias }) => {
  /**
   * Render a single cell.
   */
  const renderCell = (criteria) => {
    if (criteria.type === "term") {
      if (tool.criterias.includes(criteria.id)) {
        return <div className="cell type-term yes">Validé</div>;
      }

      return <div className="cell type-term no">Non-validé</div>;
    } else if (criteria.type === "usage") {
      const slug = `${criteria.usage}:${
        tool.configuration.usages[criteria.usage]
      }`;
      const cellData = getUsageCellData(slug);

      return (
        <div className={`cell type-usage ${slug.replace(":", "__")}`}>
          <svg aria-hidden="true" className={`icon ${cellData.icon}`}>
            <use xlinkHref={`#${cellData.icon}`}></use>
          </svg>
          <span>{cellData.label}</span>
        </div>
      );
    }
  };

  return (
    <article className="row">
      <div className="col col1">
        <p>
          <span className="index">{index + 1}</span>
          <span className="text">
            {tool.title}
            {tool.type === "custom" && "*"}
          </span>
        </p>
      </div>
      <div className="col col2">{renderCell(criterias[0])}</div>
      <div className="col col3">{renderCell(criterias[1])}</div>
    </article>
  );
};

export default ToolRow;

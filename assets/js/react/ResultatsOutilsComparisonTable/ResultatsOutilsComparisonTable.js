import React, { useState, useEffect, useMemo } from "react";
import { getInitialDiagnosticSelectedTools } from "../../utils/defaults";
import CriteriaPicker from "./CriteriaPicker/CriteriaPicker";
import ToolRow from "./TableItemRow/ToolRow";

const defaultCriterias = [
  {
    type: "usage",
    usage: "satisfaction",
  },
  {
    type: "usage",
    usage: "frequency",
  },
];

const defaultOrder = {
  column: 0,
  order: "desc",
};

const ResultatsOutilsComparisonTable = () => {
  const criterias = useMemo(() => window.OsinumTerritoire.criterias || [], []);
  const selectedTools = useMemo(() => getInitialDiagnosticSelectedTools(), []);

  const [tools, setTools] = useState([]);
  const [selectedCriterias, setSelectedCriterias] = useState(defaultCriterias);
  const [order, setOrder] = useState(defaultOrder);

  /**
   * Function that re-order the tools after the criterias or sort order have changed.
   **/
  useEffect(() => {
    const orderType = selectedCriterias[order.column].type;
    const newTools = [...selectedTools];

    if (orderType === "usage") {
      // Sort tools by their configuration.usage[type] value.
      const usage = selectedCriterias[order.column].usage;

      newTools.sort((a, b) => {
        const aUsage = a.configuration.usages[usage];
        const bUsage = b.configuration.usages[usage];

        if (aUsage > bUsage) {
          return order.order === "asc" ? 1 : -1;
        } else if (aUsage < bUsage) {
          return order.order === "asc" ? -1 : 1;
        } else {
          return 0;
        }
      });
    } else if (orderType === "term") {
      // If order is a term, sort tools with those where the term is included in their criterias property.
      const termId = selectedCriterias[order.column].id;

      newTools.sort((a, b) => {
        const aHasTerm = a.criterias.includes(termId);
        const bHasTerm = b.criterias.includes(termId);

        if (aHasTerm && !bHasTerm) {
          return order.order === "asc" ? 1 : -1;
        } else if (!aHasTerm && bHasTerm) {
          return order.order === "asc" ? -1 : 1;
        } else {
          return 0;
        }
      });
    }

    setTools(newTools);
  }, [, selectedCriterias, order]);

  /**
   * Change a specific criteria.
   */
  const changeCriteria = (index, criteria) => {
    const newSelectedCriterias = [...selectedCriterias];
    newSelectedCriterias[index] = criteria;
    setSelectedCriterias(newSelectedCriterias);
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================

  return (
    <>
      <nav className="mobile-picker mb200">
        <h5>Critère à comparer :</h5>
        <CriteriaPicker
          index={0}
          criterias={criterias}
          currentCriterias={selectedCriterias}
          currentOrder={order}
          onSelect={changeCriteria}
          onOrderChange={setOrder}
          mobile={true}
        />
      </nav>
      <header className="table-preheader mobile-only">
        <div className="row">
          <div className="col col1">
            <h6>Outils utilisés :</h6>
          </div>
          <div className="col col2">
            <h6>Évaluation :</h6>
          </div>
        </div>
      </header>
      <header className="table-preheader mobile-hidden">
        <div className="row">
          <div className="col col1">
            <h6>N° Outils utilisés :</h6>
          </div>
          <div className="col col2">
            <h6>Choisir un critère 1 :</h6>
          </div>
          <div className="col col3">
            <h6>Choisir un critère 2 :</h6>
          </div>
        </div>
      </header>
      <header className="table-header">
        <div className="row">
          <div className="col col1"></div>
          <div className="col col2">
            <CriteriaPicker
              index={0}
              criterias={criterias}
              currentCriterias={selectedCriterias}
              currentOrder={order}
              onSelect={changeCriteria}
              onOrderChange={setOrder}
            />
          </div>
          <div className="col col3">
            <CriteriaPicker
              index={1}
              criterias={criterias}
              currentCriterias={selectedCriterias}
              currentOrder={order}
              onSelect={changeCriteria}
              onOrderChange={setOrder}
            />
          </div>
        </div>
      </header>
      <div className="table-body">
        {tools.map((tool, index) => (
          <ToolRow
            key={`toolRow${tool.id}`}
            index={index}
            tool={tool}
            criterias={selectedCriterias}
          />
        ))}
      </div>
      {selectedTools.filter((tool) => tool.type === "custom").length > 0 && (
        <footer className="table-footer">
          <div className="row">
            <div className="col col1">
              <p>
                * Outils non-répertoriés par Osinum, leurs informations
                dépendent de celles que vous avez renseignées.
              </p>
            </div>
          </div>
        </footer>
      )}
    </>
  );
};

export default ResultatsOutilsComparisonTable;

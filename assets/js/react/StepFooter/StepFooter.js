import React from "react";
import { getDiagnosticSteps } from "../../utils/data";

const StepFooter = ({ currentStep }) => {
  const steps = getDiagnosticSteps();

  return (
    <footer className="mt350">
      <h2>Pour poursuivre</h2>
      <h3>Choisir un exercice</h3>
      <div className="grid per-3">
        {steps.map((step) => {
          if (step.slug === currentStep) {
            return null;
          }

          return (
            <div
              className="block card bg-vert-1 bg-vert-1-hover curp"
              key={`step${step.slug}`}
              onClick={() => {
                window.location = step.link;
              }}
            >
              <div className="icon">
                <svg aria-hidden className={`icon ${step.icon}`}>
                  <use xlinkHref={`#${step.icon}`}></use>
                </svg>
              </div>
              <h3>{step.title}</h3>
              <div className="card-label inline">
                <p>{step.duration} minutes</p>
              </div>
              <a className="button mt250" title={step.title} href={step.link}>
                {step.completed ? "Reprendre" : "C'est parti"}
                <svg aria-hidden="true" className="icon fleche-droite">
                  <use xlinkHref="#fleche-droite"></use>
                </svg>
              </a>
            </div>
          );
        })}
      </div>
      <h3 className="mt150">Voir les résultats</h3>
      <div
        className="block bg-vert-1  bg-vert-1-hover curp"
        onClick={() => {
          window.location =
            window.OsinumTerritoire.diagnostic.permalinks.results;
        }}
      >
        <div className="flex-row flex-valign-center">
          <h3 className="m0">Résultats</h3>
          <a
            className="button theme-vert-1-outline mla"
            href={window.OsinumTerritoire.diagnostic.permalinks.results}
          >
            Aller voir
            <svg aria-hidden="true" className="icon fleche-droite">
              <use xlinkHref="#fleche-droite"></use>
            </svg>
          </a>
        </div>
      </div>
    </footer>
  );
};

export default StepFooter;

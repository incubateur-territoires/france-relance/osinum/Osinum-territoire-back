import React, { useState, useEffect, useCallback } from "react";
import { createDiagnostic, updateDiagnostic } from "../../utils/ajax";
import CriteriaCard from "./CriteriaCard/CriteriaCard";

import { useIsFirstRender, usePrevious } from "../../utils/hooks";
import { getInitialDiagnosticEvaluatedCriterias } from "../../utils/defaults";
import StepFooter from "../StepFooter/StepFooter";
import StickyOnScroll from "../StickyOnScroll/StickyOnScroll";

const criterias = window.OsinumTerritoire.criterias || [];

const CriteresSelector = () => {
  const isFirstRender = useIsFirstRender();
  const [evaluatedCriterias, setEvaluatedCriterias] = useState(
    getInitialDiagnosticEvaluatedCriterias()
  );

  //======================================================
  //
  //  #####  #####  #####  #####   ####  ######   ####
  //  ##     ##     ##     ##     ##       ##    ##
  //  #####  #####  #####  #####  ##       ##     ###
  //  ##     ##     ##     ##     ##       ##       ##
  //  #####  ##     ##     #####   ####    ##    ####
  //
  //======================================================

  /**
   * Save the list of evaluated Criterias.
   */
  const saveCriterias = useCallback(async () => {
    const result = await updateDiagnostic({
      criterias:
        evaluatedCriterias.length === 0
          ? ""
          : evaluatedCriterias.map((ev) => ({
              id: ev.id,
              rating: ev.rating || 0,
            })),
    });

    if (!result.success) {
      alert(result.message || "Une erreur est survenue, veuillez réessayer.");
    }
  }, [evaluatedCriterias]);

  /**
   * When a criteria is evaluated, save the list of evaluated Criterias.
   */
  useEffect(() => {
    if (isFirstRender) {
      return;
    }

    saveCriterias();
  }, [evaluatedCriterias]);

  //=====================================================
  //
  //  #####  ##   ##  #####  ##     ##  ######   ####
  //  ##     ##   ##  ##     ####   ##    ##    ##
  //  #####  ##   ##  #####  ##  ## ##    ##     ###
  //  ##      ## ##   ##     ##    ###    ##       ##
  //  #####    ###    #####  ##     ##    ##    ####
  //
  //=====================================================

  /**
   * When a Criteria card is evaluated.
   */
  const onCardEvaluate = (criteriaId, rating) => {
    if (evaluatedCriterias.find((ev) => ev.id === criteriaId)) {
      setEvaluatedCriterias(
        evaluatedCriterias.map((ev) => {
          if (ev.id === criteriaId) {
            return {
              ...ev,
              rating: rating,
            };
          }

          return ev;
        })
      );
    } else {
      setEvaluatedCriterias([
        ...evaluatedCriterias,
        {
          id: criteriaId,
          rating: rating,
        },
      ]);
    }
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================

  return (
    <>
      <StickyOnScroll
        element={() => (
          <div className="current-view-header full-width">
            <h3>
              Cartes Critères ({criterias.length}) - Évaluées (
              {evaluatedCriterias.length})
            </h3>
          </div>
        )}
      />

      <div className="criteres grid per-3">
        {criterias.map((criteria) => (
          <CriteriaCard
            key={`criteria${criteria.id}`}
            criteria={criteria}
            isEvaluated={
              !!evaluatedCriterias.find((ev) => ev.id === criteria.id)
            }
            rating={
              evaluatedCriterias.find((ev) => ev.id === criteria.id)?.rating ||
              0
            }
            onEvaluate={onCardEvaluate}
            withContainer={true}
          />
        ))}
      </div>
      <aside className={`widget-sidebar fixed-on-mobile`}>
        <nav className="mobile-step-navigation bottom">
          <a
            className="button mla"
            href={window.OsinumTerritoire.diagnostic.steps.outils.permalink}
          >
            Exercice suivant
            <svg aria-hidden="true" className="icon fleche-droite">
              <use xlinkHref="#fleche-droite"></use>
            </svg>
          </a>
        </nav>
      </aside>
      <StepFooter currentStep="criteres" />
    </>
  );
};

export default CriteresSelector;

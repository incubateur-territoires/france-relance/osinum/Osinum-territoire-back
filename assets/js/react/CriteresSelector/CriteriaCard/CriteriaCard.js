import React, { useState } from "react";
import Modal from "../../Modal/Modal";

const CriteriaCard = ({
  criteria,
  rating,
  onEvaluate,
  isEvaluated,
  withContainer = false,
}) => {
  const [popupVisible, setPopupVisible] = useState(false);

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  const CardContent = () => (
    <article
      className={`mini-card criteria${
        isEvaluated ? " selected" : ""
      } theme-vert-1`}
    >
      <div className="icon theme-vert-1-dark">
        <svg aria-hidden className="icon etoile">
          <use xlinkHref="#etoile"></use>
        </svg>
      </div>
      <div
        className="card-content"
        onClick={() => setPopupVisible(!popupVisible)}
      >
        <p className="pre-title">Carte Critère</p>
        <h3>{criteria.title}</h3>
        <button
          className={
            isEvaluated ? "icon-only theme-vert-1-dark" : "theme-vert-1-outline"
          }
        >
          {rating > 0 ? (
            <>
              J'évalue{" "}
              <span className="stars">
                {Array.from(Array(rating).keys()).map((i) => {
                  return (
                    <span
                      key={`rating${criteria.id}${i}`}
                      className="rating-icon"
                    >
                      <svg aria-hidden className="icon etoile-pleine">
                        <use xlinkHref="#etoile-pleine"></use>
                      </svg>
                    </span>
                  );
                })}
              </span>
            </>
          ) : (
            <>
              J'évalue{" "}
              <svg aria-hidden="true" className="icon fleche-droite">
                <use xlinkHref="#fleche-droite"></use>
              </svg>
            </>
          )}
        </button>
      </div>
      <Modal
        isVisible={popupVisible}
        onClose={() => setPopupVisible(false)}
        className="modal-vert-1-mobile"
      >
        <div className="criteria-popup-container popup-container">
          <div className="summary theme-vert-1-dark">
            <p className="number">
              <svg aria-hidden className="icon etoile">
                <use xlinkHref="#etoile"></use>
              </svg>
              Critère n°{criteria.number}
            </p>
            <h3>{criteria.title}</h3>
            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: criteria.description }}
            />
          </div>
          <div className="body">
            <h4 className="mb50">Importance du critère</h4>
            <p>
              Selon vous, pour choisir un outil numérique, quelle est
              l'importance de ce critère ? Notez-le entre 1 et 4.
            </p>
            <nav className="actions mt200">
              <ul className="ratings">
                <li className="legend min">Pas important</li>
                {[1, 2, 3, 4].map((i) => {
                  const icon = i <= rating ? "etoile-pleine" : "etoile";

                  return (
                    <li
                      className="rating-star"
                      key={`ratingButton${criteria.id}${i}`}
                    >
                      <button
                        className={i <= rating ? "active" : ""}
                        onClick={() => onEvaluate(criteria.id, i)}
                      >
                        <svg aria-hidden className={`icon ${icon}`}>
                          <use xlinkHref={`#${icon}`}></use>
                        </svg>
                        <span className="number">{i}</span>
                      </button>
                    </li>
                  );
                })}
                <li className="legend max">Important</li>
              </ul>
            </nav>

            <footer>
              <button onClick={() => setPopupVisible(false)}>
                OK, je valide
              </button>
            </footer>
          </div>
        </div>
      </Modal>
    </article>
  );

  return withContainer ? (
    <div>
      <CardContent />
    </div>
  ) : (
    <CardContent />
  );
};

export default CriteriaCard;

import React, { useRef, useState, useEffect } from "react";
import { useScrollPosition } from "@n8tb1t/use-scroll-position";

const headerHeight = document.querySelector("#masthead").offsetHeight;

const StickyOnScroll = ({ element }) => {
  const Element = element;
  const elementRef = useRef(null);
  const [isSticky, setIsSticky] = useState(false);

  useScrollPosition(
    ({ currPos }) => {
      const y = currPos.y - headerHeight;

      if (y < 0) {
        setIsSticky(true);
      } else {
        setIsSticky(false);
      }
    },
    [],
    elementRef,
    false,
    300
  );

  return (
    <div
      className={`sticky-on-scroll ${isSticky ? "sticky" : ""}`}
      ref={elementRef}
    >
      <div className="placeholder">
        <Element />
      </div>
      <div
        className="sticker"
        style={isSticky ? { top: `${headerHeight}px` } : {}}
      >
        <Element />
      </div>
    </div>
  );
};

export default StickyOnScroll;

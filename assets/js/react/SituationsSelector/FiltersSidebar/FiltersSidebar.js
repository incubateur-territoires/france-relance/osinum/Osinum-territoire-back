import React, { useState, useEffect } from "react";
import TopicLabel from "../TopicLabel/TopicLabel";

const FiltersSidebar = ({
  isDisplayingSelection,
  onDisplaySelection,
  selectedSituations,
  onFilterClick,
  activeTopicFilter,
}) => {
  const [topics, setTopics] = useState(window.OsinumTerritoire.topics || []);
  const [currentlyDisplayingMobileFilters, displayMobileFilters] =
    useState(false);

  // When displaying mobile filters, add a "displaying-filters" class to the body.
  useEffect(() => {
    if (currentlyDisplayingMobileFilters) {
      document.body.classList.add("displaying-filters");
    } else {
      document.body.classList.remove("displaying-filters");
    }
  }, [currentlyDisplayingMobileFilters]);

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  return (
    <aside
      className={`widget-sidebar fixed-on-mobile ${
        currentlyDisplayingMobileFilters ? "current-view-filters" : ""
      }`}
    >
      <div className="widget-sidebar__inner">
        <div className="selection">
          <h3>
            Ma sélection de cartes ({selectedSituations.length}/
            {window.OsinumTerritoire.situations.length})
          </h3>
          <button
            className={
              isDisplayingSelection ? "style-noir-hover" : "style-noir"
            }
            onClick={onDisplaySelection}
          >
            Voir ma sélection
            <svg aria-hidden="true" className="icon oeil">
              <use xlinkHref="#oeil"></use>
            </svg>
          </button>
          {selectedSituations.length === 0 && (
            <p className="mt50 fsi">Pas de sélection pour le moment.</p>
          )}
        </div>
        <div className="topics">
          <h3>Afficher par thématiques</h3>
          <ol>
            <li className="show-all">
              <button
                className={
                  !activeTopicFilter && !isDisplayingSelection
                    ? "style-noir-hover"
                    : "style-noir"
                }
                onClick={(e) => {
                  onFilterClick(null);
                  e.target.blur();

                  if (currentlyDisplayingMobileFilters) {
                    displayMobileFilters(false);
                  }
                }}
              >
                Toutes les thématiques
              </button>
            </li>
            {topics.map((topic) => {
              const selectedSituationsInThisTopic = selectedSituations.filter(
                (s) => s.topics.includes(topic.id)
              ).length;
              const allSituationsInThisTopic =
                window.OsinumTerritoire.situations.filter((s) =>
                  s.topics.includes(topic.id)
                ).length;

              const hasActiveClass = activeTopicFilter === topic.id; /* ||
                isDisplayingSelection && selectedSituationsInThisTopic > 0 */

              const hasDarkClass = selectedSituationsInThisTopic > 0; /* &&
                activeTopicFilter &&
                activeTopicFilter !== topic.id*/

              return (
                <li key={`topic${topic.id}`}>
                  <button
                    className={
                      hasActiveClass
                        ? `theme-${topic.color} no-hover`
                        : hasDarkClass
                        ? `theme-${topic.color}-dark theme-${topic.color}-light-hover`
                        : `theme-${topic.color}-outline theme-${topic.color}-light-hover`
                    }
                    onClick={(e) => {
                      onFilterClick(topic.id);
                      e.target.blur();

                      if (currentlyDisplayingMobileFilters) {
                        displayMobileFilters(false);
                      }
                    }}
                  >
                    <TopicLabel topic={topic} />
                    {topic.title}
                    <span className="counter">
                      ({selectedSituationsInThisTopic}/
                      {allSituationsInThisTopic})
                    </span>
                  </button>
                </li>
              );
            })}
          </ol>
        </div>
        <nav className="mobile-step-navigation bottom">
          <header>
            <button
              className={
                currentlyDisplayingMobileFilters
                  ? "theme-vert-1-dark active"
                  : ""
              }
              onClick={() =>
                displayMobileFilters(!currentlyDisplayingMobileFilters)
              }
            >
              Filtrer
            </button>
            <button
              onClick={() => {
                onDisplaySelection();
                displayMobileFilters(false);
              }}
            >
              {!isDisplayingSelection ? "Ma sélection" : "Toutes les cartes"}
            </button>
          </header>
          <a
            className="button next"
            href={window.OsinumTerritoire.diagnostic.steps.criteres.permalink}
          >
            Exercice suivant
            <svg aria-hidden="true" className="icon fleche-droite">
              <use xlinkHref="#fleche-droite"></use>
            </svg>
          </a>
        </nav>
      </div>
    </aside>
  );
};

export default FiltersSidebar;

import React, { useState } from "react";
import Modal from "../../Modal/Modal";
import TopicLabel from "../TopicLabel/TopicLabel";

const SituationCard = ({
  situation,
  onSelect,
  onUnselect,
  isSelected,
  withContainer = false,
}) => {
  const [popupVisible, setPopupVisible] = useState(false);
  const situationTopic = window.OsinumTerritoire.topics.find(
    (t) => t.id === situation.topics[0]
  );

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  const CardContent = () => (
    <article
      className={`mini-card situation${isSelected ? " selected" : ""} theme-${
        situationTopic.color
      }`}
    >
      <TopicLabel topic={situationTopic} withTitle={false} />
      <div
        className="card-content"
        onClick={() => setPopupVisible(!popupVisible)}
      >
        <p className="pre-title">Carte Situation</p>
        <h3>{situation.title}</h3>
        <button
          className={
            isSelected
              ? `theme-${situationTopic.color}-dark`
              : `theme-${situationTopic.color}-outline theme-${situationTopic.color}-hover`
          }
          onClick={(e) => {
            if (isSelected) {
              onUnselect(situation);
            } else {
              onSelect(situation);
            }

            e.stopPropagation();
          }}
        >
          C'est mon cas{" "}
          {isSelected ? (
            <svg aria-hidden="true" className="icon check">
              <use xlinkHref="#check"></use>
            </svg>
          ) : (
            <svg aria-hidden="true" className="icon plus">
              <use xlinkHref="#plus"></use>
            </svg>
          )}
        </button>
      </div>
      <Modal
        isVisible={popupVisible}
        onClose={() => setPopupVisible(false)}
        className={`modal-${situationTopic.color}`}
      >
        <div className="situation-popup-container popup-container">
          <div className={`summary theme-${situationTopic.color}`}>
            <p className="mobile-hidden number">
              <svg aria-hidden="true" className="icon support">
                <use xlinkHref="#support"></use>
              </svg>
              Situation n°{situation.number}
            </p>
            <h3>&#8243;{situation.title}&#8243;</h3>
            <TopicLabel
              topic={situationTopic}
              withTitle={true}
              className="mobile-hidden"
            />
          </div>
          <div className="body">
            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: situation.description }}
            />
            <p className="mt200 mb50">
              <strong>Ma structure vit cette situation :</strong>
            </p>
            <div className="inline-buttons">
              <button
                className={
                  isSelected
                    ? `theme-${situationTopic.color}-dark`
                    : `theme-${situationTopic.color}-outline theme-${situationTopic.color}-hover`
                }
                onClick={() => {
                  if (!isSelected) {
                    onSelect(situation);
                    setPopupVisible(false);
                  }
                }}
              >
                C'est mon cas
                <svg aria-hidden="true" className="icon check">
                  <use xlinkHref="#check"></use>
                </svg>
              </button>
              <button
                className={
                  !isSelected
                    ? `theme-${situationTopic.color}-dark`
                    : `theme-${situationTopic.color}-outline theme-${situationTopic.color}-hover`
                }
                onClick={() => {
                  if (isSelected) {
                    onUnselect(situation);
                    setPopupVisible(false);
                  }
                }}
              >
                Pas mon cas
                <svg aria-hidden="true" className="icon fermer">
                  <use xlinkHref="#fermer"></use>
                </svg>
              </button>
            </div>
            {/* <p className="small mt75">
              * Des suggestions de ressources et outils vous seront faites en
              fonction de votre sélection de cartes.
            </p> */}
          </div>
        </div>
      </Modal>
    </article>
  );

  return withContainer ? (
    <div>
      <CardContent />
    </div>
  ) : (
    <CardContent />
  );
};

export default SituationCard;

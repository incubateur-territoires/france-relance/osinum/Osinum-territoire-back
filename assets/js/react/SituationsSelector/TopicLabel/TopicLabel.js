import React from "react";

const TopicLabel = ({ topic, withTitle = false, className = "" }) => {
  return (
    <div className={`icon ${className} ${withTitle ? "with-title" : ""}`}>
      <svg aria-hidden className={`icon ${topic.icon}`}>
        <use xlinkHref={`#${topic.icon}`}></use>
      </svg>
      {withTitle && <span>{topic.title}</span>}
    </div>
  );
};

export default TopicLabel;

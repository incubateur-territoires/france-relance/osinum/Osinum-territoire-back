import React, { useState } from "react";

const CurrentViewHeader = ({
  isDisplayingSelection,
  activeTopicFilter,
  selectedSituations,
}) => {
  const [currentlyDisplayingMobileDescription, displayMobileDescription] =
    useState(false);

  /**
   * Default view: no filter, not displaying my selection.
   */
  if (!activeTopicFilter && !isDisplayingSelection) {
    return (
      <div className="current-view-header full-width displaying-all">
        <h3>
          Toutes Cartes Situations ({window.OsinumTerritoire.situations.length})
        </h3>
      </div>
    );
  }

  /**
   * My selection view.
   */
  if (isDisplayingSelection) {
    return (
      <div className="current-view-header noir displaying-selected-situations">
        <h3>
          Ma sélection - Cartes ({selectedSituations.length}/
          {window.OsinumTerritoire.situations.length})
        </h3>
      </div>
    );
  }

  /**
   * Active filter view.
   */
  if (activeTopicFilter) {
    const topic = window.OsinumTerritoire.topics.find(
      (t) => t.id === activeTopicFilter
    );
    const situationsWithThisTopic = window.OsinumTerritoire.situations.filter(
      (s) => s.topics.includes(activeTopicFilter)
    ).length;

    return (
      <div
        onClick={() =>
          displayMobileDescription(!currentlyDisplayingMobileDescription)
        }
        className={`current-view-header custom-bg displaying-filtered-situations bg-${
          topic.color
        } color-${topic.color}-dark ${
          currentlyDisplayingMobileDescription ? "displaying-description" : ""
        }`}
      >
        <h3>
          {topic.title} - Cartes ({situationsWithThisTopic})
        </h3>
        <div
          dangerouslySetInnerHTML={{ __html: topic.description }}
          className="description"
        />
      </div>
    );
  }

  return null;
};

export default CurrentViewHeader;

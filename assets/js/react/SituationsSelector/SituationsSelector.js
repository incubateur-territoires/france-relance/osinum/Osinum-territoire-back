import React, { useState, useEffect, useCallback } from "react";
import { createDiagnostic, updateDiagnostic } from "../../utils/ajax";
import SituationCard from "./SituationCard/SituationCard";

import { useIsFirstRender, usePrevious } from "../../utils/hooks";
import FiltersSidebar from "./FiltersSidebar/FiltersSidebar";
import { getInitialDiagnosticSelectedSituations } from "../../utils/defaults";
import CurrentViewHeader from "./CurrentViewHeader/CurrentViewHeader";
import StepFooter from "../StepFooter/StepFooter";
import { getSituations } from "../../utils/data";
import StickyOnScroll from "../StickyOnScroll/StickyOnScroll";

const SituationsSelector = () => {
  const isFirstRender = useIsFirstRender();
  const [situations, setSituations] = useState(getSituations());
  const [activeTopicFilter, setActiveTopicFilter] = useState(null);
  const [isDisplayingSelection, displaySelection] = useState(false);
  const [currentlyUsedFilter, setCurrentlyUsedFilter] = useState(null);
  const previousActiveTopicFilter = usePrevious(activeTopicFilter);
  const previousDisplayingSelection = usePrevious(isDisplayingSelection);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedSituations, setSelectedSituations] = useState(
    getInitialDiagnosticSelectedSituations()
  );

  //======================================================
  //
  //  #####  #####  #####  #####   ####  ######   ####
  //  ##     ##     ##     ##     ##       ##    ##
  //  #####  #####  #####  #####  ##       ##     ###
  //  ##     ##     ##     ##     ##       ##       ##
  //  #####  ##     ##     #####   ####    ##    ####
  //
  //======================================================

  /**
   * Save the list of selected Situations.
   */
  const saveSituations = useCallback(async () => {
    setIsLoading(true);
    const result = await updateDiagnostic({
      situations:
        selectedSituations.length === 0
          ? ""
          : selectedSituations.map((s) => s.id),
    });
    setIsLoading(false);

    if (!result.success) {
      alert(result.message || "Une erreur est survenue, veuillez réessayer.");
    }
  }, [selectedSituations]);

  /**
   * When selection changes, save the list of selected Situations.
   */
  useEffect(() => {
    if (isFirstRender) {
      return;
    }

    saveSituations();
  }, [selectedSituations]);

  /**
   * Change the list of Situations when the activeTopicFilter changes.
   */
  useEffect(() => {
    // Changing the topic filter.
    if (
      currentlyUsedFilter === "topic" /*&&
      previousActiveTopicFilter !== activeTopicFilter*/
    ) {
      if (activeTopicFilter) {
        setSituations(
          getSituations().filter((s) => s.topics.includes(activeTopicFilter))
        );
      } else {
        setSituations(getSituations());
      }

      displaySelection(false);
    }

    // Changing the "Voir ma sélection".
    else if (
      currentlyUsedFilter === "manual" &&
      previousDisplayingSelection !== isDisplayingSelection
    ) {
      if (isDisplayingSelection) {
        setSituations(selectedSituations);
      } else {
        setSituations(getSituations());
      }

      setActiveTopicFilter(null);
    }
  }, [activeTopicFilter, isDisplayingSelection]);

  //=====================================================
  //
  //  #####  ##   ##  #####  ##     ##  ######   ####
  //  ##     ##   ##  ##     ####   ##    ##    ##
  //  #####  ##   ##  #####  ##  ## ##    ##     ###
  //  ##      ## ##   ##     ##    ###    ##       ##
  //  #####    ###    #####  ##     ##    ##    ####
  //
  //=====================================================

  /**
   * When a Situation card is selected.
   */
  const onCardSelect = (situation) => {
    setSelectedSituations([...selectedSituations, situation]);
  };

  /**
   * When a Situation card is unselected.
   */
  const onCardUnselect = (situation) => {
    setSelectedSituations(
      selectedSituations.filter((s) => s.id !== situation.id)
    );
  };

  /**
   * When clicking the "Voir ma sélection" button.
   */
  const onDisplaySelection = () => {
    setCurrentlyUsedFilter("manual");
    displaySelection(!isDisplayingSelection);

    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  /**
   * When clicking a single topic filter in the sidebar.
   */
  const onFilterClick = (topicId) => {
    setCurrentlyUsedFilter("topic");
    displaySelection(false);

    if (!topicId || activeTopicFilter === topicId) {
      setActiveTopicFilter(null);
    } else {
      setActiveTopicFilter(topicId);
    }

    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================

  return (
    <>
      <StickyOnScroll
        element={() => (
          <CurrentViewHeader
            isDisplayingSelection={isDisplayingSelection}
            activeTopicFilter={activeTopicFilter}
            selectedSituations={selectedSituations}
          />
        )}
      />
      <div className="situations grid per-3">
        {situations.map((situation) => (
          <SituationCard
            key={`situation${situation.id}`}
            situation={situation}
            isSelected={!!selectedSituations.find((s) => s.id === situation.id)}
            onSelect={onCardSelect}
            onUnselect={onCardUnselect}
            withContainer={true}
          />
        ))}
      </div>
      <FiltersSidebar
        isDisplayingSelection={isDisplayingSelection}
        activeTopicFilter={activeTopicFilter}
        selectedSituations={selectedSituations}
        onDisplaySelection={onDisplaySelection}
        onFilterClick={onFilterClick}
      />
      <StepFooter currentStep="situations" />
    </>
  );
};

export default SituationsSelector;

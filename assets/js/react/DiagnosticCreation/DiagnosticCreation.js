import React, { useState, useEffect } from "react";
import { createDiagnostic } from "../../utils/ajax";
import CopyButton from "../CopyButton/CopyButton";
import cookies from "cookiesjs";

const DiagnosticCreation = () => {
  const [selectedFirstStep, setSelectedFirstStep] = useState("");
  const [submitIsPossible, setSubmitPossible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [formValues, setFormValues] = useState({ title: "", email: "" });
  const [createdDiagnostic, setSelectedDiagnostic] = useState(null);

  /**
   * Change the selected first step when the popup is opened.
   */
  const changeStepWhenPopupOpens = (e, data) => {
    if (data.id === "diagnostic-create") {
      setSelectedFirstStep(data.params);
    }
  };

  /**
   * Listen to the popup opening logic triggered outside this React app.
   */
  useEffect(() => {
    jQuery(window).on("popup-open", changeStepWhenPopupOpens);

    return () => {
      jQuery(window).off("popup-open", changeStepWhenPopupOpens);
    };
  }, []);

  /**
   * When form values change.
   */
  useEffect(() => {
    // If not currently loading, title is not empty and if the email is a valid email address, enable submit button.
    if (
      !isLoading &&
      formValues.title.length > 0 &&
      formValues.email.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)
    ) {
      setSubmitPossible(true);
    } else {
      setSubmitPossible(false);
    }
  }, [formValues, isLoading]);

  /**
   * Change form values when the user changes the input.
   */
  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  /**
   * When submitting the creation form.
   */
  const onSubmit = async (e) => {
    e.preventDefault();

    setIsLoading(true);
    const result = await createDiagnostic({
      ...formValues,
      first_step: selectedFirstStep,
    });
    setIsLoading(false);

    if (!result.success) {
      alert(result.message || "Une erreur est survenue, veuillez réessayer.");
    } else {
      setSelectedDiagnostic(result.data.diagnostic);

      // Store diag in cookies
      cookies(
        {
          diagID: parseInt(result.data.diagnostic.id),
          diagToken: result.data.diagnostic.token,
        },
        {
          autojson: false,
        }
      );
    }
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================

  // First step: enter the diagnostic name and email.
  if (!createdDiagnostic) {
    return (
      <div>
        <h2 className="modal-title">Nouveau diagnostic</h2>
        <p className="mb200">Pour démarrer votre diagnostic :</p>

        <form id="diagnostic-create-form" action="#">
          <div className="input">
            <h3 className="m0 mt100 mb50">
              Donnez un titre à votre diagnostic
            </h3>
            <label htmlFor="diag-title">Titre du diagnostic</label>
            <input
              type="text"
              name="title"
              id="diag-title"
              value={formValues.title}
              onChange={handleChange}
              required
              placeholder="Titre du diagnostic"
            />
          </div>
          <div className="input">
            <h3 className="m0 mt250 mb50">Renseignez votre adresse mail</h3>
            <label htmlFor="diag-email">Votre adresse mail</label>
            <input
              type="email"
              name="email"
              value={formValues.email}
              onChange={handleChange}
              id="diag-email"
              required
              placeholder="Renseignez votre adresse mail"
            />
          </div>
          <footer className="clearfix mt300">
            <input type="hidden" name="step" value={selectedFirstStep} />
            <button
              disabled={!submitIsPossible}
              onClick={onSubmit}
              className="fr theme-vert-1-outline"
            >
              Suite
              <svg aria-hidden="true" className="icon fleche-droite">
                <use xlinkHref="#fleche-droite"></use>
              </svg>
            </button>
          </footer>
        </form>
      </div>
    );
  }

  // Second step: copy diagnostic URL and start the diagnostic process.
  return (
    <div>
      <h2 className="modal-title">Lien envoyé à l'adresse mail</h2>
      <p>Pour retrouver ou partager votre diagnostic :</p>
      <h3 className="mt150">Le lien vers votre diagnostic :</h3>
      <div className="flex-row flex-valign-bottom">
        <div className="input flex">
          <div className="input-container">
            <label className="tiny-label" htmlFor="permalink">
              Le lien vers votre diagnostic :
            </label>
            <input
              type="text"
              name="permalink"
              id="permalink"
              value={createdDiagnostic.permalink}
              readOnly={true}
            />
          </div>
        </div>
        <div className="button-container">
          <CopyButton text={createdDiagnostic.permalink} />
        </div>
      </div>
      <p className="mt150 mb75">
        Nous vous avons envoyé à l’adresse mail renseignée, votre permalien, un
        lien url, à partir duquel vous pourrez retourner à tous moments sur
        votre diagnostic.
      </p>
      <p>
        Vous pouvez également le partager à d’autres personnes, qui auront ainsi
        accès au diagnostic.
      </p>
      <footer className="clearfix mt400">
        <a
          href={createdDiagnostic.permalink_step}
          className="button theme-vert-1-outline fr"
        >
          Commencer le diagnostic
          <svg aria-hidden="true" className="icon fleche-droite">
            <use xlinkHref="#fleche-droite"></use>
          </svg>
        </a>
      </footer>
    </div>
  );
};

export default DiagnosticCreation;

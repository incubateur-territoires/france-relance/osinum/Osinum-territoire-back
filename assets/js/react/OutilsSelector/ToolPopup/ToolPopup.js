import React, { useState, useEffect } from "react";
import {
  getButtonRatingLabel,
  toolCriteriasQuestions,
} from "../../../utils/data";
import Modal from "../../Modal/Modal";

/**
 * Empty initial tool configuration object.
 */
const initialToolConfiguration = {
  practices: [],
  usages: {
    satisfaction: null,
    frequency: null,
  },
  criterias: {
    securite: false,
    "open-source": false,
    "hebergement-europeen": false,
    "vie-privee": false,
    "identifiant-commun": false,
    "en-francais": false,
    "fonctionne-smartphone": false,
    "travail-distance": false,
    sobriete: false,
    ergonomique: false,
  },
};

const practices = window.OsinumTerritoire.practices || [];

const ToolPopup = ({
  visible,
  tool,
  fromPractice = null,
  selectedTool,
  onFormFinish,
  onClose,
}) => {
  const [currentFormStep, setCurrentFormStep] = useState(0);
  const [currentDescriptionQuestion, setCurrentDescriptionQuestion] =
    useState(0);
  const [finishedFormSteps, setFinishedFormSteps] = useState([]);
  const [toolConfiguration, setToolConfiguration] = useState(
    selectedTool?.configuration || initialToolConfiguration
  );
  const isSelected = !!selectedTool;
  const isCustom = tool.type === "custom";
  const showCriteriasStep = isCustom; /*&& !isSelected*/ // Only show criterias step the first time.

  /**
   * If we are not displaying the last step, remove the Criterias from current configuration object.
   */
  useEffect(() => {
    let newToolConfiguration = { ...toolConfiguration };

    // Add the practice from previous popup in the configuration object.
    if (fromPractice) {
      newToolConfiguration.practices = [
        ...newToolConfiguration.practices,
        fromPractice.id,
      ];

      setToolConfiguration(newToolConfiguration);
    }

    if (!showCriteriasStep) {
      if (typeof toolConfiguration.criterias !== "undefined") {
        delete newToolConfiguration.criterias;
      }

      setToolConfiguration(newToolConfiguration);
    }
  }, []);

  //======================================================
  //
  //  #####  #####  #####  #####   ####  ######   ####
  //  ##     ##     ##     ##     ##       ##    ##
  //  #####  #####  #####  #####  ##       ##     ###
  //  ##     ##     ##     ##     ##       ##       ##
  //  #####  ##     ##     #####   ####    ##    ####
  //
  //======================================================

  /**
   * When changing form steps, save it to the visited steps list.
   */
  useEffect(() => {
    if (!finishedFormSteps.includes(currentFormStep)) {
      setFinishedFormSteps([...finishedFormSteps, currentFormStep]);
    }
  }, [currentFormStep]);

  //=====================================================
  //
  //  #####  ##   ##  #####  ##     ##  ######   ####
  //  ##     ##   ##  ##     ####   ##    ##    ##
  //  #####  ##   ##  #####  ##  ## ##    ##     ###
  //  ##      ## ##   ##     ##    ###    ##       ##
  //  #####    ###    #####  ##     ##    ##    ####
  //
  //=====================================================

  /**
   * When clicking on a form step.
   */
  const onFormStepClick = (index) => {
    if (finishedFormSteps.includes(index)) {
      setCurrentFormStep(index);
      setCurrentDescriptionQuestion(0);
    }
  };

  /**
   * When clicking on a practice.
   */
  const onPracticeClick = (practiceId) => {
    const newToolConfiguration = { ...toolConfiguration };
    newToolConfiguration.practices = [...newToolConfiguration.practices];

    if (newToolConfiguration.practices.includes(practiceId)) {
      newToolConfiguration.practices = newToolConfiguration.practices.filter(
        (practice) => practice !== practiceId
      );
    } else {
      newToolConfiguration.practices.push(practiceId);
    }

    setToolConfiguration(newToolConfiguration);
  };

  /**
   * When clicking on a usage (satisfaction or frequency).
   */
  const onUsageClick = (type, value) => {
    const newToolConfiguration = { ...toolConfiguration };
    newToolConfiguration.usages = {
      ...newToolConfiguration.usages,
      [type]: value,
    };

    setToolConfiguration(newToolConfiguration);
  };

  /**
   * When clicking on a description criteria question answer.
   */
  const onCriteriaClick = (criteriaId) => {
    const newToolConfiguration = { ...toolConfiguration };

    newToolConfiguration.criterias = {
      ...newToolConfiguration.criterias,
      [criteriaId]: newToolConfiguration.criterias[criteriaId] ? false : true,
    };

    setToolConfiguration(newToolConfiguration);

    // jQuery("ol.criterias-selector").css("display", "block");
  };

  /**
   * When finalizing the form: submit and close popup.
   */
  const onFormFinalize = () => {
    onFormFinish(tool.id, toolConfiguration);
    onClose();
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  return (
    <Modal isVisible={visible} onClose={onClose} id="tool-popup">
      <div
        className={`tool-popup-container popup-container form-step-${currentFormStep}`}
      >
        <div className="summary theme-bleu-1">
          <p className="number">
            <svg aria-hidden className="icon ecran">
              <use xlinkHref="#ecran"></use>
            </svg>
            Outil
          </p>
          <h3>{tool.title}</h3>
          {tool.description.length > 0 && (
            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: tool.description }}
            />
          )}
          {currentFormStep > 0 && (
            <ol className="form-steps mobile-hidden">
              <li
                className={currentFormStep === 1 ? "active" : ""}
                onClick={() => onFormStepClick(1)}
              >
                1. Pratique de l'outil
              </li>
              <li
                className={currentFormStep === 2 ? "active" : ""}
                onClick={() => onFormStepClick(2)}
              >
                2. Utilisation de l'outil
              </li>
              {showCriteriasStep && (
                <li
                  className={currentFormStep === 3 ? "active" : ""}
                  onClick={() => onFormStepClick(3)}
                >
                  3. Description de l'outil
                </li>
              )}
            </ol>
          )}
        </div>
        <div className="body">
          {currentFormStep === 0 && (
            <div className="form-step full-height form-step-0">
              <h3>Informations de l'outil</h3>
              {!isSelected ? (
                <>
                  <p>Compléter les informations de l'outil.</p>
                  <ol className="form-steps">
                    <li>1. Pratique de l'outil</li>
                    <li>2. Utilisation de l'outil</li>
                    {showCriteriasStep && <li>3. Description de l'outil</li>}
                  </ol>
                  <button
                    className="mt250 fr"
                    onClick={() => setCurrentFormStep(1)}
                  >
                    C'est parti
                    <svg aria-hidden="true" className="icon fleche-droite">
                      <use xlinkHref="#fleche-droite"></use>
                    </svg>
                  </button>
                </>
              ) : (
                <>
                  <p>Vous souhaitez apporter des modifications à cet outil.</p>
                  <ol className="form-steps">
                    <li>1. Pratique de l'outil</li>
                    <li>2. Utilisation de l'outil</li>
                    {showCriteriasStep && <li>3. Description de l'outil</li>}
                  </ol>
                  <button
                    className="mta fr"
                    onClick={() => setCurrentFormStep(1)}
                  >
                    Modifier
                    <svg aria-hidden="true" className="icon fleche-droite">
                      <use xlinkHref="#fleche-droite"></use>
                    </svg>
                  </button>
                </>
              )}
            </div>
          )}
          {currentFormStep === 1 && (
            <div className="form-step full-height form-step-1">
              <h3 className="mt0">1. Pratique de l'outil</h3>
              <p className="mb100">Sélectionner ce que l'outil vous permet.</p>
              <ol className="list-items-selector">
                {practices.map((practice, index) => {
                  const practiceIsSelected =
                    toolConfiguration.practices.includes(practice.id);

                  return (
                    <li
                      key={`practice${practice.id}${index}`}
                      className={practiceIsSelected ? "active" : ""}
                      onClick={() => onPracticeClick(practice.id)}
                    >
                      <span>{practice.title}</span>
                      {practiceIsSelected ? (
                        <svg aria-hidden className="icon fermer">
                          <use xlinkHref="#fermer"></use>
                        </svg>
                      ) : (
                        <svg aria-hidden className="icon plus">
                          <use xlinkHref="#plus"></use>
                        </svg>
                      )}
                    </li>
                  );
                })}
              </ol>
              <button
                className="mta fr"
                onClick={() => setCurrentFormStep(2)}
                disabled={toolConfiguration.practices.length === 0}
              >
                Ok
                <svg aria-hidden="true" className="icon fleche-droite">
                  <use xlinkHref="#fleche-droite"></use>
                </svg>
              </button>
            </div>
          )}
          {currentFormStep === 2 && (
            <div className="form-step full-height form-step-2">
              <h3>2. Utilisation de l'outil</h3>
              <p className="mb150">Sélectionnez les choix qui conviennent.</p>
              <div className="form-substep mt0i">
                <p className="small m0 color-bleu-1-dark">
                  1/2 - Satisfaction d'utilisation
                </p>
                <p className="medium mt50 mb100">
                  Concernant l'usage de l'outil, vous êtes&nbsp;:
                </p>
                <ol className="rating-buttons per-3">
                  {Array.from(Array(3).keys()).map((i) => {
                    const checkboxData = getButtonRatingLabel(
                      "satisfaction",
                      i
                    );

                    return (
                      <li
                        key={`buttonUsageRating${i}`}
                        className={
                          toolConfiguration.usages.satisfaction ===
                          checkboxData.value
                            ? "selected"
                            : ""
                        }
                      >
                        <label
                          htmlFor={`satisfaction${i}`}
                          className={`level${i}`}
                        >
                          <input
                            type="radio"
                            id={`satisfaction${i}`}
                            onChange={() =>
                              onUsageClick("satisfaction", checkboxData.value)
                            }
                            checked={
                              toolConfiguration.usages.satisfaction ===
                              checkboxData.value
                            }
                          />
                          <svg
                            aria-hidden="true"
                            className={`icon ${checkboxData.icon}`}
                          >
                            <use xlinkHref={`#${checkboxData.icon}`}></use>
                          </svg>
                          <span>{checkboxData.label}</span>
                        </label>
                      </li>
                    );
                  })}
                </ol>
              </div>
              <div className="form-substep">
                <p className="small m0 color-bleu-1-dark">
                  2/2 - Fréquence d'utilisation
                </p>
                <p className="medium mt50 mb100">
                  Vous utilisez cet outil&nbsp;:
                </p>
                <ol className="rating-buttons per-3">
                  {Array.from(Array(3).keys()).map((i) => {
                    const checkboxData = getButtonRatingLabel("frequency", i);

                    return (
                      <li
                        key={`buttonUsageRating${i}`}
                        className={
                          toolConfiguration.usages.frequency ===
                          checkboxData.value
                            ? "selected"
                            : ""
                        }
                      >
                        <label
                          htmlFor={`frequency${i}`}
                          className={`level${i}`}
                        >
                          <input
                            type="radio"
                            id={`frequency${i}`}
                            onChange={() =>
                              onUsageClick("frequency", checkboxData.value)
                            }
                            checked={
                              toolConfiguration.usages.frequency ===
                              checkboxData.value
                            }
                          />
                          <svg
                            aria-hidden="true"
                            className={`icon ${checkboxData.icon}`}
                          >
                            <use xlinkHref={`#${checkboxData.icon}`}></use>
                          </svg>
                          <span>{checkboxData.label}</span>
                        </label>
                      </li>
                    );
                  })}
                </ol>
              </div>
              <button
                className="mta fr"
                onClick={() => {
                  if (showCriteriasStep) {
                    setCurrentFormStep(3);
                  } else {
                    onFormFinalize();
                  }
                }}
                disabled={
                  typeof toolConfiguration.usages.satisfaction !== "number" ||
                  typeof toolConfiguration.usages.frequency !== "number"
                }
              >
                Ok
                <svg aria-hidden="true" className="icon fleche-droite">
                  <use xlinkHref="#fleche-droite"></use>
                </svg>
              </button>
            </div>
          )}
          {currentFormStep === 3 && (
            <div className="form-step full-height form-step-3">
              <h3>3. Description de l'outil</h3>
              <p className="mb150">
                Est-ce que cet outil valide les critères suivants&nbsp;? Activez
                les critères validés par l'outil.
              </p>
              <ol className="criterias-selector">
                {toolCriteriasQuestions.map((question, index) => {
                  const isChecked =
                    toolConfiguration.criterias &&
                    toolConfiguration.criterias[question.id];

                  return (
                    <li
                      className={`question question-${index + 1}${
                        isChecked ? " checked" : ""
                      }`}
                      key={`question${question.id}`}
                    >
                      <p className="small color-bleu-1-dark">
                        Critère {index + 1}/{toolCriteriasQuestions.length}
                      </p>
                      <label htmlFor={`criteria${question.id}`}>
                        <span className="label">{question.question}</span>
                        <span className="fake-toggle">
                          <input
                            type="checkbox"
                            id={`criteria${question.id}`}
                            onChange={() => onCriteriaClick(question.id)}
                            checked={isChecked}
                          />
                          {isChecked ? (
                            <span>&nbsp;&nbsp;Validé</span>
                          ) : (
                            <span>Invalidé</span>
                          )}
                        </span>
                      </label>
                    </li>
                  );
                })}
              </ol>
              <button
                className="mta fr"
                onClick={() => {
                  onFormFinalize();
                }}
              >
                Ok
                <svg aria-hidden="true" className="icon fleche-droite">
                  <use xlinkHref="#fleche-droite"></use>
                </svg>
              </button>
            </div>
          )}
        </div>
      </div>
    </Modal>
  );
};

export default ToolPopup;

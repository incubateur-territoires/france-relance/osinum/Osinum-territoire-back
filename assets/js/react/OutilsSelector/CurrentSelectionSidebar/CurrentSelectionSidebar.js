import React, { useState, useEffect } from "react";
import { arrayUnique } from "../../../utils/helpers";

const CurrentSelectionSidebar = ({ practices, tools, selectedTools }) => {
  const [fullSelectedPractices, setFullSelectedPractices] = useState([]);
  const [fullSelectedTools, setFullSelectedTools] = useState([]);
  const [currentlyDisplayingMobileFilters, displayMobileFilters] = useState("");

  /**
   * When list of selected tools changes, update the list of full selected practices + tools objects.
   */
  useEffect(() => {
    let practicesIds = [];

    selectedTools.map((st) => {
      if (st.configuration.practices.length > 0) {
        practicesIds = practicesIds.concat(st.configuration.practices);
      }
    });

    setFullSelectedPractices(
      arrayUnique(practicesIds)
        .map((id) => practices.find((p) => p.id === id))
        .filter((p) => !!p)
    );

    setFullSelectedTools(
      selectedTools
        .map((st) => tools.find((t) => t.id === st.id))
        .filter((st) => !!st)
    );
  }, [selectedTools]);

  return (
    <aside
      className={`widget-sidebar fixed-on-mobile ${
        currentlyDisplayingMobileFilters !== ""
          ? `current-view-filters-${currentlyDisplayingMobileFilters}`
          : ""
      }`}
    >
      <div className="widget-sidebar__inner">
        <div className="selection">
          <h3>Vos outils numériques actuels</h3>
          <p className="mb75">
            Vous avez indiqué que vous utilisez les outils suivants&nbsp;:
          </p>
          {fullSelectedTools.length > 0 ? (
            <ol>
              {fullSelectedTools.map((tool) => (
                <li key={tool.id} className="button no-hover theme-bleu-1">
                  <span>{tool.title}</span>
                </li>
              ))}
            </ol>
          ) : (
            <p className="fsi">Pas de sélection pour le moment.</p>
          )}
        </div>
        <div className="practices">
          <h3>Vos pratiques déjà outillées</h3>
          <p className="mb75">
            Vous êtes déjà équipé pour les pratiques numériques suivantes&nbsp;:
          </p>
          {fullSelectedPractices.length > 0 ? (
            <ol>
              {fullSelectedPractices.map((practice) => (
                <li key={practice.id} className="button no-hover theme-vert-1">
                  <span>{practice.title}</span>
                </li>
              ))}
            </ol>
          ) : (
            <p className="fsi">Pas de sélection pour le moment.</p>
          )}
        </div>
        <nav className="mobile-step-navigation bottom">
          {/* <header>
            <button
              className={
                currentlyDisplayingMobileFilters === "tools"
                  ? "theme-vert-1-dark active"
                  : ""
              }
              onClick={() => {
                if (
                  currentlyDisplayingMobileFilters === "" ||
                  currentlyDisplayingMobileFilters === "practices"
                ) {
                  displayMobileFilters("tools");
                } else {
                  displayMobileFilters("");
                }
              }}
            >
              Vos outils
            </button>
            <button
              className={
                currentlyDisplayingMobileFilters === "practices"
                  ? "theme-vert-1-dark active"
                  : ""
              }
              onClick={() => {
                if (
                  currentlyDisplayingMobileFilters === "" ||
                  currentlyDisplayingMobileFilters === "tools"
                ) {
                  displayMobileFilters("practices");
                } else {
                  displayMobileFilters("");
                }
              }}
            >
              Vos pratiques
            </button>
          </header> */}
          <a
            className="button next"
            href={
              window.OsinumTerritoire.diagnostic.steps.difficultes.permalink
            }
          >
            Exercice suivant
            <svg aria-hidden="true" className="icon fleche-droite">
              <use xlinkHref="#fleche-droite"></use>
            </svg>
          </a>
        </nav>
      </div>
    </aside>
  );
};

export default CurrentSelectionSidebar;

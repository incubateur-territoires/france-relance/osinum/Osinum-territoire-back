import React, { useState, useEffect, useRef } from "react";
import { useKeyPress } from "../../../utils/hooks";
import Modal from "../../Modal/Modal";

const PracticePopup = ({
  tools,
  visible,
  creating,
  practice,
  selectedTools,
  onClose,
  onEditTool,
  onDeleteTool,
  onAddTool,
}) => {
  const [addFormVisible, setAddFormVisibility] = useState(false);
  const [toolName, setToolName] = useState("");
  const [selectedToolName, setSelectedToolName] = useState(null);
  const [creatingCustomTool, setCreatingCustomTool] = useState(false);
  const [suggestionsVisible, setSuggestionsVisibility] = useState(false);
  const [selectedSuggestion, setSelectedSuggestion] = useState(null);
  const [suggestedTools, setSuggestedTools] = useState([]);
  const hasTools = selectedTools.length > 0;
  const [cursor, setCursor] = useState(0);
  const downKeyPress = useKeyPress("ArrowDown");
  const upKeyPress = useKeyPress("ArrowUp");
  const enterKeyPress = useKeyPress("Enter");
  const inputRef = useRef(null);

  /**
   * Select a specific existing tool from the list of suggestions.
   */
  const selectExistingTool = (tool) => {
    setCreatingCustomTool(false);
    setSelectedSuggestion(tool.id);
    setSuggestionsVisibility(false);
    setSelectedToolName(tool.title);
  };

  //======================================================
  //
  //  #####  #####  #####  #####   ####  ######   ####
  //  ##     ##     ##     ##     ##       ##    ##
  //  #####  #####  #####  #####  ##       ##     ###
  //  ##     ##     ##     ##     ##       ##       ##
  //  #####  ##     ##     #####   ####    ##    ####
  //
  //======================================================

  /**
   * When entering a tool name, find all tools which have the input tool name in their name.
   */
  useEffect(() => {
    setSelectedSuggestion(null);

    if (toolName.length === 0) {
      setSuggestedTools([]);
      setCreatingCustomTool(false);
    } else {
      setSuggestedTools(
        tools.filter(
          (t) =>
            t && t.id && t.title.toLowerCase().includes(toolName.toLowerCase())
        )
      );

      setCreatingCustomTool(true);
    }
  }, [toolName]);

  //=====================================================
  //
  //  #####  ##   ##  #####  ##     ##  ######   ####
  //  ##     ##   ##  ##     ####   ##    ##    ##
  //  #####  ##   ##  #####  ##  ## ##    ##     ###
  //  ##      ## ##   ##     ##    ###    ##       ##
  //  #####    ###    #####  ##     ##    ##    ####
  //
  //=====================================================

  useEffect(() => {
    if (suggestedTools.length > 0 && suggestionsVisible && downKeyPress) {
      setCursor((prevState) =>
        prevState < suggestedTools.length - 1 ? prevState + 1 : prevState
      );
    }
  }, [downKeyPress]);

  useEffect(() => {
    if (suggestedTools.length > 0 && suggestionsVisible && upKeyPress) {
      setCursor((prevState) => (prevState > 0 ? prevState - 1 : prevState));
    }
  }, [upKeyPress]);

  useEffect(() => {
    if (suggestedTools.length > 0 && suggestionsVisible && enterKeyPress) {
      const clickedTool = suggestedTools[cursor];
      const alreadySelected = selectedTools.includes(clickedTool.id);

      if (!alreadySelected) {
        selectExistingTool(clickedTool);
      }
    }
  }, [cursor, enterKeyPress]);

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  return (
    <Modal isVisible={visible} onClose={onClose} id="practice-popup">
      <div className={`practice-popup-container popup-container`}>
        <div className="summary theme-vert-1">
          <p className="number">
            <svg aria-hidden className="icon ecran">
              <use xlinkHref="#ecran"></use>
            </svg>
            Pratique n°{practice.number}
          </p>
          <h3>{practice.title}</h3>
          {practice.description.length > 0 && (
            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: practice.description }}
            />
          )}
        </div>
        <div className="body">
          <div className="tools-list">
            <p className="m0 fwn">
              Le(s) outil(s) que j'utilise pour cette pratique&nbsp;:
            </p>
            {hasTools ? (
              <ul>
                {selectedTools.map((selectedToolId) => {
                  const tool = tools.find((t) => t.id === selectedToolId);

                  return (
                    <li
                      key={`tool${selectedToolId}`}
                      className="flex-row flex-valign-center"
                    >
                      <span className="mra">{tool.title}</span>
                      <button
                        className="small icon-only"
                        onClick={() => onEditTool(selectedToolId)}
                      >
                        <svg aria-hidden className="icon edit">
                          <use xlinkHref="#edit"></use>
                        </svg>
                      </button>
                      <button
                        className="small icon-only"
                        onClick={() => onDeleteTool(selectedToolId)}
                      >
                        <svg aria-hidden className="icon fermer">
                          <use xlinkHref="#fermer"></use>
                        </svg>
                      </button>
                    </li>
                  );
                })}
              </ul>
            ) : (
              <p className="no-value mt125 mb225">
                Aucun outil ajouté pour le moment.
              </p>
            )}
          </div>
          <div className="add-tool-form-container">
            {!addFormVisible ? (
              <button
                className="mb50"
                onClick={() => setAddFormVisibility(true)}
              >
                Ajouter un outil
                <svg aria-hidden="true" className="icon plus">
                  <use xlinkHref="#plus"></use>
                </svg>
              </button>
            ) : (
              <div className="add-tool-form">
                <h5 className="mt100 mb50">Ajouter un outil</h5>
                <p className="mt50 mb75 medium">
                  Renseigner le nom de l'outil :
                </p>
                <div className="input">
                  <div className="input-container">
                    <label className="tiny-label" htmlFor="tool-name">
                      Nom de l'outil
                    </label>
                    <input
                      type="text"
                      name="toolName"
                      id="tool-name"
                      value={toolName}
                      autoComplete="off"
                      ref={inputRef}
                      onChange={(e) => {
                        if (e.target.value !== toolName) {
                          setSuggestionsVisibility(true);
                          setToolName(e.target.value);
                          setCursor(-1);
                        }
                      }}
                      placeholder="Renseigner le nom de l'outil…"
                    />
                    {selectedToolName && (
                      <input
                        type="text"
                        className="selected-tool-name"
                        value={selectedToolName}
                        onClick={() => {
                          setSelectedToolName(null);
                          setToolName("");
                          inputRef.current.focus();
                        }}
                        readOnly
                      />
                    )}
                  </div>
                  {suggestedTools.length > 0 && suggestionsVisible && (
                    <ul className="suggestions">
                      {suggestedTools.map((tool, toolIndex) => {
                        const alreadySelected = selectedTools.includes(tool.id);

                        return (
                          <li
                            key={`suggestion${tool.id}`}
                            className={`${
                              selectedSuggestion === tool.id ? "active" : ""
                            } ${toolIndex === cursor ? "selected" : ""}`}
                            onClick={() => {
                              if (!alreadySelected) {
                                selectExistingTool(tool);
                              }
                            }}
                          >
                            <span>
                              {tool.title}
                              {alreadySelected ? " (déjà ajouté)" : ""}
                            </span>
                          </li>
                        );
                      })}
                    </ul>
                  )}
                </div>
                {creating ? (
                  <p>Création de l'outil en cours…</p>
                ) : (
                  <button
                    className="add-button mb50"
                    onClick={() => {
                      const oldToolName = toolName;
                      setToolName("");
                      setSelectedToolName(null);
                      // onClose();

                      onAddTool(
                        creatingCustomTool ? oldToolName : selectedSuggestion,
                        creatingCustomTool
                      );
                    }}
                    disabled={
                      toolName === "" && !selectedSuggestion ? true : false
                    }
                  >
                    Ajouter
                    <svg aria-hidden="true" className="icon plus">
                      <use xlinkHref="#plus"></use>
                    </svg>
                  </button>
                )}
              </div>
            )}
          </div>
          {hasTools && !addFormVisible && (
            <footer>
              <button className="secondary" onClick={onClose}>
                Ok
              </button>
            </footer>
          )}
        </div>
      </div>
    </Modal>
  );
};

export default PracticePopup;

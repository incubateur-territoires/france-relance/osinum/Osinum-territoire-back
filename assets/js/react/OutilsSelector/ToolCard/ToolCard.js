import React, { useState } from "react";

const practices = window.OsinumTerritoire.practices || [];

const ToolCard = ({
  tool,
  selectedTool,
  onPopupOpen,
  withContainer = false,
}) => {
  const isSelected = !!selectedTool;

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  const CardContent = () => (
    <article
      className={`mini-card tool${
        isSelected
          ? " selected theme-bleu-1"
          : " theme-bleu-1 theme-bleu-1-hover"
      }`}
    >
      <div className="icon">
        <svg aria-hidden className="icon ecran">
          <use xlinkHref="#ecran"></use>
        </svg>
      </div>
      <div className="card-content" onClick={onPopupOpen}>
        <p className="pre-title">Carte Outil numérique</p>
        <h3>{tool.title}</h3>
        {isSelected && (
          <ol className="practices-list">
            <li className="mini-title">Mes pratiques</li>
            {selectedTool.configuration.practices.map((practiceId) => {
              const practice = practices.find((p) => p.id === practiceId);

              return (
                <li
                  key={`practice${practiceId}`}
                  className="button theme-vert-1"
                >
                  {practice.title}
                </li>
              );
            })}
          </ol>
        )}
        <button onClick={onPopupOpen}>
          {isSelected ? (
            <>
              Modifier{" "}
              <svg aria-hidden="true" className="icon plus">
                <use xlinkHref="#plus"></use>
              </svg>
            </>
          ) : (
            <>
              J'utilise{" "}
              <svg aria-hidden="true" className="icon plus">
                <use xlinkHref="#plus"></use>
              </svg>
            </>
          )}
        </button>
      </div>
    </article>
  );

  return withContainer ? (
    <div>
      <CardContent />
    </div>
  ) : (
    <CardContent />
  );
};

export default ToolCard;

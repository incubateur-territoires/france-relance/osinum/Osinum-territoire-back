import React, { useState, useEffect, useCallback } from "react";
import { createTool, updateDiagnostic } from "../../utils/ajax";
import PracticeCard from "./PracticeCard/PracticeCard";
import ToolCard from "./ToolCard/ToolCard";
import ToolPopup from "./ToolPopup/ToolPopup";
import PracticePopup from "./PracticePopup/PracticePopup";

import { useIsFirstRender, usePrevious } from "../../utils/hooks";
import { getInitialDiagnosticSelectedTools } from "../../utils/defaults";
import { getPractices, getTools } from "../../utils/data";
import CurrentViewHeader from "./CurrentViewHeader/CurrentViewHeader";
import CurrentSelectionSidebar from "./CurrentSelectionSidebar/CurrentSelectionSidebar";
import StepFooter from "../StepFooter/StepFooter";
import StickyOnScroll from "../StickyOnScroll/StickyOnScroll";

const practices = getPractices();

const OutilsSelector = () => {
  const isFirstRender = useIsFirstRender();
  const [currentView, setCurrentView] = useState("practices");
  const [isLoading, setIsLoading] = useState(false);
  const [tools, setTools] = useState(getTools());
  const [isCreatingCustomTool, setIsCreatingCustomTool] = useState(false);
  const [toolPopupVisible, setToolPopupVisible] = useState(false);
  const [popupTool, setPopupTool] = useState(null);
  const [practicePopupVisible, setPracticePopupVisible] = useState(false);
  const [popupPractice, setPopupPractice] = useState(null);
  const [selectedTools, setSelectedTools] = useState(
    getInitialDiagnosticSelectedTools()
  );

  //======================================================
  //
  //  #####  #####  #####  #####   ####  ######   ####
  //  ##     ##     ##     ##     ##       ##    ##
  //  #####  #####  #####  #####  ##       ##     ###
  //  ##     ##     ##     ##     ##       ##       ##
  //  #####  ##     ##     #####   ####    ##    ####
  //
  //======================================================

  /**
   * Save the list of selected Tools.
   */
  const saveTools = useCallback(async () => {
    setIsLoading(true);
    const result = await updateDiagnostic({
      tools:
        selectedTools.length === 0
          ? ""
          : selectedTools.map((st) => ({
              id: st.id,
              configuration: st.configuration,
            })),
    });
    setIsLoading(false);

    if (!result.success) {
      alert(result.message || "Une erreur est survenue, veuillez réessayer.");
    }
  }, [selectedTools]);

  /**
   * When a tool is evaluated, save the list of evaluated Tools.
   */
  useEffect(() => {
    if (isFirstRender) {
      return;
    }

    saveTools();
  }, [selectedTools]);

  //=====================================================
  //
  //  #####  ##   ##  #####  ##     ##  ######   ####
  //  ##     ##   ##  ##     ####   ##    ##    ##
  //  #####  ##   ##  #####  ##  ## ##    ##     ###
  //  ##      ## ##   ##     ##    ###    ##       ##
  //  #####    ###    #####  ##     ##    ##    ####
  //
  //=====================================================

  /**
   * When adding a Tool to the selection.
   */
  const onSelectTool = (toolId, configuration) => {
    let newSelectedTools = [...selectedTools];

    if (newSelectedTools.find((t) => t.id === toolId)) {
      newSelectedTools = newSelectedTools.map((st) => {
        if (st.id === toolId) {
          return {
            ...st,
            configuration,
          };
        }

        return st;
      });
    } else {
      newSelectedTools.push({
        id: toolId,
        configuration,
      });
    }

    setSelectedTools(newSelectedTools);
  };

  /**
   * When clicking the "Edit" Tool button in a Practice popup.
   */
  const onEditTool = (toolId) => {
    setPopupTool(tools.find((t) => t.id === toolId));
    setToolPopupVisible(true);
  };

  /**
   * When deleting a Tool from the selection.
   */
  const onDeleteTool = (toolId) => {
    let newSelectedTools = [...selectedTools];
    newSelectedTools = newSelectedTools.filter((t) => t.id !== toolId);
    setSelectedTools(newSelectedTools);
  };

  /**
   * When adding a new Tool from the Practice popup.
   * Receives the Tool ID if existing, or only the Tool name if new/custom.
   */
  const onAddToolFromPracticePopup = async (toolIdOrToolName, custom) => {
    if (!custom) {
      onEditTool(toolIdOrToolName);
    } else {
      setIsCreatingCustomTool(true);
      setIsLoading(true);

      const result = await createTool({
        title: toolIdOrToolName,
      });

      setTools([result.data.tool, ...tools]);

      setPopupTool(result.data.tool);
      setToolPopupVisible(true);

      setIsCreatingCustomTool(false);
      setIsLoading(false);

      if (!result.success) {
        alert(result.message || "Une erreur est survenue, veuillez réessayer.");
      }
    }

    // setPracticePopupVisible(false);
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================

  return (
    <>
      <div className="mobile-hidden">
        <CurrentViewHeader
          onViewChange={setCurrentView}
          currentView={currentView}
          practices={practices}
          tools={tools}
        />
      </div>
      <div className="mobile-only">
        <StickyOnScroll
          element={() => (
            <div className="current-view-header full-width mobile-only">
              <h3>Cartes Pratiques ({practices.length})</h3>
            </div>
          )}
        />
      </div>
      {currentView === "practices" && (
        <div className="practices grid per-3">
          {practices.map((practice) => (
            <PracticeCard
              key={`practice${practice.id}`}
              tools={tools}
              practice={practice}
              selectedTools={selectedTools
                .filter((st) =>
                  st.configuration.practices.includes(practice.id)
                )
                .map((st) => st.id)}
              onPopupOpen={() => {
                setPopupPractice(practice);
                setPracticePopupVisible(true);
              }}
              withContainer={true}
            />
          ))}
        </div>
      )}
      {currentView === "tools" && (
        <div className="tools grid per-3">
          {tools.map((tool) => (
            <ToolCard
              key={`tool${tool.id}`}
              tool={tool}
              selectedTool={
                selectedTools.find((st) => st.id === tool.id) || null
              }
              onPopupOpen={() => {
                setPopupTool(tool);
                setToolPopupVisible(true);
              }}
              withContainer={true}
            />
          ))}
        </div>
      )}
      {practicePopupVisible && popupPractice && (
        <PracticePopup
          tools={tools}
          visible={practicePopupVisible}
          creating={isCreatingCustomTool}
          practice={popupPractice}
          selectedTools={selectedTools
            .filter((st) =>
              st.configuration.practices.includes(popupPractice.id)
            )
            .map((st) => st.id)}
          onClose={() => {
            setPracticePopupVisible(false);
            setPopupPractice(null);
          }}
          onEditTool={onEditTool}
          onDeleteTool={onDeleteTool}
          onAddTool={onAddToolFromPracticePopup}
        />
      )}
      {toolPopupVisible && popupTool && (
        <ToolPopup
          tools={tools}
          visible={toolPopupVisible}
          tool={popupTool}
          fromPractice={popupPractice}
          selectedTool={
            selectedTools.find((st) => st.id === popupTool.id) || null
          }
          onClose={() => {
            setToolPopupVisible(false);
            setPopupTool(null);
          }}
          onFormFinish={onSelectTool}
        />
      )}
      <CurrentSelectionSidebar
        practices={practices}
        tools={tools}
        selectedTools={selectedTools}
        onPracticeClick={(practice) => {
          setPopupPractice(practice);
          setPracticePopupVisible(true);
        }}
        onToolClick={(tool) => {
          setPopupTool(tool);
          setToolPopupVisible(true);
        }}
      />
      <StepFooter currentStep="outils" />
    </>
  );
};

export default OutilsSelector;

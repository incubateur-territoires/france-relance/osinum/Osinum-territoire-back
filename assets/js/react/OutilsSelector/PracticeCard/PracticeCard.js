import React, { useState } from "react";

const PracticeCard = ({
  tools,
  practice,
  onPopupOpen,
  selectedTools,
  withContainer = false,
}) => {
  const isSelected = selectedTools.length > 0;

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  const CardContent = () => (
    <article
      className={`mini-card practice${
        isSelected ? " selected" : ""
      } theme-vert-1`}
    >
      <div className="icon">
        <svg aria-hidden className="icon ecran">
          <use xlinkHref="#ecran"></use>
        </svg>
      </div>
      <div className="card-content" onClick={onPopupOpen}>
        <p className="pre-title">Carte Pratique</p>
        <h3>{practice.title}</h3>
        {isSelected && (
          <ol className="tools-list">
            <li className="mini-title">Mes outils</li>
            {selectedTools.map((selectedToolId) => {
              const tool = tools.find((t) => t.id === selectedToolId);

              return (
                <li
                  key={`tool${selectedToolId}`}
                  className="button theme-bleu-1"
                >
                  {tool.title}
                </li>
              );
            })}
          </ol>
        )}
        <button className={isSelected ? "" : ""}>
          {isSelected ? (
            <>
              Ajouter un outil{" "}
              <svg aria-hidden="true" className="icon plus">
                <use xlinkHref="#plus"></use>
              </svg>
            </>
          ) : (
            <>
              J'ai un outil{" "}
              <svg aria-hidden="true" className="icon plus">
                <use xlinkHref="#plus"></use>
              </svg>
            </>
          )}
        </button>
      </div>
    </article>
  );

  return withContainer ? (
    <div>
      <CardContent />
    </div>
  ) : (
    <CardContent />
  );
};

export default PracticeCard;

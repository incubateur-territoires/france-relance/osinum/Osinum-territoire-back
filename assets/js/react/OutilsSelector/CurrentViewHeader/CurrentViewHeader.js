import React from "react";

const CurrentViewHeader = ({ onViewChange, currentView, practices, tools }) => {
  return (
    <div className="current-view-header block bg-blanc mb250">
      <div className="inline-buttons mb125">
        <button
          className={currentView === "practices" ? "theme-vert-1-dark" : ""}
          onClick={() => onViewChange("practices")}
        >
          Cartes Pratiques ({practices.length})
        </button>
        <button
          className={
            currentView === "tools"
              ? "theme-bleu-1-dark"
              : "theme-bleu-1-dark-hover color-bleu-1-hover"
          }
          onClick={() => onViewChange("tools")}
        >
          Cartes Outils ({tools.length})
        </button>
      </div>
      {currentView === "practices" && (
        <p className="mb0 mt50">
          Quels outils utilisez-vous pour les pratiques suivantes :
        </p>
      )}
      {currentView === "tools" && (
        <p className="mb0 mt50">
          Quelles pratiques avez-vous avec les outils numériques suivants :
        </p>
      )}
    </div>
  );
};

export default CurrentViewHeader;

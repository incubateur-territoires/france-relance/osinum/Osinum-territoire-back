import React from "react";

const GroupLabel = ({ group, withTitle = false, className = "" }) => {
  return (
    <div className={`icon ${className} ${withTitle ? "with-title" : ""}`}>
      <svg aria-hidden className={`icon ${group.icon}`}>
        <use xlinkHref={`#${group.icon}`}></use>
      </svg>
      {withTitle && <span>{group.title}</span>}
    </div>
  );
};

export default GroupLabel;

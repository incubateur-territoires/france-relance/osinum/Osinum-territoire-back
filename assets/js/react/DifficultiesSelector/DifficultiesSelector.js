import React, { useState, useEffect, useCallback } from "react";
import { createDiagnostic, updateDiagnostic } from "../../utils/ajax";
import DifficultyCard from "./DifficultyCard/DifficultyCard";

import { useIsFirstRender, usePrevious } from "../../utils/hooks";
import FiltersSidebar from "./FiltersSidebar/FiltersSidebar";
import { getInitialDiagnosticSelectedDifficulties } from "../../utils/defaults";
import CurrentViewHeader from "./CurrentViewHeader/CurrentViewHeader";
import StepFooter from "../StepFooter/StepFooter";
import { getDifficulties } from "../../utils/data";
import StickyOnScroll from "../StickyOnScroll/StickyOnScroll";

const DifficultiesSelector = () => {
  const isFirstRender = useIsFirstRender();
  const [difficulties, setDifficulties] = useState(getDifficulties());
  const [activeGroupFilter, setActiveGroupFilter] = useState(null);
  const [isDisplayingSelection, displaySelection] = useState(false);
  const [currentlyUsedFilter, setCurrentlyUsedFilter] = useState(null);
  const previousActiveGroupFilter = usePrevious(activeGroupFilter);
  const previousDisplayingSelection = usePrevious(isDisplayingSelection);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedDifficulties, setSelectedDifficulties] = useState(
    getInitialDiagnosticSelectedDifficulties()
  );

  //======================================================
  //
  //  #####  #####  #####  #####   ####  ######   ####
  //  ##     ##     ##     ##     ##       ##    ##
  //  #####  #####  #####  #####  ##       ##     ###
  //  ##     ##     ##     ##     ##       ##       ##
  //  #####  ##     ##     #####   ####    ##    ####
  //
  //======================================================

  /**
   * Save the list of selected Difficulties.
   */
  const saveDifficulties = useCallback(async () => {
    setIsLoading(true);
    const result = await updateDiagnostic({
      difficulties:
        selectedDifficulties.length === 0
          ? ""
          : selectedDifficulties.map((s) => s.id),
    });
    setIsLoading(false);

    if (!result.success) {
      alert(result.message || "Une erreur est survenue, veuillez réessayer.");
    }
  }, [selectedDifficulties]);

  /**
   * When selection changes, save the list of selected Difficulties.
   */
  useEffect(() => {
    if (isFirstRender) {
      return;
    }

    saveDifficulties();
  }, [selectedDifficulties]);

  /**
   * Change the list of Difficulties when the activeGroupFilter changes.
   */
  useEffect(() => {
    // Changing the group filter.
    if (
      currentlyUsedFilter === "group" /*&&
      previousActiveGroupFilter !== activeGroupFilter*/
    ) {
      if (activeGroupFilter) {
        setDifficulties(
          getDifficulties().filter((s) => s.groups.includes(activeGroupFilter))
        );
      } else {
        setDifficulties(getDifficulties());
      }

      displaySelection(false);
    }

    // Changing the "Voir ma sélection".
    else if (
      currentlyUsedFilter === "manual" &&
      previousDisplayingSelection !== isDisplayingSelection
    ) {
      if (isDisplayingSelection) {
        setDifficulties(selectedDifficulties);
      } else {
        setDifficulties(getDifficulties());
      }

      setActiveGroupFilter(null);
    }
  }, [activeGroupFilter, isDisplayingSelection]);

  //=====================================================
  //
  //  #####  ##   ##  #####  ##     ##  ######   ####
  //  ##     ##   ##  ##     ####   ##    ##    ##
  //  #####  ##   ##  #####  ##  ## ##    ##     ###
  //  ##      ## ##   ##     ##    ###    ##       ##
  //  #####    ###    #####  ##     ##    ##    ####
  //
  //=====================================================

  /**
   * When a Difficulty card is selected.
   */
  const onCardSelect = (difficulty) => {
    setSelectedDifficulties([...selectedDifficulties, difficulty]);
  };

  /**
   * When a Difficulty card is unselected.
   */
  const onCardUnselect = (difficulty) => {
    setSelectedDifficulties(
      selectedDifficulties.filter((s) => s.id !== difficulty.id)
    );
  };

  /**
   * When clicking the "Voir ma sélection" button.
   */
  const onDisplaySelection = () => {
    setCurrentlyUsedFilter("manual");
    displaySelection(!isDisplayingSelection);

    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  /**
   * When clicking a single group filter in the sidebar.
   */
  const onFilterClick = (groupId) => {
    setCurrentlyUsedFilter("group");
    displaySelection(false);

    if (activeGroupFilter === groupId) {
      setActiveGroupFilter(null);
    } else {
      setActiveGroupFilter(groupId);
    }

    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================

  return (
    <>
      <StickyOnScroll
        element={() => (
          <CurrentViewHeader
            isDisplayingSelection={isDisplayingSelection}
            activeGroupFilter={activeGroupFilter}
            selectedDifficulties={selectedDifficulties}
          />
        )}
      />
      <div className="difficulties grid per-3">
        {difficulties.map((difficulty) => (
          <DifficultyCard
            key={`difficulty${difficulty.id}`}
            difficulty={difficulty}
            isSelected={
              !!selectedDifficulties.find((s) => s.id === difficulty.id)
            }
            onSelect={onCardSelect}
            onUnselect={onCardUnselect}
            withContainer={true}
          />
        ))}
      </div>
      <FiltersSidebar
        isDisplayingSelection={isDisplayingSelection}
        activeGroupFilter={activeGroupFilter}
        selectedDifficulties={selectedDifficulties}
        onDisplaySelection={onDisplaySelection}
        onFilterClick={onFilterClick}
      />
      <StepFooter currentStep="difficultes" />
    </>
  );
};

export default DifficultiesSelector;

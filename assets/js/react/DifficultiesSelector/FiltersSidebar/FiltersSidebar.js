import React, { useState, useEffect } from "react";
import { getDifficulties, getGroups } from "../../../utils/data";
import GroupLabel from "../GroupLabel/GroupLabel";

const FiltersSidebar = ({
  isDisplayingSelection,
  onDisplaySelection,
  selectedDifficulties,
  onFilterClick,
  activeGroupFilter,
}) => {
  const [groups, setGroups] = useState(getGroups());
  const [currentlyDisplayingMobileFilters, displayMobileFilters] =
    useState(false);

  // When displaying mobile filters, add a "displaying-filters" class to the body.
  useEffect(() => {
    if (currentlyDisplayingMobileFilters) {
      document.body.classList.add("displaying-filters");
    } else {
      document.body.classList.remove("displaying-filters");
    }
  }, [currentlyDisplayingMobileFilters]);

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  return (
    <aside
      className={`widget-sidebar fixed-on-mobile ${
        currentlyDisplayingMobileFilters ? "current-view-filters" : ""
      }`}
    >
      <div className="widget-sidebar__inner">
        <div className="selection">
          <h3>
            Ma sélection de cartes ({selectedDifficulties.length}/
            {getDifficulties().length})
          </h3>
          <button
            className={
              isDisplayingSelection ? "style-noir-hover" : "style-noir"
            }
            onClick={onDisplaySelection}
          >
            Voir ma sélection
            <svg aria-hidden="true" className="icon oeil">
              <use xlinkHref="#oeil"></use>
            </svg>
          </button>
          {selectedDifficulties.length === 0 && (
            <p className="mt50 fsi">Pas de sélection pour le moment.</p>
          )}
        </div>
        <div className="groups">
          <h3>Afficher par thématiques</h3>
          <ol>
            <li className="show-all">
              <button
                className={
                  !activeGroupFilter && !isDisplayingSelection
                    ? "style-noir-hover"
                    : "style-noir"
                }
                onClick={(e) => {
                  onFilterClick(null);
                  e.target.blur();

                  if (currentlyDisplayingMobileFilters) {
                    displayMobileFilters(false);
                  }
                }}
              >
                Toutes les thématiques
              </button>
            </li>
            {groups.map((group) => {
              const selectedDifficultiesInThisGroup =
                selectedDifficulties.filter((s) =>
                  s.groups.includes(group.id)
                ).length;
              const allDifficultiesInThisGroup = getDifficulties().filter((s) =>
                s.groups.includes(group.id)
              ).length;

              const hasActiveClass = activeGroupFilter === group.id; /*||
                (isDisplayingSelection && selectedDifficultiesInThisGroup > 0)*/

              const hasDarkClass = selectedDifficultiesInThisGroup > 0;

              return (
                <li key={`group${group.id}`}>
                  <button
                    onClick={() => {
                      onFilterClick(group.id);

                      if (currentlyDisplayingMobileFilters) {
                        displayMobileFilters(false);
                      }
                    }}
                    className={
                      hasActiveClass
                        ? `theme-${group.color} theme-${group.color}-light no-hover`
                        : hasDarkClass
                        ? `theme-${group.color}-dark theme-${group.color}-light-hover`
                        : `theme-${group.color}-outline theme-${group.color}-light-hover`
                    }
                  >
                    <GroupLabel group={group} />
                    {group.title}
                    <span className="counter">
                      ({selectedDifficultiesInThisGroup}/
                      {allDifficultiesInThisGroup})
                    </span>
                  </button>
                </li>
              );
            })}
          </ol>
        </div>
        <nav className="mobile-step-navigation bottom">
          <header>
            <button
              className={
                currentlyDisplayingMobileFilters
                  ? "theme-vert-1-dark active"
                  : ""
              }
              onClick={() =>
                displayMobileFilters(!currentlyDisplayingMobileFilters)
              }
            >
              Filtrer
            </button>
            <button
              onClick={() => {
                onDisplaySelection();
                displayMobileFilters(false);
              }}
            >
              {!isDisplayingSelection ? "Ma sélection" : "Toutes les cartes"}
            </button>
          </header>
          <a
            className="button next"
            href={window.OsinumTerritoire.diagnostic.permalinks.results}
          >
            Résultats
            <svg aria-hidden="true" className="icon fleche-droite">
              <use xlinkHref="#fleche-droite"></use>
            </svg>
          </a>
        </nav>
      </div>
    </aside>
  );
};

export default FiltersSidebar;

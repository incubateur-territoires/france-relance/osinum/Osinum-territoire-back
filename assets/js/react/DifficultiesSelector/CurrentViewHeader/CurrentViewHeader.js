import React, { useState } from "react";
import { getDifficulties, getGroups } from "../../../utils/data";

const CurrentViewHeader = ({
  isDisplayingSelection,
  activeGroupFilter,
  selectedDifficulties,
}) => {
  const [currentlyDisplayingMobileDescription, displayMobileDescription] =
    useState(false);

  /**
   * Default view: no filter, not displaying my selection.
   */
  if (!activeGroupFilter && !isDisplayingSelection) {
    return (
      <div className="current-view-header full-width displaying-all">
        <h3>Toutes les thématiques - Cartes ({getDifficulties().length})</h3>
      </div>
    );
  }

  /**
   * My selection view.
   */
  if (isDisplayingSelection) {
    return (
      <div className="current-view-header noir displaying-selected-difficulties">
        <h3>
          Ma sélection - Cartes ({selectedDifficulties.length}/
          {getDifficulties().length})
        </h3>
      </div>
    );
  }

  /**
   * Active filter view.
   */
  if (activeGroupFilter) {
    const group = getGroups().find((t) => t.id === activeGroupFilter);
    const difficultiesWithThisGroup = getDifficulties().filter((s) =>
      s.groups.includes(activeGroupFilter)
    ).length;

    return (
      <div
        onClick={() =>
          displayMobileDescription(!currentlyDisplayingMobileDescription)
        }
        className={`current-view-header custom-bg displaying-filtered-difficulties bg-${
          group.color
        }-dark ${
          currentlyDisplayingMobileDescription ? "displaying-description" : ""
        }`}
      >
        <h3 className={`color-${group.color}`}>
          {group.title} - Cartes ({difficultiesWithThisGroup})
        </h3>
        <div
          dangerouslySetInnerHTML={{ __html: group.description }}
          className="description"
        />
      </div>
    );
  }

  return null;
};

export default CurrentViewHeader;

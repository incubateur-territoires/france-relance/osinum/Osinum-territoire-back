import React, { useState } from "react";
import { getGroups } from "../../../utils/data";
import Modal from "../../Modal/Modal";
import GroupLabel from "../GroupLabel/GroupLabel";

const DifficultyCard = ({
  difficulty,
  onSelect,
  onUnselect,
  isSelected,
  withContainer = false,
}) => {
  const [popupVisible, setPopupVisible] = useState(false);
  const difficultyGroup = getGroups().find(
    (t) => t.id === difficulty.groups[0]
  );

  //=======================================================
  //
  //  #####    #####  ##     ##  ####    #####  #####
  //  ##  ##   ##     ####   ##  ##  ##  ##     ##  ##
  //  #####    #####  ##  ## ##  ##  ##  #####  #####
  //  ##  ##   ##     ##    ###  ##  ##  ##     ##  ##
  //  ##   ##  #####  ##     ##  ####    #####  ##   ##
  //
  //=======================================================
  const CardContent = () => (
    <article
      className={`mini-card mini-card-dark difficulty${
        isSelected
          ? ` selected theme-${difficultyGroup.color}-dark`
          : ` theme-${difficultyGroup.color}`
      }`}
    >
      <GroupLabel
        group={difficultyGroup}
        withTitle={false}
        className={`theme-${difficultyGroup.color}-dark`}
      />
      <div
        className="card-content"
        onClick={() => setPopupVisible(!popupVisible)}
      >
        <p className="pre-title">Carte Difficulté</p>
        <h3>{difficulty.title}</h3>
        <button
          onClick={(e) => {
            if (isSelected) {
              onUnselect(difficulty);
            } else {
              onSelect(difficulty);
            }

            e.stopPropagation();
          }}
          className={
            isSelected
              ? `active theme-${difficultyGroup.color}`
              : `theme-${difficultyGroup.color}-outline theme-${difficultyGroup.color}-hover`
          }
        >
          {isSelected ? (
            <>
              Concerné{" "}
              <svg aria-hidden="true" className="icon check">
                <use xlinkHref="#check"></use>
              </svg>
            </>
          ) : (
            <>
              Ça me concerne{" "}
              <svg aria-hidden="true" className="icon plus">
                <use xlinkHref="#plus"></use>
              </svg>
            </>
          )}
        </button>
      </div>
      <Modal
        isVisible={popupVisible}
        onClose={() => setPopupVisible(false)}
        className={`modal-${difficultyGroup.color} modal-${difficultyGroup.color}-mobile`}
        id="difficulty"
      >
        <div className="difficulty-popup-container popup-container">
          <div className={`summary theme-${difficultyGroup.color}-dark`}>
            <p className="mobile-hidden number">
              <svg aria-hidden="true" className="icon chantier">
                <use xlinkHref="#chantier"></use>
              </svg>
              Difficulté n°{difficulty.number}
            </p>
            <h3>&#8243;{difficulty.title}&#8243;</h3>
            <GroupLabel
              group={difficultyGroup}
              withTitle={true}
              className="mobile-hidden"
            />
          </div>
          <div className="body">
            <div
              className="description"
              dangerouslySetInnerHTML={{ __html: difficulty.description }}
            />
            <p className="mt200 mb50">
              <strong>Ma structure vit cette contrainte :</strong>
            </p>
            <div className="inline-buttons">
              <button
                className={
                  isSelected
                    ? `theme-${difficultyGroup.color}-dark`
                    : `theme-${difficultyGroup.color}-outline theme-${difficultyGroup.color}-hover`
                }
                onClick={() => {
                  if (!isSelected) {
                    onSelect(difficulty);
                    setPopupVisible(false);
                  }
                }}
              >
                Ça me concerne
                <svg aria-hidden="true" className="icon check">
                  <use xlinkHref="#check"></use>
                </svg>
              </button>
              <button
                className={
                  !isSelected
                    ? `theme-${difficultyGroup.color}-dark`
                    : `theme-${difficultyGroup.color}-outline theme-${difficultyGroup.color}-hover`
                }
                onClick={() => {
                  if (isSelected) {
                    onUnselect(difficulty);
                    setPopupVisible(false);
                  }
                }}
              >
                Ne me concerne pas
                <svg aria-hidden="true" className="icon fermer">
                  <use xlinkHref="#fermer"></use>
                </svg>
              </button>
            </div>
            {/* <p className="small mt75">
              * Des suggestions de ressources et outils vous seront faites en
              fonction de votre sélection de cartes.
            </p> */}
          </div>
        </div>
      </Modal>
    </article>
  );

  return withContainer ? (
    <div>
      <CardContent />
    </div>
  ) : (
    <CardContent />
  );
};

export default DifficultyCard;

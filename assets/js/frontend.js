import React from "react";
import { render } from "react-dom";
import cookies from "cookiesjs";
import MicroModal from "micromodal";
import copy from "copy-to-clipboard";
import printJS from "print-js";
import { default as ReactModal } from "react-modal";

import "../fonts/OpenSans-Regular-normal.js";
import "../fonts/OpenSans-bold.js";
import "../fonts/Colaborate-bold.js";
import "../fonts/Colaborate-normal.js";

import DiagnosticCreation from "./react/DiagnosticCreation/DiagnosticCreation";
import SituationsSelector from "./react/SituationsSelector/SituationsSelector";
import CriteresSelector from "./react/CriteresSelector/CriteresSelector";
import OutilsSelector from "./react/OutilsSelector/OutilsSelector";
import ResultatsOutilsComparisonTable from "./react/ResultatsOutilsComparisonTable/ResultatsOutilsComparisonTable";

//========================================================
//
//      ##   #####   ##   ##  #####  #####    ##    ##
//      ##  ##   ##  ##   ##  ##     ##  ##    ##  ##
//      ##  ##   ##  ##   ##  #####  #####      ####
//  ##  ##   #####   ##   ##  ##     ##  ##      ##
//   ####   ##        #####   #####  ##   ##     ##
//
//========================================================
import "./modules/popups";
import { capitalizeFirstLetter } from "./utils/helpers";
import DifficultiesSelector from "./react/DifficultiesSelector/DifficultiesSelector";
import {
  findDiagnosticByEmail,
  sendUserRequest,
  updateDiagnostic,
} from "./utils/ajax";

(function ($) {
  /**
   * Document ready.
   */
  $(function () {
    /**
     * When visiting a Diagnostic step, store a cookie (so that we can hide the intro section next time).
     */
    if (typeof OsinumTerritoire.current_step !== "undefined") {
      const key = `OsiterVisitedStep${capitalizeFirstLetter(
        OsinumTerritoire.current_step
      )}`;

      if (!cookies(key)) {
        cookies({ [key]: Math.round(Date.now() / 1000) });
      }
    }
  });

  /**
   * Diagnostic find by email.
   */
  $(document).on(
    "submit",
    "#diagnostic-find-by-email-container",
    async function (e) {
      e.preventDefault();

      const result = await findDiagnosticByEmail($("#author-email").val());

      if (!result.success) {
        alert(result.message || "Une erreur est survenue, veuillez réessayer.");
      } else {
        if (result.data.found > 0) {
          MicroModal.close("diagnostic-find-by-email");
          alert(
            "Un e-mail contenant la liste de vos diagnostics précédemment créés vient de vous être envoyé."
          );
        } else {
          alert(
            "Aucun diagnostic n'a été trouvé pour cette adresse e-mail. Veuillez vérifier l'orthographe de l'adresse mail indiquée."
          );
        }
      }
    }
  );

  /**
   * User request forms (attend/organize event/contact Osinum).
   */
  $(document).on("submit", ".user-request-container", async function (e) {
    e.preventDefault();

    const modalId = $(this).parents(".modal").eq(0).attr("id");
    const type = $(this).find("input.type-field").val();
    const email = $(this).find("input.email-field").val();
    const result = await sendUserRequest(type, email);

    if (!result.success) {
      alert(result.message || "Une erreur est survenue, veuillez réessayer.");
    } else {
      MicroModal.close(modalId);
      alert(
        "Votre demande a bien été envoyée. Nous vous répondrons dans les plus brefs délais."
      );
    }
  });

  /**
   * Diagnostic permalink form.
   */
  $(document).on("submit", "#diagnostic-permalink", function (e) {
    e.preventDefault();

    window.location.href = $(this).find("input#permalink").val();
  });

  /**
   * Diagnostic widget intro panel.
   */
  if ($("section.intro-panel").length > 0) {
    $("section.intro-panel").on("click", "button.access-widget", function () {
      $("section.intro-panel").hide();
      $("section.widget-panel").show();
    });

    if (!!OsinumTerritoire.bypassIntro) {
      $("section.intro-panel").hide();
      $("section.widget-panel").show();
    }
  }

  /**
   * Lead form submission.
   */
  $(document).on(
    "submit",
    "#diagnostic-lead-form-container",
    async function (e) {
      e.preventDefault();

      const result = await updateDiagnostic({
        lead_form: $(this).serializeArray(),
      });

      if (!result.success) {
        alert(result.message || "Une erreur est survenue, veuillez réessayer.");
      } else {
        MicroModal.close("diagnostic-lead-form");
      }
    }
  );

  /**
   * Liens "Copier" pour enregistrer dans le presse-papier.
   */
  $("[data-copy-clipboard]").on("click", function (e) {
    e.preventDefault();
    copy($(this).data("copy-clipboard"));
    const currentText = $(this).html();
    $(this).html(
      'Copié <svg aria-hidden="true" class="icon check"><use xlink:href="#check"></use></svg>'
    );
    setTimeout(() => {
      $(this).html(currentText);
    }, 2500);
  });

  /**
   * Automatically show the Lead form popup on the results page, if not already done.
   */
  if (
    OsinumTerritoire.current_step === "resultats" &&
    !window.OsinumTerritoire.diagnostic.has_completed_lead_form
  ) {
    MicroModal.show("diagnostic-lead-form");
  }

  /**
   * Mobile navigation.
   */
  $("[data-open-mobile-navigation]").click(function () {
    $("nav#site-navigation").slideToggle();
    $("html, body").animate({ scrollTop: 0 }, "slow");
  });

  /**
   * Clickable card shortcut.
   */
  $(".clickable-card").on("click", function () {
    const $button = $(this).find("a.button");

    if ($button.length > 0) {
      window.location.href = $button.attr("href");
    }
  });

  /**
   * Revealer.
   */
  $("[data-reveal]").click(function () {
    const selector = $(this).data("reveal");
    $(this).toggleClass("revealing");
    $(selector).slideToggle();
  });

  /**
   * Tabs.
   */
  $(".tabs li").click(function () {
    if ($(this).hasClass("active")) {
      return;
    }

    const index = $(this).index();
    const $tabs = $(this).parents(".tabs").eq(0);
    const $tabContents = $(".tab-content");

    $tabs.find("li").removeClass("active");
    $(this).addClass("active");
    $tabContents.filter(".active").removeClass("active");
    $tabContents.eq(index).addClass("active");
  });

  /**
   * Print button.
   */
  $("[data-print]").click(function () {
    printJS({
      printable: $(this).data("print"),
      type: "html",
      css: [window.OsinumTerritoire.assets.css_url],
      style: "* { font-family: sans-serif; font-size: 10px; }",
    });
  });

  /**
   * Save as PDF button.
   */
  $("[data-save]").click(function () {
    window.print();
  });

  /**
   * Fake select
   */
  $(".fake-select").each(function () {
    $(this).wrap('<div class="fake-select-container"></div>');
    $(this).after(
      '<svg aria-hidden="true" class="icon caret-bas"><use xlink:href="#caret-bas"></use></svg>'
    );
  });

  /**
   * Auto-click/open something via URL parameter.
   */
  if (!!window.OsinumTerritoire.auto_open) {
    $(`#${window.OsinumTerritoire.auto_open}`).click();
  }
})(jQuery);

//============================================
//
//  #####    #####    ###     ####  ######
//  ##  ##   ##      ## ##   ##       ##
//  #####    #####  ##   ##  ##       ##
//  ##  ##   ##     #######  ##       ##
//  ##   ##  #####  ##   ##   ####    ##
//
//============================================

let modalAppElementSet = false;

// If we have a main#diagnostic-body element on the page, we set it as the app element
if (!modalAppElementSet && document.querySelector("main#diagnostic-body")) {
  ReactModal.setAppElement("main#diagnostic-body");
  modalAppElementSet = true;
}

const widgets = [
  { id: "diagnostic-create-container", component: DiagnosticCreation },
  { id: "situations-selector-container", component: SituationsSelector },
  { id: "criteres-selector-container", component: CriteresSelector },
  { id: "outils-selector-container", component: OutilsSelector },
  { id: "difficultes-selector-container", component: DifficultiesSelector },
  {
    id: "resultats-outils-comparaison-container",
    component: ResultatsOutilsComparisonTable,
  },
];

widgets.forEach((widget) => {
  const container = document.querySelector(`#${widget.id}`);

  if (container) {
    const Component = widget.component;

    // Set cookie.
    if (
      !!window.OsinumTerritoire &&
      !!window.OsinumTerritoire.diagnostic &&
      !!window.OsinumTerritoire.diagnostic.id &&
      !!window.OsinumTerritoire.diagnostic.token
    ) {
      cookies(
        {
          diagID: parseInt(window.OsinumTerritoire.diagnostic.id),
          diagToken: window.OsinumTerritoire.diagnostic.token,
        },
        {
          autojson: false,
        }
      );
    }

    document.addEventListener("DOMContentLoaded", () => {
      render(<Component />, container);
    });
  }
});

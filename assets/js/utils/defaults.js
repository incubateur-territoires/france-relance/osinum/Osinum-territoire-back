/**
 * Get an array of Situation objects that are selected for the current Diagnostic.
 */
export const getInitialDiagnosticSelectedSituations = () => {
  return window.OsinumTerritoire.diagnostic.data.situations
    .map((id) => window.OsinumTerritoire.situations.find((s) => s.id === id))
    .filter((s) => !!s);
};

/**
 * Get an array of Difficulty objects that are selected for the current Diagnostic.
 */
export const getInitialDiagnosticSelectedDifficulties = () => {
  return window.OsinumTerritoire.diagnostic.data.difficulties
    .map((id) => window.OsinumTerritoire.difficulties.find((d) => d.id === id))
    .filter((d) => !!d);
};

/**
 * Get an array of Criterias objects (id and rating) for the current Diagnostic.
 */
export const getInitialDiagnosticEvaluatedCriterias = () => {
  return window.OsinumTerritoire.diagnostic.data.evaluated_criterias
    .map((evaluatedCriteria) => {
      const criteria = window.OsinumTerritoire.criterias.find(
        (s) => s.id === evaluatedCriteria.id
      );

      if (!criteria) {
        return null;
      }

      return {
        ...criteria,
        rating: evaluatedCriteria.rating || 0,
      };
    })
    .filter((s) => !!s);
};

/**
 * Get an array of selected Tools for the current Diagnostic.
 */
export const getInitialDiagnosticSelectedTools = () => {
  return window.OsinumTerritoire.diagnostic.data.selected_tools.filter(
    (st) => !!window.OsinumTerritoire.tools.find((t) => t.id === st.id)
  );
};

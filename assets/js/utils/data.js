export const getDiagnosticSteps = () => [
  {
    slug: "situations",
    icon: "support",
    title: "Exercice Situations",
    duration: 6,
    link: window.OsinumTerritoire.diagnostic.steps.situations.permalink,
    completed: window.OsinumTerritoire.diagnostic.steps.situations.completed,
  },
  {
    slug: "criteres",
    icon: "etoile",
    title: "Exercice Critères",
    duration: 5,
    link: window.OsinumTerritoire.diagnostic.steps.criteres.permalink,
    completed: window.OsinumTerritoire.diagnostic.steps.criteres.completed,
  },
  {
    slug: "outils",
    icon: "ecran",
    title: "Exercice Outils",
    duration: 10,
    link: window.OsinumTerritoire.diagnostic.steps.outils.permalink,
    completed: window.OsinumTerritoire.diagnostic.steps.outils.completed,
  },
  {
    slug: "difficultes",
    icon: "chantier",
    title: "Exercice Difficultés",
    duration: 5,
    link: window.OsinumTerritoire.diagnostic.steps.difficultes.permalink,
    completed: window.OsinumTerritoire.diagnostic.steps.difficultes.completed,
  },
];

export const getSituations = () => {
  return window.OsinumTerritoire.situations.filter((s) => s && s.id);
};

export const getDifficulties = () => {
  return window.OsinumTerritoire.difficulties.filter((d) => d && d.id);
};

export const getTools = () => {
  return window.OsinumTerritoire.tools.filter((t) => t && t.id);
};

export const getPractices = () => {
  return window.OsinumTerritoire.practices || [];
};

export const getTopics = () => {
  return window.OsinumTerritoire.topics || [];
};

export const getGroups = () => {
  return window.OsinumTerritoire.groups || [];
};

export const getButtonRatingLabel = (type = "", index = 1) => {
  const matrix = {
    satisfaction: [
      { value: -2, label: "Insatisfait", icon: "sad" },
      { value: -1, label: "Peu satisfait", icon: "neutral" },
      { value: 2, label: "Très satisfait", icon: "smiley" },
    ],
    frequency: [
      { value: -2, label: "Rarement", icon: "bar1" },
      { value: 1, label: "Régulièrement", icon: "bar2" },
      { value: 2, label: "Au quotidien", icon: "bar3" },
    ],
    criteria: [
      { value: -1, label: "Plutôt non" },
      { value: 0, label: "Je ne sais pas" },
      { value: 1, label: "Plutôt oui" },
    ],
  };

  if (matrix[type]) {
    const label = matrix[type][index];

    if (label) {
      return label;
    }
  }

  return { value: index, label: index + 1 };
};

/**
 * Tool description/criterias questions.
 */
export const toolCriteriasQuestions = [
  {
    id: "securite",
    question: "L'outil est sécurisé :",
  },
  {
    id: "open-source",
    question: "L'outil est libre, open source :",
  },
  {
    id: "hebergement-europeen",
    question: "L'outil est hébergé en Europe :",
  },
  {
    id: "vie-privee",
    question: "L'outil respecte la vie privée :",
  },
  {
    id: "identifiant-commun",
    question:
      "L'accès à l'outil est sous un identifiant commun à d'autres outils :",
  },
  {
    id: "en-francais",
    question: "L'outil est documenté et traduit en français :",
  },
  {
    id: "fonctionne-smartphone",
    question: "L'outil fonctionne sur un smartphone :",
  },
  {
    id: "travail-distance",
    question: "L'outil permet le travail à distance :",
  },
  {
    id: "sobriete",
    question: "L'outil est sobre :",
  },
  {
    id: "ergonomique",
    question: "L'outil a une bonne ergonomie :",
  },
];

/**
 * Get a usage cell data.
 */
export const getUsageCellData = (slug) => {
  const matrix = [
    { slug: "satisfaction:-2", label: "Insatisfait", icon: "sad" },
    { slug: "satisfaction:-1", label: "Peu satisfait", icon: "neutral" },
    { slug: "satisfaction:2", label: "Très satisfait", icon: "smiley" },
    { slug: "frequency:-2", label: "Rarement", icon: "bar1" },
    { slug: "frequency:1", label: "Régulièrement", icon: "bar2" },
    { slug: "frequency:2", label: "Au quotidien", icon: "bar3" },
  ];

  return matrix.find((m) => m.slug === slug);
};

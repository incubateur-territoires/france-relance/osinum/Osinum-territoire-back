import axios from "axios";

/**
 * Send an AJAX call to the main plugin API public route (POST).
 */
export const doAjaxCall = async (
  action,
  data,
  method = "post",
  hasFileUpload = false
) => {
  const dataKey = hasFileUpload ? "data" : "params";

  const config = {
    url: `${window.OsinumTerritoire.api.rest_url}${action}`,
    method: method,
    headers: {
      "Content-Type": hasFileUpload
        ? "multipart/form-data"
        : "application/json",
      "X-WP-Nonce": window.OsinumTerritoire.api.nonce,
    },
    timeout: 30 * 1000,
    [dataKey]: data,
  };

  return axios.request(config).then(
    (response) => {
      return Promise.resolve(response.data);
    },
    (error) => {
      return Promise.reject(error);
    }
  );
};

/**
 * Wrapper function calling the AJAX route and returning an object based on the Promise.
 */
export const ajax = async (
  action,
  data = {},
  method = "post",
  hasFileUpload = false
) => {
  try {
    document.getElementById("page").classList.add("is-loading-ajax");
    const payload = await doAjaxCall(action, data, method, hasFileUpload);
    document.getElementById("page").classList.remove("is-loading-ajax");

    return {
      success: true,
      ...payload,
    };
  } catch (error) {
    return {
      error: error.message,
    };
  }
};

/**
 * Create a new Diagnostic.
 */
export const createDiagnostic = async (data) => {
  return await ajax("diagnostic-create", data);
};

/**
 * Find diagnostic by e-mail.
 */
export const findDiagnosticByEmail = async (email) => {
  return await ajax("diagnostic-find-by-email", { email });
};

/**
 * Send a user request (attend/organize workshop or contact Osinum).
 */
export const sendUserRequest = async (type, email) => {
  return await ajax("user-request", { type, email });
};

/**
 * Update a Diagnostic.
 */
export const updateDiagnostic = async (data) => {
  return await ajax("diagnostic-update", {
    diagnostic_id: window.OsinumTerritoire.diagnostic.id || null,
    diagnostic_token: window.OsinumTerritoire.diagnostic.token || null,
    ...data,
    step: window.OsinumTerritoire.current_step || null,
  });
};

/**
 * Step #3: create a new Tool.
 */
export const createTool = async (data) => {
  return await ajax("tool-create", {
    diagnostic_id: window.OsinumTerritoire.diagnostic.id || null,
    diagnostic_token: window.OsinumTerritoire.diagnostic.token || null,
    ...data,
  });
};

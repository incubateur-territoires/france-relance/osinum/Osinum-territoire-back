import MicroModal from "micromodal";

(function ($) {
  /**
   * Document ready.
   */
  $("button.modal__close").click(function (e) {
    e.preventDefault();
  });

  /**
   * AJAX popups logic.
   */
  $("body").on("click", "[data-popup]", function () {
    const $el = $(this);
    let popupData = $el.data("popup").split(":");
    const popupId = popupData[0];
    const popupParams = typeof popupData[1] !== undefined ? popupData[1] : null;

    MicroModal.show(popupId);

    $(window).trigger("popup-open", { id: popupId, params: popupParams });
  });

  /**
   * Intercept popup openings.
   */
  $(window).on("popup-open", function (e, data) {
    if (data.id === "terms-descriptions") {
      const termId = parseInt(data.params);

      $("#terms-descriptions .term-content")
        .hide()
        .filter(`#term-content-${termId}`)
        .show();
    }

    if (data.id === "resultats-outil-tableau" && data.params === "download") {
      $('button[data-print="outils-tableau"]').trigger("click");
    }
  });
})(jQuery);

# Osinum Territoire

Logique du back-end et des diagnostics pour le site OSINUM Territoires.

## Installation en local
- requiert PHP 8.1
- dans le dossier plugins git clone git@gitlab.bsa-web.fr:medias-cite-osinum-autodiag/plugins/diagnostic-back.git
- cd diagnostic-back
- composer install
- npm install
- npm run build
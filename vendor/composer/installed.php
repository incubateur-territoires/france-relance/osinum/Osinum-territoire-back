<?php return array(
    'root' => array(
        'name' => 'bsa/ositer',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'bd22c54b4b3fef736572bf592cb37b45752ab352',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'bsa/ositer' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'bd22c54b4b3fef736572bf592cb37b45752ab352',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'chillerlan/php-qrcode' => array(
            'pretty_version' => '4.3.3',
            'version' => '4.3.3.0',
            'reference' => '6356b246948ac1025882b3f55e7c68ebd4515ae3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../chillerlan/php-qrcode',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'chillerlan/php-settings-container' => array(
            'pretty_version' => '2.1.4',
            'version' => '2.1.4.0',
            'reference' => '1beb7df3c14346d4344b0b2e12f6f9a74feabd4a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../chillerlan/php-settings-container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'composepress/core' => array(
            'pretty_version' => '0.9.0',
            'version' => '0.9.0.0',
            'reference' => '18d546c588cf751c5251b351e112245d0467481b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../composepress/core',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'composepress/dice' => array(
            'pretty_version' => '0.6.4',
            'version' => '0.6.4.0',
            'reference' => '5a7858181935908109062109d61e43d664a117cc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../composepress/dice',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dompdf/dompdf' => array(
            'pretty_version' => 'v2.0.0',
            'version' => '2.0.0.0',
            'reference' => '79573d8b8a141ec8a17312515de8740eed014fa9',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dompdf/dompdf',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'masterminds/html5' => array(
            'pretty_version' => '2.7.5',
            'version' => '2.7.5.0',
            'reference' => 'f640ac1bdddff06ea333a920c95bbad8872429ab',
            'type' => 'library',
            'install_path' => __DIR__ . '/../masterminds/html5',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phenx/php-font-lib' => array(
            'pretty_version' => '0.5.4',
            'version' => '0.5.4.0',
            'reference' => 'dd448ad1ce34c63d09baccd05415e361300c35b4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phenx/php-font-lib',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phenx/php-svg-lib' => array(
            'pretty_version' => '0.4.1',
            'version' => '0.4.1.0',
            'reference' => '4498b5df7b08e8469f0f8279651ea5de9626ed02',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phenx/php-svg-lib',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sabberworm/php-css-parser' => array(
            'pretty_version' => '8.4.0',
            'version' => '8.4.0.0',
            'reference' => 'e41d2140031d533348b2192a83f02d8dd8a71d30',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabberworm/php-css-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wpackio/enqueue' => array(
            'pretty_version' => '2.2.1',
            'version' => '2.2.1.0',
            'reference' => '3f7b42e871d17d26cd90083cfe2b0dbc5bed0f54',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wpackio/enqueue',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);

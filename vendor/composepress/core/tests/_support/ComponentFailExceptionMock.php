<?php


use ComposePress\Core\Abstracts\Component_0_9_0_0;
use ComposePress\Core\Exception\ComponentInitFailure;

class ComponentFailExceptionMock extends Component_0_9_0_0 {
	/**
	 * @return \ComposePress\Core\Exception\ComponentInitFailure
	 */
	public function setup() {
		return new ComponentInitFailure( 'fail' );
	}
}

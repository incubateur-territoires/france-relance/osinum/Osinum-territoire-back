<?php


use ComposePress\Core\Abstracts\Component_0_9_0_0;

class ComponentFailExceptionChildrenMock extends Component_0_9_0_0 {

	private $child;
	private $child2;

	public function __construct() {
		$this->child  = new ComponentChildFailExceptionMock();
		$this->child2 = new ComponentChildFailExceptionMock();
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}
}

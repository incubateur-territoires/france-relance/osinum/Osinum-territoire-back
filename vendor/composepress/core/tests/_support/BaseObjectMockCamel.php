<?php

use ComposePress\Core\Abstracts\BaseObject_0_9_0_0;

/**
 * Class BaseObjectMock
 *
 * @package ComposePress\Core\Abstracts
 * @property string $test
 * @property string $is_test
 */
class BaseObjectMockCamel extends BaseObject_0_9_0_0 {
	private $test = 'test';
	private $is_test = 'test';
	private $get_test = 'test';

	/**
	 * @return mixed
	 */
	public function getTest() {
		return $this->test;
	}

	/**
	 * @param string $test
	 */
	public function setTest( $test ) {
		$this->test = $test;
	}

	public function init() {

	}

	/**
	 * @return string
	 */
	public function isIs_test() {
		return $this->is_test;
	}

	/**
	 * @return string
	 */
	public function getGet_test() {
		return $this->get_test;
	}

}

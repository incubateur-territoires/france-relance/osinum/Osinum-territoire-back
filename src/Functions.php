<?php

namespace OsinumTerritoire;

use OsinumTerritoire\Models\Diagnostic;
use OsinumTerritoire\Models\Criteria;
use OsinumTerritoire\Models\Tool;
use OsinumTerritoire\Models\Practice;
use OsinumTerritoire\Models\DifficultyGroup;

class Functions {
	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get a specific Diagnostic.
	 *
	 * @param WP_Post|integer $diagnostic
	 * @return Diagnostic
	 */
	public function get_diagnostic( $diagnostic ) {
		return _ositer()->get_data()->get_diagnostic( $diagnostic );
	}

	/**
	 * Get a specific Situation.
	 *
	 * @param WP_Post|integer $situation
	 * @return Situation
	 */
	public function get_situation( $situation ) {
		return _ositer()->get_data()->get_situation( $situation );
	}

	/**
	 * Get a specific Tool.
	 *
	 * @param WP_Term|integer $tool
	 * @return Tool
	 */
	public function get_tool( $tool ) {
		return _ositer()->get_data()->get_tool( $tool );
	}

	/**
	 * Get a specific Practice.
	 *
	 * @param WP_Term|integer $practice
	 * @return Practice
	 */
	public function get_practice( $practice ) {
		return _ositer()->get_data()->get_practice( $practice );
	}

	/**
	 * Get a specific Criteria.
	 *
	 * @param WP_Term|integer $criteria
	 * @return Criteria
	 */
	public function get_criteria( $criteria ) {
		return _ositer()->get_data()->get_criteria( $criteria );
	}

	/**
	 * Get a specific Difficulty.
	 *
	 * @param WP_Post|integer $difficulty
	 * @return Difficulty
	 */
	public function get_difficulty( $difficulty ) {
		return _ositer()->get_data()->get_difficulty( $difficulty );
	}

	/**
	 * Get a specific Resource.
	 *
	 * @param WP_Post|integer $resource
	 * @return Resource
	 */
	public function get_resource( $resource ) {
		return _ositer()->get_data()->get_resource( $resource );
	}

	/**
	 * Get a specific Difficulty Group.
	 *
	 * @param WP_Term|integer $difficulty_group
	 * @return DifficultyGroup
	 */
	public function get_difficulty_group( $difficulty_group ) {
		return _ositer()->get_data()->get_difficulty_group( $difficulty_group );
	}

	/**
	 * Get a global site setting from the ACF options page.
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function get_setting( $key ) {
		return get_field( $key, 'option' );
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Has the user already visited a specific Diagnostic step?
	 *
	 * @param string $step
	 * @return boolean
	 */
	public function has_already_visited( $step = '' ) {
		return (
			isset( $_COOKIE['OsiterVisitedStepPratiques'] )
			&& $_COOKIE['OsiterVisitedStepPratiques'] > 0
			&& ( ! defined( 'OSITER_ALWAYS_DISPLAY_STEP_INTRO' ) || ! OSITER_ALWAYS_DISPLAY_STEP_INTRO )
		);
	}

	//=====================
	//                     
	//  ####     #####   
	//  ##  ##  ##   ##  
	//  ##  ##  ##   ##  
	//  ##  ##  ##   ##  
	//  ####     #####   
	//                     
	//=====================

	/**
	 * Send a e-mail.
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $template
	 * @param array $data
	 * @return void
	 */
	public function send_email( $to = '', $subject = '', $template = '', $data = [] ) {
		return _ositer()->get_email()->send( $to, $subject, $template, $data );
	}

	//================================================
	//                                                
	//  #####  #####     #####   ##     ##  ######  
	//  ##     ##  ##   ##   ##  ####   ##    ##    
	//  #####  #####    ##   ##  ##  ## ##    ##    
	//  ##     ##  ##   ##   ##  ##    ###    ##    
	//  ##     ##   ##   #####   ##     ##    ##    
	//                                                
	//================================================

	/**
	 * Output a SVG icon.
	 *
	 * @param string $svg
	 * @param boolean $echo
	 * @return string
	 */
	public function icon( $svg = 'plus', $echo = true ) {
		// DomPDF does not render SVG with <use>, so inject the SVG content in an <img> tag instead.
		if ( _ositer()->get_urls()->is_rendering_pdf() ) {
			$svg_path = _ositer()->get_plugin_path() . '/dist/images/icons/' . $svg . '.svg';

			// Check if a $icon-pdf.svg file exists.
			if ( file_exists( _ositer()->get_plugin_path() . '/dist/images/icons/' . $svg . '-pdf.svg' ) ) {
				$svg_path = _ositer()->get_plugin_path() . '/dist/images/icons/' . $svg . '-pdf.svg';
			}

			$svg_content = file_get_contents( $svg_path );
			$image_tag   = '<img src="data:image/svg+xml;base64,' . base64_encode( $svg_content ) . '" class="icon ' . $svg . '" />';

			if ( $echo ) {
				echo $image_tag;
				return;
			}

			return $image_tag;
		}

		$icon = sprintf(
			'<svg aria-hidden="true" class="icon %1$s"><use xlink:href="#%1$s"></use></svg>',
			$svg
		);

		if ( ! $echo ) {
			return $icon;
		}

		echo $icon;
	}
}

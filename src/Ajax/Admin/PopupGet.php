<?php

namespace OsinumTerritoire\Ajax\Admin;

use OsinumTerritoire\Ajax\Abstracts\Route;
use OsinumTerritoire\Models\ChildcareNeed;
use OsinumTerritoire\Models\RentalNeed;
use OsinumTerritoire\Models\SaleNeed;
use OsinumTerritoire\Models\Log;

class PopupGet extends Route {
	protected $popups = [
		'diagnostic',
	];

	/**
	 * Validate the /public/popup-get/ AJAX request.
	 *
	 * @return true|\WP_Error
	 */
	public function is_valid() {
		if ( empty( $this->get_param( 'popup' ) ) || ! in_array( $this->get_param( 'popup' ), $this->popups, true ) ) {
			return new \WP_Error( 'missing_type', __( 'Le type de popup à générer est absent ou invalide.', 'ositer' ), [ 'status' => 401 ] );
		}

		return true;
	}

	/**
	 * Get the vital variables for a popup.
	 *
	 * @param string $popup
	 * @return array
	 */
	protected function get_popup_variables( $popup ) {
		switch ( $popup ) {
			default:
				return null;

			case 'diagnostic':
				return [
					'diagnostic' => ositer()->get_diagnostic( (int) $this->get_param( 'params' ) ),
				];
		}
	}

	/**
	 * Process a valid request: get the popup data.
	 *
	 * @return array
	 */
	public function process() {
		$popup     = $this->get_param( 'popup' );
		$variables = (object) $this->get_popup_variables( $popup );

		ob_start();
		include_once _ositer()->get_frontend()->template->get_template_file( "admin/popups/ajax-{$popup}" );

		return [
			'success' => true,
			'data'    => ob_get_clean(),
		];
	}
}

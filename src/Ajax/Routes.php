<?php

namespace OsinumTerritoire\Ajax;

use OsinumTerritoire\_Core\Component;

class Routes extends Component {
	const ROUTES_PREFIX = 'diagnostic/v1';

	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_filter( 'rest_url_prefix', 'modify_rest_api_url_prefix' );
	}

	/**
	 * Store our public routes slugs and classes.
	 * Namespace is "Publik" with a "k" because "Public" is a reserved word :'(
	 *
	 * @var array
	 */
	protected $public_routes = [
		'diagnostic-create'        => \OsinumTerritoire\Ajax\Publik\DiagnosticCreate::class,
		'diagnostic-update'        => \OsinumTerritoire\Ajax\Publik\DiagnosticUpdate::class,
		'tool-create'              => \OsinumTerritoire\Ajax\Publik\ToolCreate::class,
		'diagnostic-find-by-email' => \OsinumTerritoire\Ajax\Publik\DiagnosticFindByEmail::class,
		'user-request'             => \OsinumTerritoire\Ajax\Publik\UserRequest::class,
	];

	/**
	 * Store our admin routes slugs and classes.
	 *
	 * @var array
	 */
	protected $admin_routes = [
		'popup-get'   => \OsinumTerritoire\Ajax\Admin\PopupGet::class,
	];

	/**
	 * Initialize the routes.
	 */
	public function __construct() {
		add_action( 'rest_api_init', [ $this, 'register_routes' ] );
	}

	/**
	 * Get the slugs/classes for public routes.
	 *
	 * @return array
	 */
	protected function get_public_routes() {
		return (array) $this->public_routes;
	}

	/**
	 * Get the slugs/classes for admin routes.
	 *
	 * @return array
	 */
	protected function get_admin_routes() {
		return (array) $this->admin_routes;
	}

	/**
	 * Instantiate the API class in charge of validating and processing the action.
	 *
	 * @param \WP_REST_Request $request
	 * @return void
	 */
	protected function get_route_corresponding_class( $request ) {
		$context = $request->get_param( 'context' );

		switch ( $context ) {
			default:
			case 'public':
				$class = $this->get_public_routes()[ $request->get_param( 'action' ) ];
				break;

			case 'admin':
				$class = $this->get_admin_routes()[ $request->get_param( 'action' ) ];
				break;
		}

		if ( ! class_exists( $class ) ) {
			return null;
		}

		return new $class( $request );
	}

	/**
	 * Register the API routes.
	 *
	 * @return void
	 */
	public function register_routes() {
		register_rest_route(
			self::ROUTES_PREFIX,
			'(?P<context>[a-zA-Z0-9-]+)/(?P<action>[a-zA-Z0-9-]+)',
			[
				'methods'             => 'POST',
				'permission_callback' => [ $this, 'authorize_routes' ],
				'validate_callback'   => [ $this, 'validate_routes' ],
				'callback'            => [ $this, 'process_routes' ],
			]
		);
	}

	/**
	 * Authorize access to a route.
	 *
	 * @param \WP_REST_Request $request
	 * @return boolean
	 */
	public function authorize_routes( \WP_REST_Request $request ) {
		$context = $request->get_param( 'context' );
		$action  = $request->get_param( 'action' );

		switch ( $context ) {
			default:
				return true;

			case 'admin':
				return current_user_can( 'manage_options' );

			case 'public':
				return true;
		}
	}

	/**
	 * Validate public routes arguments.
	 *
	 * @param \WP_REST_Request $request
	 * @return boolean
	 */
	public function validate_routes( \WP_REST_Request $request ) {
		switch ( $request->get_param( 'context' ) ) {
			default:
			case 'public':
				return ! in_array( false, [
					'action' => in_array( $request->get_param( 'action' ), array_keys( $this->get_public_routes() ), true ),
				], true );

			case 'admin':
				return ! in_array( false, [
					'action' => in_array( $request->get_param( 'action' ), array_keys( $this->get_admin_routes() ), true ),
				], true );
		}
	}

	/**
	 * Process the /api/<action> requests.
	 *
	 * @param \WP_REST_Request $request
	 * @return void
	 */
	public function process_routes( \WP_REST_Request $request ) {
		$api_class = $this->get_route_corresponding_class( $request );

		do_action( 'ositer/ajax/before-processing', $request );

		if ( ! $api_class ) {
			$response = new \WP_Error( 'unknown_route', __( 'Cette route d\'API n\'existe pas.', 'ositer' ) );
		} else {
			$valid = $api_class->is_valid();

			if ( is_wp_error( $valid ) ) {
				$response = $valid;
			} else {
				$response = $api_class->process();
			}
		}

		do_action( 'ositer/ajax/response', $response, $request );

		return new \WP_REST_Response( $this->shape_response( $response ) );
	}

	/**
	 * Shape a response: flatten a WP_Error, or send the success object as is.
	 *
	 * @param mixed $response
	 * @return array
	 */
	protected function shape_response( $response ) {
		if ( defined( 'OSITER_DELAY_AJAX' ) && OSITER_DELAY_AJAX ) {
			sleep( 3 );
		}

		if ( is_wp_error( $response ) ) {
			$response = [
				'success' => false,
				'error'   => true,
				'code'    => $response->get_error_code(),
				'message' => $response->get_error_message(),
			];
		}

		return $response;
	}

	/**
	 * Change the REST API URL prefix from 'wp-json' to 'api'.
	 *
	 * @param string $prefix
	 * @return string
	 */
	public function modify_rest_api_url_prefix( $prefix ) {
		return 'api';
	}
}

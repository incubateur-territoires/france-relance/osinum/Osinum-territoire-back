<?php

namespace OsinumTerritoire\Ajax\Abstracts;

use OsinumTerritoire\Models\Diagnostic;

abstract class Route {
	protected $request    = null;
	// protected $diagnostic = null;

	/**
	 * Constructor.
	 *
	 * @param \WP_REST_Request $request
	 */
	public function __construct( \WP_REST_Request $request ) {
		$this->request  = $request;
		// $this->diagnostic = ositer()->get_diagnostic( get_current_user_id() );
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Does this Request have a specific parameter?
	 *
	 * @param string $key
	 * @return boolean
	 */
	public function has_param( $key ) {
		return in_array( $key, array_keys( $this->get_params() ), true );
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get WP_Rest_Request object.
	 *
	 * @return \WP_REST_Request
	 */
	public function get_request() {
		return $this->request;
	}

	/**
	 * Get a request parameter.
	 *
	 * @param string $param
	 * @return mixed
	 */
	public function get_param( $param ) {
		return $this->get_request()->get_param( $param );
	}

	/**
	 * Get all request parameters.
	 *
	 * @return array
	 */
	public function get_params() {
		return $this->get_request()->get_params();
	}

	/**
	 * Get all file parameters.
	 *
	 * @return array
	 */
	public function get_files() {
		return $this->get_request()->get_file_params();
	}

	/**
	 * Get current Diagnostic.
	 *
	 * @return Diagnostic
	 */
	// public function get_diagnostic() {
	// 	return $this->diagnostic;
	// }

	/**
	 * Get the AJAX version of the request (provided by the "x-fb-ajax" header).
	 *
	 * @return integer
	 */
	public function get_ajax_version() {
		$version = $this->get_request()->get_header( 'x-fb-ajax' );
		$version = ! empty( $version ) ? (int) $version : 1;

		return $version;
	}
}

<?php

namespace OsinumTerritoire\Ajax\Publik;

use OsinumTerritoire\Ajax\Abstracts\Route;
use OsinumTerritoire\Models\Diagnostic;
use OsinumTerritoire\Models\Tool;

class ToolCreate extends Route {
	/**
	 * Currently edited Diagnostic.
	 *
	 * @var Diagnostic
	 */
	protected $diagnostic;

	/**
	 * Validate the /public/tool-create/ AJAX request.
	 *
	 * @return true|\WP_Error
	 */
	public function is_valid() {
		if ( empty( $this->get_param( 'diagnostic_id' ) ) || empty( $this->get_param( 'diagnostic_token' ) ) ) {
			return new \WP_Error( 'missing_parameters', __( 'Veuillez indiquer un identifiant de diagnostic et son token.', 'ositer' ), [ 'status' => 403 ] );
		}

		$this->diagnostic = ositer()->get_diagnostic( (int) $this->get_param( 'diagnostic_id' ) );

		if ( ! $this->diagnostic ) {
			return new \WP_Error( 'invalid_diagnostic', __( 'Le diagnostic demandé n\'a pas été trouvé.', 'ositer' ), [ 'status' => 404 ] );
		}

		if ( ! $this->diagnostic->can_access( $this->get_param( 'diagnostic_token' ) ) ) {
			return new \WP_Error( 'access_denied', __( 'Vous ne pouvez pas éditer ce diagnostic.', 'ositer' ), [ 'status' => 404 ] );
		}

		if ( empty( $this->get_param( 'title' ) ) ) {
			return new \WP_Error( 'missing_title', __( 'Veuillez indiquer un titre d\'outil.', 'ositer' ), [ 'status' => 400 ] );
		}

		return true;
	}

	/**
	 * Process a valid request: create a Tool only for this Diagnostic.
	 *
	 * @return array
	 */
	public function process() {
		$title = sanitize_text_field( $this->get_param( 'title' ) );
		$tool  = Tool::create_for_diagnostic( $title, $this->diagnostic );

		if ( is_wp_error( $tool ) ) {
			return $tool;
		}

		return [
			'success' => true,
			'data'    => [
				'tool' => $tool->get_json(),
			]
		];
	}
}

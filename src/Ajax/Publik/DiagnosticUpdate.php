<?php

namespace OsinumTerritoire\Ajax\Publik;

use OsinumTerritoire\Ajax\Abstracts\Route;
use OsinumTerritoire\Models\Diagnostic;

class DiagnosticUpdate extends Route {
	/**
	 * Currently edited Diagnostic.
	 *
	 * @var Diagnostic
	 */
	protected $diagnostic;

	/**
	 * Validate the /public/diagnostic-update/ AJAX request.
	 *
	 * @return true|\WP_Error
	 */
	public function is_valid() {
		if ( empty( $this->get_param( 'diagnostic_id' ) ) || empty( $this->get_param( 'diagnostic_token' ) ) ) {
			return new \WP_Error( 'missing_parameters', __( 'Veuillez indiquer un identifiant de diagnostic et son token.', 'ositer' ), [ 'status' => 403 ] );
		}

		$this->diagnostic = ositer()->get_diagnostic( (int) $this->get_param( 'diagnostic_id' ) );

		if ( ! $this->diagnostic ) {
			return new \WP_Error( 'invalid_diagnostic', __( 'Le diagnostic demandé n\'a pas été trouvé.', 'ositer' ), [ 'status' => 404 ] );
		}

		if ( ! $this->diagnostic->can_access( $this->get_param( 'diagnostic_token' ) ) ) {
			return new \WP_Error( 'access_denied', __( 'Vous ne pouvez pas éditer ce diagnostic.', 'ositer' ), [ 'status' => 404 ] );
		}

		return true;
	}

	/**
	 * Process a valid request: update a Diagnostic.
	 *
	 * @return array
	 */
	public function process() {
		$step = sanitize_text_field( $this->get_param( 'step' ) );

		// Update the list of selected Situations.
		if ( $this->has_param( 'situations' ) ) {
			$situations = is_array( $this->get_param( 'situations' ) ) ? array_map( 'absint', $this->get_param( 'situations' ) ) : [];

			$this->diagnostic->set_meta( 'situations', $situations );
			$this->diagnostic->set_meta( 'situations_count', count( $situations ) );
		}

		// Update the list of evaluated Criterias.
		if ( $this->has_param( 'criterias' ) ) {
			$criterias = is_array( $this->get_param( 'criterias' ) ) ? array_map( 'json_decode', $this->get_param( 'criterias' ) ) : [];

			$this->diagnostic->set_meta( 'evaluated_criterias', $criterias );
			$this->diagnostic->set_meta( 'evaluated_criterias_count', count( $criterias ) );
		}

		// Update the list of selected Tools.
		if ( $this->has_param( 'tools' ) ) {
			$tools = is_array( $this->get_param( 'tools' ) ) ? array_map( 'json_decode', $this->get_param( 'tools' ) ) : [];
			$tools = array_map( function( $tool_data ) {
				// If we have Criterias, we need to update the custom Tool with them.
				if ( isset( $tool_data->configuration->criterias ) ) {
					$tool = ositer()->get_tool( $tool_data->id );

					if ( $tool && $tool->comes_from( $this->diagnostic ) ) {
						$tool->update_criterias( $tool_data->configuration->criterias );
					}

					// unset( $tool_data->configuration->criterias );
				}

				return $tool_data;
			}, $tools );

			$this->diagnostic->set_meta( 'selected_tools', $tools );
			$this->diagnostic->set_meta( 'selected_tools_count', count( $tools ) );
		}

		// Update the list of selected Difficulties.
		if ( $this->has_param( 'difficulties' ) ) {
			$difficulties = is_array( $this->get_param( 'difficulties' ) ) ? array_map( 'absint', $this->get_param( 'difficulties' ) ) : [];

			$this->diagnostic->set_meta( 'difficulties', $difficulties );
			$this->diagnostic->set_meta( 'difficulties_count', count( $difficulties ) );
		}

		// Update the Diagnostic lead form values.
		if ( $this->has_param( 'lead_form' ) ) {
			$values = is_array( $this->get_param( 'lead_form' ) ) ? array_map( 'json_decode', $this->get_param( 'lead_form' ) ) : [];

			$this->diagnostic->save_lead_form( $values );
		}

		do_action( 'ositer/diagnostic/updated', $this->diagnostic, $this->get_params(), $step );

		return [
			'success' => true,
		];
	}
}

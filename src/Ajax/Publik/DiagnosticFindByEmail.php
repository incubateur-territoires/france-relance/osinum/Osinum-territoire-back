<?php

namespace OsinumTerritoire\Ajax\Publik;

use OsinumTerritoire\Ajax\Abstracts\Route;
use OsinumTerritoire\Models\Diagnostic;
use OsinumTerritoire\Models\Host;

class DiagnosticFindByEmail extends Route {
	/**
	 * Validate the /public/diagnostic-find-by-email/ AJAX request.
	 *
	 * @return true|\WP_Error
	 */
	public function is_valid() {
		if ( empty( $this->get_param( 'email' ) ) ) {
			return new \WP_Error( 'missing_data', __( 'Veuillez indiquer une adresse e-mail.', 'ositer' ), [ 'status' => 400 ] );
		}

		if ( ! is_email( $this->get_param( 'email' ) ) ) {
			return new \WP_Error( 'invalid_email', __( 'Veuillez indiquer une adresse e-mail valide.', 'ositer' ), [ 'status' => 400 ] );
		}

		return true;
	}

	/**
	 * Process a valid request: create a new Diagnostic.
	 *
	 * @return array
	 */
	public function process() {
		$email       = sanitize_email( $this->get_param( 'email' ) );
		$diagnostics = _ositer()->get_data()->find_diagnostic_by_email( $email );

		if ( ! empty( $diagnostics ) ) {
			do_action( 'ositer/email/send-author-diagnostics-list', $diagnostics, $email );
		}

		return [
			'success' => true,
			'data'    => [
				'found' => count( $diagnostics ),
			]
		];
	}
}

<?php

namespace OsinumTerritoire\Ajax\Publik;

use OsinumTerritoire\Ajax\Abstracts\Route;
use OsinumTerritoire\Models\Diagnostic;
use OsinumTerritoire\Models\Host;

class DiagnosticCreate extends Route {
	/**
	 * Validate the /public/diagnostic-create/ AJAX request.
	 *
	 * @return true|\WP_Error
	 */
	public function is_valid() {
		if ( empty( $this->get_param( 'title' ) ) || empty( $this->get_param( 'email' ) ) ) {
			return new \WP_Error( 'missing_data', __( 'Veuillez indiquer un titre et une adresse e-mail.', 'ositer' ), [ 'status' => 400 ] );
		}

		if ( ! is_email( $this->get_param( 'email' ) ) ) {
			return new \WP_Error( 'invalid_email', __( 'Veuillez indiquer une adresse e-mail valide.', 'ositer' ), [ 'status' => 400 ] );
		}

		return true;
	}

	/**
	 * Process a valid request: create a new Diagnostic.
	 *
	 * @return array
	 */
	public function process() {
		$first_step = ! empty( $this->get_param( 'first_step' ) ) ? sanitize_text_field( $this->get_param( 'first_step' ) ) : '';
		$diagnostic = Diagnostic::create(
			sanitize_text_field( $this->get_param( 'title' ) ),
			[
				'email' => sanitize_email( $this->get_param( 'email' ) )
			]
		);

		if ( is_wp_error( $diagnostic ) ) {
			return $diagnostic;
		}

		return [
			'success' => true,
			'data' => [
				'diagnostic' => [
					'id'             => $diagnostic->get_id(),
					'token'          => $diagnostic->get_token(),
					'permalink'      => $diagnostic->get_private_permalink(),
					'permalink_step' => $diagnostic->get_private_permalink( $first_step ),
				],
			]
		];
	}
}

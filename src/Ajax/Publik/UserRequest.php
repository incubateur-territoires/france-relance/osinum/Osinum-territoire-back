<?php

namespace OsinumTerritoire\Ajax\Publik;

use OsinumTerritoire\Ajax\Abstracts\Route;

class UserRequest extends Route {
	/**
	 * Allowed request types.
	 *
	 * @var array
	 */
	protected $types = [ 'workshop-attend', 'workshop-organize', 'osinum-contact', ];

	/**
	 * Validate the /public/user-request/ AJAX request.
	 *
	 * @return true|\WP_Error
	 */
	public function is_valid() {
		if ( empty( $this->get_param( 'type' ) ) || ! in_array( $this->get_param( 'type' ), $this->types, true ) ) {
			return new \WP_Error( 'missing_type', __( 'Ce type de demande de contact n\'a pas été reconnu.', 'ositer' ), [ 'status' => 400 ] );
		}

		if ( empty( $this->get_param( 'email' ) ) ) {
			return new \WP_Error( 'missing_data', __( 'Veuillez indiquer une adresse e-mail.', 'ositer' ), [ 'status' => 400 ] );
		}

		if ( ! is_email( $this->get_param( 'email' ) ) ) {
			return new \WP_Error( 'invalid_email', __( 'Veuillez indiquer une adresse e-mail valide.', 'ositer' ), [ 'status' => 400 ] );
		}

		return true;
	}

	/**
	 * Process a valid request: create a new Diagnostic.
	 *
	 * @return array
	 */
	public function process() {
		$type  = sanitize_text_field( $this->get_param( 'type' ) );
		$email = sanitize_email( $this->get_param( 'email' ) );
		
		do_action( 'ositer/email/user-request', $type, $email );

		return [
			'success' => true,
		];
	}
}

<?php

namespace OsinumTerritoire;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Models\Communication;
use OsinumTerritoire\Models\Customer;

class Scheduler extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'ositer/scheduler/execute', [ $this, 'execute_action' ], 10, 2 );
	}

	/**
	 * Execute an action.
	 *
	 * @param string $action
	 * @param array $args
	 * @return void
	 */
	public function execute_action( $action, $args = [] ) {
		$method = "do_{$action}";

		if ( ! method_exists( $this, $method ) ) {
			do_action( 'ositer/scheduler/action_without_method', $action, $args );
			return;
		}

		$args_values = array_values( $args );

		$this->{$method}( ...$args_values );
	}

	/**
	 * Schedule a single action.
	 *
	 * @param string $action
	 * @param array $args
	 * @param integer|null $time
	 * @return void
	 */
	public function schedule( $action, $args = [], $time = null ) {
		if ( is_null( $time ) ) {
			$time = time() + 5;
		}

		if ( $this->is_already_scheduled( $action, $args ) ) {
			return;
		}

		as_schedule_single_action( $time, 'ositer/scheduler/execute', [ 'action' => $action, 'args' => $args ] );
	}

	/**
	 * Is an action already scheduled?
	 *
	 * @param string $action
	 * @param array $args
	 * @return boolean
	 */
	public function is_already_scheduled( $action, $args = [] ) {
		return ! empty( as_get_scheduled_actions(
			[
				'hook'     => 'ositer/scheduler/execute',
				'args'     => [ 'action' => $action, 'args' => $args ],
				'status'   => \ActionScheduler_Store::STATUS_PENDING,
				'per_page' => -1,
			]
		) );
	}

	//===========================================================
	//                                                           
	//    ###     ####  ######  ##   #####   ##     ##   ####  
	//   ## ##   ##       ##    ##  ##   ##  ####   ##  ##     
	//  ##   ##  ##       ##    ##  ##   ##  ##  ## ##   ###   
	//  #######  ##       ##    ##  ##   ##  ##    ###     ##  
	//  ##   ##   ####    ##    ##   #####   ##     ##  ####   
	//                                                           
	//===========================================================

	/**
	 * Do something…
	 *
	 * @param integer $customer_id
	 * @return void
	 */
	public function do_something( $customer_id ) {
		$customer = ositer()->get_customer( $customer_id );

		if ( ! $customer ) {
			return;
		}

		$customer->create_sendinblue_contact();
	}
}

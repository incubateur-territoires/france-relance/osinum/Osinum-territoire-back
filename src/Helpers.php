<?php

namespace OsinumTerritoire;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Admin\Logger;

class Helpers extends Component {
	/**
	 * Find first item in an array that matches a condition.
	 *
	 * @param callable $callback
	 * @param array $array
	 * @return mixed
	 */
	public static function array_find( callable $callback, array $array ) {
		foreach ( $array as $key => $value ) {
			if ( $callback( $value, $key, $array ) ) {
				return $value;
			}
		}

		return null;
	}

	/**
	 * Search something in array
	 *
	 * @param array $array
	 * @param string $key
	 * @param mixed $value
	 * @return array
	 */
	public static function search( $array, $key, $value ) {
		$results = array();

		if (is_array( $array) ) {
			if (isset( $array[$key] ) && $array[$key] == $value) {
				$results[] = $array;
			}

			foreach ( $array as $subarray) {
				$results = array_merge( $results, self::search( $subarray, $key, $value) );
			}
		}

		return $results;
	}

	/**
	 * Search by key name in multidimensional array
	 */
	public static function search_by_key( $array, $key ) {
		$results = array();

		if (is_array( $array) ) {
			if (isset( $array[$key] ) ) {
				$results[] = $array[$key];
			}

			foreach ( $array as $subarray) {
				$results = array_merge( $results, self::search_by_key( $subarray, $key) );
			}
		}

		return $results;
	}

	/**
	 * Insert element before a specific key in array
	 */
	public static function array_insert_before( $key, array &$array, $new_key, $new_value ) {
		if (array_key_exists( $key, $array) ) {
			$new = array();
			foreach ( $array as $k => $value) {
				if ( $k === $key) {
					$new[$new_key] = $new_value;
				}
				$new[$k] = $value;
			}
			return $new;
		}
		return FALSE;
	}

	/**
	 * Insert element after a specific key in array
	 */
	public static function array_insert_after( $key, array &$array, $new_key, $new_value ) {
		if ( array_key_exists( $key, $array ) ) {
			$new = array();
			foreach ( $array as $k => $value) {
				$new[$k] = $value;
				if ( $k === $key ) {
					$new[$new_key] = $new_value;
				}
			}
			return $new;
		}
		return FALSE;
	}

	//======================================================
	//
	//  ######   #####   ##  ##  #####  ##     ##   ####
	//    ##    ##   ##  ## ##   ##     ####   ##  ##
	//    ##    ##   ##  ####    #####  ##  ## ##   ###
	//    ##    ##   ##  ## ##   ##     ##    ###     ##
	//    ##     #####   ##  ##  #####  ##     ##  ####
	//
	//======================================================

	/**
	* Generates a token based on a dynamic parameter and a secret key
	* Credit & love goes to @julio
	*/
	public static function generate_token( $param, $key ) {
		return substr( md5( $param . $key ), 0, 16 );
	}

	/**
	* Check if a generated token is valid
	* Credit & love goes to @julio
	*/
	public static function check_token( $param, $key, $reference = false ) {
		if (!$reference) {
			if (isset( $_GET[ 't' ] ) ) {
				$reference = $_GET[ 't' ];
			} else {
				return false;
			}
		}

		$check = substr(md5( $param . $key), 0, 16 );

		if (hash_equals( $reference, $check) ) {
			return true;
		}

		return false;
	}

	//==============================================================================================================
	//
	//  ######  #####  ###    ###  #####   ##        ###    ######  #####        #####  ##  ##      #####   ####
	//    ##    ##     ## #  # ##  ##  ##  ##       ## ##     ##    ##           ##     ##  ##      ##     ##
	//    ##    #####  ##  ##  ##  #####   ##      ##   ##    ##    #####        #####  ##  ##      #####   ###
	//    ##    ##     ##      ##  ##      ##      #######    ##    ##           ##     ##  ##      ##        ##
	//    ##    #####  ##      ##  ##      ######  ##   ##    ##    #####        ##     ##  ######  #####  ####
	//
	//==============================================================================================================

	/**
	* Format telephone number
	*/
	public static function format_phone( $phone, $right = false, $french = false ) {
		$phone = str_replace( '+33', '0', $phone);
		$phone = preg_replace( '/\D/', '', $phone);

		if ( $right) {
			$phone = str_pad( $phone, 10, '0', STR_PAD_RIGHT);
		} else {
			$phone = str_pad( $phone, 10, '0', STR_PAD_LEFT);
		}

		$phone = substr( $phone, 0, 10 );

		if ( $french) {
			return trim( chunk_split( $phone, 2, ' ' ) );
		}

		return $phone;
	}

	/**
	 * Format a phone number to be saved in the database.
	 *
	 * @param string $phone
	 * @return string
	 */
	public static function format_phone_for_database( $phone ) {
		return preg_replace( '/\D/', '', $phone );
	}

	/**
	 * Return english week days
	 */
	public static function get_week_days() {
		return [ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ];
	}

	/**
	 * Check if URL is absolute
	 * Love goes to https://stackoverflow.com/questions/7392274/checking-for-relative-vs-absolute-paths-urls-in-php
	 */
	public static function maybe_relativize_absolute_url( $url ) {
		$pattern = "/^(?:ftp|https?|feed):\/\/(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*
		(?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@)?(?:
		(?:[a-z0-9\-\.]|%[0-9a-f]{2})+|(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\] ) )(?::[0-9]+)?(?:[\/|\?]
		(?:[\w#!:\.\?\+=&@$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})*)?$/xi";

		$absolute = (bool) preg_match( $pattern, $url);

		if (!$absolute) {
			return sprintf( '%1$s%2$s', untrailingslashit(home_url() ), $url);
		}

		return $url;
	}

	//=====================================================================
	//
	//  ######  #####      ###     ####  ##  ##  ##  ##     ##   ####
	//    ##    ##  ##    ## ##   ##     ## ##   ##  ####   ##  ##
	//    ##    #####    ##   ##  ##     ####    ##  ##  ## ##  ##  ###
	//    ##    ##  ##   #######  ##     ## ##   ##  ##    ###  ##   ##
	//    ##    ##   ##  ##   ##   ####  ##  ##  ##  ##     ##   ####
	//
	//=====================================================================

	/**
	 * Get user IP address.
	 *
	 * @return string
	 */
	public static function get_user_ip() {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

	/**
	 * Get a list of distinct values for a specific meta key for a specific post type.
	 *
	 * @param string $key
	 * @param string $post_type
	 * @return array
	 */
	public static function get_distinct_values_for_meta_key( $key, $post_type ) {
		global $wpdb;
		$values       = [];

		$values = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT DISTINCT(meta_value)
				FROM $wpdb->postmeta AS meta
				LEFT JOIN $wpdb->posts AS posts ON meta.post_id = posts.ID
				WHERE meta.meta_key = %s
				AND posts.post_type = %s",
				$key,
				$post_type
			)
		);

		if ( is_array( $values ) && ! empty( $values ) ) {
			$values = array_map( function( $value ) {
				return $value->meta_value;
			}, $values );

			asort( $values );
		}

		return $values;
	}

	/**
	 * Check if a date is following a specific format we desire.
	 *
	 * @link https://stackoverflow.com/a/44282902
	 * @param string $date
	 * @param string $format
	 * @return boolean
	 */
	public static function is_date_valid( $date, $format = 'Y-m-d' ) {
		$date_object = \DateTime::createFromFormat( $format, $date );
		return $date_object && $date_object->format( $format ) === $date;
	}

	/**
	 * Upload a file in WP media gallery
	 *
	 * @param array $file File(s) from $_FILES
	 * @param integer $attachment_parent
	 * @return int
	 */
	public static function upload_file( $file = [], $attachment_parent = 0 ) {
		require_once ABSPATH . 'wp-admin/includes/admin.php';
		return self::create_wp_file_attachment( $file, $attachment_parent );
	}

	/**
	 * Copy a distant file (from an URL) in a local file and create the proper corresponding attachment.
	 *
	 * @param string $url
	 * @param string $filename
	 * @param integer $attachment_parent
	 * @return int
	 */
	public static function import_distant_file( $url, $filename, $attachment_parent = 0 ) {
		require_once ABSPATH . 'wp-admin/includes/file.php';
		require_once ABSPATH . 'wp-admin/includes/image.php';

		$local_file = [
			'name'     => wp_basename( $filename ),
			'tmp_name' => download_url( $url ),
		];

		// If error storing temporarily, return the error.
		if ( is_wp_error( $local_file['tmp_name'] ) ) {
			return $local_file['tmp_name'];
		}

		$uploaded_file = wp_handle_sideload( $local_file, [ 'test_form' => false ] );

		if ( isset( $uploaded_file['error'] ) ) {
			return $uploaded_file['error'] || $uploaded_file['upload_error_handler'];
		}

		$filename   = $uploaded_file['file'];
		$attachment = [
			'post_mime_type' => $uploaded_file['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			'guid'           => $uploaded_file['url'],
		];
		$attachment_id   = wp_insert_attachment( $attachment, $uploaded_file['url'], $attachment_parent );
		$attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );

		wp_update_attachment_metadata( $attachment_id, $attachment_data );

		if ( 0 < intval( $attachment_id ) ) {
			return (int) $attachment_id;
		}

		return false;
	}

	/**
	 * Create WP file attachment
	 *
	 * @param array $file
	 * @param integer $attachment_parent
	 * @return int|false
	 */
	public static function create_wp_file_attachment( $file, $attachment_parent = 0 ) {
		$file_return = wp_handle_upload( $file, [ 'test_form' => false ] );

		if ( isset( $file_return['errors'] ) || isset( $file_return['upload_error_handler'] ) ) {
			return $file_return['errors'] || $file_return['upload_error_handler'];
		} else {
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			$filename   = $file_return['file'];
			$attachment = [
				'post_mime_type' => $file_return['type'],
				'post_title'     => preg_replace('/\.[^.]+$/', '', basename( $filename ) ),
				'post_content'   => '',
				'post_status'    => 'inherit',
				'guid'           => $file_return['url']
			];
			$attachment_id   = wp_insert_attachment( $attachment, $file_return['url'], $attachment_parent );
			$attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );

			wp_update_attachment_metadata( $attachment_id, $attachment_data );

			if ( 0 < intval( $attachment_id ) ) {
				return (int) $attachment_id;
			}

			return false;
		}
	}

	/**
	 * Process a CSV file using a callback function.
	 *
	 * @param string $file_path
	 * @param \Closure $callback
	 * @param string $sep
	 * @param boolean $with_headers
	 * @return void
	 */
	public static function process_csv( $file_path, $callback, $ignore_empty_cells = false, $with_headers = false, $sep = ',' ) {
		$file = fopen( $file_path, "r" );
		$r    = 0;

		while ( ( $row = fgetcsv( $file, 10000, $sep ) ) !== false ) {
			if ( $r === 0 ) {
				$headers = $row;
			} else {
				$result = (object) [ 'headers' => $headers, 'values' => (object) [] ];

				foreach ( $headers as $h => $header ) {
					if ( $ignore_empty_cells && empty( $row[ $h ] ) ) {
						continue;
					}

					$property                    = str_replace( '-', '_', sanitize_title( $header ) );
					$result->values->{$property} = $row[ $h ];
				}

				$callback( $with_headers ? $result : $result->values );
			}

			$r++;
		}

		fclose( $file );
	}

	/**
	 * Output a popup.
	 *
	 * @param string $popup_id
	 * @param string $title
	 * @return void
	 */
	public static function output_popup( $popup_id = '', $title = null, $data = [] ) {
		extract( $data );
		?>
		<div id="<?php echo $popup_id; ?>" class="modal micromodal-slide" aria-hidden="true">
			<div class="modal__overlay" tabindex="-1" data-micromodal-close>
				<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="admin-modal-title" >
					<button class="modal__close" aria-label="Fermer la popup" data-micromodal-close>
						<?php ositer()->icon( 'fermer' ); ?>
					</button>
					<div class="modal-content-container">
						<div class="modal-content">
							<?php if ( ! is_null( $title ) ) {
								printf( '<h2 class="modal-title">%1$s</h2>', esc_html( $title ) );
							}

							include _ositer()->get_frontend()->template->get_template_file( "popups/{$popup_id}" );
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Camel case to underscored slug.
	 *
	 * @link https://stackoverflow.com/a/35719689
	 * @param string $string
	 * @return string
	 */
	public static function camel_to_slug( $string ) {
		return strtolower( preg_replace( ['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string ) );
	}

	/**
	 * Create or update a taxonomy term.
	 *
	 * @param string $taxonomy
	 * @param string $name
	 * @param array $metas
	 * @return integer
	 */
	public static function create_term( $taxonomy, $name, $metas = [], $args = [] ) {
		$existing = term_exists( isset( $args['slug'] ) ? $args['slug'] : $name, $taxonomy );
		$created  = false;

		if ( ! $existing ) {
			$term    = wp_insert_term( sanitize_text_field( $name ), $taxonomy, $args );
			$term_id = ! is_wp_error( $term ) && isset( $term['term_id'] ) ? (int) $term['term_id'] : null;
			$created = true;
		} else {
			$term_id = (int) $existing['term_id'];
		}

		if ( $term_id && ! empty( $metas ) ) {
			foreach ( $metas as $meta_key => $meta_value ) {
				update_term_meta( $term_id, $meta_key, sanitize_text_field( $meta_value ) );
			}
		}

		do_action( $created ? 'ositer/term-created' : 'lpn/term-updated', $taxonomy, $term_id );

		return $term_id;
	}

	/**
	 * Get a class name from a slug/string.
	 *
	 * @param string $string
	 * @return mixed
	 */
	public static function get_class_from_string( $string ) {
		$matrix = [
			Config:: CPT_DIAGNOSTIC       => '\OsinumTerritoire\Models\Diagnostic',
			Config:: CPT_TOOL             => '\OsinumTerritoire\Models\Tool',
			Config:: CPT_SITUATION        => '\OsinumTerritoire\Models\Situation',
			Config:: CPT_DIFFICULTY       => '\OsinumTerritoire\Models\Difficulty',
			Config:: CPT_RESOURCE         => '\OsinumTerritoire\Models\Resource',
			'resource'                    => '\OsinumTerritoire\Models\Resource',
			Config:: TAX_TOPIC            => '\OsinumTerritoire\Models\Topic',
			Config:: TAX_PRACTICE         => '\OsinumTerritoire\Models\Practice',
			Config:: TAX_CRITERIA         => '\OsinumTerritoire\Models\Criteria',
			Config:: TAX_DIFFICULTY_GROUP => '\OsinumTerritoire\Models\DifficultyGroup'
		];

		return array_key_exists( $string, $matrix ) ? $matrix[ $string ] : null;
	}

	/**
	 * Transform a slug into a nice value.
	 *
	 * @param string $slug
	 * @param string $context
	 * @return string
	 */
	public static function readify( $slug, $context = null ) {
		$matrix      = include _ositer()->get_plugin_path() . '/src/Schema/Strings.php';
		$matrix_slug = ! is_null( $context ) ? $context . '__' . $slug : $slug;

		return array_key_exists( $matrix_slug, $matrix ) ? $matrix[ $matrix_slug ] : ucfirst( str_replace( '-', ' ', $slug ) );
	}
	
	/**
	 * Get criteria data from a slug.
	 * 
	 * @param string $criteria_slug
	 * @return object
	 */
	public static function get_criteria_data( $criteria_slug ) {
		$matrix = json_decode( '[
			{ "slug": "satisfaction:-2", "label": "Insatisfait", "icon": "sad" },
			{ "slug": "satisfaction:-1", "label": "Peu satisfait", "icon": "neutral" },
			{ "slug": "satisfaction:2", "label": "Très satisfait", "icon": "smiley" },
			{ "slug": "frequency:-2", "label": "Rarement", "icon": "bar1" },
			{ "slug": "frequency:1", "label": "Régulièrement", "icon": "bar2" },
			{ "slug": "frequency:2", "label": "Au quotidien", "icon": "bar3" }
		]' );

		$found = array_values( array_filter( $matrix, function( $item ) use ( $criteria_slug ) {
			return $item->slug === $criteria_slug;
		} ) );

		return ! empty( $found ) ? $found[0] : null;
	}
}

<?php

namespace OsinumTerritoire\Admin\Tables;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;
use OsinumTerritoire\Helpers;

class Tools extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_filter( 'manage_' . Config::CPT_TOOL . '_posts_columns', 'add_custom_columns' );
		$this->add_action( 'manage_' . Config::CPT_TOOL . '_posts_custom_column', 'output_custom_columns_content', 10, 3 );
	}

	/**
	 * Add custom columns.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function add_custom_columns( $columns ) {
		$columns = Helpers::array_insert_before( 'date', $columns, 'visibility', __( 'Visibilité', 'ositer' ) );

		return $columns;
	}

	/**
	 * Output custom columns content.
	 *
	 * @param string $column
	 * @param integer $post_id
	 * @return void
	 */
	public function output_custom_columns_content( $column, $post_id ) {
		$tool = ositer()->get_tool( $post_id );

		switch ( $column ) {
			default:
				break;

			case 'visibility':
				if ( ! $tool->is_custom() ) {
					echo __( 'Publique', 'ositer' );
				} else {
					$diagnostic = $tool->get_diagnostic();

					if ( $diagnostic ) {
						printf(
							__( 'Privée (<a href="%2$s">diagnostic #%1$d</a>)', 'ositer' ),
							$diagnostic->get_id(),
							$diagnostic->get_edit_link()
						);
					} else {
						echo __( 'Privée', 'ositer' );
					}
				}
				break;
		}
	}
}

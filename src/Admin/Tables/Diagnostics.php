<?php

namespace OsinumTerritoire\Admin\Tables;

use DateTimeZone;
use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;
use OsinumTerritoire\Helpers;

class Diagnostics extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_filter( 'manage_' . Config::CPT_DIAGNOSTIC . '_posts_columns', 'add_custom_columns' );
		$this->add_action( 'manage_' . Config::CPT_DIAGNOSTIC . '_posts_custom_column', 'output_custom_columns_content', 10, 3 );
	}

	/**
	 * Add custom columns.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function add_custom_columns( $columns ) {
		$columns = Helpers::array_insert_before( 'date', $columns, 'details', __( 'Infos', 'ositer' ) );

		return $columns;
	}

	/**
	 * Output custom columns content.
	 *
	 * @param string $column
	 * @param integer $post_id
	 * @return void
	 */
	public function output_custom_columns_content( $column, $post_id ) {
		$diagnostic = ositer()->get_diagnostic( $post_id );

		switch ( $column ) {
			default:
				break;

			case 'details':
				echo '<ul>';
				echo $diagnostic->is_completed() ? sprintf( '<li><span class="dashicons dashicons-yes-alt"></span> %1$s</li>', __( 'Terminé', 'ositer' ) ) : sprintf( '<li><span class="dashicons dashicons-hourglass"></span> %1$s</li>', __( 'Incomplet', 'ositer' ) );
				echo sprintf( '<li><span class="dashicons dashicons-admin-users"></span> <a href="mailto:%1$s" target="_blank">%1$s</a></li>', $diagnostic->get_email() );
				echo sprintf( '<li><span class="dashicons dashicons-admin-site-alt3"></span> <a href="%1$s" target="_blank">%1$s</a></li>', str_replace( home_url(), '', $diagnostic->get_private_permalink() ) );
				echo sprintf( '<li><span class="dashicons dashicons-pdf"></span> <a href="%1$s" target="_blank">%2$s</a></li>', $diagnostic->get_private_permalink( 'imprimer' ), __( 'PDF résultats', 'ositer' ) );
				echo '</ul>';
				break;
		}
	}
}

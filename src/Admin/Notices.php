<?php

namespace OsinumTerritoire\Admin;

use OsinumTerritoire\_Core\Component;

class Notices extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'admin_notices', [ $this, 'render_notices' ], 50 );
	}

	/**
	 * Display custom notices.
	 *
	 * @return void
	 */
	public function render_notices() {
		if ( ! isset( $_GET['notice'] ) ) {
			return;
		}

		switch ( $_GET['notice'] ) {
			default:
				break;

			case 'csv-users-imported':
				$this->display_notice( sprintf( __( '%1$d comptes vont bientôt être créés (ou mis à jour) puis notifiés par e-mail.', 'ositer' ), (int) $_GET['scheduled'] ) );
				break;
		}
	}

	/**
	 * Display a single notice.
	 *
	 * @param string $message
	 * @param string $class
	 * @return void
	 */
	public function display_notice( $message = '', $class = 'success' ) {
		printf(
			'<div class="notice notice-%2$s"><p>%1$s</p></div>',
			$message,
			$class
		);
	}
}

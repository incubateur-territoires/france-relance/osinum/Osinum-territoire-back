<?php

namespace OsinumTerritoire\Admin;

use OsinumTerritoire\_Core\Component;

class Settings extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'init', 'register_site_settings_page' );
	}

	/**
	 * Register site options page.
	 *
	 * @return void
	 */
	public function register_site_settings_page() {
		acf_add_options_page( [
			'page_title' => __( 'Configuration du site', 'ositer' ),
			'menu_title' => __( 'Configuration', 'ositer' ),
			'menu_slug'  => 'ositer-settings',
			'capability' => 'manage_options',
			'position'   => 80,
			'icon_url'   => 'dashicons-admin-tools',
		] );
	}
}

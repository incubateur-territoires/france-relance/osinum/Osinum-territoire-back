<?php

namespace OsinumTerritoire\Admin\Metaboxes;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;
use OsinumTerritoire\Helpers;
use OsinumTerritoire\Models\Communication;

class Diagnostics extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'admin_init', [ $this, 'register_metaboxes' ], 10 );
	}

	/**
	 * Register metaboxes.
	 *
	 * @return void
	 */
	public function register_metaboxes() {
		add_meta_box(
			'ositer-metabox',
			__( 'Formulaire collectivité', 'ositer' ),
			[ $this, 'output_lead_metabox' ],
			Config::CPT_DIAGNOSTIC
		);
	}

	/**
	 * Output a custom metabox.
	 *
	 * @param \WP_Post $post
	 * @return void
	 */
	public function output_lead_metabox( $post ) {
		if ( ! $post ) {
			return;
		}

		$diagnostic = ositer()->get_diagnostic( $post );

		if ( ! $diagnostic->has_completed_lead_form() ) {
			printf( '<p>%1$s</p>', __( 'Le formulaire de lead n\'a pas encore été rempli.', 'ositer' ) );
		} else {
			$values = $diagnostic->get_lead_form_values();
			include_once _ositer()->get_frontend()->template->get_template_file( 'admin/diagnostic-lead-table' );
		}
	}
}

<?php

namespace OsinumTerritoire\Admin;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class ACF extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'acf/update_field_group', 'alter_update_field_group', 1, 1 );
		$this->add_filter( 'acf/settings/load_json', 'alter_json_load_point' );
		$this->add_action( 'acf/render_field/key=field_6367bd87d92d8', 'override_icons_list_render', 10 );
	}

	/**
	 * Filter ACF update
	 */
	public function alter_update_field_group( $group ) {
		$local_field_groups_keys = Config::ACF_FIELDS_KEYS;
		if ( ! $local_field_groups_keys ) {
			return $group;
		}
		if ( isset( $group[ 'key' ] ) && ! empty( $group[ 'key' ] ) ) {
			if ( in_array( $group[ 'key' ], $local_field_groups_keys ) ) {
				add_action( 'acf/settings/save_json', [ $this, 'alter_json_save_point' ], 9999, 1 );
			}
		}
		return $group;
	}

	/**
	 * Load ACF json data from custom folder.
	 *
	 * @param string $path
	 * @return string
	 */
	public function alter_json_load_point( $path ) {
		$path[] = $this->get_acf_json_path();

		return $path;
	}

	/**
	 * Save ACF json data from plugin folder, not theme folder.
	 *
	 * @param string $path
	 * @return string
	 */
	public function alter_json_save_point( $path ) {
		if ( defined( 'OSITER_SAVE_ACF_FIELDS_TO_JSON' ) && OSITER_SAVE_ACF_FIELDS_TO_JSON ) {
			return $this->get_acf_json_path();
		}

		return $path;
	}

	/**
	 * Get ACF JSON path.
	 *
	 * @return string
	 */
	public function get_acf_json_path() {
		return $this->plugin->get_plugin_path() . '/src/Schema/Fields';
	}

	/**
	 * Validate Situations post saving.
	 *
	 * @return void
	 */
	public function validate_situations_post() {
		if ( ! isset( $_POST['post_type'] ) || $_POST['post_type'] !== Config::CPT_SITUATION ) {
			return;
		}

		if ( ! isset( $_POST['tax_input']['topic'] ) || empty( $_POST['tax_input']['topic'] ) ) {
			acf_add_validation_error( 'post_title', __( 'Veuillez assigner une thématique à cette situation.', 'ositer' ) );
		}
	}

	/**
	 * Display icons list on the site options page.
	 *
	 * @param array $field
	 * @return void
	 */
	function override_icons_list_render( $field ) {
		$icons = get_field_object( 'field_62cd6c9ec69f6' )['choices'];
		$html  = '<ol class="icons-list">';

		foreach ( $icons as $icon => $name ) {
			$html .= sprintf(
				'<li>%1$s %2$s</li>',
				ositer()->icon( $icon, false ),
				$name
			);
		}

		$html .= '</ol>';

		echo $html;
	}
}

<?php

namespace OsinumTerritoire\Admin;

use OsinumTerritoire\_Core\Component;

class Menu extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		// add_action( 'admin_menu', [ $this, 'add_new_admin_menu_items' ], 50 );
	}

	/**
	 * Extend the admin menu and add new custom menus.
	 *
	 * @return void
	 */
	public function add_new_admin_menu_items() {
		// Log admin page.
		add_menu_page(
			__( 'Logs des événements', 'ositer' ),
			__( 'Logs', 'ositer' ),
			'manage_options',
			'ositer-logs',
			[ '\OsinumTerritoire\Admin\Pages\Logs', 'output' ],
			'dashicons-info-outline',
			84
		);
	}
}

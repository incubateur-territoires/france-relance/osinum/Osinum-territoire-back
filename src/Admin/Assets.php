<?php

namespace OsinumTerritoire\Admin;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Helpers;
use OsinumTerritoire\Plugin;

class Assets extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_admin_assets' ], 10 );
		// add_filter( 'ositer/admin/js_data', [ $this, 'construct_js_data' ], 10 );
	}

	/**
	 * Enqueue admin JS ans CSS assets.
	 *
	 * @return void
	 */
	public function enqueue_admin_assets() {
		$enqueuer = new \WPackio\Enqueue(
			'OsinumTerritoire',
			'dist',
			Plugin::VERSION,
			'plugin',
			sprintf( '%1$s/%2$s', $this->get_plugin()->get_plugin_path(), 'osinum-territoire-back' )
		);

		// Enqueue CSS.
		$css = $enqueuer->enqueue( 'ositer_back_css', 'backend', [
			'js'        => true,
			'css'       => true,
			'js_dep'    => [],
			'css_dep'   => [],
			'in_footer' => true,
		] );

		// wp_localize_script(
		// 	$js['js'][1]['handle'],
		// 	'OsinumTerritoire',
		// 	apply_filters( 'ositer/admin/js_data', [] )
		// );
	}

	/**
	 * Construct the basic JS data object.
	 *
	 * @param array $data
	 * @return array
	 */
	public function construct_js_data( $data ) {
		return [
			'api' => [
				'rest_url' => esc_url_raw( rest_url() ),
				'nonce'    => wp_create_nonce( 'wp_rest' ),
			],
		];
	}
}

<?php

namespace OsinumTerritoire\Admin;

use OsinumTerritoire\_Core\Component;

class Actions extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @todo Sub-classes for each action.
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'admin_init', 'do_custom_action' );
	}

	/**
	 * Custom action.
	 *
	 * @return void
	 */
	public function do_custom_action() {
		if ( isset( $_GET['action'] ) && $_GET['action'] === 'switch_admin_ui' ) {
			if ( ! current_user_can( 'administrator') || ! wp_verify_nonce( $_GET['_wpnonce'], 'sw!tch_adm1n-ui' ) ) {
				return;
			}

			//

			wp_safe_redirect( admin_url(), 301 );
			exit();

		}
	}
}

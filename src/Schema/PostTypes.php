<?php

namespace OsinumTerritoire\Schema;

use OsinumTerritoire\Config;

defined( 'ABSPATH' ) || exit;

use OsinumTerritoire\_Core\Component;

class PostTypes extends Component {
	/**
	 * Setup hook.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'init', 'register_diagnostic_post_type' );
		$this->add_action( 'init', 'register_resource_post_type' );
		$this->add_action( 'init', 'register_situation_post_type' );
		$this->add_action( 'init', 'register_tool_post_type' );
		$this->add_action( 'init', 'register_difficulty_post_type' );
	}

	/**
	 * Register the Diagnostic post type.
	 *
	 * @return void
	 */
	public function register_diagnostic_post_type() {
		register_post_type(
			Config::CPT_DIAGNOSTIC,
			[
				'labels'      => [
					'name'          => esc_html__( 'Diagnostics', 'ositer' ),
					'singular_name' => esc_html__( 'Diagnostic', 'ositer' ),
					'menu_name'     => esc_html__( 'Diagnostics', 'ositer' ),
				],
				'supports'      => [ 'title', ],
				'public'        => false,
				'has_archive'   => false,
				'show_in_rest'  => false,
				'show_ui'       => true,
				'menu_position' => 22.3,
				'menu_icon'     => 'dashicons-clipboard',
			]
		);
	}

	/**
	 * Register the Resource post type.
	 *
	 * @return void
	 */
	public function register_resource_post_type() {
		register_post_type(
			Config::CPT_RESOURCE,
			[
				'labels'      => [
					'name'          => esc_html__( 'Ressources', 'ositer' ),
					'singular_name' => esc_html__( 'Ressource', 'ositer' ),
					'menu_name'     => esc_html__( 'Ressources', 'ositer' ),
					'edit_item'     => esc_html__( 'Modifier cette ressource', 'ositer' ),
				],
				'description'	=> ositer()->get_setting( 'resource_desc' ) ? ositer()->get_setting( 'resource_desc' ) : '',
				'supports'      => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
				'public'        => true,
				'has_archive'   => 'ressources',
				'show_in_rest'  => true,
				'show_ui'       => true,
				'menu_position' => 22.1,
				'menu_icon'     => 'dashicons-welcome-learn-more',
				'rewrite'		=> [
					'slug'		=> 'ressource'
				]
			]
		);
	}

	/**
	 * Register the Situation post type.
	 *
	 * @return void
	 */
	public function register_situation_post_type() {
		register_post_type(
			Config::CPT_SITUATION,
			[
				'labels'      => [
					'name'          => esc_html__( 'Situations', 'ositer' ),
					'singular_name' => esc_html__( 'Situation', 'ositer' ),
					'menu_name'     => esc_html__( 'Situations', 'ositer' ),
					'edit_item'     => esc_html__( 'Modifier la situation', 'ositer' ),
				],
				'supports'      => [ 'title', ],
				'public'        => false,
				'has_archive'   => false,
				'show_in_rest'  => false,
				'show_ui'       => true,
				'menu_position' => 22.1,
				'menu_icon'     => 'dashicons-location',
			]
		);
	}

	/**
	 * Register the Tool post type.
	 *
	 * @return void
	 */
	public function register_tool_post_type() {
		register_post_type(
			Config::CPT_TOOL,
			[
				'labels'      => [
					'name'          => esc_html__( 'Outils numériques', 'ositer' ),
					'singular_name' => esc_html__( 'Outil numérique', 'ositer' ),
					'menu_name'     => esc_html__( 'Outils num.', 'ositer' ),
					'edit_item'     => esc_html__( 'Modifier l\'outil numérique', 'ositer' ),
				],
				'description'	=> ositer()->get_setting( 'tool_desc' ) ? ositer()->get_setting( 'tool_desc' ) : '',
				'supports'      => [ 'title', 'editor', 'excerpt', 'thumbnail' ],
				'public'        => true,
				'has_archive'   => 'outils',
				'show_in_rest'  => false,
				'show_ui'       => true,
				'menu_position' => 22.2,
				'menu_icon'     => 'dashicons-hammer',
				'rewrite'		=> [
					'slug'		=> 'outil'
				]
			]
		);
	}

	/**
	 * Register the Difficulty post type.
	 *
	 * @return void
	 */
	public function register_difficulty_post_type() {
		register_post_type(
			Config::CPT_DIFFICULTY,
			[
				'labels'      => [
					'name'          => esc_html__( 'Difficultés', 'ositer' ),
					'singular_name' => esc_html__( 'Difficulté', 'ositer' ),
					'menu_name'     => esc_html__( 'Difficultés', 'ositer' ),
					'edit_item'     => esc_html__( 'Modifier cette difficulté', 'ositer' ),
				],
				'supports'      => [ 'title', ],
				'public'        => true,
				'has_archive'   => true,
				'show_in_rest'  => false,
				'show_ui'       => true,
				'menu_position' => 22.2,
				'menu_icon'     => 'dashicons-warning',
			]
		);
	}
}

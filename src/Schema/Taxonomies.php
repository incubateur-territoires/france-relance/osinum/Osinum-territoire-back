<?php

namespace OsinumTerritoire\Schema;

use OsinumTerritoire\Config;

defined( 'ABSPATH' ) || exit;

use OsinumTerritoire\_Core\Component;

class Taxonomies extends Component {
	/**
	 * Setup hook.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'init', 'register_topic_taxonomy', 16 );
		$this->add_action( 'init', 'register_practice_taxonomy', 16 );
		$this->add_action( 'init', 'register_criteria_taxonomy', 16 );
		$this->add_action( 'init', 'register_difficulty_group_taxonomy', 16 );
	}

	/**
	 * Register the Topic taxonomy.
	 *
	 * @return void
	 */
	public function register_topic_taxonomy() {
		register_taxonomy(
			Config::TAX_TOPIC,
			[ Config::CPT_SITUATION, Config::CPT_RESOURCE ],
			[
				'labels' => [
					'name'          => __( 'Thématiques', 'ositer' ),
					'singular_name' => __( 'Thématique', 'ositer' ),
					'menu_name'     => __( 'Thématiques', 'ositer' ),
					'all_items'     => __( 'Toutes les thématiques', 'ositer' ),
					'new_item_name' => __( 'Nouvelle thématique', 'ositer' ),
					'add_new_item'  => __( 'Créer thématique', 'ositer' ),
					'edit_item'     => __( 'Editer', 'ositer' ),
					'update_item'   => __( 'Mettre à jour', 'ositer' ),
					'view_item'     => __( 'Voir', 'ositer' ),
				],
				'hierarchical'      => false,
				'public'            => false,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
			]
		);
	}

	/**
	 * Register the Practice taxonomy.
	 *
	 * @return void
	 */
	public function register_practice_taxonomy() {
		register_taxonomy(
			Config::TAX_PRACTICE,
			[ Config::CPT_SITUATION, Config::CPT_TOOL, Config::CPT_DIFFICULTY, Config::CPT_RESOURCE ],
			[
				'labels' => [
					'name'          => __( 'Pratiques', 'ositer' ),
					'singular_name' => __( 'Pratique', 'ositer' ),
					'menu_name'     => __( 'Pratiques', 'ositer' ),
					'all_items'     => __( 'Toutes les pratiques', 'ositer' ),
					'new_item_name' => __( 'Nouvelle pratique', 'ositer' ),
					'add_new_item'  => __( 'Créer pratique', 'ositer' ),
					'edit_item'     => __( 'Editer', 'ositer' ),
					'update_item'   => __( 'Mettre à jour', 'ositer' ),
					'view_item'     => __( 'Voir', 'ositer' ),
				],
				'hierarchical'      => false,
				'public'            => false,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
			]
		);
	}

	/**
	 * Register the Criteria taxonomy.
	 *
	 * @return void
	 */
	public function register_criteria_taxonomy() {
		register_taxonomy(
			Config::TAX_CRITERIA,
			[ Config::CPT_TOOL ],
			[
				'labels' => [
					'name'          => __( 'Critères', 'ositer' ),
					'singular_name' => __( 'Critère', 'ositer' ),
					'menu_name'     => __( 'Critères', 'ositer' ),
					'all_items'     => __( 'Tous les critères', 'ositer' ),
					'new_item_name' => __( 'Nouveau critère', 'ositer' ),
					'add_new_item'  => __( 'Créer critère', 'ositer' ),
					'edit_item'     => __( 'Editer', 'ositer' ),
					'update_item'   => __( 'Mettre à jour', 'ositer' ),
					'view_item'     => __( 'Voir', 'ositer' ),
				],
				'hierarchical'      => true,
				'public'            => false,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
			]
		);
	}

	/**
	 * Register the Difficulty Group taxonomy.
	 *
	 * @return void
	 */
	public function register_difficulty_group_taxonomy() {
		register_taxonomy(
			Config::TAX_DIFFICULTY_GROUP,
			[ Config::CPT_DIFFICULTY, Config::CPT_RESOURCE ],
			[
				'labels' => [
					'name'          => __( 'Groupes', 'ositer' ),
					'singular_name' => __( 'Groupe', 'ositer' ),
					'menu_name'     => __( 'Groupes', 'ositer' ),
					'all_items'     => __( 'Tous les groupes', 'ositer' ),
					'new_item_name' => __( 'Nouveau groupe', 'ositer' ),
					'add_new_item'  => __( 'Créer groupe', 'ositer' ),
					'edit_item'     => __( 'Editer', 'ositer' ),
					'update_item'   => __( 'Mettre à jour', 'ositer' ),
					'view_item'     => __( 'Voir', 'ositer' ),
				],
				'hierarchical'      => false,
				'public'            => false,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
			]
		);
	}
}

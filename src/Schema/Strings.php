<?php

namespace OsinumTerritoire\Schema\Strings;

defined( 'ABSPATH' ) || exit;

return [
	'infos-region'                            => 'Région',
	'infos-region__auvergne-rhone'            => 'Auvergne-Rhône-Alpes',
	'infos-region__bourgogne-franche'         => 'Bourgogne-Franche-Comté',
	'infos-region__bretagne'                  => 'Bretagne',
	'infos-region__centre-val-loire'          => 'Centre-Val de Loire',
	'infos-region__corse'                     => 'Corse',
	'infos-region__grand-est'                 => 'Grand Est',
	'infos-region__guadeloupe'                => 'Guadeloupe',
	'infos-region__guyane'                    => 'Guyane',
	'infos-region__hauts-france'              => 'Hauts-de-France',
	'infos-region__ile-france'                => 'Ile-de-France',
	'infos-region__reunion'                   => 'La Réunion',
	'infos-region__martinique'                => 'Martinique',
	'infos-region__mayotte'                   => 'Mayotte',
	'infos-region__normandie'                 => 'Normandie',
	'infos-region__nouvelle-aquitaine'        => 'Nouvelle-Aquitaine',
	'infos-region__occitanie'                 => 'Occitanie',
	'infos-region__pays-loire'                => 'Pays de la Loire',
	'infos-region__paca'                      => 'Provence-Alpes-Côte d’Azur',
	'infos-type-collectivite'                 => 'Type de collectivité',
	'infos-type-collectivite__commune'        => 'Commune',
	'infos-type-collectivite__commu-communes' => 'Communauté de communes',
	'infos-type-collectivite__commu-agglo'    => 'Communauté d\'agglomération',
	'infos-type-collectivite__departement'    => 'Département',
	'infos-taille-collectivite'               => 'Taille collectivité',
	'infos-taille-collectivite__-500'         => '- de 500',
	'infos-taille-collectivite__500-999'      => '500 à 999',
	'infos-taille-collectivite__1000-4999'    => '1 000 à 4 999',
	'infos-taille-collectivite__5000-9999'    => '5 000 à 9 999',
	'infos-taille-collectivite__10000-19999'  => '10 000 à 19 999',
	'infos-taille-collectivite__20000-49999'  => '20 000 à 49 999',
	'infos-taille-collectivite__50000+'       => '50 000 et +',
	'infos-role'                              => 'Rôle',
	'infos-role__agent'                       => 'Agent',
	'infos-role__agent-num'                   => 'Agent en charge du numérique',
	'infos-role__elu'                         => 'Élu-e',
	'infos-role__elu-num'                     => 'Élu-e au numérique',
	'suite-contact'                           => 'Accepte être recontacté',
	'suite-consulter-resultats-comparaison'   => 'Accepte comparaison résultats',
	'suite-disposer-donnees'                  => 'Accepte disposer données',
];

<?php

namespace OsinumTerritoire\Schema;

defined( 'ABSPATH' ) || exit;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class Tables extends Component {
	/**
	 * Setup hook.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/activation', 'create_custom_table' );
	}

	/**
	 * Register the vital tables for: Custom.
	 *
	 * @return void
	 */
	public function create_custom_table() {
		// 
	}
}

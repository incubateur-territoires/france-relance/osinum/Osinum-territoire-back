<?php

namespace OsinumTerritoire\Commands\Abstracts;

use OsinumTerritoire\_Core\Component;
use WP_CLI;

abstract class Command extends Component {
	/**
	 * The command name.
	 *
	 * @var string
	 */
	protected $command = '';

	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'init', [ $this, 'register_command' ], 10 );
	}

	/**
	 * Register the CLI command.
	 *
	 * @todo Make an abstract class for that.
	 * @return void
	 */
	public function register_command() {
		if ( ! class_exists( 'WP_CLI' ) ) {
			return;
		}

		WP_CLI::add_command( sprintf( '%1$s %2$s', $this->parent->get_namespace(), $this->command ), [ $this, 'run' ] );
	}

	/**
	 * Run the command.
	 *
	 * @return void
	 */
	abstract public function run( $args, $assoc_args );
}

<?php

namespace OsinumTerritoire\Commands;

use OsinumTerritoire\Commands\Abstracts\Command;
use OsinumTerritoire\Config;
use WP_CLI;

class CreateFakeDifficulties extends Command {
	/**
	 * The command name.
	 * 
	 * @var string
	 */
	protected $command = 'create-fake-difficulties';

	/**
	 * Run some command.
	 *
	 * @return void
	 */
	public function run( $args, $assoc_args ) {
		$amount    = isset( $assoc_args['amount'] ) ? (int) $assoc_args['amount'] : 10;
		$progress  = \WP_CLI\Utils\make_progress_bar( sprintf( 'Création de %1$d difficultés.', $amount ), $amount );
		$created   = 0;
		$faker     = \Faker\Factory::create( 'fr_FR' );
		$groups    = get_terms( [ 'taxonomy' => Config::TAX_DIFFICULTY_GROUP, 'hide_empty' => false, 'fields' => 'ids', ] );
		$practices = get_terms( [ 'taxonomy' => Config::TAX_PRACTICE, 'hide_empty' => false, 'fields' => 'ids', ] );

		for ( $i = 1; $i <= $amount; $i++ ) {
			$difficulty_id = wp_insert_post( [
				'post_type'   => Config::CPT_DIFFICULTY,
				'post_title'  => ucfirst( $faker->words( rand( 3, 6 ), true ) ),
				'post_status' => 'publish',
				'tax_input'   => [
					Config:: TAX_DIFFICULTY_GROUP    => [ $faker->randomElement( $groups ) ],
					Config:: TAX_PRACTICE => [ $faker->randomElement( $practices ) ]
				],
				'meta_input' => [
					'source' => 'faker',
				],
			] );
			
			if ( $difficulty_id > 0 ) {
				$created++;

				update_field( 'subtitle', $faker->paragraph( 2 ), $difficulty_id );
				update_field( 'number', rand( 1, 500 ), $difficulty_id );
				update_field( 'position', rand( 1, 100 ), $difficulty_id );
			}

			$progress->tick();
		}

		WP_CLI::success( sprintf( '%1$d / %2$d difficultés créées.', $created, $amount ) );
	}
}

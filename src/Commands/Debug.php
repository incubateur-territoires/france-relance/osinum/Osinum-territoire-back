<?php

namespace OsinumTerritoire\Commands;

use OsinumTerritoire\Commands\Abstracts\Command;
use OsinumTerritoire\Config;
use WP_CLI;

class Debug extends Command {
	/**
	 * The command name.
	 * 
	 * @var string
	 */
	protected $command = 'debug';

	/**
	 * Run some command.
	 *
	 * @return void
	 */
	public function run( $args, $assoc_args ) {
		$tools_ids = get_posts( [
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'posts_per_page'         => 1000,
			'fields'                 => 'ids',
			'post_type'              => Config::CPT_TOOL,
			'meta_query'             => [
				[
					'key'     => 'visibility',
					'compare' => 'NOT EXISTS',
				]
			]
		] );

		foreach ( $tools_ids as $tool_id ) {
			update_post_meta( $tool_id, 'visibility', 'public' );
		}

		WP_CLI::success( sprintf( '%1$d outils rendus publics.', count( $tools_ids ) ) );
	}
}

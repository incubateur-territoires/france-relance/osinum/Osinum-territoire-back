<?php

namespace OsinumTerritoire\Commands;

use OsinumTerritoire\Commands\Abstracts\Command;
use OsinumTerritoire\Config;
use WP_CLI;

class CreateFakeTools extends Command {
	/**
	 * The command name.
	 * 
	 * @var string
	 */
	protected $command = 'create-fake-tools';

	/**
	 * Run some command.
	 *
	 * @return void
	 */
	public function run( $args, $assoc_args ) {
		$amount    = isset( $assoc_args['amount'] ) ? (int) $assoc_args['amount'] : 10;
		$progress  = \WP_CLI\Utils\make_progress_bar( sprintf( 'Création de %1$d outils.', $amount ), $amount );
		$created   = 0;
		$faker     = \Faker\Factory::create( 'fr_FR' );
		$practices = get_terms( [ 'taxonomy' => Config::TAX_PRACTICE, 'hide_empty' => false, 'fields' => 'ids', ] );

		for ( $i = 1; $i <= $amount; $i++ ) {
			$tool_id = wp_insert_post( [
				'post_type'   => Config::CPT_TOOL,
				'post_title'  => ucfirst( $faker->words( rand( 3, 6 ), true ) ),
				'post_status' => 'publish',
				'tax_input'   => [
					Config::TAX_PRACTICE => [ $faker->randomElement( $practices ) ]
				],
				'meta_input' => [
					'visibility' => 'public',
					'source'     => 'faker',
				],
			] );
			
			if ( $tool_id > 0 ) {
				$created++;

				update_field( 'position', rand( 1, 100 ), $tool_id );
			}

			$progress->tick();
		}

		WP_CLI::success( sprintf( '%1$d / %2$d outils créés.', $created, $amount ) );
	}
}

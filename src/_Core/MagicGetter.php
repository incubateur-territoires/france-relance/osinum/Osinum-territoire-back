<?php

namespace OsinumTerritoire\_Core;

trait MagicGetter {
	/**
	 * Magic getter when trying to directly access some values.
	 *
	 * @param  string $field Field to get.
	 * @throws Exception     Throws an exception if the field is invalid.
	 * @return mixed         Value of the field.
	 */
	public function __get( $field ) {
		$method_name = strtolower( "get_{$field}" );

		if ( method_exists( $this, $method_name ) ) {
			return $this->$method_name();
		}

		throw new \Exception( sprintf( '%1$s can not call method %2$s.', self::class, $method_name ) );
	}

	/**
	 * Magic getter when trying to call a get_xyz method that does not exist.
	 * Will return the $xyz property if it exists.
	 *
	 * @param  string $method Method to call.
	 * @throws Exception     Throws an exception if the field is invalid.
	 * @return mixed         Value of the field.
	 */
	public function __call( $method, $args ) {
		$property = str_replace( 'get_', '', $method );

		if ( isset( $this->$property ) ) {
			return $this->$property;
		}

		throw new \Exception( sprintf( '%1$s can not retrieve property %2$s.', self::class, $property ) );
	}
}

<?php

namespace OsinumTerritoire\_Core;

/**
 * Class Component
 *
 * @package OsinumTerritoire\_Core
 */
abstract class Component extends \ComposePress\Core\Abstracts\Component_0_9_0_0 {
	/**
	 * Get main plugin class.
	 *
	 * @return \OsinumTerritoire\Plugin
	 */
	public function get_plugin() {
		return parent::get_plugin();
	}

	/**
	 * Add an action hook.
	 *
	 * @param string $hook
	 * @param string $method
	 * @param integer $priority
	 * @param integer $args_count
	 * @return void
	 */
	public function add_action( $hook = '', $method = '', $priority = 10, $args_count = 1 ) {
		$full_method = method_exists( $this, $method ) ? [ $this, $method ] : $method;

		return add_action( $hook, $full_method, $priority, $args_count );
	}

	/**
	 * Add a filter hook.
	 *
	 * @param string $hook
	 * @param string $method
	 * @param integer $priority
	 * @param integer $args_count
	 * @return void
	 */
	public function add_filter( $hook = '', $method = '', $priority = 10, $args_count = 1 ) {
		$full_method = method_exists( $this, $method ) ? [ $this, $method ] : $method;

		return add_filter( $hook, $full_method, $priority, $args_count );
	}
}

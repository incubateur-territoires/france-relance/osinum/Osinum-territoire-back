<?php

namespace OsinumTerritoire\_Core;

defined( 'ABSPATH' ) || exit;

class Cache {
	/**
	 * Array storing our data
	 *
	 * @var array
	 */
	protected static $data = [];

	/**
	 * Do we already have a cached data?
	 *
	 * @param string $key
	 * @return boolean
	 */
	public static function has( $key ) {
		return array_key_exists( $key, self::$data );
	}

	/**
	 * Cache some data
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public static function set( $key, $value ) {
		self::$data[ $key ] = $value;
	}

	/**
	 * Retrieve cached data
	 *
	 * @param string $key
	 * @return mixed
	 */
	public static function get( $key ) {
		if ( self::has( $key ) ) {
			return self::$data[ $key ];
		}

		return null;
	}
}

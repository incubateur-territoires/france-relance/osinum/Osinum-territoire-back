<?php


namespace OsinumTerritoire;

use OsinumTerritoire\_Core\Plugin as PluginBase;
use OsinumTerritoire\_Modules\Admin;
use OsinumTerritoire\_Modules\Ajax;
use OsinumTerritoire\_Modules\Commands;
use OsinumTerritoire\_Modules\Crons;
use OsinumTerritoire\_Modules\Debug;
use OsinumTerritoire\_Modules\Events;
use OsinumTerritoire\_Modules\Frontend;
use OsinumTerritoire\_Modules\Hooks;
use OsinumTerritoire\_Modules\Schema;
use OsinumTerritoire\Frontend\Urls;
use OsinumTerritoire\Data;
use OsinumTerritoire\Email;
use OsinumTerritoire\Functions;
use OsinumTerritoire\Scheduler;
use OsinumTerritoire\Strings;

/**
 * Class Plugin
 *
 * @package OsinumTerritoire
 */
class Plugin extends PluginBase {
	/**
	 * Plugin constants.
	 */
	const VERSION          = '1.0.0';
	const PLUGIN_SLUG      = 'ositer';
	const PLUGIN_NAMESPACE = '\OsinumTerritoire';

	/**
	 * Child components.
	 */
	protected $admin     = Admin::class;
	protected $ajax      = Ajax::class;
	protected $commands  = Commands::class;
	protected $crons     = Crons::class;
	protected $data      = Data::class;
	protected $email     = Email::class;
	protected $debug     = Debug::class;
	protected $events    = Events::class;
	protected $frontend  = Frontend::class;
	protected $functions = Functions::class;
	protected $hooks     = Hooks::class;
	protected $scheduler = Scheduler::class;
	protected $schema    = Schema::class;
	protected $strings   = Strings::class;
	protected $urls      = Urls::class;

	/**
	 * Load the plugin components.
	 *
	 * @return void
	 */
	protected function load_components() {
		if ( is_admin() ) {
			$this->load( 'admin' );

			if ( ! class_exists( 'WP_List_Table' ) ) {
				require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
			}
		}

		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			$this->load( 'debug' );
		}

		$this->load( 'admin' );
		$this->load( 'ajax' );
		$this->load( 'commands' );
		$this->load( 'crons' );
		$this->load( 'data' );
		$this->load( 'email' );
		$this->load( 'events' );
		$this->load( 'frontend' );
		$this->load( 'functions' );
		$this->load( 'hooks' );
		$this->load( 'scheduler' );
		$this->load( 'schema' );
		$this->load( 'strings' );
		$this->load( 'urls' );

		return true;
	}

	/**
	 * Get Admin class.
	 *
	 * @return Admin
	 */
	public function get_admin() {
		return $this->admin;
	}

	/**
	 * Get AJAX class.
	 *
	 * @return Ajax
	 */
	public function get_ajax() {
		return $this->ajax;
	}

	/**
	 * Get Commands class.
	 *
	 * @return Commands
	 */
	public function get_commands() {
		return $this->commands;
	}

	/**
	 * Get Crons class.
	 *
	 * @return Crons
	 */
	public function get_crons() {
		return $this->crons;
	}

	/**
	 * Get Data class.
	 *
	 * @return Data
	 */
	public function get_data() {
		return $this->data;
	}

	/**
	 * Get Email class.
	 *
	 * @return Email
	 */
	public function get_email() {
		return $this->email;
	}

	/**
	 * Get Debug class.
	 *
	 * @return Debug
	 */
	public function get_debug() {
		return $this->debug;
	}

	/**
	 * Get Events class.
	 *
	 * @return Events
	 */
	public function get_events() {
		return $this->events;
	}

	/**
	 * Get Frontend class.
	 *
	 * @return Frontend
	 */
	public function get_frontend() {
		return $this->frontend;
	}

	/**
	 * Get Functions class.
	 *
	 * @return Functions
	 */
	public function get_functions() {
		return $this->functions;
	}

	/**
	 * Get Hooks class.
	 *
	 * @return Hooks
	 */
	public function get_hooks() {
		return $this->hooks;
	}

	/**
	 * Get Schema class.
	 *
	 * @return Schema
	 */
	public function get_schema() {
		return $this->schema;
	}

	/**
	 * Get Scheduler class.
	 *
	 * @return Scheduler
	 */
	public function get_scheduler() {
		return $this->scheduler;
	}

	/**
	 * Get Strings class.
	 *
	 * @return Strings
	 */
	public function get_strings() {
		return $this->strings;
	}

	/**
	 * Get URLs class.
	 *
	 * @return Urls
	 */
	public function get_urls() {
		return $this->urls;
	}

	/**
	 * Setup vital stuff.
	 *
	 * @return void
	 */
	public function setup() {
	}

	/**
	 * Plugin activation and upgrade
	 *
	 * @param $network_wide
	 *
	 * @return void
	 */
	public function activate( $network_wide ) {
		do_action( 'ositer/activation', $network_wide );
	}

	/**
	 * Plugin de-activation
	 *
	 * @param $network_wide
	 *
	 * @return void
	 */
	public function deactivate( $network_wide ) {
		do_action( 'ositer/deactivation', $network_wide );
	}

	/**
	 * Plugin uninstall
	 *
	 * @return void
	 */
	public function uninstall() {}

	/**
	 * Get the plugin path.
	 *
	 * @return void
	 */
	public function get_plugin_path() {
		return str_replace( '/src', '', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
	}

	/**
	 * Get the plugin URL.
	 *
	 * @return void
	 */
	public function get_plugin_url() {
		return str_replace( '/src', '', untrailingslashit( plugin_dir_url( __FILE__ ) ) );
	}
}

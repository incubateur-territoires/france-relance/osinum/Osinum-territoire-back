<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Frontend extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Frontend';

	protected $modules = [
		'Actions',
		'Assets',
		'Navigation',
		'Template',
		'Urls',
		'WebRenderer',
		'DiagnosticJson',
		'PdfRenderer',
	];
}

<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Ajax extends Manager {
	const  MODULE_NAMESPACE = '\OsinumTerritoire\Ajax';

	/**
	 * Get the routes prefix for a specific context.
	 *
	 * @param integer $version
	 * @return void
	 */
	public function get_route_prefix( $context = 'app' ) {
		return $this->{ "route_prefix_for_{$context}" };
	}

	protected $modules = [
		'Routes',
	];
}

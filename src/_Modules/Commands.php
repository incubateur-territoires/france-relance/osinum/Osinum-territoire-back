<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Commands extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Commands';
	public $namespace_v1   = 'ositer';

	public function get_namespace( $version = 1 ) {
		return $this->namespace_v1;
	}

	protected $modules = [
		'CreateFakeSituations',
		'CreateFakeDifficulties',
		'CreateFakeTools',
		'Debug',
	];
}

<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Schema extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Schema';

	protected $modules = [
		'PostTypes',
		'Tables',
		'Taxonomies',
	];
}

<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Admin extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Admin';

	protected $modules = [
		'Actions',
		'ACF',
		'Assets',
		'Menu',
		'Notices',
		'Settings',

		// Tables customization (not loaded by Pages above).
		'\OsinumTerritoire\Admin\Tables\Diagnostics',
		'\OsinumTerritoire\Admin\Tables\Tools',

		// Metaboxes.
		'\OsinumTerritoire\Admin\Metaboxes\Diagnostics',
	];
}

<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Debug extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Debug';

	protected $modules = [
		'Ngrok',
		'Tests',
	];
}

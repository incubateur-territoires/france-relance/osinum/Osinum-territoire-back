<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Events extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Events';

	protected $modules = [
		'Emails',
		'DiagnosticWasCreated',
		'DiagnosticWasUpdated',
	];
}

<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Crons extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Crons';

	protected $modules = [
		'DiagnosticsCleaner',
	];
}

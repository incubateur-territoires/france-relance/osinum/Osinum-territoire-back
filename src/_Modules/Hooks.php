<?php

namespace OsinumTerritoire\_Modules;

use OsinumTerritoire\_Core\Manager;

class Hooks extends Manager {
	const MODULE_NAMESPACE = '\OsinumTerritoire\Hooks';

	protected $modules = [
		'Admin',
		'Cache',
		'DataSave',
		'General',
	];
}

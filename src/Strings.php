<?php

namespace OsinumTerritoire;

use OsinumTerritoire\_Core\Component;

class Strings extends Component {
	/**
	 * Transform a slug to its legible equivalent.
	 *
	 * @param string $slug
	 * @return string
	 */
	public function beautify( $slug, $context = '' ) {
		$matrix = [
			'context_slug' => __( 'Traduction', 'ositer' ),
		];

		$key = ! empty( $context ) ? sprintf( '%1$s_%2$s', $context, $slug ) : $slug;

		return array_key_exists( $key, $matrix ) ? $matrix[ $key ] : $slug;
	}
}

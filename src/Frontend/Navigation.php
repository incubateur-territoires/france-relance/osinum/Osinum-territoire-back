<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class Navigation extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/sidebar/navigation-items', 'set_default_navigation_menu_items', 10 );
		$this->add_action( 'ositer/sidebar/navigation-items', 'add_diagnostic_item', 20 );
		$this->add_action( 'ositer/sidebar/navigation-items', 'add_results_item_on_diagnostic_pages', 30 );
	}

	/**
	 * Set the default menu items.
	 *
	 * @param array $items
	 * @return array
	 */
	public function set_default_navigation_menu_items( $items ) {
		return array_merge( $items, [
			(object) [
				'slug'     => 'tools',
				'label'    => __( 'Catalogue d\'outils', 'ositer' ),
				'url'      => get_post_type_archive_link( Config::CPT_TOOL ),
				'active'   => is_post_type_archive( Config::CPT_TOOL ) || is_singular( Config::CPT_TOOL ),
				'children' => [],
			],
			(object) [
				'slug'     => 'resources',
				'label'    => __( 'Ressources', 'ositer' ),
				'url'      => get_post_type_archive_link( Config::CPT_RESOURCE ),
				'active'   => is_post_type_archive( Config::CPT_RESOURCE ) || is_singular( Config::CPT_RESOURCE ) || is_category() || is_tag(),
				'children' => [],
			],
		] );
	}

	/**
	 * Add the Diagnostic item (maybe with children if it's not the New Diagnostic page).
	 *
	 * @param array $items
	 * @return array
	 */
	public function add_diagnostic_item( $items ) {
		$is_diagnostic_page = _ositer()->get_urls()->is_diagnostic_page();
		$is_diagnostic_archive_page = _ositer()->get_urls()->is_diagnostic_archive_page();
		$children           = [];
		$diagnostic         = null;

		if ( $is_diagnostic_page || $is_diagnostic_archive_page ) {
			$diagnostic = _ositer()->get_data()->get_current_diagnostic();
		}

		if ( ! is_null( $diagnostic ) && $is_diagnostic_page && ! _ositer()->get_urls()->is_diagnostic_creation_page() ) {
			$children   = [
				(object) [
					'slug'   => 'situations',
					'label'  => __( 'Situations <span class="color-vert-1-alt">6 min</span>', 'ositer' ),
					'url'    => $diagnostic->get_private_permalink( 'situations' ),
					'active' => get_query_var( 'step' ) === 'situations',
					'done'   => $diagnostic->has_done_step( 'situations' ),
				],
				(object) [
					'slug'   => 'criteres',
					'label'  => __( 'Critères <span class="color-vert-1-alt">10 min</span>', 'ositer' ),
					'url'    => $diagnostic->get_private_permalink( 'criteres' ),
					'active' => get_query_var( 'step' ) === 'criteres',
					'done'   => $diagnostic->has_done_step( 'criteres' ),
				],
				(object) [
					'slug'   => 'outils',
					'label'  => __( 'Outils <span class="color-vert-1-alt">10 min</span>', 'ositer' ),
					'url'    => $diagnostic->get_private_permalink( 'outils' ),
					'active' => get_query_var( 'step' ) === 'outils',
					'done'   => $diagnostic->has_done_step( 'outils' ),
				],
				(object) [
					'slug'   => 'difficultes',
					'label'  => __( 'Difficultés <span class="color-vert-1-alt">5 min</span>', 'ositer' ),
					'url'    => $diagnostic->get_private_permalink( 'difficultes' ),
					'active' => get_query_var( 'step' ) === 'difficultes',
					'done'   => $diagnostic->has_done_step( 'difficultes' ),
				],
			];
		}

		$items[] = (object) [
			'slug'     => 'diagnostic',
			'label'    => __( 'Diagnostic', 'ositer' ),
			'icon'     => 'chargement',
			'url'      => ! is_null( $diagnostic ) ? $diagnostic->get_private_permalink() : _ositer()->get_urls()->get_url( 'diagnostic-create' ),
			'active'   => ! is_null( $diagnostic ) && $is_diagnostic_page && ! _ositer()->get_urls()->is_diagnostic_results_page() && ! _ositer()->get_urls()->is_diagnostic_archive_page(),
			'children' => $children,
		];

		return $items;
	}

	/**
	 * Add the Results item only on the Diagnostic pages (but without children on Diagnostic home).
	 *
	 * @param array $items
	 * @return array
	 */
	public function add_results_item_on_diagnostic_pages( $items ) {
		if ( _ositer()->get_urls()->is_diagnostic_page() && ! _ositer()->get_urls()->is_diagnostic_creation_page() ) {
			$is_results_page = _ositer()->get_urls()->is_diagnostic_results_page();
			$children        = [];
			$diagnostic      = _ositer()->get_data()->get_current_diagnostic();

			if ( $is_results_page ) {
				$children   = [
					(object) [
						'slug'     => 'results-situations',
						'label'    => __( 'Situations', 'ositer' ),
						'url'      => $diagnostic->get_private_permalink( 'resultats/situations' ),
						'active'   => get_query_var( 'step' ) === 'resultats-situations',
					],
					(object) [
						'slug'     => 'results-criteres',
						'label'    => __( 'Critères', 'ositer' ),
						'url'      => $diagnostic->get_private_permalink( 'resultats/criteres' ),
						'active'   => get_query_var( 'step' ) === 'resultats-criteres',
					],
					(object) [
						'slug'     => 'results-outils',
						'label'    => __( 'Outils', 'ositer' ),
						'url'      => $diagnostic->get_private_permalink( 'resultats/outils' ),
						'active'   => get_query_var( 'step' ) === 'resultats-outils',
					],
					(object) [
						'slug'     => 'results-difficultes',
						'label'    => __( 'Difficultés', 'ositer' ),
						'url'      => $diagnostic->get_private_permalink( 'resultats/difficultes' ),
						'active'   => get_query_var( 'step' ) === 'resultats-difficultes',
					],
				];
			}

			$items[]    = (object) [
				'slug'     => 'results',
				'label'    => __( 'Résultats', 'ositer' ),
				'url'      => $diagnostic->get_private_permalink( 'resultats' ),
				'active'   => $is_results_page,
				'children' => $children,
			];
		}

		return $items;
	}
}

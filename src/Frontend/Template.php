<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;

class Template extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'template_include', 'force_frontend_template', 9999 );
		$this->add_action( 'body_class', 'add_custom_body_class' );
	}

	/**
	 * Get full path to a template file.
	 *
	 * @param string $file
	 * @return string
	 */
	public function get_template_file( $file ) {
		$file = sprintf(
			'%1$s/templates/%2$s.php',
			$this->get_plugin()->get_plugin_path(),
			$file
		);

		return file_exists( $file ) ? $file : null;
	}

	/**
	 * On single Diagnostic pages (create/edit/print), load a template file from the plugin.
	 *
	 * @return string
	 */
	public function force_frontend_template( $template = '' ) {
		if ( _ositer()->get_urls()->is_diagnostic_page() ) {
			if ( _ositer()->get_urls()->is_diagnostic_print_page() ) {
				$template = $this->get_template_file( 'single-diagnostic-print' );
			} else {
				$template = $this->get_template_file( 'single-diagnostic' );
			}
		}

		if ( _ositer()->get_urls()->is_diagnostic_archive_page() ) {
			if ( _ositer()->get_urls()->is_diagnostic_tools_page() ) {
				$template = $this->get_template_file( 'archive-tools' );
			}
			if ( _ositer()->get_urls()->is_diagnostic_resources_page() ) {
				$template = $this->get_template_file( 'archive-resources' );
			}
		}

		if ( _ositer()->get_urls()->is_singular_cpt() ) {
			if ( _ositer()->get_urls()->is_singular_tool() ) {
				$template = $this->get_template_file( 'single-tool' );
			}
			if ( _ositer()->get_urls()->is_singular_resource() ) {
				$template = $this->get_template_file( 'single-resource' );
			}
		}

		return $template;
	}

	/**
	 * Add custom body class on the Diagnostic page.
	 *
	 * @param array $classes
	 * @return array
	 */
	public function add_custom_body_class( $classes = [] ) {
		if ( _ositer()->get_urls()->is_diagnostic_page() ) {
			$classes[] = 'single-diagnostic';
		}

		$step = get_query_var( 'step', null );

		if ( ! is_null( $step ) ) {
			$classes[] = 'step-' . $step;

			if ( strpos( $step, 'resultats' ) !== false ) {
				$classes[] = 'diagnostic-results';
			}
		}

		return $classes;
	}

	/**
	 * Render a specific card for a content Model.
	 *
	 * @param object|OsinumTerritoire\Models\Abstracts\Post|OsinumTerritoire\Models\Abstracts\Term $data
	 * @param string $type
	 * @param string $class
	 * @return void
	 */
	public function render_card( $data, $type = '', $class = '' ) {
		// Prepare $data to be a proper Model, not a simple JSON object.
		$matrix = [
			'diagnostic'       => '\OsinumTerritoire\Models\Diagnostic',
			'tool'             => '\OsinumTerritoire\Models\Tool',
			'situation'        => '\OsinumTerritoire\Models\Situation',
			'difficulty'       => '\OsinumTerritoire\Models\Difficulty',
			'resource'         => '\OsinumTerritoire\Models\Resource',
			'topic'            => '\OsinumTerritoire\Models\Topic',
			'practice'         => '\OsinumTerritoire\Models\Practice',
			'criteria'         => '\OsinumTerritoire\Models\Criteria',
			'difficulty_group' => '\OsinumTerritoire\Models\DifficultyGroup',
		];

		if ( $data instanceof \stdClass ) {
			$data = new $matrix[ $type ]( $data->id );
		} elseif ( is_numeric( $data ) ) {
			$data = new $matrix[ $type ]( (int) $data );
		}

		// Template from the theme.
		ob_start();
		get_template_part( "template-parts/cards/{$type}", null, [ $type => $data, 'class' => $class ] );
		$card = ob_get_clean();

		if ( empty( $card ) ) {
			extract( [ $type => $data ] );

			$file = $this->get_template_file( "cards/{$type}" );

			if ( ! is_null( $file ) ) {
				include $file;
			}
		} else {
			echo $card;
		}
	}
}

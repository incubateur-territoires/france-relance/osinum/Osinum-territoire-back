<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Models\Diagnostic;
use OsinumTerritoire\Helpers;

class WebRenderer extends Component {
	/**
	 * Current Diagnostic.
	 *
	 * @var Diagnostic
	 */
	protected $diagnostic = null;

	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		// Layout.
		$this->add_action( 'ositer/frontend/single-diagnostic/header', 'output_header' );
		$this->add_action( 'ositer/frontend/single-diagnostic/sidebar', 'output_sidebar' );
		$this->add_action( 'ositer/frontend/single-diagnostic/body', 'output_body' );
		$this->add_action( 'ositer/frontend/single-diagnostic/body', 'output_popups', 9000 );

		// Renderer for each step (accueil, situations, outils, critères, resultats).
		$this->add_action( 'ositer/frontend/single-diagnostic/step/accueil', 'output_step_accueil' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/situations', 'output_step_situations' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/outils', 'output_step_outils' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/criteres', 'output_step_criteres' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/difficultes', 'output_step_difficultes' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/resultats', 'output_step_resultats_home' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/resultats-situations', 'output_step_resultats_situations' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/resultats-criteres', 'output_step_resultats_criteres' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/resultats-outils', 'output_step_resultats_outils' );
		$this->add_action( 'ositer/frontend/single-diagnostic/step/resultats-difficultes', 'output_step_resultats_difficultes' );

		$this->add_action( 'ositer/frontend/single-diagnostic/steps/archive-resources', 'output_archive_resources' );
		$this->add_action( 'ositer/frontend/single-diagnostic/steps/archive-tools', 'output_archive_tools' );

		$this->add_action( 'ositer/frontend/single-diagnostic/steps/single-resource', 'output_single_resource' );
		$this->add_action( 'ositer/frontend/single-diagnostic/steps/single-tool', 'output_single_tool' );

		// Inject the Permalink widget at the bottom of the left sidebar.
		$this->add_action( 'ositer/sidebar/after-navigation', 'output_permalink_widget' );
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get current Diagnostic.
	 *
	 * @return Diagnostic|null
	 */
	public function get_current_diagnostic() {
		if ( is_null( $this->diagnostic ) ) {
			$this->diagnostic = _ositer()->get_data()->get_current_diagnostic();
		}

		return $this->diagnostic;
	}

	//=======================================================
	//                                                       
	//   #####   ##   ##  ######  #####   ##   ##  ######  
	//  ##   ##  ##   ##    ##    ##  ##  ##   ##    ##    
	//  ##   ##  ##   ##    ##    #####   ##   ##    ##    
	//  ##   ##  ##   ##    ##    ##      ##   ##    ##    
	//   #####    #####     ##    ##       #####     ##    
	//                                                       
	//=======================================================

	/**
	 * Output the Diagnostic page header bar (home link + mobile burger button).
	 *
	 * @return void
	 */
	public function output_header() {
		$diagnostic = _ositer()->get_data()->get_current_diagnostic();
		if ( ! is_null( $diagnostic ) ) {
			include_once _ositer()->get_frontend()->template->get_template_file( 'partials/header' );
		}
	}

	/**
	 * Output the Diagnostic sidebar (navigation + "copy link" area).
	 *
	 * @return void
	 */
	public function output_sidebar() {
		include_once _ositer()->get_frontend()->template->get_template_file( 'partials/sidebar' );
	}

	/**
	 * Output the Diagnostic page content.
	 *
	 * @return void
	 */
	public function output_body() {
		$diagnostic = $this->get_current_diagnostic();
		$action     = get_query_var( 'action', 'diagnostic-edit' );
		$step       = get_query_var( 'step', 'accueil' );

		ob_start();

		if ( _ositer()->get_urls()->is_diagnostic_archive_page() ) {
			if ( _ositer()->get_urls()->is_diagnostic_tools_page() ) {
				do_action( "ositer/frontend/single-diagnostic/steps/archive-tools", $diagnostic );
			}
			if ( _ositer()->get_urls()->is_diagnostic_resources_page() ) {
				do_action( "ositer/frontend/single-diagnostic/steps/archive-resources", $diagnostic );
			}
		} elseif ( _ositer()->get_urls()->is_singular_cpt() ) {
			if ( _ositer()->get_urls()->is_singular_tool() ) {
				do_action( "ositer/frontend/single-diagnostic/steps/single-tool", $diagnostic );
			}
			if ( _ositer()->get_urls()->is_singular_resource() ) {
				do_action( "ositer/frontend/single-diagnostic/steps/single-resource", $diagnostic );
			}
		} else {
			switch ( $action ) {
				default:
				case 'diagnostic-create':
				case 'diagnostic-edit':
					do_action( "ositer/frontend/single-diagnostic/step/{$step}", $diagnostic );
					break;
			}
		}

		$content = ob_get_clean();
		printf( '<div class="page-content">%1$s</div>', $content );
	}

	//=========================================
	//                                         
	//   ####  ######  #####  #####    ####  
	//  ##       ##    ##     ##  ##  ##     
	//   ###     ##    #####  #####    ###   
	//     ##    ##    ##     ##         ##  
	//  ####     ##    #####  ##      ####   
	//                                         
	//=========================================

	/**
	 * Output the Diagnostic step #0: accueil.
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_accueil( $diagnostic ) {
		$is_creation = is_null( $diagnostic );
		$is_edition  = ! $is_creation;

		include_once _ositer()->get_frontend()->template->get_template_file( 'steps/accueil' );
	}

	/**
	 * Output the Diagnostic step #1: situations.
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_situations( $diagnostic ) {
		include_once _ositer()->get_frontend()->template->get_template_file( 'steps/situations' );
	}

	/**
	 * Output the Diagnostic step #2: outils.
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_outils( $diagnostic ) {
		include_once _ositer()->get_frontend()->template->get_template_file( 'steps/outils' );
	}

	/**
	 * Output the Diagnostic step #3: criteres.
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_criteres( $diagnostic ) {
		include_once _ositer()->get_frontend()->template->get_template_file( 'steps/criteres' );
	}

	/**
	 * Output the Diagnostic step #4: difficultes.
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_difficultes( $diagnostic ) {
		include_once _ositer()->get_frontend()->template->get_template_file( 'steps/difficultes' );
	}

	/**
	 * Output the Diagnostic: résultats (home).
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_resultats_home( $diagnostic ) {
		$current_step     = 'accueil';
		$lead_form_values = $diagnostic->get_lead_form_values();

		include_once _ositer()->get_frontend()->template->get_template_file( 'steps/resultats' );
	}

	/**
	 * Output the Diagnostic: résultats (home).
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_resultats_situations( $diagnostic, $print = false ) {
		$current_step            = 'situations';
		$situations              = _ositer()->get_data()->get_situations_json_objects();
		$topics                  = _ositer()->get_data()->get_topics_json_objects();
		$selected_situations_ids = $diagnostic->get_situations( true );
		$selected_situations     = $diagnostic->get_situations();
		$suggested_practices     = $diagnostic->get_suggested_practices( true );
		$suggested_tools         = $diagnostic->get_suggested_tools( true );
		$suggested_resources     = [];

		usort( $suggested_practices, function( $a, $b ) {
			return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
		} );

		usort( $suggested_tools, function( $a, $b ) {
			return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
		} );

		foreach ( $selected_situations as $situation ) {
			foreach ( $situation->get_resources() as $resource ) {
				if ( ! array_key_exists( $resource->get_id(), $suggested_resources ) ) {
					$suggested_resources[ $resource->get_id() ] = $resource;
				}
			}
		}
		
		include_once _ositer()->get_frontend()->template->get_template_file( ! $print ? 'steps/resultats-situations' : 'pdf/steps/situations' );
	}

	/**
	 * Output the Diagnostic: résultats (home).
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_resultats_criteres( $diagnostic, $print = false ) {
		$current_step        = 'criteres';
		$criterias           = _ositer()->get_data()->get_criterias_json_objects();
		$evaluated_criterias = $diagnostic->get_evaluated_criterias();
		$tools               = $diagnostic->get_selected_tools( true );

		// Inject rating in Criteria objects.
		foreach ( $criterias as $criteria ) {
			$evaluation       = Helpers::array_find( fn ( $evaluation ) => $evaluation->id === $criteria->id, $evaluated_criterias );
			$criteria->rating = $evaluation ? $evaluation->rating : null;
			$criteria->tools  = array_values( array_filter( $tools, fn ( $tool ) => in_array( $criteria->id, $tool->criterias, true ) ) );
		}

		usort( $criterias, function( $a, $b ) {
			return $a->rating === $b->rating ? 0 : ( $a->rating > $b->rating ? -1 : 1 );
		} );

		include_once _ositer()->get_frontend()->template->get_template_file( ! $print ? 'steps/resultats-criteres' : 'pdf/steps/criteres' );
	}

	/**
	 * Output the Diagnostic: résultats (home).
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_resultats_outils( $diagnostic, $print = false ) {
		$current_step   = 'outils';
		$practices      = _ositer()->get_data()->get_practices_json_objects();
		$tools          = array_merge( _ositer()->get_data()->get_tools_json_objects(), $diagnostic->get_custom_tools( true ) );
		$selected_tools = $diagnostic->get_selected_tools();
		$criterias      = _ositer()->get_data()->get_criterias_json_objects();

		// Enhance selected tools.
		$selected_tools = array_filter( array_map( function( $selected_tool ) use ( $tools ) {
			$full_tool = Helpers::array_find( fn ( $tool ) => $tool->id === $selected_tool->id, $tools );

			if ( ! $full_tool ) {
				return null;
			}
			
			$full_tool->configuration = $selected_tool->configuration;

			return $full_tool;
		}, $selected_tools ) );

		// Put custom tools at the top.
		usort( $selected_tools, function( $a, $b ) {
			return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
		} );

		// Enhance Practices objects to inject the selected tools in it.
		$practices = array_map( function( $practice ) use ( $selected_tools ) {
			$practice->selected_tools = array_values( array_filter( $selected_tools, fn ( $selected_tool ) => in_array( $practice->id, $selected_tool->configuration->practices, true ) ) );
			return $practice;
		}, $practices );

		// Filter practices to remove the ones without selected tools.
		$practices = array_values( array_filter( $practices, fn ( $practice ) => count( $practice->selected_tools ) > 0 ) );

		// Inject the "satisfaction" and "frequency" fake criterias at the beginning of the array.
		$criterias = array_merge(
			[
				(object) [
					'id'     => 'satisfaction',
					'title'  => __( 'Satisfaction d\'utilisation', 'ositer' ),
				],
				(object) [
					'id'     => 'frequency',
					'title'  => __( 'Fréquence d\'utilisation', 'ositer' ),
				],
			],
			$criterias
		);

		include_once _ositer()->get_frontend()->template->get_template_file( ! $print ? 'steps/resultats-outils' : 'pdf/steps/outils' );
	}

	/**
	 * Output the Diagnostic: résultats (home).
	 *
	 * @param Diagnostic|null $diagnostic Could be null if this is the creation page.
	 * @return void
	 */
	public function output_step_resultats_difficultes( $diagnostic, $print = false ) {
		$current_step              = 'difficultes';
		$difficulties              = _ositer()->get_data()->get_difficulties_json_objects();
		$groups                    = _ositer()->get_data()->get_difficulty_groups_json_objects();
		$selected_difficulties     = $diagnostic->get_difficulties();
		$selected_difficulties_ids = array_map( fn ( $difficulty ) => $difficulty->get_id(), $selected_difficulties );
		$selected_groups           = [];
		$suggested_resources       = [];

		foreach ( $selected_difficulties as $difficulty ) {
			foreach ( $difficulty->get_groups( true ) as $group_id ) {
				if ( ! array_key_exists( $group_id, $selected_groups ) ) {
					$selected_groups[ $group_id ] = ositer()->get_difficulty_group( $group_id );
				}
			}
		}

		foreach ( $selected_groups as $group ) {
			foreach ( $group->get_resources() as $resource ) {
				if ( ! array_key_exists( $resource->get_id(), $suggested_resources ) ) {
					$suggested_resources[ $resource->get_id() ] = $resource;
				}
			}
		}

		include_once _ositer()->get_frontend()->template->get_template_file( ! $print ? 'steps/resultats-difficultes' : 'pdf/steps/difficultes' );
	}

	/**
	 * Output Resource CPT archive
	 */
	public function output_archive_resources() {
		// Template from the theme.
		ob_start();
		get_template_part( "template-parts/steps/archive-resources" );
		$archive = ob_get_clean();

		if ( empty( $archive ) ) {
			include_once _ositer()->get_frontend()->template->get_template_file( 'steps/archive-resources' );
		} else {
			echo $archive;
		}
	}

	/**
	 * Output Tool CPT archive
	 */
	public function output_archive_tools() {
		// Template from the theme.
		ob_start();
		get_template_part( "template-parts/steps/archive-tools" );
		$archive = ob_get_clean();

		if ( empty( $archive ) ) {
			include_once _ositer()->get_frontend()->template->get_template_file( 'steps/archive-tools' );
		} else {
			echo $archive;
		}
	}

	/**
	 * Output Tool CPT Single
	 */
	public function output_single_tool() {
		// Template from the theme.
		ob_start();
		get_template_part( "template-parts/steps/single-tool" );
		$single = ob_get_clean();

		if ( empty( $single ) ) {
			include_once _ositer()->get_frontend()->template->get_template_file( 'steps/single-tool' );
		} else {
			echo $single;
		}
	}

	/**
	 * Output Resource CPT Single
	 */
	public function output_single_resource() {
		// Template from the theme.
		ob_start();
		get_template_part( "template-parts/steps/single-resource" );
		$single = ob_get_clean();

		if ( empty( $single ) ) {
			include_once _ositer()->get_frontend()->template->get_template_file( 'steps/single-resource' );
		} else {
			echo $single;
		}
	}

	/**
	 * Output the Diagnostic permalink copy button widget.
	 *
	 * @return void
	 */
	public function output_permalink_widget() {
		$diagnostic = _ositer()->get_data()->get_current_diagnostic();

		if ( ! is_null( $diagnostic ) && ! _ositer()->get_urls()->is_diagnostic_archive_page() && ! _ositer()->get_urls()->is_singular_cpt() ) {
			include_once _ositer()->get_frontend()->template->get_template_file( 'partials/permalink-widget' );
		}
	}

	/**
	 * Output popups.
	 *
	 * @return void
	 */
	public function output_popups() {
		$diagnostic = _ositer()->get_data()->get_current_diagnostic();

		if ( $diagnostic ) {
			Helpers::output_popup( 'permalink', __( 'Le permalien', 'ositer' ) );
			Helpers::output_popup( 'share', __( 'Partager le diagnostic', 'ositer' ), [ 'diagnostic' => $diagnostic, ] );
		}

		if ( _ositer()->get_urls()->is_singular_resource() ) {
			$resource = new \OsinumTerritoire\Models\Resource( get_the_ID() );
			Helpers::output_popup( 'share-single-resource', __( 'Partager la ressource', 'ositer' ), [ 'resource' => $resource, ] );
		}

		if ( _ositer()->get_urls()->is_singular_tool() ) {
			$tool = new \OsinumTerritoire\Models\Tool( get_the_ID() );
			Helpers::output_popup( 'share-single-tool', __( 'Partager l\'outil', 'ositer' ), [ 'tool' => $tool, ] );
		}
	}
}

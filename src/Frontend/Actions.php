<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;

class Actions extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @todo Sub-classes for each action.
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'template_redirect', 'clear_diag_cookies_on_new_diag_creation_page' );
	}

	/**
	 * Clear cookies on new diag creation page.
	 *
	 * @return void
	 */
	public function clear_diag_cookies_on_new_diag_creation_page() {
		if (
			isset( $_COOKIE['diagID'] )
			&& isset( $_COOKIE['diagToken'] )
			&& get_query_var( 'action', null ) === 'diagnostic-create'
		) {
			setcookie( 'diagID', '', time() - 3600, '/' );
			setcookie( 'diagToken', '', time() - 3600, '/' );

			$url = _ositer()->get_urls()->get_url( 'diagnostic-create' );

			if ( isset( $_GET['open'] ) ) {
				$url = add_query_arg( 'open', sanitize_text_field( $_GET['open'] ), $url );
			}

			wp_safe_redirect( $url );
			exit;
		}
	}
}

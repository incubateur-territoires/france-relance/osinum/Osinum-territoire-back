<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Diagnostic;

class DiagnosticJson extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_filter( 'ositer/frontend/diagnostic/js', 'add_default_data_to_json', 10, 3 );
		$this->add_filter( 'ositer/frontend/diagnostic/js/situations', 'add_data_to_json_for_step_situations', 10, 2 );
		$this->add_filter( 'ositer/frontend/diagnostic/js/criteres', 'add_data_to_json_for_step_criteres', 10, 2 );
		$this->add_filter( 'ositer/frontend/diagnostic/js/outils', 'add_data_to_json_for_step_outils', 10, 2 );
		$this->add_filter( 'ositer/frontend/diagnostic/js/difficultes', 'add_data_to_json_for_step_difficultes', 10, 2 );
		$this->add_filter( 'ositer/frontend/diagnostic/js/resultats', 'add_data_to_json_for_step_resultats', 10, 2 );
		$this->add_filter( 'ositer/frontend/diagnostic/js/resultats-outils', 'add_data_to_json_for_step_resultats_outils', 10, 2 );
	}

	/**
	 * Inject extra data into JSON for ALL steps.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 * @param string $step
	 */
	public function add_default_data_to_json( $data, $diagnostic, $step ) {
		$data['debug']        = WP_DEBUG;
		$data['bypassIntro']  = defined( 'OSITER_ALWAYS_DISPLAY_STEP_INTRO' ) && ! OSITER_ALWAYS_DISPLAY_STEP_INTRO;
		$data['current_step'] = $step;
		$data['auto_open']    = isset( $_GET['open'] ) ? sanitize_text_field( $_GET['open'] ) : false;
		$data['diagnostic']   = $diagnostic ? [
			'id'         => $diagnostic->get_id(),
			'token'      => $diagnostic->get_token(),
			'permalinks' => [
				'home'    => $diagnostic->get_private_permalink(),
				'results' => $diagnostic->get_private_permalink( 'resultats' ),
				'print'   => $diagnostic->get_private_permalink( 'imprimer' ),
			],
			'steps' => [
				'situations' => (object) [
					'permalink' => $diagnostic->get_private_permalink( 'situations' ),
					'completed' => $diagnostic->has_done_step( 'situations' ),
				],
				'criteres' => (object) [
					'permalink' => $diagnostic->get_private_permalink( 'criteres' ),
					'completed' => $diagnostic->has_done_step( 'criteres' ),
				],
				'outils' => (object) [
					'permalink' => $diagnostic->get_private_permalink( 'outils' ),
					'completed' => $diagnostic->has_done_step( 'outils' ),
				],
				'difficultes' => (object) [
					'permalink' => $diagnostic->get_private_permalink( 'difficultes' ),
					'completed' => $diagnostic->has_done_step( 'difficultes' ),
				],
			],
			'data' => [],
		] : null;

		return $data;
	}

	/**
	 * Inject extra data into JSON for step: Situations.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 */
	public function add_data_to_json_for_step_situations( $data, $diagnostic ) {
		$data['situations'] = _ositer()->get_data()->get_situations_json_objects();
		$data['topics']     = _ositer()->get_data()->get_topics_json_objects();

		// Add previously selected Situations.
		$data['diagnostic']['data']['situations'] = $diagnostic->get_situations( true );

		return $data;
	}

	/**
	 * Inject extra data into JSON for step: Critères.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 */
	public function add_data_to_json_for_step_criteres( $data, $diagnostic ) {
		$data['criterias'] = _ositer()->get_data()->get_criterias_json_objects();

		// Add previously evaluated Criterias.
		$data['diagnostic']['data']['evaluated_criterias'] = $diagnostic->get_evaluated_criterias();

		return $data;
	}

	/**
	 * Inject extra data into JSON for step: Outils.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 */
	public function add_data_to_json_for_step_outils( $data, $diagnostic ) {
		$data['practices'] = _ositer()->get_data()->get_practices_json_objects();
		$data['tools']     = array_merge( _ositer()->get_data()->get_tools_json_objects(), $diagnostic->get_custom_tools( true ) );

		usort( $data['tools'], function( $a, $b ) {
			return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
		} );

		// Add previously selected Tools.
		$data['diagnostic']['data']['selected_tools'] = $diagnostic->get_selected_tools();

		return $data;
	}

	/**
	 * Inject extra data into JSON for step: Difficultés.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 */
	public function add_data_to_json_for_step_difficultes( $data, $diagnostic ) {
		$data['difficulties'] = _ositer()->get_data()->get_difficulties_json_objects();
		$data['groups']       = _ositer()->get_data()->get_difficulty_groups_json_objects();

		// Add previously selected Difficulties.
		$data['diagnostic']['data']['difficulties'] = $diagnostic->get_difficulties( true );

		return $data;
	}

	/**
	 * Inject extra data into JSON for step: Résultats.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 */
	public function add_data_to_json_for_step_resultats( $data, $diagnostic ) {
		$data['diagnostic']['has_completed_lead_form'] = $diagnostic->has_completed_lead_form();

		return $data;
	}

	/**
	 * Inject extra data into JSON for step: Résultats Outils.
	 * 
	 * @param array $data
	 * @param Diagnostic $diagnostic
	 */
	public function add_data_to_json_for_step_resultats_outils( $data, $diagnostic ) {
		$data['criterias'] = _ositer()->get_data()->get_criterias_json_objects();
		$data['tools']     = array_merge( _ositer()->get_data()->get_tools_json_objects(), $diagnostic->get_custom_tools( true ) );

		usort( $data['tools'], function( $a, $b ) {
			return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
		} );

		$data['diagnostic']['data']['selected_tools'] = $diagnostic->get_selected_tools( true );

		return $data;
	}
}

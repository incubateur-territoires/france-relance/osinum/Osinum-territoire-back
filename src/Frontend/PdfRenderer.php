<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Models\Diagnostic;
use Dompdf\Dompdf;
use OsinumTerritoire\Helpers;
use OsinumTerritoire\Plugin;

class PdfRenderer extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/frontend/single-diagnostic/print', 'output_pdf' );
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get current Diagnostic.
	 *
	 * @return Diagnostic|null
	 */
	public function get_current_diagnostic() {
		return $this->get_plugin()->get_frontend()->get_module( 'WebRenderer' )->get_current_diagnostic();
	}

	//=======================================================
	//                                                       
	//   #####   ##   ##  ######  #####   ##   ##  ######  
	//  ##   ##  ##   ##    ##    ##  ##  ##   ##    ##    
	//  ##   ##  ##   ##    ##    #####   ##   ##    ##    
	//  ##   ##  ##   ##    ##    ##      ##   ##    ##    
	//   #####    #####     ##    ##       #####     ##    
	//                                                       
	//=======================================================

	/**
	 * Output the Diagnostic PDF content.
	 *
	 * @return void
	 */
	public function output_pdf() {
		define( 'OSINUM_RENDERING_PDF', true );

		$diagnostic = $this->get_current_diagnostic();
		$pdf        = new Dompdf( [ 'isFontSubsettingEnabled' => false ] );
		$options    = $pdf->getOptions();

		$options->set( [
			'isRemoteEnabled' => true,
			'debugLayout'     => false,
		] );

		$pdf->setOptions( $options );
		$pdf->setPaper( 'a4', 'landscape' );

		// Get content from template.
		ob_start();
		include_once _ositer()->get_frontend()->template->get_template_file( 'pdf/document' );
		$content = ob_get_clean();

		$pdf->loadHtml( $content );
		$pdf->render();

		// Add page number footer.
		$pdf->getCanvas()->page_text( 770, 550, "p. {PAGE_NUM}/{PAGE_COUNT}", $pdf->getFontMetrics()->getFont( 'Open Sans', 'normal' ), 10, [ 0, 0, 0 ] );

		$pdf->stream( $diagnostic->get_pdf_filename(), [ 'Attachment' => 0 ] );
	}

	/**
	 * Output PDF assets (CSS stylesheet).
	 *
	 * @return void
	 */
	public function output_pdf_assets() {
		$enqueuer = new \WPackio\Enqueue(
			'OsinumTerritoireFront',
			'dist',
			Plugin::VERSION,
			'plugin',
			sprintf( '%1$s/%2$s', $this->get_plugin()->get_plugin_path(), 'osinum-territoire-back' )
		);

		// Enqueue CSS.
		$css = $enqueuer->enqueue( 'ositer_front_css', 'frontend', [
			'js'        => false,
			'css'       => true,
			'js_dep'    => [],
			'css_dep'   => [],
		] );

		$css_url = Helpers::array_find( function( $item ) {
			return strpos( $item['handle'], 'front_css' ) !== false;
		}, $css['css'] )['url'];

		printf(
			'<link rel="stylesheet" href="%1$s" media="all" />',
			apply_filters( 'ositer/pdf/css-url', $css_url )
		);
	}

	/**
	 * Output a specific step/module.
	 * Uses the WebRenderer module to render the step, but with a different template file to alter the result.
	 *
	 * @param string $step
	 * @return void
	 */
	public function output_step( $step ) {
		$renderer = $this->get_plugin()->get_frontend()->get_module( 'WebRenderer' );

		if ( method_exists( $renderer, "output_step_resultats_{$step}" ) ) {
			$renderer->{"output_step_resultats_{$step}"}( $this->get_current_diagnostic(), true );
		}
	}
}

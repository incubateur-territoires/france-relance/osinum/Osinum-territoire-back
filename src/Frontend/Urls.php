<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class Urls extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'init', [ $this, 'add_custom_rewrite_rules' ] );
		add_action( 'ositer/activation', [ $this, 'flush_rules' ] );
		add_action( 'template_redirect', [ $this, 'maybe_redirect_diagnostic_pages' ] );
	}

	/**
	 * Flush rules on plugin activation.
	 *
	 * @return void
	 */
	public function flush_rules() {
		flush_rewrite_rules();
	}

	/**
	 * Get a specific URL.
	 *
	 * @param string $key
	 * @param array $params
	 * @return string
	 */
	public function get_url( $key, $params = [] ) {
		switch ( $key ) {
			default:
			case 'homepage':
			case 'home':
				return home_url();

			case 'new-diagnostic':
			case 'diagnostic-create':
				return sprintf( '%1$s/diag/nouveau', untrailingslashit( home_url() ) );

			case 'tools':
				return get_post_type_archive_link( Config::CPT_TOOL );

			case 'blog':
				return get_post_type_archive_link( 'post' );
				
			case 'ressources':
			case 'resources':
				return get_post_type_archive_link( Config::CPT_RESOURCE );
		}
	}

	/**
	 * Add custom rewrite rules/URLs for the front-end.
	 *
	 * @return void
	 */
	public function add_custom_rewrite_rules() {
		add_rewrite_tag( '%action%', '([^&]+)' );
		add_rewrite_tag( '%step%', '([^&]+)' );
		add_rewrite_tag( '%diagnostic_id%', '([^&]+)' );
		add_rewrite_tag( '%diagnostic_token%', '([^&]+)' );
		add_rewrite_rule( 'diag/nouveau?$', 'index.php?action=diagnostic-create', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/situations?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=situations', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/outils?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=outils', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/criteres?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=criteres', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/difficultes?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=difficultes', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/resultats?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=resultats', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/resultats/situations?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=resultats-situations', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/resultats/criteres?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=resultats-criteres', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/resultats/outils?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=resultats-outils', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/resultats/difficultes?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-edit&step=resultats-difficultes', 'top' );
		add_rewrite_rule( 'diag/([^/]+)/([^/]+)/imprimer?$', 'index.php?diagnostic_id=$matches[1]&diagnostic_token=$matches[2]&action=diagnostic-print', 'top' );
	}

	/**
	 * Do some redirections on single Diagnostic page.
	 *
	 * @return void
	 */
	public function maybe_redirect_diagnostic_pages() {
		if ( $this->is_diagnostic_edition_page() || $this->is_diagnostic_print_page() ) {
			$token      = get_query_var( 'diagnostic_token', null );
			$diagnostic = _ositer()->get_data()->get_current_diagnostic();
			$can_access = ( $diagnostic && $diagnostic->can_access( $token ) );

			if ( ! $can_access ) {
				wp_safe_redirect( $this->get_url( 'diagnostic-create' ) );
				exit();
			}
		}
	}

	//=======================================
	//                                       
	//   ####   #####   ##     ##  ####    
	//  ##     ##   ##  ####   ##  ##  ##  
	//  ##     ##   ##  ##  ## ##  ##  ##  
	//  ##     ##   ##  ##    ###  ##  ##  
	//   ####   #####   ##     ##  ####    
	//                                       
	//=======================================

	/**
	 * Is this a Diagnostic page (create/edit/print)?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_page() {
		return $this->is_diagnostic_creation_page() || $this->is_diagnostic_edition_page() || $this->is_diagnostic_print_page();
	}

	/**
	 * Is this the "Create new Diagnostic" page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_creation_page() {
		return get_query_var( 'action' ) === 'diagnostic-create';
	}

	/**
	 * Is this the "Edit existing Diagnostic" page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_edition_page() {
		return (
			get_query_var( 'action' ) === 'diagnostic-edit'
			&& ! is_null( get_query_var( 'diagnostic_id', null ) )
			&& ! is_null( get_query_var( 'diagnostic_token', null ) )
		);
	}

	/**
	 * Is this one of the Diagnostic Results page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_results_page() {
		return (
			$this->is_diagnostic_edition_page()
			&& in_array(
				get_query_var( 'step' ),
				[ 'resultats', 'resultats-situations', 'resultats-criteres', 'resultats-outils', 'resultats-difficultes', ],
				true
			)
		);
	}

	/**
	 * Is this the an archive page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_archive_page() {
		return (
			is_post_type_archive( Config::CPT_TOOL ) || is_post_type_archive( ( Config::CPT_RESOURCE ) )
		);
	}

	/**
	 * Is this the "Tools" archive page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_tools_page() {
		return (
			is_post_type_archive( Config::CPT_TOOL )
		);
	}

	/**
	 * Is this the "Resources" archive page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_resources_page() {
		return (
			is_post_type_archive( Config::CPT_RESOURCE )
		);
	}

	/**
	 * Is this the "Singular" page?
	 *
	 * @return boolean
	 */
	public function is_singular_cpt() {
		return (
			is_singular( [ Config::CPT_RESOURCE, Config::CPT_TOOL ] )
		);
	}

	/**
	 * Is this the "Singular" Tool page?
	 *
	 * @return boolean
	 */
	public function is_singular_tool() {
		return (
			is_singular( [ Config::CPT_TOOL ] )
		);
	}

	/**
	 * Is this the "Singular" Resource page?
	 *
	 * @return boolean
	 */
	public function is_singular_resource() {
		return (
			is_singular( [ Config::CPT_RESOURCE ] )
		);
	}


	/**
	 * Is this the "Print existing Diagnostic" page?
	 *
	 * @return boolean
	 */
	public function is_diagnostic_print_page() {
		return (
			get_query_var( 'action' ) === 'diagnostic-print'
			&& ! is_null( get_query_var( 'diagnostic_id', null ) )
			&& ! is_null( get_query_var( 'diagnostic_token', null ) )
		);
	}

	/**
	 * Are we rendering a PDF?
	 *
	 * @return boolean
	 */
	public function is_rendering_pdf() {
		return defined( 'OSINUM_RENDERING_PDF' ) && OSINUM_RENDERING_PDF;
	}
}

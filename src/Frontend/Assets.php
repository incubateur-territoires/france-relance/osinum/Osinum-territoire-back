<?php

namespace OsinumTerritoire\Frontend;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Helpers;
use OsinumTerritoire\Plugin;

class Assets extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_front_assets' ], 10 );
		add_action( 'wp_footer', [ $this, 'inject_svg_symbols' ], 10 );
		add_action( 'admin_footer-toplevel_page_ositer-settings', [ $this, 'inject_svg_symbols' ], 10 );
	}

	/**
	 * Enqueue front JS ans CSS assets.
	 *
	 * @return void
	 */
	public function enqueue_front_assets() {
		/*if ( ! _ositer()->get_urls()->is_diagnostic_page() && ! _ositer()->get_urls()->is_diagnostic_archive_page() && ! _ositer()->get_urls()->is_singular_cpt() ) {
			return;
		}*/

		$enqueuer = new \WPackio\Enqueue(
			'OsinumTerritoire',
			'dist',
			Plugin::VERSION,
			'plugin',
			sprintf( '%1$s/%2$s', $this->get_plugin()->get_plugin_path(), 'osinum-territoire-back' )
		);

		// Enqueue CSS.
		$css = $enqueuer->enqueue( 'ositer_front_css', 'frontend', [
			'js'        => true,
			'css'       => true,
			'js_dep'    => [],
			'css_dep'   => [],
			'in_footer' => true,
		] );

		// Enqueue JS.
		$js = $enqueuer->enqueue( 'ositer_front_js', 'frontend', [
			'js'        => true,
			'css'       => false,
			'js_dep'    => [ 'jquery' ],
			'css_dep'   => [],
			'in_footer' => true,
		] );

		$data = [
			'api' => [
				'rest_url' => esc_url_raw( sprintf( '%1$s/diagnostic/v1/public/', untrailingslashit( rest_url() ) ) ),
				'nonce'    => wp_create_nonce( 'wp_rest' ),
			],
			'assets' => [
				'css_url' => $css['css'][0]['url'],
			],
		];

		$diagnostic = _ositer()->get_data()->get_current_diagnostic();
		$step       = get_query_var( 'step', 'accueil' );

		$data = apply_filters( 'ositer/frontend/diagnostic/js', $data, $diagnostic, $step );
		$data = apply_filters( "ositer/frontend/diagnostic/js/{$step}", $data, $diagnostic );

		wp_localize_script( $js['js'][1]['handle'], 'OsinumTerritoire', $data );
	}

	/**
	 * Inject SVG symbols in the footer.
	 *
	 * @return void
	 */
	public function inject_svg_symbols() {
		include _ositer()->get_frontend()->template->get_template_file( 'partials/svg-symbols' );
	}
}

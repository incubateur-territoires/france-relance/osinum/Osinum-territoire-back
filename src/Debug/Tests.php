<?php

namespace OsinumTerritoire\Debug;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;
use OsinumTerritoire\Models\ChatManager;
use OsinumTerritoire\Models\ChatMessage;

class Tests extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/ajax/before-processing', 'start_ajax_timer', 1 );
		$this->add_action( 'ositer/ajax/response', 'end_ajax_timer', 1, 2 );
		$this->add_action( 'ositer/ajax/before-processing', 'debug_ajax_requests', 10 );
		$this->add_filter( 'ositer/pdf/css-url', 'transform_pdf_css_url_with_browsersync_url', 10 );
	}

	/**
	 * Start microtime timer when receiving an AJAX request.
	 * 
	 * @param \WP_REST_Request $request
	 */
	public function start_ajax_timer( $request ) {
		if ( WP_DEBUG && class_exists( 'MSK' ) ) {
			define( 'OSITER_AJAX_START', microtime( true ) );
		}
	}

	/**
	 * End microtime timer when sending an AJAX response.
	 *
	 * @param mixed $response
	 * @param \WP_REST_Request $request
	 * @return void
	 */
	public function end_ajax_timer( $response, $request ) {
		if ( WP_DEBUG && class_exists( 'MSK' ) && defined( 'OSITER_AJAX_START' ) && OSITER_AJAX_START > 0 ) {
			$time = ( microtime( true ) - OSITER_AJAX_START ) * 1000;

			\MSK::debug( sprintf( '%1$s took %2$s ms', $request->get_route(), $time ) );
		}
	}


	/**
	 * Debug incoming AJAX requests params.
	 *
	 * @return void
	 */
	public function debug_ajax_requests( $request ) {
		if ( class_exists( 'MSK' ) ) {
			\MSK::debug( $request->get_params() );

			if ( ! empty( $request->get_file_params() ) ) {
				\MSK::debug( $request->get_file_params() );
			}
		}
	}

	/**
	 * Transform the osinum.bsa PDF assets URL into 192…
	 *
	 * @param string $url
	 * @return string
	 */
	public function transform_pdf_css_url_with_browsersync_url( $url ) {
		if ( defined( 'OSITER_USE_BROWSERSYNC_URL_FOR_PDF_CSS' ) && OSITER_USE_BROWSERSYNC_URL_FOR_PDF_CSS ) {
			$url = str_replace( 'osinum.bsa', '192.168.1.94:3000', $url );
		}

		return $url;
	}
}

<?php

namespace OsinumTerritoire\Debug;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class Ngrok extends Component {
	private $home_url;

	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		if ( 
			( defined( 'WP_CLI' ) && WP_CLI )
			|| ( ! defined( 'OSITER_USE_NGROK_URL') || ! OSITER_USE_NGROK_URL )
		) {
			return;
		}

		$this->home_url = Config::NGROK_URL;

		add_action( 'wp_get_attachment_image_src', [ $this, 'modify_image_src' ] );
	}

	/**
	 * Modify the domain in a given URL.
	 *
	 * @param string $url
	 * @return string
	 */
	protected function modify_domain( $url ) {
		return str_replace(
			untrailingslashit( home_url() ),
			untrailingslashit( $this->home_url ),
			$url
		);
	}

	/**
	 * Modify images URLs.
	 *
	 * @param array $image
	 * @return array
	 */
	public function modify_image_src( $image ) {
		if ( isset( $image[0] ) ) {
			$image[0] = $this->modify_domain( $image[0] );
		}

		return $image;
	}
}

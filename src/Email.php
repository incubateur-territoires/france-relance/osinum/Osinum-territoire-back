<?php

namespace OsinumTerritoire;

use OsinumTerritoire\_Core\Component;

class Email extends Component {
	/**
	 * Fields containing e-mail data.
	 *
	 * @var array
	 */
	protected $data = [];

	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		//
	}

	/**
	 * Send a e-mail.
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $template
	 * @param array $data
	 * @return void
	 */
	public function send( $to = '', $subject = '', $template = '', $data = [] ) {
		$this->prepare_data( $data );

		ob_start();
		include_once _ositer()->get_frontend()->template->get_template_file( 'emails/header' );
		include_once _ositer()->get_frontend()->template->get_template_file( "emails/content/{$template}" );
		include_once _ositer()->get_frontend()->template->get_template_file( 'emails/footer' );

		$content = ob_get_clean();

		return wp_mail(
			$to,
			$subject,
			$content,
			[ 'Content-Type: text/html; charset=UTF-8' ]
		);
	}

	/**
	 * Prepare data.
	 *
	 * @param array $data
	 * @return void
	 */
	public function prepare_data( $data ) {
		$this->data['logo'] = $this->get_plugin()->get_plugin_url() . '/dist/images/logo-white.png';

		foreach ( $data as $key => $value ) {
			$this->data[ $key ] = $value;
		}
	}

	/**
	 * Get a specific field value.
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function get( $key ) {
		return isset( $this->data[ $key ] ) ? $this->data[ $key ] : null;
	}

	/**
	 * Output a button.
	 *
	 * @param string $label
	 * @param string $url
	 * @return string
	 */
	public function button( $label, $url ) {
		include_once _ositer()->get_frontend()->template->get_template_file( 'emails/button' );
	}
}

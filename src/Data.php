<?php

namespace OsinumTerritoire;

use OsinumTerritoire\Models\Diagnostic;
use OsinumTerritoire\Models\Situation;
use OsinumTerritoire\Models\Tool;
use OsinumTerritoire\Models\Difficulty;
use OsinumTerritoire\Models\Topic;
use OsinumTerritoire\Models\Practice;
use OsinumTerritoire\Models\Criteria;
use OsinumTerritoire\Models\DifficultyGroup;
use OsinumTerritoire\Models\Resource;

class Data {
	//===============================
	//                               
	//   #####   ##     ##  #####  
	//  ##   ##  ####   ##  ##     
	//  ##   ##  ##  ## ##  #####  
	//  ##   ##  ##    ###  ##     
	//   #####   ##     ##  #####  
	//                               
	//===============================

	/**
	 * Get a specific Diagnostic.
	 *
	 * @param integer|WP_Post $diagnostic_id
	 * @return \OsinumTerritoire\Models\Diagnostic
	 */
	public function get_diagnostic( $diagnostic_id  ) {
		if ( is_a( $diagnostic_id, 'WP_Post' ) ) {
			$diagnostic_id = $diagnostic_id->ID;
		}

		return get_post_type( $diagnostic_id ) === Config::CPT_DIAGNOSTIC ? new Diagnostic( (int) $diagnostic_id ) : null;
	}

	/**
	 * Get a specific Situation.
	 *
	 * @param integer|WP_Post $situation_id
	 * @return \OsinumTerritoire\Models\Situation
	 */
	public function get_situation( $situation_id  ) {
		if ( is_a( $situation_id, 'WP_Post' ) ) {
			$situation_id = $situation_id->ID;
		}

		return get_post_type( $situation_id ) === Config::CPT_SITUATION ? new Situation( (int) $situation_id ) : null;
	}

	/**
	 * Get a specific Tool.
	 *
	 * @param integer|WP_Post $tool_id
	 * @return \OsinumTerritoire\Models\Tool
	 */
	public function get_tool( $tool_id  ) {
		if ( is_a( $tool_id, 'WP_Post' ) ) {
			$tool_id = $tool_id->ID;
		}

		return get_post_type( $tool_id ) === Config::CPT_TOOL ? new Tool( (int) $tool_id ) : null;
	}

	/**
	 * Get a specific Difficulty.
	 *
	 * @param integer|WP_Post $difficulty_id
	 * @return \OsinumTerritoire\Models\Difficulty
	 */
	public function get_difficulty( $difficulty_id  ) {
		if ( is_a( $difficulty_id, 'WP_Post' ) ) {
			$difficulty_id = $difficulty_id->ID;
		}

		return get_post_type( $difficulty_id ) === Config::CPT_DIFFICULTY ? new Difficulty( (int) $difficulty_id ) : null;
	}

	/**
	 * Get a specific Resource.
	 *
	 * @param integer|WP_Post $resource_id
	 * @return \OsinumTerritoire\Models\Resource
	 */
	public function get_resource( $resource_id  ) {
		if ( is_a( $resource_id, 'WP_Post' ) ) {
			$resource_id = $resource_id->ID;
		}

		return get_post_type( $resource_id ) === Config::CPT_RESOURCE ? new Resource( (int) $resource_id ) : null;
	}

	/**
	 * Get a specific Difficulty Group.
	 *
	 * @param integer|WP_Term $difficulty_group_id
	 * @return \OsinumTerritoire\Models\DifficultyGroup
	 */
	public function get_difficulty_group( $difficulty_group_id  ) {
		if ( is_a( $difficulty_group_id, 'WP_Term' ) ) {
			$difficulty_group_id = $difficulty_group_id->term_id;
		}

		$difficulty_group = get_term_by( 'id', $difficulty_group_id, Config::TAX_DIFFICULTY_GROUP );

		return $difficulty_group ? new DifficultyGroup( $difficulty_group ) : null;
	}

	/**
	 * Get a specific Practice.
	 *
	 * @param integer|WP_Term $practice_id
	 * @return \OsinumTerritoire\Models\Practice
	 */
	public function get_practice( $practice_id  ) {
		if ( is_a( $practice_id, 'WP_Term' ) ) {
			$practice_id = $practice_id->term_id;
		}

		$practice = get_term_by( 'id', $practice_id, Config::TAX_PRACTICE );

		return $practice ? new Practice( $practice ) : null;
	}

	/**
	 * Get a specific Criteria.
	 *
	 * @param integer|WP_Term $criteria_id
	 * @return \OsinumTerritoire\Models\Criteria
	 */
	public function get_criteria( $criteria_id  ) {
		if ( is_a( $criteria_id, 'WP_Term' ) ) {
			$criteria_id = $criteria_id->term_id;
		}

		$criteria = get_term_by( 'id', $criteria_id, Config::TAX_CRITERIA );

		return $criteria ? new Criteria( $criteria ) : null;
	}

	/**
	 * Get a specific Topic.
	 *
	 * @param integer|WP_Term $topic_id
	 * @return \OsinumTerritoire\Models\Topic
	 */
	public function get_topic( $topic_id  ) {
		if ( is_a( $topic_id, 'WP_Term' ) ) {
			$topic_id = $topic_id->term_id;
		}

		$topic = get_term_by( 'id', $topic_id, Config::TAX_TOPIC );

		return $topic ? new Topic( $topic ) : null;
	}

	/**
	 * Get current Diagnostic on single Diagnostic page.
	 *
	 * @return Diagnostic|null
	 */
	public function get_current_diagnostic() {
		static $id = null;

		if ( is_null( $id ) ) {
			$id = get_query_var( 'diagnostic_id', null );
		}

		// Try to get Diag by Cookies
		if ( is_null( $id ) && isset( $_COOKIE[ 'diagID' ] ) && isset( $_COOKIE[ 'diagToken' ] ) ) {
			$diagnostic = ositer()->get_diagnostic( (int) $_COOKIE[ 'diagID' ] );
			$is_valid = $diagnostic->can_access( $_COOKIE[ 'diagToken' ] );
			if ( $is_valid ) {
				$id = $_COOKIE[ 'diagID' ];
			}
		}

		return ! is_null( $id ) ? $this->get_diagnostic( $id ) : null;
	}

	//==============================================
	//                                              
	//  ###    ###    ###    ##     ##  ##    ##  
	//  ## #  # ##   ## ##   ####   ##   ##  ##   
	//  ##  ##  ##  ##   ##  ##  ## ##    ####    
	//  ##      ##  #######  ##    ###     ##     
	//  ##      ##  ##   ##  ##     ##     ##     
	//                                              
	//==============================================

	/**
	 * Get all Situations.
	 *
	 * @return Situation[]
	 */
	public function get_situations() {
		$situations_ids = get_posts( [
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'posts_per_page'         => 1000,
			'fields'                 => 'ids',
			'post_type'              => Config::CPT_SITUATION,
		] );

		return ! empty( $situations_ids ) ? array_map( fn( $situation_id ) => $this->get_situation( $situation_id ), $situations_ids ) : [];
	}

	/**
	 * Get all Difficulties.
	 *
	 * @return Difficulty[]
	 */
	public function get_difficulties() {
		$difficulties_ids = get_posts( [
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'posts_per_page'         => 1000,
			'fields'                 => 'ids',
			'post_type'              => Config::CPT_DIFFICULTY,
		] );

		return ! empty( $difficulties_ids ) ? array_map( fn( $difficulty_id ) => $this->get_difficulty( $difficulty_id ), $difficulties_ids ) : [];
	}

	/**
	 * Get Tools.
	 *
	 * @param array $args
	 * @return Tool[]
	 */
	public function get_tools( $args = [] ) {
		$tools_ids = get_posts( wp_parse_args( $args, [
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'posts_per_page'         => 1000,
			'fields'                 => 'ids',
			'post_type'              => Config::CPT_TOOL,
			'meta_query'             => [
				[
					'key'   => 'visibility',
					'value' => 'public',
				]
			]
		] ) );

		return ! empty( $tools_ids ) ? array_map( fn( $tool_id ) => $this->get_tool( $tool_id ), $tools_ids ) : [];
	}

	/**
	 * Get Resources.
	 *
	 * @param array $args
	 * @return Resource[]
	 */
	public function get_resources( $args = [] ) {
		$resources_ids = get_posts( wp_parse_args( $args, [
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'posts_per_page'         => 1000,
			'fields'                 => 'ids',
			'post_type'              => Config::CPT_RESOURCE,
		] ) );

		return ! empty( $resources_ids ) ? array_map( fn( $resource_id ) => $this->get_resource( $resource_id ), $resources_ids ) : [];
	}

	/**
	 * Get all Topics.
	 *
	 * @return Topic[]
	 */
	public function get_topics() {
		$topics_ids = get_terms( [
			'taxonomy'               => Config::TAX_TOPIC,
			'update_term_meta_cache' => false,
			'hide_empty'             => false,
			'fields'                 => 'ids',
			'number'                 => 1000,
		] );

		return ! empty( $topics_ids ) ? array_map( fn( $topic_id ) => $this->get_topic( $topic_id ), $topics_ids ) : [];
	}

	/**
	 * Get all Difficulty Group.
	 *
	 * @return DifficultyGroup[]
	 */
	public function get_difficulty_groups() {
		$difficulty_groups_ids = get_terms( [
			'taxonomy'               => Config::TAX_DIFFICULTY_GROUP,
			'update_term_meta_cache' => false,
			'hide_empty'             => false,
			'fields'                 => 'ids',
			'number'                 => 1000,
		] );

		return ! empty( $difficulty_groups_ids ) ? array_map( fn( $difficulty_group_id ) => $this->get_difficulty_group( $difficulty_group_id ), $difficulty_groups_ids ) : [];
	}

	/**
	 * Get all Practices.
	 *
	 * @return Topic[]
	 */
	public function get_practices() {
		$practices_ids = get_terms( [
			'taxonomy'               => Config::TAX_PRACTICE,
			'update_term_meta_cache' => false,
			'hide_empty'             => false,
			'fields'                 => 'ids',
			'number'                 => 1000,
		] );

		return ! empty( $practices_ids ) ? array_map( fn( $practice_id ) => $this->get_practice( $practice_id ), $practices_ids ) : [];
	}

	/**
	 * Get all Criterias.
	 *
	 * @param array $args
	 * @return Criteria[]
	 */
	public function get_criterias( $args = [] ) {
		$criterias_ids = get_terms( wp_parse_args( $args, [
			'taxonomy'               => Config::TAX_CRITERIA,
			'parent'                 => 0,
			'update_term_meta_cache' => false,
			'hide_empty'             => false,
			'fields'                 => 'ids',
			'number'                 => 1000,
		] ) );

		return ! empty( $criterias_ids ) ? array_map( fn( $criteria_id ) => $this->get_criteria( $criteria_id ), $criterias_ids ) : [];
	}

	/**
	 * Get the tree of Criterias with negative/neutral/positive corresponding terms for each.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_criterias_tree( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'criterias_tree' );

		if ( ! ( $tree = get_transient( $transient_name ) ) || $force_refresh ) {
			$tree = [];

			foreach ( Config::CRITERIAS_SLUGS as $slug ) {
				$criteria_parent_id = ositer()->get_setting( 'criteria-' . $slug );

				if ( empty( $criteria_parent_id ) ) {
					continue;
				}

				$subtree = (object) [ 'criteria' => $slug, 'id' => $criteria_parent_id, 'children' => [] ];

				foreach ( $this->get_criterias( [ 'parent' => $criteria_parent_id, ] ) as $criteria ) {
					$subtree->children[] = (object) [
						'id'    => $criteria->get_id(),
						'score' => $criteria->get_score(),
					];
				}

				$tree[] = $subtree;
			}

			set_transient( $transient_name, $tree, WEEK_IN_SECONDS );
		}

		return $tree;
	}

	/**
	 * Get results steps data.
	 *
	 * @return array
	 */
	public function get_results_steps() {
		return [
			(object) [
				'slug'  => 'situations',
				'title' => __( 'Situations', 'ositer' ),
				'icon'  => 'support',
			],
			(object) [
				'slug'  => 'criteres',
				'title' => __( 'Critères', 'ositer' ),
				'icon'  => 'etoile',
			],
			(object) [
				'slug'  => 'outils',
				'title' => __( 'Outils', 'ositer' ),
				'icon'  => 'ecran',
			],
			(object) [
				'slug'  => 'difficultes',
				'title' => __( 'Difficultés', 'ositer' ),
				'icon'  => 'chantier',
			],
		];
	}
	
	/**
	 * Find all Diagnostics associated with a specific email address.
	 *
	 * @param string $email
	 * @return array
	 */
	public function find_diagnostic_by_email( $email = '' ) {
		$diagnostics_ids = get_posts( [
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'posts_per_page'         => 1000,
			'fields'                 => 'ids',
			'post_type'              => Config::CPT_DIAGNOSTIC,
			'meta_query'             => [
				[
					'key'   => 'email',
					'value' => $email,
				]
			]
		] );

		return ! empty( $diagnostics_ids ) ? array_map( fn( $diagnostic_id ) => $this->get_diagnostic( $diagnostic_id ), $diagnostics_ids ) : [];
	}

	//======================================================================
	//                                                                      
	//  #####   #####   #####              ##   ####   #####   ##     ##  
	//  ##     ##   ##  ##  ##             ##  ##     ##   ##  ####   ##  
	//  #####  ##   ##  #####              ##   ###   ##   ##  ##  ## ##  
	//  ##     ##   ##  ##  ##         ##  ##     ##  ##   ##  ##    ###  
	//  ##      #####   ##   ##         ####   ####    #####   ##     ##  
	//                                                                      
	//======================================================================

	/**
	 * Get Situation JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_situations_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'situations_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $situation ) => $situation->get_json(), $this->get_situations() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}

	/**
	 * Get Difficulties JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_difficulties_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'difficulties_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $difficultie ) => $difficultie->get_json(), $this->get_difficulties() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}

	/**
	 * Get Tools JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_tools_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'tools_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $tool ) => $tool->get_json(), $this->get_tools() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}

	/**
	 * Get Topics JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_topics_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'topics_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $topic ) => $topic->get_json(), $this->get_topics() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}

	/**
	 * Get Difficulty Groups JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_difficulty_groups_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'difficulties_groups_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $topic ) => $topic->get_json(), $this->get_difficulty_groups() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}

	/**
	 * Get Practices JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_practices_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'practices_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $practice ) => $practice->get_json(), $this->get_practices() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}

	/**
	 * Get Criterias JSON objects, cached.
	 *
	 * @param boolean $force_refresh
	 * @return array
	 */
	public function get_criterias_json_objects( $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'criterias_jsons' );

		if ( ! ( $jsons = get_transient( $transient_name ) ) || $force_refresh ) {
			$jsons = array_map( fn( $criteria ) => $criteria->get_json(), $this->get_criterias() );

			usort( $jsons, function( $a, $b ) {
				return $a->position === $b->position ? 0 : ( $a->position < $b->position ? -1 : 1 );
			} );

			set_transient( $transient_name, $jsons, WEEK_IN_SECONDS );
		}

		return $jsons;
	}
}

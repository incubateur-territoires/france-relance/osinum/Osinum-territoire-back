<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\Post;

/**
 * Main class in charge of interacting with an Difficulty post.
 */
class Difficulty extends Post {
	/**
	 * Set the post type for this Post model.
	 *
	 * @return void
	 */
	protected function set_post_type() {
		$this->post_type = Config::CPT_DIFFICULTY;
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Is this something?
	 *
	 * @return boolean
	 */
	public function is_something() {
		return true;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get groups.
	 *
	 * @param boolean $only_ids
	 * @return array
	 */
	public function get_groups( $only_ids = false ) {
		$groups = $this->get_terms( Config::TAX_DIFFICULTY_GROUP, true );

		if ( $only_ids ) {
			return array_map( fn( $group ) => (int) $group->get_id(), $groups );
		}

		return array_map( fn( $group ) => $group->get_json(), $groups );
	}

	/**
	 * Get a JSON object of that Difficulty.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_acf_meta( 'subtitle' ) ),
			'number'      => (int) $this->get_acf_meta( 'number' ),
			'position'    => (int) $this->get_acf_meta( 'position' ),
			'groups'      => $this->get_groups( true ),
		];
	}
}

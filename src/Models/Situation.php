<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\HasPractices;
use OsinumTerritoire\Models\Abstracts\Post;

/**
 * Main class in charge of interacting with an Situation post.
 */
class Situation extends Post {
	use HasPractices;

	/**
	 * Set the post type for this Post model.
	 *
	 * @return void
	 */
	protected function set_post_type() {
		$this->post_type = Config::CPT_SITUATION;
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Is this something?
	 *
	 * @return boolean
	 */
	public function is_something() {
		return true;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get resources.
	 *
	 * @return Resource[]
	 */
	public function get_resources() {
		$practices = array_map( fn( $practice_id ) => ositer()->get_practice( $practice_id ), $this->get_practices( true ) );
		$resources = [];
		
		foreach ( $practices as $practice ) {
			foreach ( $practice->get_resources() as $resource ) {
				if ( ! array_key_exists( $resource->get_id(), $resources ) ) {
					$resources[ $resource->get_id() ] = $resource;
				}
			}
		}

		return $resources;
	}

	/**
	 * Get topics.
	 *
	 * @param boolean $only_ids
	 * @return array
	 */
	public function get_topics( $only_ids = false ) {
		$topics = $this->get_terms( Config::TAX_TOPIC, true );

		if ( $only_ids ) {
			return array_map( fn( $topic ) => (int) $topic->get_id(), $topics );
		}

		return array_map( fn( $topic ) => $topic->get_json(), $topics );
	}

	/**
	 * Get related tools (sharing the same Practices terms).
	 *
	 * @param boolean $only_ids
	 * @return array
	 */
	public function get_tools( $only_ids = false ) {
		$practices = $this->get_terms( Config::TAX_PRACTICE, false, [ 'fields' => 'ids' ] );
		
		if ( empty( $practices ) ) {
			return [];
		}

		$tools = _ositer()->get_data()->get_tools( [
			'tax_query' => [
				[
					'taxonomy' => Config::TAX_PRACTICE,
					'field'    => 'term_id',
					'terms'    => $practices,
				],
			],
		] );

		return $only_ids ? array_map( fn( $tool ) => (int) $tool->get_id(), $tools ) : array_map( fn( $tool ) => $tool->get_json(), $tools );
	}

	/**
	 * Get a JSON object of that Situation.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_acf_meta( 'subtitle' ) ),
			'number'      => (int) $this->get_acf_meta( 'number' ),
			'position'    => (int) $this->get_acf_meta( 'position' ),
			'practices'   => $this->get_practices(),
			'topics'      => $this->get_topics( true ),
			// 'tools'     => $this->get_tools( true ),
			// 'resources' => $this->get_resources(),
		];
	}
}

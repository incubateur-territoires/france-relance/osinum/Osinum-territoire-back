<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\Term;

/**
 * Main class in charge of interacting with a Topic term.
 */
class Topic extends Term {
	/**
	 * Set the taxonomy for this Term model.
	 *
	 * @return void
	 */
	protected function set_taxonomy() {
		$this->taxonomy = Config::TAX_TOPIC;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get color slug.
	 *
	 * @return string
	 */
	public function get_color() {
		return $this->get_acf_meta( 'color' );
	}

	/**
	 * Get a JSON object of that Topic.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_acf_meta( 'description' ) ),
			'position'    => (int) $this->get_acf_meta( 'position' ),
			'color'       => $this->get_color(),
			'icon'        => $this->get_acf_meta( 'icon' ),
		];
	}
}

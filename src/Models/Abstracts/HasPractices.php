<?php

namespace OsinumTerritoire\Models\Abstracts;

use OsinumTerritoire\Config;

trait HasPractices {
	/**
	 * Get practices.
	 *
	 * @param boolean $only_ids
	 * @return array
	 */
	public function get_practices( $only_ids = false ) {
		$practices = $this->get_terms( Config::TAX_PRACTICE, true );

		if ( $only_ids ) {
			return array_map( fn( $practice ) => (int) $practice->get_id(), $practices );
		}

		return array_map( fn( $practice ) => $practice->get_json(), $practices );
	}
}

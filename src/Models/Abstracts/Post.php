<?php

namespace OsinumTerritoire\Models\Abstracts;

use OsinumTerritoire\_Core\MagicGetter;
use OsinumTerritoire\Helpers;

abstract class Post {
	use MagicGetter;

	/**
	 * The post type we are dealing with
	 *
	 * @var string
	 */
	protected $post_type = null;

	/**
	 * The WP_Post object
	 *
	 * @var WP_Post
	 */
	protected $object;

	/**
	 * The WP_Post ID
	 *
	 * @var int
	 */
	protected $object_id;

	/**
	 * Sub-classes are required to implement this in order to define the desired post type.
	 */
	abstract protected function set_post_type();

	/**
	 * Instantiate a Post model.
	 *
	 * @todo Instantiation could be done via a (int) ID only too, in some cases.
	 * @param \WP_Post $object The WP_Post to deal with.
	 */
	public function __construct( $object ) {
		if ( is_null( $this->post_type ) ) {
			$this->set_post_type();
		}

		if ( is_numeric( $object ) ) {
			$object = get_post( (int) $object );
		} elseif ( is_singular( $this->post_type ) ) {
			$object = get_queried_object();
		}

		if ( ! is_a( $object, 'WP_Post' ) ) {
			throw new \Exception( 'You can only instantiate a new Abstract_Post using a WP_Post or the post ID.' );
		}

		if ( $object->post_type !== $this->post_type ) {
			throw new \Exception( sprintf( '%1$s post type does not match model post type %2$s.', $object->post_type, $this->post_type ) );
		}

		$this->object    = $object;
		$this->object_id = $object->ID;
	}

	/**
	 * Get WP_Post.
	 */
	public function get_post() {
		return $this->object;
	}

	/**
	 * Get WP_Post->ID.
	 *
	 * @return int The Post ID.
	 */
	public function get_id() {
		return (int) $this->object_id;
	}

	/**
	 * Get a post meta.
	 *
	 * @param string $meta_key The meta to retrieve.
	 * @return mixed The post meta value.
	 */
	public function get_meta( $meta_key, $cf = false ) {
		if ( $cf ) {
			return carbon_get_post_meta( $this->get_id(), $meta_key );
		}

		return get_post_meta( $this->get_id(), $meta_key, true );
	}

	/**
	 * Get an ACF meta.
	 *
	 * @param string $meta_key
	 * @param boolean $format_value
	 * @return mixed
	 */
	public function get_acf_meta( $meta_key, $format_value = true ) {
		return get_field( $meta_key, $this->get_id(), $format_value );
	}

	/**
	 * Get content.
	 *
	 * @return string The post content.
	 */
	public function get_content( $filtered = false ) {
		return $filtered ? apply_filters( 'the_content', $this->get_post()->post_content ) : $this->get_post()->post_content;
	}

	/**
	 * Get excerpt.
	 *
	 * @return string The post excerpt.
	 */
	public function get_excerpt() {
		return wp_trim_words( $this->get_post()->post_excerpt, apply_filters( 'excerpt_length', 10 ), apply_filters( 'excerpt_more', '...' ) );
	}

	/**
	 * Get edit URL for this post
	 *
	 * @return string
	 */
	public function get_edit_link() {
		return get_edit_post_link( $this->get_id() );
	}

	/**
	 * Get title.
	 *
	 * @return string The post title.
	 */
	public function get_title() {
		return $this->get_post()->post_title;
	}

	/**
	 * Proxy function to get the post name
	 *
	 * @return string The post title.
	 */
	public function get_name() {
		return $this->get_title();
	}

	/**
	 * Get slug (post name).
	 *
	 * @return string The post slug.
	 */
	public function get_slug() {
		return $this->get_post()->post_name;
	}

	/**
	 * Get permalink.
	 *
	 * @return string The post permalink.
	 */
	public function get_permalink() {
		return get_permalink( $this->get_id() );
	}

	/**
	 * Proxy to get permalink (url).
	 *
	 * @return string The post permalink.
	 */
	public function get_url() {
		return $this->get_permalink();
	}

	/**
	 * Proxy to get permalink (link).
	 *
	 * @return string The post permalink.
	 */
	public function get_link() {
		return $this->get_permalink();
	}

	/**
	 * Get WP_Post->post_status.
	 *
	 * @return string
	 */
	public function get_status() {
		return $this->get_post()->post_status;
	}

	/**
	 * Get the post thumbnail.
	 *
	 * @param string $size The image size.
	 * @return string The post thumbnail image URL.
	 */
	public function get_thumbnail( $size = 'full' ) {
		return get_the_post_thumbnail_url( $this->get_id(), $size );
	}

	/**
	 * Get the post thumbnail img.
	 *
	 * @param string $size The image size.
	 * @param array $attrs Attributes for the image
	 * @return string The post thumbnail image.
	 */
	public function get_thumbnail_img( $size = 'full', $attrs = [] ) {
		return get_the_post_thumbnail( $this->get_id(), $size, $attrs );
	}

	/**
	 * Return the post thumbnail ID
	 *
	 * @return int Thumbnail ID.
	 */
	public function get_thumbnail_id() {
		return get_post_thumbnail_id( $this->get_id() );
	}

	/**
	 * Return the post creation date, formatted or not.
	 *
	 * @param string $format
	 * @return string
	 */
	public function get_creation_date( $format = null ) {
		return ! $format ? $this->get_post()->post_date : date_i18n( $format, strtotime( $this->get_post()->post_date ) );
	}

	/**
	 * Return the post creation date, formatted or not, in UTC/GMT time.
	 *
	 * @param string $format
	 * @return string
	 */
	public function get_creation_date_gmt( $format = null ) {
		return ! $format ? $this->get_post()->post_date_gmt : date_i18n( $format, strtotime( $this->get_post()->post_date_gmt ) );
	}

	/**
	 * Return author User ID
	 *
	 * @return integer
	 */
	public function get_author_id() {
		return (int) $this->get_post()->post_author;
	}

	/**
	 * Get an enhanced list of terms assigned to post.
	 *
	 * @todo Check if taxonomy is hierarchical and maybe include children/parent.
	 * @param string $taxonomy The taxonomy to look for terms.
	 * @return array An array of objects containing data about terms.
	 */
	public function get_terms( $taxonomy = 'category', $enhance = true, $args = [], $single = false ) {
		if ( ! taxonomy_exists( $taxonomy ) ) {
			throw new \Exception( sprintf( '%1$s taxonomy does not exist.', $taxonomy ) );
		}

		$terms           = wp_get_object_terms( $this->get_id(), $taxonomy, $args );
		$potential_class = Helpers::get_class_from_string( $taxonomy );

		if ( $enhance && $potential_class && class_exists( $potential_class ) ) {
			$terms = array_map( function( $term ) use ( $potential_class ) {
				return ( new $potential_class( $term ) );
			}, $terms );
		}

		if ( $single && ! empty( $terms ) ) {
			return $terms[0];
		}

		return $terms;
	}

	//==================================================================================
	//                                                                                  
	//   ####   #####   ##     ##  ####              ######    ###     ####     ####  
	//  ##     ##   ##  ####   ##  ##  ##              ##     ## ##   ##       ##     
	//  ##     ##   ##  ##  ## ##  ##  ##              ##    ##   ##  ##  ###   ###   
	//  ##     ##   ##  ##    ###  ##  ##              ##    #######  ##   ##     ##  
	//   ####   #####   ##     ##  ####    ##          ##    ##   ##   ####    ####   
	//                                                                                  
	//==================================================================================

	/**
	 * Check the current object has a thumbnail
	 *
	 * @return bool
	 */
	public function has_thumbnail() {
		return has_post_thumbnail( $this->get_id() );
	}

	/**
	 * Has excerpt?
	 *
	 * @return boolean
	 */
	public function has_excerpt() {
		return strlen( $this->get_excerpt() ) > 0;
	}

	/**
	 * Is user admin of the post ? 
	 * Edit this class by class to make it more granular
	 */
	function is_admin() {
		return ( $this->post->post_author === get_current_user_id() || current_user_can( 'manage_options' ) );
	}

	//=========================================================
	//                                                         
	//   ####  #####  ######  ######  #####  #####     ####  
	//  ##     ##       ##      ##    ##     ##  ##   ##     
	//   ###   #####    ##      ##    #####  #####     ###   
	//     ##  ##       ##      ##    ##     ##  ##      ##  
	//  ####   #####    ##      ##    #####  ##   ##  ####   
	//                                                         
	//=========================================================

	/**
	 * Set terms on this Post.
	 *
	 * @param string $taxonomy
	 * @param array|string $values
	 * @param boolean $append
	 * @return void
	 */
	public function set_terms( $taxonomy, $values, $append = false ) {
		return wp_set_object_terms( $this->get_id(), $values, $taxonomy, $append );
	}

	/**
	 * Set meta value
	 *
	 * @param string $meta_key
	 * @param mixed $meta_value
	 * @param boolean $cf Carbon Fields meta?
	 * @return void
	 */
	public function set_meta( $meta_key, $meta_value ) {
		update_post_meta( $this->get_id(), $meta_key, $meta_value );
	}

	/**
	 * Set ACF meta value
	 *
	 * @param string $meta_key
	 * @param mixed $meta_value
	 * @param boolean $cf Carbon Fields meta?
	 * @return void
	 */
	public function set_acf_meta( $meta_key, $meta_value ) {
		update_field( $meta_key, $meta_value, $this->get_id() );
	}

	/**
	 * Delete post meta (native)
	 */
	public function delete_meta( $meta_key ) {
		return delete_post_meta( $this->get_id(), $meta_key );
	}

	/**
	 * Update the post.
	 * 
	 * @param array $data
	 */
	public function update( $data = [] ) {
		$data['ID'] = $this->get_id();

		return wp_update_post( $data );
	}
}

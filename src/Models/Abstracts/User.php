<?php

namespace OsinumTerritoire\Models\Abstracts;

use OsinumTerritoire\_Core\MagicGetter;
use OsinumTerritoire\_Core\Cache;

/**
 * A model to get easy access to a user and their data, with superpowers.
 *
 * @link https://github.com/BeAPI/bea-plugin-boilerplate
 */
abstract class User {
	use MagicGetter;

	/**
	 * The WP_User object
	 *
	 * @var WP_User
	 */
	protected $user;

	/**
	 * The WP_User ID
	 *
	 * @var int
	 */
	protected $user_id;

	/**
	 * Instantiate a User model.
	 *
	 * @todo Instantiation could be done via a (int) ID only too, in some cases.
	 * @param \WP_User|int $user The WP_User to deal with, or user ID.
	 */
	public function __construct( $user = null ) {
		if ( is_numeric( $user ) ) {
			$user_id = $user;
		} elseif ( is_null( $user ) && is_user_logged_in() ) {
			$user_id = get_current_user_id();
		} elseif ( is_a( $user, 'WP_User' ) ) {
			$user_id = $user->ID;
		}

		$cache_key = sprintf( 'user_%1$d', (int) $user_id );

		if ( Cache::has( $cache_key ) ) {
			$user = Cache::get( $cache_key );
		} else {
			$user = get_user_by( 'id', (int) $user_id );
			Cache::set( $cache_key, $user );
		}

		if ( ! is_a( $user, 'WP_User' ) ) {
			throw new \Exception( 'You can only instantiate a new Abstract_User using a WP_User or the user ID.' );
		}

		$this->user      = $user;
		$this->user_id   = (int) $user->ID;
		$this->user_data = get_userdata( $user->ID );

	}

	/**
	 * Get WP_User object
	 *
	 * @return \WP_User
	 */
	public function get_user() {
		return $this->user;
	}

	/**
	 * Get WP_User->ID.
	 *
	 * @return integer The User ID.
	 */
	public function get_id() {
		return (int) $this->user_id;
	}

	/**
	 * Get user data
	 *
	 * @return object
	 */
	public function get_user_data() {
		return $this->user_data;
	}

	/**
	 * Get user login
	 *
	 * @return string
	 */
	public function get_login() {
		return $this->user->user_login;
	}

	/**
	 * Get display name
	 *
	 * @return string
	 */
	public function get_display_name() {
		return $this->user->display_name;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function get_name() {
		$first_name = $this->get_first_name();
		$last_name  = $this->get_last_name();

		if ( ! empty( $first_name ) || ! empty( $last_name ) ) {
			return sprintf( '%1$s %2$s', $first_name, $last_name );
		}

		return null;
	}

	/**
	 * Get first name
	 *
	 * @return string
	 */
	public function get_first_name() {
		return $this->user_data->first_name;
	}

	/**
	 * Get last name
	 *
	 * @return string
	 */
	public function get_last_name() {
		return $this->user_data->last_name;
	}

	/**
	 * Get e-mail address
	 *
	 * @return string
	 */
	public function get_email() {
		return $this->user_data->user_email;
	}

	/**
	 * Return the user registration date, formatted or not.
	 *
	 * @param string $format
	 * @return string
	 */
	public function get_creation_date( $format = null ) {
		return ! $format ? $this->user_data->user_registered : date_i18n( $format, strtotime( $this->user_data->user_registered ) );
	}

	/**
	 * Get the profile edit user link.
	 *
	 * @return string
	 */
	public function get_edit_link() {
		return get_edit_user_link( $this->get_id() );
	}

	/**
	 * Get user meta value.
	 *
	 * @param string $meta_key The meta to retrieve.
	 * @return mixed The post meta value.
	 */
	public function get_meta( $meta_key ) {
		return get_user_meta( $this->get_id(), $meta_key, true );
	}

	/**
	 * Set user meta value.
	 *
	 * @param string $meta_key
	 * @param mixed $meta_value
	 * @param boolean $cf Carbon Fields meta?
	 * @return void
	 */
	public function set_meta( $meta_key, $meta_value ) {
		update_user_meta( $this->get_id(), $meta_key, $meta_value );
	}

	/**
	 * Delete a specific user meta.
	 *
	 * @param string $meta_key
	 * @return void
	 */
	public function delete_meta( $meta_key ) {
		delete_user_meta( $this->get_id(), $meta_key );
	}

	//=======================================
	//                                       
	//   ####   #####   ##     ##  ####    
	//  ##     ##   ##  ####   ##  ##  ##  
	//  ##     ##   ##  ##  ## ##  ##  ##  
	//  ##     ##   ##  ##    ###  ##  ##  
	//   ####   #####   ##     ##  ####    
	//                                       
	//=======================================

	/**
	 * Does this User has a specific role?
	 *
	 * @param string $roles
	 * @return boolean
	 */
	public function has_role( $roles ) {
		$user_roles      = $this->get_user()->roles;
		$comparing_roles = is_array( $roles ) ? $roles : [ $roles ];

		if ( count( array_intersect( $comparing_roles, (array) $user_roles ) ) > 0 ) {
			return true;
		}

		return false;
	}

	/**
	 * Is this an admin?
	 *
	 * @return boolean
	 */
	public function is_admin() {
		return $this->has_role( 'administrator' );
	}

	/**
	 * Is this a Customer?
	 *
	 * @return boolean
	 */
	public function is_customer() {
		return $this->has_role( 'customer' );
	}
}

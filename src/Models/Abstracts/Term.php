<?php

namespace OsinumTerritoire\Models\Abstracts;

use OsinumTerritoire\_Core\MagicGetter;

/**
 * A model to get easy access to taxonomy terms, with superpowers.
 */
abstract class Term {
	use MagicGetter;

	/**
	 * The taxonomy we are dealing with
	 *
	 * @var string
	 */
	protected $taxonomy = null;

	/**
	 * The WP_Term object
	 *
	 * @var WP_Term
	 */
	protected $object;

	/**
	 * The WP_Term term_ID
	 *
	 * @var int
	 */
	protected $object_id;

	/**
	 * Store meta values throughout our session.
	 *
	 * @var array
	 */
	protected $meta_values = [];

	/**
	 * Sub-classes are required to implement this in order to define the desired taxonomy.
	 */
	abstract protected function set_taxonomy();

	/**
	 * Instantiate a Term model.
	 *
	 * @todo Instantiation could be done via a (int) ID only too, in some cases.
	 * @param \WP_Term $object The WP_Term to deal with.
	 */
	public function __construct( $object ) {
		if ( is_null( $this->taxonomy ) ) {
			$this->set_taxonomy();
		}

		if ( is_numeric( $object ) ) {
			$object = get_term_by( 'id', (int) $object, $this->taxonomy );
		}

		if ( ! is_a( $object, 'WP_Term' ) ) {
			throw new \Exception( 'You can only instantiate a new Abstract_Term using a WP_Term or the term ID.' );
		}

		if ( $object->taxonomy !== $this->taxonomy ) {
			throw new \Exception( sprintf( '%1$s taxonomy does not match model taxonomy %s.', $object->taxonomy, $this->taxonomy ) );
		}

		$this->object    = $object;
		$this->object_id = $object->term_id;
	}

	/**
	 * Get WP_Term.
	 */
	public function get_term() {
		return $this->object;
	}

	/**
	 * Get WP_Term->ID.
	 *
	 * @return int The Term ID.
	 */
	public function get_id() {
		return $this->object_id;
	}

	/**
	 * Get term taxonomy.
	 *
	 * @return string The term taxonomy.
	 */
	public function get_taxonomy() {
		return $this->taxonomy;
	}

	/**
	 * Get a term meta.
	 *
	 * @todo If get_meta_{$meta_key} method exists, use it.
	 * @param string $meta_key The meta to retrieve.
	 * @return mixed The post meta value.
	 */
	public function get_meta( $meta_key, $cf = false ) {
		if ( ! isset( $this->meta_values[ $meta_key ] ) ) {
			if ( $cf ) {
				$this->meta_values[ $meta_key ] = \carbon_get_term_meta( $this->get_id(), $meta_key );
			} else {
				$this->meta_values[ $meta_key ] = get_term_meta( $this->get_id(), $meta_key, true );
			}
		}

		return $this->meta_values[ $meta_key ];
	}

	/**
	 * Get an ACF meta.
	 *
	 * @param string $meta_key
	 * @param boolean $format_value
	 * @return mixed
	 */
	public function get_acf_meta( $meta_key, $format_value = true ) {
		return get_field( $meta_key, $this->get_term(), $format_value );
	}

	/**
	 * Set meta value
	 *
	 * @param string $meta_key
	 * @param mixed $meta_value
	 * @param boolean $cf Carbon Fields meta?
	 * @return void
	 */
	public function set_meta( $meta_key, $meta_value, $cf = false ) {
		if ( $cf ) {
			\carbon_set_term_meta( $this->get_id(), $meta_key, $meta_value );
		} else {
			update_term_meta( $this->get_id(), $meta_key, $meta_value );
		}
	}

	/**
	 * Get name.
	 *
	 * @return string The term name.
	 */
	public function get_name() {
		return $this->get_term()->name;
	}

	/**
	 * Proxy function to get title (or name).
	 *
	 * @return string The term title.
	 */
	public function get_title() {
		return $this->get_name();
	}

	/**
	 * Get slug.
	 *
	 * @return string The term slug.
	 */
	public function get_slug() {
		return $this->get_term()->slug;
	}

	/**
	 * Get description.
	 *
	 * @return string The term description.
	 */
	public function get_description( $filtered = false ) {
		$description = $this->get_term()->description;

		if ( empty( $description ) ) {
			return null;
		}

		return $filtered ? apply_filters( 'the_content', $description ) : $description;
	}

	/**
	 * Proxy function to get content (or description).
	 *
	 * @return string The term description.
	 */
	public function get_content( $filtered = false ) {
		return $this->get_description( $filtered );
	}

	/**
	 * Get term archive permalink.
	 *
	 * @return string The term archive permalink.
	 */
	public function get_archive_permalink() {
		return get_term_link( $this->get_term(), $this->get_taxonomy() );
	}

	/**
	 * Proxy to get permalink (url).
	 *
	 * @return string The post permalink.
	 */
	public function get_url() {
		return $this->get_archive_permalink();
	}

	/**
	 * Proxy to get permalink (url).
	 *
	 * @return string The post permalink.
	 */
	public function get_permalink() {
		return $this->get_url();
	}

	/**
	 * Get term edit link.
	 *
	 * @return string
	 */
	public function get_edit_link() {
		return get_edit_term_link( $this->get_id(), $this->get_taxonomy() );
	}
}

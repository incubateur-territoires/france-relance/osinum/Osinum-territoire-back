<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Helpers;
use OsinumTerritoire\Models\Abstracts\Term;

/**
 * Main class in charge of interacting with a Criteria term.
 */
class Criteria extends Term {
	/**
	 * Set the taxonomy for this Term model.
	 *
	 * @return void
	 */
	protected function set_taxonomy() {
		$this->taxonomy = Config::TAX_CRITERIA;
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Does this Criteria already have rating subterms?
	 *
	 * @return boolean
	 */
	public function has_rating_subterms() {
		return (int) $this->get_meta( 'rating_subterms_created' ) > 0;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get a JSON object of that Topic.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_acf_meta( 'description' ) ),
			'number'      => (int) $this->get_acf_meta( 'number' ),
			'position'    => (int) $this->get_acf_meta( 'position' ),
		];
	}

	/**
	 * Get score.
	 *
	 * @return integer
	 */
	public function get_score() {
		return (int) $this->get_meta( 'score' );
	}

	//=====================================
	//                                     
	//   ####  #####    ##   ##  ####    
	//  ##     ##  ##   ##   ##  ##  ##  
	//  ##     #####    ##   ##  ##  ##  
	//  ##     ##  ##   ##   ##  ##  ##  
	//   ####  ##   ##   #####   ####    
	//                                     
	//=====================================

	/**
	 * Create rating subterms for this Criteria.
	 *
	 * @return void
	 */
	public function create_rating_subterms() {
		if ( $this->has_rating_subterms() ) {
			return;
		}

		$name   = $this->get_name();
		$slug   = $this->get_slug();
		$matrix = [
			(object) [ 'slug' => 'negative', 'title' => sprintf( __( '%1$s (non)', 'ositer' ), $name ), 'score' => -1 ],
			(object) [ 'slug' => 'neutral', 'title' => sprintf( __( '%1$s (neutre)', 'ositer' ), $name ), 'score' => 0 ],
			(object) [ 'slug' => 'positive', 'title' => sprintf( __( '%1$s (oui)', 'ositer' ), $name ), 'score' => 1 ],
		];

		foreach ( $matrix as $criteria ) {
			Helpers::create_term(
				Config::TAX_CRITERIA,
				$criteria->title,
				[ 'score' => $criteria->score ],
				[ 'slug' => sanitize_title( sprintf( '%1$s-%2$s', $slug, $criteria->slug ) ), 'parent' => $this->get_id() ]
			);
		}

		$this->set_meta( 'rating_subterms_created', time() );
	}
}

<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\Term;

/**
 * Main class in charge of interacting with a Practice term.
 */
class Practice extends Term {
	/**
	 * Set the taxonomy for this Term model.
	 *
	 * @return void
	 */
	protected function set_taxonomy() {
		$this->taxonomy = Config::TAX_PRACTICE;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get Tools categorized in this Practice.
	 *
	 * @return Tool[]
	 */
	public function get_tools() {
		return _ositer()->get_data()->get_tools( [
			'tax_query' => [
				[
					'taxonomy' => Config::TAX_PRACTICE,
					'field'    => 'term_id',
					'terms'    => $this->get_id(),
				],
			],
		] );
	}

	/**
	 * Get Resources categorized in this Practice.
	 *
	 * @return Tool[]
	 */
	public function get_resources() {
		return _ositer()->get_data()->get_resources( [
			'tax_query' => [
				[
					'taxonomy' => Config::TAX_PRACTICE,
					'field'    => 'term_id',
					'terms'    => $this->get_id(),
				],
			],
		] );
	}

	/**
	 * get description.
	 *
	 * @return string
	 */
	public function get_description( $filtered = false ) {
		return $this->get_acf_meta( 'description' );
	} 

	/**
	 * Get a JSON object of that Topic.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_description() ),
			'number'      => (int) $this->get_acf_meta( 'number' ),
			'position'    => (int) $this->get_acf_meta( 'position' ),
		];
	}
}

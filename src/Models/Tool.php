<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Helpers;
use OsinumTerritoire\Models\Abstracts\HasPractices;
use OsinumTerritoire\Models\Abstracts\Post;

/**
 * Main class in charge of interacting with an Tool post.
 */
class Tool extends Post {
	use HasPractices;

	/**
	 * Set the post type for this Post model.
	 *
	 * @return void
	 */
	protected function set_post_type() {
		$this->post_type = Config::CPT_TOOL;
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Is this a custom Tool created by a user during on step #3 of a Diagnostic?
	 *
	 * @return boolean
	 */
	public function is_custom() {
		return $this->get_acf_meta( 'visibility' ) === 'private';
	}

	/**
	 * Does a custom Tool comes from a specific Diagnostic?
	 *
	 * @param Diagnostic $diagnostic
	 * @return boolean
	 */
	public function comes_from( $diagnostic ) {
		return (int) $this->get_meta( 'diagnostic' ) === $diagnostic->get_id();
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get the initial Diagnostic this (custom) Tool was created for.
	 *
	 * @return Diagnostic
	 */
	public function get_diagnostic() {
		return $this->is_custom() ? ositer()->get_diagnostic( (int) $this->get_meta( 'diagnostic' ) ) : null;
	}

	/**
	 * Get a list of Criterias for this Tool.
	 *
	 * @return array
	 */
	public function get_criterias( $ids_only = false ) {
		$criterias = $this->get_terms( Config::TAX_CRITERIA );

		return $ids_only ? array_map( fn ( $criteria ) => $criteria->get_id(), $criterias ) : $criterias;
	}

	/**
	 * get description.
	 *
	 * @return string
	 */
	public function get_description( $format = true ) {
		return $this->get_acf_meta( 'description', $format );
	} 

	/**
	 * Get a JSON object of that Tool.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'permalink'   => get_permalink( $this->get_id() ),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_description() ),
			'position'    => $this->is_custom() ? -1 : (int) $this->get_acf_meta( 'position' ),
			'type'        => $this->is_custom() ? 'custom' : 'public',
			'criterias'   => $this->get_criterias( true ),
		];
	}

	//=====================================
	//                                     
	//   ####  #####    ##   ##  ####    
	//  ##     ##  ##   ##   ##  ##  ##  
	//  ##     #####    ##   ##  ##  ##  
	//  ##     ##  ##   ##   ##  ##  ##  
	//   ####  ##   ##   #####   ####    
	//                                     
	//=====================================

	/**
	 * Create a new Tool from a Diagnostic.
	 *
	 * @param string $title
	 * @return Diagnostic
	 */
	public static function create_for_diagnostic( $title, $diagnostic ) {
		$tool_id = wp_insert_post( [
			'post_title'  => sanitize_text_field( $title ),
			'post_type'   => Config::CPT_TOOL,
			'post_status' => 'publish',
		], true );

		if ( is_wp_error( $tool_id ) ) {
			return $tool_id;
		}

		$tool = new Tool( $tool_id );

		$tool->set_acf_meta( 'visibility', 'private' );
		$tool->set_acf_meta( 'diagnostic', $diagnostic->get_id() );

		do_action( 'ositer/tool/created-for-diagnostic', $tool, $diagnostic );

		return $tool;
	}

	/**
	 * Update criterias. Input data is the criteria slug as key, and score as value.
	 * This is only for a custom Tool created by a user.
	 *
	 * @param array $criterias
	 * @return void
	 */
	public function update_criterias( $criterias = [] ) {
		$terms = [];

		foreach ( $criterias as $slug => $score ) {
			// Only assign Criterias if the score is positive (1).
			if ( (int) $score <= 0 ) {
				continue;
			}

			$criteria_term_id = ositer()->get_setting( 'criteria-' . $slug );

			if ( empty( $criteria_term_id ) ) {
				continue;
			}

			$terms[] = (int) $criteria_term_id;
		}

		$this->set_terms( Config::TAX_CRITERIA, $terms );
		$this->set_meta( 'criterias_updated', time() );
	}
}

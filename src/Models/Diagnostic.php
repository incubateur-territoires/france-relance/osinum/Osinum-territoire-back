<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\Post;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

/**
 * Main class in charge of interacting with an Diagnostic post.
 */
class Diagnostic extends Post {
	/**
	 * Set the post type for this Post model.
	 *
	 * @return void
	 */
	protected function set_post_type() {
		$this->post_type = Config::CPT_DIAGNOSTIC;
	}

	//==========================================
	//                                          
	//   ####  ##   ##  #####   ####  ##  ##  
	//  ##     ##   ##  ##     ##     ## ##   
	//  ##     #######  #####  ##     ####    
	//  ##     ##   ##  ##     ##     ## ##   
	//   ####  ##   ##  #####   ####  ##  ##  
	//                                          
	//==========================================

	/**
	 * Can we access/edit this Diagnostic?
	 *
	 * @param string $token
	 * @return boolean
	 */
	public function can_access( $token = '' ) {
		return $this->get_token() === $token;
	}

	/**
	 * Is step X done?
	 *
	 * @param string $step
	 * @return boolean
	 */
	public function has_done_step( $step = '' ) {
		return (int) $this->get_meta( "step_{$step}_updated_at" ) > 0;
	}

	/**
	 * Have all these Diagnostic steps been completed?
	 *
	 * @return boolean
	 */
	public function is_completed() {
		return (int) $this->get_meta( 'completed_at' ) > 0;
	}

	/**
	 * Has current user completed the lead form on this Diagnostic?
	 *
	 * @return boolean
	 */
	public function has_completed_lead_form() {
		return (int) $this->get_meta( 'lead_form_completed_at' ) > 0;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get access token.
	 *
	 * @return string
	 */
	public function get_token() {
		return $this->get_meta( 'token' );
	}

	/**
	 * Get the last update.
	 *
	 * @param string $format
	 * @return string
	 */
	public function get_last_update( $format = 'Y-m-d H:i:s' ) {
		return wp_date( $format, $this->get_meta( 'updated_at' ) );
	}

	/**
	 * Get the QR code URL.
	 *
	 * @return string
	 */
	public function get_qr_code_url() {
		$url = $this->get_meta( 'qr_code_url' );

		return empty( $url ) ? $this->generate_qr_code() : $url;
	}

	/**
	 * Get lead form values.
	 *
	 * @return array
	 */
	public function get_lead_form_values() {
		return (array) $this->get_meta( 'lead_form_values' );
	}

	/**
	 * Get the PDF filename for this Diagnostic.
	 *
	 * @return string
	 */
	public function get_pdf_filename() {
		return sprintf( 'osinum-%1$d-%2$s.pdf', $this->get_id(), sanitize_title( $this->get_title() ) );
	}

	/**
	 * Get author email.
	 *
	 * @return string
	 */
	public function get_email() {
		return $this->get_meta( 'email' );
	}

	/**
	 * Get private URL for this Diagnostic.
	 *
	 * @param string $step
	 * @return string
	 */
	public function get_private_permalink( $step = null ) {
		$url = sprintf( '%1$s/diag/%2$d/%3$s', untrailingslashit( home_url() ), $this->get_id(), $this->get_token() );

		if ( ! is_null( $step ) ) {
			$url = sprintf( '%1$s/%2$s', $url, $step );
		}

		return $url;
	}

	/**
	 * Get Situations.
	 *
	 * @param boolean $ids_only
	 * @return Situation[]|int[]
	 */
	public function get_situations( $ids_only = false ) {
		$situations = $this->get_meta( 'situations' );

		if ( ! is_array( $situations ) ) {
			return [];
		}
		
		if ( $ids_only ) {
			return array_map( 'absint', $situations );
		}

		return array_map( fn( $situation ) => ositer()->get_situation( $situation ), $situations );
	}

	/**
	 * Get Difficulties.
	 *
	 * @param boolean $ids_only
	 * @return Difficulty[]|int[]
	 */
	public function get_difficulties( $ids_only = false ) {
		$difficulties = $this->get_meta( 'difficulties' );

		if ( ! is_array( $difficulties ) ) {
			return [];
		}
		
		if ( $ids_only ) {
			return array_map( 'absint', $difficulties );
		}

		return array_map( fn( $difficulty ) => ositer()->get_difficulty( $difficulty ), $difficulties );
	}

	/**
	 * Get evaluated Criterias.
	 *
	 * @return array An array of { id: X, rating: Y } objects.
	 */
	public function get_evaluated_criterias() {
		$evaluated_criterias = $this->get_meta( 'evaluated_criterias' );

		return is_array( $evaluated_criterias ) ? $evaluated_criterias : [];
	}

	/**
	 * Get tools that were selected in step #3 (outils).
	 *
	 * @param boolean $json
	 * @return array An array of { id: toolId, configuration: {} } objects.
	 */
	public function get_selected_tools( $json = false ) {
		$selected_tools = $this->get_meta( 'selected_tools' );

		if ( is_array( $selected_tools ) && $json ) {
			return array_filter( array_map( function( $id_and_config ) {
				$tool = ositer()->get_tool( $id_and_config->id );
				
				if ( $tool ) {
					$tool_json                = $tool->get_json();
					$tool_json->configuration = $id_and_config->configuration;


					return $tool_json;
				}

				return null;
			}, $selected_tools ) );
		}

		return is_array( $selected_tools ) ? $selected_tools : [];
	}

	/**
	 * Get Tools that were specific custom created for this Diagnostic by the user.
	 *
	 * @return Tool[]
	 */
	public function get_custom_tools( $json = false ) {
		$custom_tools = _ositer()->get_data()->get_tools( [
			'meta_query' => [
				[
					'key'   => 'visibility',
					'value' => 'private',
				],
				[
					'key'   => 'diagnostic',
					'value' => $this->get_id(),
				],
			],
		] );

		return $json ? array_map( fn( $custom_tool ) => $custom_tool->get_json(), $custom_tools ) : $custom_tools;
	}

	/**
	 * Get suggested Practices coming from the selected Situations.
	 *
	 * @return Practice[]|stdClass[]
	 */
	public function get_suggested_practices( $json = false, $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'suggested_practices_ids_' . $this->get_id() );

		if ( ! ( $ids = get_transient( $transient_name ) ) || $force_refresh ) {
			$situations = $this->get_situations();
			$ids        = [];
			
			foreach ( $situations as $situation ) {
				foreach ( $situation->get_practices( true ) as $practice_id ) {
					if ( ! in_array( $practice_id, $ids, true ) ) {
						$ids[] = (int) $practice_id;
					}
				}
			}
			
			set_transient( $transient_name, $ids, WEEK_IN_SECONDS );
		}

		$results = array_map( fn( $practice_id ) => ositer()->get_practice( $practice_id ), $ids );

		return $json ? array_map( fn( $practice ) => $practice->get_json(), $results ) : $results;
	}

	/**
	 * Get suggested Tools coming from the suggested Practices.
	 *
	 * @return Tool[]|stdClass[]
	 */
	public function get_suggested_tools( $json = false, $force_refresh = false ) {
		$transient_name = Config::get_transient_name( 'suggested_tools_ids_' . $this->get_id() );

		if ( ! ( $ids = get_transient( $transient_name ) ) || $force_refresh ) {
			$practices = $this->get_suggested_practices();
			$ids     = [];
			
			foreach ( $practices as $practice ) {
				foreach ( $practice->get_tools() as $tool ) {
					if ( ! in_array( $tool->get_id(), $ids, true ) ) {
						$ids[] = $tool->get_id();
					}
				}
			}
			
			set_transient( $transient_name, $ids, WEEK_IN_SECONDS );
		}

		$results = array_map( fn( $tool_id ) => ositer()->get_tool( $tool_id ), $ids );

		return $json ? array_map( fn( $tool ) => $tool->get_json(), $results ) : $results;
	}

	//=====================================
	//                                     
	//   ####  #####    ##   ##  ####    
	//  ##     ##  ##   ##   ##  ##  ##  
	//  ##     #####    ##   ##  ##  ##  
	//  ##     ##  ##   ##   ##  ##  ##  
	//   ####  ##   ##   #####   ####    
	//                                     
	//=====================================

	/**
	 * Create a new Diagnostic.
	 *
	 * @param string $title
	 * @return Diagnostic
	 */
	public static function create( $title = '', $metas = [] ) {
		$diagnostic_id = wp_insert_post( [
			'post_title'  => sanitize_text_field( $title ),
			'post_type'   => Config::CPT_DIAGNOSTIC,
			'post_status' => 'publish',
		], true );

		if ( is_wp_error( $diagnostic_id ) ) {
			return $diagnostic_id;
		}

		$diagnostic = new Diagnostic( $diagnostic_id );

		foreach ( $metas as $key => $value ) {
			$diagnostic->set_meta( $key, sanitize_text_field( $value ) );
		}

		do_action( 'ositer/diagnostic/created', $diagnostic );

		return $diagnostic;
	}

	/**
	 * Generate the QR Code representing this Diagnostic private permalink.
	 *
	 * @return void
	 */
	public function generate_qr_code() {
		$options = new QROptions( [
			'version'          => 7,
			'outputType'       => QRCode::OUTPUT_IMAGE_PNG,
			'eccLevel'         => 0b01,
			'scale'            => 12,
			'imageBase64'      => false,
			'imageTransparent' => false,
			'quietzoneSize'    => 2,
		]);
		
		$qrcode    = new QRCode( $options );
		$filename  = sprintf( 'diagnostic-%1$d-%2$s.png', $this->get_id(), $this->get_token() );
		$file_path = sprintf( '%1$s/%2$s', wp_upload_dir()['path'], $filename );
		$file_url  = sprintf( '%1$s/%2$s', wp_upload_dir()['url'], $filename );

		$qrcode->render( $this->get_private_permalink(), $file_path );

		$this->set_meta( 'qr_code_url', $file_url );

		return $file_url;
	}

	/**
	 * Save lead form values.
	 *
	 * @param array $fields
	 * @return void
	 */
	public function save_lead_form( $fields ) {
		$clean_fields               = [];
		$has_already_completed_form = $this->has_completed_lead_form();

		foreach ( $fields as $field ) {
			if ( in_array( $field->name, Config::LEAD_FORM_FIELDS, true ) ) {
				$clean_fields[ $field->name ] = sanitize_text_field( $field->value );
			} else {
				\MSK::debug( 'Field not allowed in lead form: ' . $field->name );
			}
		}
		
		$this->set_meta( 'lead_form_values', $clean_fields );
		$this->set_meta( 'lead_form_completed_at', time() );

		do_action( 'ositer/diagnostic/lead-form-saved', $this, $clean_fields, $has_already_completed_form );
	}
}

<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\HasPractices;
use OsinumTerritoire\Models\Abstracts\Post;

/**
 * Main class in charge of interacting with an Resource post.
 */
class Resource extends Post {
	/**
	 * Set the post type for this Post model.
	 *
	 * @return void
	 */
	protected function set_post_type() {
		$this->post_type = Config::CPT_RESOURCE;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get Practices.
	 *
	 * @return Practice[]
	 */
	public function get_practices() {
		return $this->get_terms( Config::TAX_PRACTICE, true );
	}

	/**
	 * Get Topics.
	 *
	 * @return Topic[]
	 */
	public function get_topics() {
		return $this->get_terms( Config::TAX_TOPIC, true );
	}

	/**
	 * Get Difficulty Groups.
	 *
	 * @return DifficultyGroup[]
	 */
	public function get_difficulty_groups() {
		return $this->get_terms( Config::TAX_DIFFICULTY_GROUP, true );
	}

	/**
	 * Get Tools.
	 *
	 * @return Tool[]
	 */
	public function get_tools() {
		return $this->get_acf_meta( Config::RESOURCE_TOOLS_RELATION );
	}
}

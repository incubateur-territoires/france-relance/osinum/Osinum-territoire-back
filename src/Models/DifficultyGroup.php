<?php

namespace OsinumTerritoire\Models;

use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Abstracts\Term;

/**
 * Main class in charge of interacting with a Difficulty Group term.
 */
class DifficultyGroup extends Term {
	/**
	 * Set the taxonomy for this Term model.
	 *
	 * @return void
	 */
	protected function set_taxonomy() {
		$this->taxonomy = Config::TAX_DIFFICULTY_GROUP;
	}

	//============================
	//                            
	//   ####    #####  ######  
	//  ##       ##       ##    
	//  ##  ###  #####    ##    
	//  ##   ##  ##       ##    
	//   ####    #####    ##    
	//                            
	//============================

	/**
	 * Get color slug.
	 *
	 * @return string
	 */
	public function get_color() {
		return $this->get_acf_meta( 'color' );
	}

	/**
	 * Get a JSON object of that Topic.
	 *
	 * @return object
	 */
	public function get_json() {
		return (object) [
			'id'          => $this->get_id(),
			'title'       => sanitize_text_field( $this->get_title() ),
			'description' => wp_kses_post( $this->get_acf_meta( 'description' ) ),
			'position'    => (int) $this->get_acf_meta( 'position' ),
			'color'       => $this->get_color(),
			'icon'        => $this->get_acf_meta( 'icon' ),
		];
	}

	/**
	 * Get all Resources in this Difficulty Group.
	 *
	 * @return Resource[]
	 */
	public function get_resources() {
		return _ositer()->get_data()->get_resources( [
			'tax_query' => [
				[
					'taxonomy' => Config::TAX_DIFFICULTY_GROUP,
					'field'    => 'term_id',
					'terms'    => $this->get_id(),
				],
			],
		] );
	}
}

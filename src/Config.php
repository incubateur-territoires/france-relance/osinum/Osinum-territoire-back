<?php

namespace OsinumTerritoire;

class Config {
	// Post type name to store Diagnostics data.
	const CPT_DIAGNOSTIC = 'diagnostic';

	// Post type name to store Situations data.
	const CPT_SITUATION = 'situation';

	// Post type name to store Tools data.
	const CPT_TOOL = 'tool';

	// Post type name to store Difficulties data.
	const CPT_DIFFICULTY = 'difficulty';

	// Post type name to store Resources data.
	const CPT_RESOURCE = 'resource';

	// Taxonomy name to store Topics data.
	const TAX_TOPIC = 'topic';

	// Taxonomy name to store Practices data.
	const TAX_PRACTICE = 'practice';

	// Taxonomy name to store Criteria data.
	const TAX_CRITERIA = 'criteria';

	// Taxonomy name to store Difficulty Group data.
	const TAX_DIFFICULTY_GROUP = 'difficulty_group';

	// CPT relations for Tools
	const RESOURCE_TOOLS_RELATION = 'resource_tools';

	// List of all Diagnostic steps.
	const DIAGNOSTIC_STEPS = [ 'situations', 'outils', 'criteres', 'difficultes', ];

	// List of all official Criterias slugs used for the questions on step #3.
	const CRITERIAS_SLUGS = [
		'securite', 'open-source', 'hebergement-europeen',
		'vie-privee', 'identifiant-commun', 'en-francais',
		'fonctionne-smartphone', 'travail-distance', 'sobriete',
		'ergonomique',
	];

	// List of all accepted fields names for the Diagnostic lead form.
	const LEAD_FORM_FIELDS = [
		'infos-region', 'infos-type-collectivite', 'infos-taille-collectivite', 'infos-role',
		'suite-contact', 'suite-consulter-resultats-comparaison', 'suite-disposer-donnees',
	];

	// List of ACF Fields to save in JSON
	const ACF_FIELDS_KEYS = [
		'group_62cd676f91b38',
		'group_62cd4123d69e7',
		'group_62cec615b8581',
		'group_62e7c7f51aaf5',
		'group_62e7f6082aecf',
		'group_62e96e25b86b3',
		'group_62ea5ea44ec7b',
		'group_62ea5f3494522',
		'group_637b3d4fa2d06',
		'group_62cd40ffa7c5e'
	];

	/**
	 * Get transient name.
	 *
	 * @param string $key
	 * @return string
	 */
	public static function get_transient_name( $key ) {
		return "ositer_{$key}";
	}

	/**
	 * Get the environment slug.
	 *
	 * @return string local|dev|prod
	 */
	public static function get_env() {
		return defined( 'OSITER_ENV' ) ? OSITER_ENV : 'prod';
	}
}

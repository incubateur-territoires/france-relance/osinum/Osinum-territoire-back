<?php

namespace OsinumTerritoire\Hooks;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class Cache extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'save_post_' . Config::CPT_SITUATION, 'clean_situations_json_transient' );
		$this->add_action( 'save_post_' . Config::CPT_TOOL, 'clean_situations_json_transient' );
		$this->add_action( 'delete_post', 'clean_situations_json_transient_when_post_is_deleted', 10, 2 );
		$this->add_action( 'save_post_' . Config::CPT_TOOL, 'clean_tools_json_transient' );
		$this->add_action( 'delete_post', 'clean_tools_json_transient_when_post_is_deleted', 10, 2 );

		foreach ( [ Config::TAX_PRACTICE, Config::TAX_TOPIC, ] as $taxonomy ) {
			$this->add_action( 'created_' . $taxonomy, 'clean_situations_json_transient' );
			$this->add_action( 'edited_' . $taxonomy, 'clean_situations_json_transient' );
			$this->add_action( 'delete_' . $taxonomy, 'clean_situations_json_transient' );

			if ( $taxonomy === Config::TAX_TOPIC ) {
				$this->add_action( 'created_' . $taxonomy, 'clean_topics_json_transient' );
				$this->add_action( 'edited_' . $taxonomy, 'clean_topics_json_transient' );
				$this->add_action( 'delete_' . $taxonomy, 'clean_topics_json_transient' );
			}
		}

		$this->add_action( 'created_' . Config::TAX_CRITERIA, 'clean_criterias_json_transient', 10 );
		$this->add_action( 'edited_' . Config::TAX_CRITERIA, 'clean_criterias_json_transient', 10 );
		$this->add_action( 'delete_' . Config::TAX_CRITERIA, 'clean_criterias_json_transient', 10 );
		$this->add_action( 'created_' . Config::TAX_CRITERIA, 'clean_criterias_tree_transient', 10 );
		$this->add_action( 'edited_' . Config::TAX_CRITERIA, 'clean_criterias_tree_transient', 10 );
		$this->add_action( 'delete_' . Config::TAX_CRITERIA, 'clean_criterias_tree_transient', 10 );

		$this->add_action( 'save_post_' . Config::CPT_DIFFICULTY, 'clean_difficulties_json_transient' );
		$this->add_action( 'delete_post', 'clean_difficulties_json_transient_when_post_is_deleted', 10, 2 );
		$this->add_action( 'created_' . Config::TAX_DIFFICULTY_GROUP, 'clean_difficulties_json_transient', 10 );
		$this->add_action( 'edited_' . Config::TAX_DIFFICULTY_GROUP, 'clean_difficulties_json_transient', 10 );
		$this->add_action( 'delete_' . Config::TAX_DIFFICULTY_GROUP, 'clean_difficulties_json_transient', 10 );
		$this->add_action( 'created_' . Config::TAX_DIFFICULTY_GROUP, 'clean_difficulties_groups_json_transient', 10 );
		$this->add_action( 'edited_' . Config::TAX_DIFFICULTY_GROUP, 'clean_difficulties_groups_json_transient', 10 );
		$this->add_action( 'delete_' . Config::TAX_DIFFICULTY_GROUP, 'clean_difficulties_groups_json_transient', 10 );
	}

	/**
	 * Clean Situations JSON objects transient.
	 *
	 * @return void
	 */
	public function clean_situations_json_transient() {
		delete_transient( Config::get_transient_name( 'situations_jsons' ) );
	}

	/**
	 * Clean Topics JSON objects transient.
	 *
	 * @return void
	 */
	public function clean_topics_json_transient() {
		delete_transient( Config::get_transient_name( 'topics_jsons' ) );
	}

	/**
	 * Clean Situations JSON objects transient when a Situation or a Tool is deleted.
	 *
	 * @param integer $post_id
	 * @param \WP_Post $post
	 * @return void
	 */
	public function clean_situations_json_transient_when_post_is_deleted( $post_id, $post ) {
		if ( in_array( $post->post_type, [ Config::CPT_SITUATION, Config::CPT_TOOL, ], true ) ) {
			delete_transient( Config::get_transient_name( 'situations_jsons' ) );
		}
	}

	/**
	 * Clean Tools JSON objects transient.
	 *
	 * @return void
	 */
	public function clean_tools_json_transient() {
		delete_transient( Config::get_transient_name( 'tools_jsons' ) );
	}

	/**
	 * Clean Tools JSON objects transient when a Tool is deleted.
	 *
	 * @param integer $post_id
	 * @param \WP_Post $post
	 * @return void
	 */
	public function clean_tools_json_transient_when_post_is_deleted( $post_id, $post ) {
		if ( in_array( $post->post_type, [ Config::CPT_TOOL, ], true ) ) {
			delete_transient( Config::get_transient_name( 'tools_jsons' ) );
		}
	}

	/**
	 * Clean Criteria JSON objects transient.
	 *
	 * @return void
	 */
	public function clean_criterias_json_transient() {
		delete_transient( Config::get_transient_name( 'criterias_jsons' ) );
	}

	/**
	 * Clean Criteria tree transient.
	 *
	 * @return void
	 */
	public function clean_criterias_tree_transient() {
		delete_transient( Config::get_transient_name( 'criterias_tree' ) );
	}

	/**
	 * Clean Difficulties JSON objects transient.
	 *
	 * @return void
	 */
	public function clean_difficulties_json_transient() {
		delete_transient( Config::get_transient_name( 'difficulties_jsons' ) );
	}

	/**
	 * Clean Difficulties JSON objects transient when a Difficulty is deleted.
	 *
	 * @param integer $post_id
	 * @param \WP_Post $post
	 * @return void
	 */
	public function clean_difficulties_json_transient_when_post_is_deleted( $post_id, $post ) {
		if ( in_array( $post->post_type, [ Config::CPT_DIFFICULTY, ], true ) ) {
			delete_transient( Config::get_transient_name( 'difficulties_jsons' ) );
		}
	}

	/**
	 * Clean Difficulties Groups JSON objects transient.
	 *
	 * @param integer $post_id
	 * @param \WP_Post $post
	 * @return void
	 */
	public function clean_difficulties_groups_json_transient() {
		delete_transient( Config::get_transient_name( 'difficulties_groups_jsons' ) );
	}
}

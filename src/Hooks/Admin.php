<?php

namespace OsinumTerritoire\Hooks;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Helpers;

class Admin extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		// $this->add_action( 'admin_footer', 'output_popup_container' );
		$this->add_filter( 'upload_mimes', 'allow_svg_uploads' );
		$this->add_filter( 'get_terms', 'inject_acf_description_in_wp_term_only_in_admin', 10, 2 );
	}

	/**
	 * Output the necessary popup container in footer.
	 *
	 * @return void
	 */
	public function output_popup_container() {
		Helpers::output_popup( 'admin-modal' );
	}

	/**
	 * Allow SVG uploads in the media gallery.
	 *
	 * @param array $mimes
	 * @return array
	 */
	public function allow_svg_uploads( $mimes = [] ) {
		$mimes['svg'] = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Inject WP_Term ACF description in WP_Term only in admin.
	 *
	 * @param array $terms
	 * @param string $taxonomy
	 * @return array
	 */
	public function inject_acf_description_in_wp_term_only_in_admin( $terms, $taxonomy ) {
		if ( ! is_admin() ) {
			return $terms;
		}

		return array_map(
			function ( $term ) {
				if ( is_a( $term, 'WP_Term' ) ) {
					$term->description = get_field( 'description', $term );
				}

				return $term;
			},
			$terms
		);
	}
}

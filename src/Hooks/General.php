<?php

namespace OsinumTerritoire\Hooks;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class General extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_filter( 'document_title_parts', 'modify_diagnostic_title_tag' );
		$this->add_action( 'template_redirect', 'redirect_custom_tools_to_home' );
		$this->add_action( 'template_redirect', 'remove_admin_bar_css' );
	}

	/**
	 * Modify the <title> tag on Diagnostic pages.
	 *
	 * @param array $title
	 * @return array
	 */
	public function modify_diagnostic_title_tag( $title ) {
		if ( _ositer()->get_urls()->is_diagnostic_creation_page() ) {
			$title['title'] = __( 'Nouveau diagnostic', 'ositer' );
		} else if ( _ositer()->get_urls()->is_diagnostic_page() ) {
			$diagnostic = _ositer()->get_data()->get_current_diagnostic();

			if ( $diagnostic ) {
				if ( _ositer()->get_urls()->is_diagnostic_results_page() ) {
					$title['title'] = sprintf( __( 'Résultats %1$s', 'ositer' ), $diagnostic->get_title() );
				} else {
					$title['title'] = $diagnostic->get_title();
				}
			}
		}

		return $title;
	}

	/**
	 * For custom Tools created by users, redirect their single page to the home page.
	 *
	 * @return void
	 */
	public function redirect_custom_tools_to_home() {
		if ( is_singular( Config::CPT_TOOL ) ) {
			$tool = _ositer()->get_data()->get_tool( get_queried_object_id() );

			if ( $tool && $tool->is_custom() ) {
				wp_redirect( home_url() );
				exit();
			}
		}
	}

	/**
	 * Remove the admin bar CSS on diagnostic pages.
	 *
	 * @return void
	 */
	public function remove_admin_bar_css() {
		if ( _ositer()->get_urls()->is_diagnostic_page() &&  \is_admin_bar_showing() ) {
			remove_action( 'wp_head', '_admin_bar_bump_cb' );
		}
	}
}

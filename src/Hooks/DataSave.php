<?php

namespace OsinumTerritoire\Hooks;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;

class DataSave extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$tax_criteria = Config::TAX_CRITERIA;

		// $this->add_action( "created_{$tax_criteria}", 'create_criteria_rating_terms', 10 );
	}

	/**
	 * When a Criteria is created, create rating subterms (-1/0/+1).
	 *
	 * @param integer $term_id
	 * @return void
	 */
	public function create_criteria_rating_terms( $term_id ) {
		if ( defined( 'OSITER_CREATING_CRITERIA_EVALUATION_SUBTERMS' ) ) {
			return;
		}

		define( 'OSITER_CREATING_CRITERIA_EVALUATION_SUBTERMS', true );

		$criteria = ositer()->get_criteria( $term_id );
		$criteria->create_rating_subterms();
	}
}

<?php

namespace OsinumTerritoire\Events;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Models\Diagnostic;

class Emails extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/diagnostic/created', 'send_email_after_diagnostic_creation', 100, 1 );
		$this->add_action( 'ositer/email/send-author-diagnostics-list', 'send_list_of_diagnostics_to_author', 10, 2 );
		$this->add_action( 'ositer/email/user-request', 'send_email_to_admin_on_user_request', 10, 2 );
		$this->add_action( 'ositer/diagnostic/lead-form-saved', 'send_email_to_admin_on_diagnostic_lead_form_first_submission', 10, 3 );
	}

	/**
	 * Send an e-mail to the Diagnostic author after its creation.
	 *
	 * @param Diagnostic $diagnostic
	 * @return void
	 */
	public function send_email_after_diagnostic_creation( $diagnostic ) {
		ositer()->send_email(
			$diagnostic->get_email(),
			__( 'Votre nouveau diagnostic OSINUM Territoires', 'ositer' ),
			'diagnostic-create',
			[ 'diagnostic' => $diagnostic, ]
		);
	}

	/**
	 * Send a list of Diagnostics to author.
	 *
	 * @param Diagnostic[] $diagnostic
	 * @param string $email
	 */
	public function send_list_of_diagnostics_to_author( $diagnostics = [], $email = '' ) {
		ositer()->send_email(
			$email,
			__( 'Votre liste de diagnostic(s) OSINUM Territoires', 'ositer' ),
			'diagnostics-list',
			[ 'diagnostics' => $diagnostics, ]
		);
	}

	/**
	 * On user request (workshop, contact), send an e-mail to the admin.
	 *
	 * @param string $type
	 * @param string $email
	 */
	public function send_email_to_admin_on_user_request( $type = '', $email = '' ) {
		$matrix = [
			'workshop-attend'   => __( 'Participation à un atelier', 'ositer' ),
			'workshop-organize' => __( 'Organisation d\'atelier', 'ositer' ),
			'osinum-contact'    => __( 'Être recontacté', 'ositer' ),
		];

		$nice_type = array_key_exists( $type, $matrix ) ? $matrix[ $type ] : $type;

		ositer()->send_email(
			ositer()->get_setting( 'contact_email' ),
			sprintf( __( 'Demande utilisateur : %1$s', 'ositer' ), $nice_type ),
			'user-request',
			[ 'email' => $email, 'type' => $nice_type, ]
		);
	}

	/**
	 * Send an e-mail to the admin when a Diagnostic Lead Form is submitted for the first time.
	 *
	 * @param Diagnostic $diagnostic
	 * @param array $fields
	 * @param boolean $already_completed
	 * @return void
	 */
	public function send_email_to_admin_on_diagnostic_lead_form_first_submission( $diagnostic, $fields, $already_completed ) {
		$subject = $already_completed ? __( 'Mise à jour des informations de fin de diagnostic', 'ositer' ) : __( 'Informations de fin de diagnostic', 'ositer' );

		ositer()->send_email(
			ositer()->get_setting( 'contact_email' ),
			$subject,
			'diagnostic-lead',
			[ 'diagnostic' => $diagnostic, 'fields' => $fields, 'first_submission' => ! $already_completed, ]
		);
	}
}

<?php

namespace OsinumTerritoire\Events;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Models\Diagnostic;

class DiagnosticWasCreated extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/diagnostic/created', 'generate_token', 10 );
		$this->add_action( 'ositer/diagnostic/created', 'generate_qr_code', 20 );
	}

	/**
	 * On creation, generate an access token.
	 *
	 * @param Diagnostic $diagnostic
	 * @return void
	 */
	public function generate_token( $diagnostic ) {
		$diagnostic->set_meta( 'token', wp_generate_password( 6, false, false ) );
	}

	/**
	 * On creation, generate the Diagnostic QR code.
	 *
	 * @param Diagnostic $diagnostic
	 * @return void
	 */
	public function generate_qr_code( $diagnostic ) {
		$diagnostic->generate_qr_code();
	}
}

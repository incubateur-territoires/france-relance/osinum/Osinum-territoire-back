<?php

namespace OsinumTerritoire\Events;

use OsinumTerritoire\_Core\Component;
use OsinumTerritoire\Config;
use OsinumTerritoire\Models\Diagnostic;

class DiagnosticWasUpdated extends Component {
	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		$this->add_action( 'ositer/diagnostic/updated', 'save_update_meta', 5, 3 );
		$this->add_action( 'ositer/diagnostic/updated', 'clear_transients', 10, 2 );
		$this->add_action( 'ositer/diagnostic/updated', 'maybe_mark_complete', 20, 2 );
	}

	/**
	 * Save a couple of meta relative to the update.
	 *
	 * @param Diagnostic $diagnostic
	 * @param array $params
	 * @param string $step
	 * @return void
	 */
	public function save_update_meta( $diagnostic, $params, $step ) {
		$diagnostic->set_meta( 'updated_at', time() );
		$diagnostic->set_meta( "step_{$step}_updated_at", time() );
	}

	/**
	 * Clear Diagnostic transients when it is updated.
	 *
	 * @param Diagnostic $diagnostic
	 * @param array $params
	 * @return void
	 */
	public function clear_transients( $diagnostic, $params ) {
		delete_transient( Config::get_transient_name( 'suggested_practices_ids_' . $diagnostic->get_id() ) );
		delete_transient( Config::get_transient_name( 'suggested_tools_ids_' . $diagnostic->get_id() ) );
	}

	/**
	 * Maybe mark Diagnostic as complete when it is updated, if all steps are done.
	 *
	 * @param Diagnostic $diagnostic
	 * @param array $params
	 * @return void
	 */
	public function maybe_mark_complete( $diagnostic, $params ) {
		foreach ( Config::DIAGNOSTIC_STEPS as $step ) {
			if ( $diagnostic->is_completed() || ! $diagnostic->has_done_step( $step ) ) {
				return;
			}
		}

		$diagnostic->set_meta( 'completed_at', time() );
	}
}

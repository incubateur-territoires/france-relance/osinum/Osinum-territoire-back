<?php

namespace OsinumTerritoire\Crons;

use OsinumTerritoire\_Core\Component;

abstract class Cron extends Component {
	protected $hook = '';

	/**
	 * Setup hook trigger on load.
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'init', [ $this, 'maybe_register' ], 10 );
		add_action( $this->get_hook(), [ $this, 'execute' ], 10 );
	}

	/**
	 * Register the job if it was not previously registered.
	 *
	 * @return void
	 */
	public function maybe_register() {
		if ( wp_next_scheduled( $this->get_hook() ) ) {
			return;
		}

		$this->register();
	}

	/**
	 * Get the full CRON hook name.
	 *
	 * @return string
	 */
	public function get_hook() {
		return sprintf( 'ositer/cron/%1$s', $this->hook );
	}

	/**
	 * Register the scheduled job.
	 *
	 * @return void
	 */
	abstract public function register();

	/**
	 * Do the job.
	 *
	 * @return void
	 */
	abstract public function execute();
}

<?php

namespace OsinumTerritoire\Crons;

use OsinumTerritoire\Models\Chat;

class DiagnosticsCleaner extends Cron {
	protected $hook = 'diagnostics-cleaner';

	/**
	 * Register the job.
	 *
	 * @return void
	 */
	public function register() {
		wp_schedule_event( time(), 'every_minute', $this->get_hook() );
	}

	/**
	 * Execute the job: delete empty diagnostics.
	 *
	 * @return void
	 */
	public function execute() {
		// 
	}
}
